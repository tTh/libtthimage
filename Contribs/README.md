
# Contributions

Voici une collection informelle de fonctions diverses et variées contribuées
par moi-même (et peut-être, un jour, d'autres gens...) et qui risquent
bien de n'être **pas très maintenues**, contrairement au reste du *kluge*.

Nous allons commencer par des fonctions inventées lors d'une *n*-ième session
du projet [ExplodeVidz](ExplodeVidz.md).