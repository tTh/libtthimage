

#include  <stdio.h>
#include  <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>

#include  <tthimage.h>

/* -------------------------------------------------------- */


/* -------------------------------------------------------- */

int rgb_process_by_filename(char *inf, char *outf)
{
Image_Desc	*src, *dst;
int		foo;
int		x, y, larg, offs;
int		r, g, b;

if ( (src=Image_TGA_alloc_load(inf)) == NULL )
	{
	fprintf(stderr, "%s: err load %s\n", __func__, inf);
	exit(5);
	}

printf("source '%s'   %dx%d\n", inf, src->width, src->height);

if ( (dst=Image_clone(src, 0)) == NULL )
	{
	fprintf(stderr, "%s: no mem for image cloning\n", __func__);
	exit(5);
	}

Image_clear(dst, 160, 60, 60);

/*
 *	calcul de la zone à traiter
 */
larg = src->width / 3;
offs = (src->width - larg) / 2;
printf("largeur  %d   offset   %d\n", larg, offs);


for (y=0; y<src->height; y++) {
	for (x=0; x<larg; x++) {

		Image_getRGB(src, x+offs, y, &r, &g, &b);

		Image_plotRGB(dst, x,          y, r, r, r);
		Image_plotRGB(dst, x+offs,     y, g, g, g);
		Image_plotRGB(dst, x+(2*offs), y, b, b, b);

		}
	}

Image_egalise_cos010(dst, dst, 0);

foo = Image_TGA_save(outf, dst, 0);
if (foo) {
	fprintf(stderr, "%s: error %d saving %s\n", __func__, foo, outf);
	exit(5);
	}

return 0;
}

/* -------------------------------------------------------- */
int main(int argc, char *argv[])
{
int	foo;

if (3 != argc)
	{
	exit(1);
	}

foo = rgb_process_by_filename(argv[1], argv[2]);
if (foo) {
	fprintf(stderr, "process by filename -> %d\n", foo);
	}

return 0;
}

/* -------------------------------------------------------- */
