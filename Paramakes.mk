#
#		GLOBAL MAKE CONFIGURATION
#		For the "tTh" image library
#

DESTDIR=/usr/local
SHARED_FILES=$(DESTDIR)/share/libimage
HTML_DIR=$(DESTDIR)/html

#-----------------------------------------------------------------
#
#	pour le debuging:             option -g
#	pour le profiling:            option -pg
#	pour tracer plein de trucs:   -DDEBUG_LEVEL=1
#	if IMGCOMMENT, the image comment is written to the TGA file,
#		but this files can't be loaded by Xv...
#
#	pour coredumper dans les situations graves: -DABORT=1
#
# 		use -Wmissing-prototypes ?

LIBIMG_OPT=-DFORCE_ABORT=0 -DDEBUG_LEVEL=0 -DIMGCOMMENT=0
CC_OPTS=-Wall -W -g -ansi -O1 -fPIC -no-pie
CC_HACKS=-DNEED_ALLOCA_H 

CFLAGS= $(CC_OPTS)			\
	$(LIBIMG_OPT)			\
	$(CC_HACKS) 			\
	-DDESTDIR=\"$(DESTDIR)\"	\
	-DSHAREDIR=\"$(SHARED_FILES)\"	\
	-DCC_OPTS=\"'$(CC_OPTS)'\"

LINKOPT=-lm
RANLIB=wc -c

# modify it 'as you like'
AR=ar 

#
#			and valgrind is your friend
#