# libtthimage

Ce kluge a pour but avoué de brotcher les images avec vigueur.

Du code né dans les années 1990 sur un Kenitec 286 sous msdos 4 ou 5,
puis qui s'est un jour retrouvé dans un Linux (kernel 0.99) et qui végète
depuis ~2005. Il est venu le temps de le remettre en route, mais
la tâche n'est pas terminée.

## Construction

Tout va bien se passer. Mais c'est assez rudimentaire, il y a deux scripts.
Le premier, [`build.sh`](./build.sh), va compiler la bibliothèque et les utilitaires.
Le second, [`install.sh`](./install.sh) va mettre les choses au bon endroit.
Il est vivement conseillé de bien les lire avant de les utiliser.

Les options globales de compilation sont dans
[Paramakes.mk](Paramakes.mk). Là aussi, soyez attentifs.

## Utilisation

C'est là que ça se [complique](Docs/). Même moi, parfois je m'y perds.
Vous poulez voir quelques [exemples](http://la.buvette.org/images/images.html)
de l'ancien temps, avant la reprise en main de ces dernières années.

## Les outils

Plein de petits [binaires](Tools/) qui font plein de choses
plus ou moins ésotériques. En général, pour avoir de l'aide,
il faut leur donner une seule option : `list`, ce qui n'est pas très
conventionnel, mais bon, *legacy*, toussa...

## La suite ?

Bah, c'est l'heure du _gloumiam_, alors on passe à table.
Et après, on va continuer une grande campagne de nettoyage du
code, il reste un gazillion de warnings.
