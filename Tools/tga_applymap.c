/*
	WARNING!	this prog is bogus!
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
void usage(int flag)
{
fprintf(stderr, "* tga_applymap v 0.0.15 [%s] (dwtfywl) Krabulator 1910\n",
				TGA_OUTILS_VERSION);
fprintf(stderr, "  compiled %s at %s\n", __DATE__, __TIME__);

fprintf(stderr, "Usage:\n\ttga_applymap M src.tga color.map dst.tga\n");
fprintf(stderr, "       M is 1 (color mode) or 2 (gray mode)\n");
if (flag) Image_print_version(0);
exit(5);
}
/*::------------------------------------------------------------------::*/
int main(int argc, char *argv[])
{
Image_Desc	*src, *dst;
int		foo;
RGB_map		map;

dump_command_line(argc, argv, 0);

if (argc==2 && !strcmp(argv[1], "-?"))	usage(1);

if (argc != 5) usage(0);

if ( (src=Image_TGA_alloc_load(argv[2])) == NULL ) {
	fprintf(stderr, "can't load '%s'\n", argv[2]);
	exit(1);
	}

memset(&map, 0, sizeof(map));

if ( (foo=Image_load_color_Map(argv[3], "map", &map)) ) {
	fprintf(stderr, "%s: %s: err%d %s\n", argv[0], argv[3], 
					foo, Image_err2str(foo));
	exit(1);
	}

if (must_be_verbose()) {
	fprintf(stderr, "il y a %d couleurs dans la palette '%s'\n",
			map.nbre, argv[3]);
	}

if ( (dst=Image_clone(src, 0))==NULL ) {
	fprintf(stderr, "can't clone %p, exiting...\n", src);
	exit(2);
	}

/* ----------- */
switch (argv[1][0]) {
	default:
	case '1':
		foo=Image_apply_Map(src, dst, &map);
		break;
	case '2':
		foo=Image_gray_Map(src, dst, &map);
		break;
	case '3':
		fprintf(stderr, "Not implemented\n");
		exit(5);
	}
/* ----------- */

if (foo) {
	fprintf(stderr, "%s: apply map: erreur %d: %s\n", 
					argv[0], foo, Image_err2str(foo));
	}

foo = Image_TGA_save(argv[4], dst, 0);
if (foo) {
	fprintf(stderr, "%s: TGA_save: err #%d\n\t\t %s\n",
			argv[0], foo, Image_err2str(foo));
	exit(1);
	}

exit (0);
}
/*::------------------------------------------------------------------::*/
