/*
	----------------------------------------------
	  utilitaire pour ditherizer (uh?) une image
	----------------------------------------------
	 http://krabulator.free.fr/devel/libimage.html
	----------------------------------------------
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
#define		DI_2X2			2
#define		DI_3X3_0		11
#define		DI_3X3_1		12
#define		DI_BAYER0		16
#define		DI_ATKINSON		18
#define		DI_CRUDE		30
#define		DI_DBLTR		38
#define		DI_ERROR		45
#define		DI_RANDOM		60
#define		DI_BYR8RND		70
#define		DI_XPER			100

mot_clef mots_clef[] =
{
{ "2x2",	DI_2X2,		"",	"not working"		},
{ "3x3_0",	DI_3X3_0,	"",	""			},
{ "3x3_1",	DI_3X3_1,	"",	""			},
{ "bayer0",	DI_BAYER0,	"", 	""			},
{ "atkinson",	DI_ATKINSON,	"i",	"mmmm ?"		},
{ "crude",	DI_CRUDE,	"i",	""			},
{ "dbltresh",	DI_DBLTR,	"ii",	"hi & low tresholds"	},
{ "error",	DI_ERROR,	"",	"simple err diffusion"	},
{ "random",	DI_RANDOM,	"i",	"random treshold"	},
{ "byr8rnd",	DI_BYR8RND,	"ii",	"new 29 nov 2013"	},
{ "xper",	DI_XPER,	"i",	"param: 1..254"		},
{ NULL,		0,		NULL,	NULL			}
};

/*::------------------------------------------------------------------::*/
void usage(int flag)
{
fprintf(stderr, "* tga_dither v 0.0.26 [%s] (dwtfywl 2022) tTh\n",
					TGA_OUTILS_VERSION);
fprintf(stderr, "usage:\n\ttga_dither avant.tga mode avant.tga [params]\n");
if (flag)
	liste_mots_clefs(mots_clef, 42);
else
	Image_print_version(0);
exit(5);
}
/*::------------------------------------------------------------------::*/
int main (int argc, char *argv[])
{
Image_Desc	*src, *dst;
int		foo;
int		commande, nbargs, idx;

dump_command_line(argc, argv, 0);

if ( argc==2 && !strcmp(argv[1], "list") ) usage(1);
if ( argc < 4)	usage(0);

idx = cherche_mot_clef(argv[2], mots_clef, &commande, &nbargs);
if (idx < 0)
	{
	fprintf(stderr, "tga_dither: keyword %s unknow.\n", argv[2]);
	exit(5);
	}

#if DEBUG_LEVEL
fprintf(stderr, "idx %d commande %d nbargs %d argc %d\n",
					idx, commande, nbargs, argc);
#endif

foo = parse_parametres(argc, argv, mots_clef[idx].ptypes, 4);

if ( (src=Image_TGA_alloc_load(argv[1])) == NULL )
	{
	fprintf(stderr, "tga_dither: err load %s\n", argv[1]);
	exit(5);
	}
if ( (dst=Image_clone(src, 1)) == NULL )
	{
	fprintf(stderr, "no mem for image cloning\n");
	exit(5);
	}

switch (commande)
	{
	case DI_2X2:
		foo = Image_dither_2x2(src, dst, 255);
		break;
	case DI_3X3_0:
		foo = Image_dither_3x3_0(src, dst, 127);
		break;
	case DI_3X3_1:
		foo = Image_dither_3x3_1(src, dst, 255);
		break;
	case DI_BAYER0:
		foo = Image_dither_Bayer_0(src, dst, 255);
		break;
	case DI_ATKINSON:
		foo = Image_dither_atkinson(src, dst, 255);
		break;
	case DI_CRUDE:
		foo = Image_dither_crude(src, dst, GIP(0));
		break;
	case DI_DBLTR:
		foo = Image_dither_double_seuil(src, dst, GIP(0), GIP(1), 255);
		break;
	case DI_ERROR:
		foo = Image_dither_simple_error(src, dst, 255);
		break;
	case DI_RANDOM:
		foo = Image_dither_seuil_random(src, dst, 255);
		break;
	case DI_BYR8RND:
		srand(getpid());
		foo = Image_dither_bayer8x8rnd(src, dst, GIP(0), GIP(1));
		break;
	case DI_XPER:
		foo = Image_dither_3x3_1(src, dst, GIP(0));
		break;
	default:
		fprintf(stderr, "dithering method not implemented\n");
		foo=-1;
		break;
	}

if (foo)
	fprintf(stderr, "dither %d retour = %d, %s\n", commande,
					 foo, Image_err2str(foo));

foo = Image_TGA_save(argv[3], dst, 0);

return 0;
}
/*::------------------------------------------------------------------::*/
