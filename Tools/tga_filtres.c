/*
 *
 *		F I L T R E S
 *
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
#define		F_LISS2X2	2
#define		F_ADAPT0	3
#define		F_HIPASS	8
#define		F_LOPASS	14
#define		F_PREWITT	17
#define		F_RANDOM	18
#define		F_RANDOM2	19
#define		F_SMOOTH	30
#define		F_SOBEL		40
#define		F_SOBEL4	41

mot_clef mots_clef[] =
{
{ "liss2x2",	F_LISS2X2,	"",	"lissage 2x2"			},
{ "adapt0",	F_ADAPT0,	"",	"proto ymmv"			},
{ "hipass",	F_HIPASS,	"", 	""				},
{ "lopass",	F_LOPASS,	"", 	""				},
{ "prewitt",	F_PREWITT,	"i", 	"rotation [0..8]"		},
{ "random",	F_RANDOM,	"ii",	"try it..."			},
{ "random2",	F_RANDOM2,	"iiii",	"try it..."			},
{ "smooth",	F_SMOOTH,	"", 	""				},
{ "sobel",	F_SOBEL,	"i", 	""				},
{ "sobel4",	F_SOBEL4,	"i", 	""				},
{ NULL,		0,		NULL,	NULL				}
};
/*::------------------------------------------------------------------::*/
void usage(int flag)
{
fprintf(stderr, "*** tga_filtres v 0.0.18 [%s] %s\n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);

fprintf(stderr, "\nUsage:\n");
fprintf(stderr, "\ttga_filtres <src.tga> FILTR <dst.tga> [p1] ... [pn]\n");
if (flag) {
	Image_print_version(0);
	liste_mots_clefs(mots_clef, 42);
	}
else {
	fprintf(stderr, "\ttry 'tga_filtres list' for a list.\n");
	}
exit(5);
}
/*::------------------------------------------------------------------::*/
/*
 *	argv[1]		source.tga
 *	argv[2]		type filtrage
 *	argv[3]		destination.tga
 */
#define FIRST_PARAM	4
int main(int argc, char *argv[])
{
Image_Desc	*src, *dst;
int		foo;
int		commande, nbargs, idx;

dump_command_line(argc, argv, 0);
if (argc==2 && !strcmp(argv[1], "-?"))		usage(0);
if (argc==2 && !strcmp(argv[1], "list"))	usage(1);
if (argc < 4) usage(0);

srand(getpid());

/* recherche du type d'effet demand� */
idx = cherche_mot_clef(argv[2], mots_clef, &commande, &nbargs);
if (idx < 0) {
	fprintf(stderr, "tga_filtres: mot-clef %s inconnu...\n", argv[2]);
	exit (5);
	}
#if DEBUG_LEVEL
fprintf(stderr, "%s --> idx=%d, commande=%d, %d args\n",
			argv[2], idx, commande, nbargs);
fprintf(stderr, "argc = %d\n", argc);
#endif

if ( (argc-nbargs) != FIRST_PARAM ) {
	fprintf(stderr, "%s: bad number of parameters\n", argv[0]);
	exit(5);
	}

/* analyse des param�tres */
foo = parse_parametres(argc, argv, mots_clef[idx].ptypes, FIRST_PARAM);

if ((src = Image_TGA_alloc_load(argv[1]))==NULL) {
	fprintf(stderr, "tga_filtres: can't load image %s\n", argv[1]);
	exit(5);
	}

if ( (dst=Image_clone(src, 0)) == NULL ) {
	fprintf(stderr, "tga_filtres: can't clone %p\n", src);
	exit(1);
	}
Image_clear(dst, 0, 0, 0);

switch (commande)
	{
	case F_LISS2X2:
		Image_2x2_lissage(src, dst);
		break;
	case F_ADAPT0:
		foo = Image_filtadapt_0(src, dst, 5);
		break;
	case F_HIPASS:
		foo = Image_filtre_passe_haut(src, dst);
		break;
	case F_LOPASS:
		foo = Image_filtre_passe_bas(src, dst);
		break;
	case F_PREWITT:
		foo = Image_filtre_Prewitt(src, dst, GIP(0));
		break;
	case F_RANDOM:
		foo = Image_filtre_random(src, dst, GIP(0), GIP(1));
		break;
	case F_RANDOM2:
		foo = Image_filtre_random_2(src, dst,
				GIP(0), GIP(1), GIP(2), GIP(3));
		break;
	case F_SMOOTH:
		foo = Image_lissage_3x3(src, dst);
		break;
	case F_SOBEL:
		foo = Image_filtre_Sobel(src, dst, GIP(0));
		break;
	case F_SOBEL4:
		foo = Image_filtre_Sobel_4(src, dst, GIP(0));
		break;
	}

if (foo) {
	Image_print_error("tga_filtres ", foo);
	exit(1);
	}
foo = Image_TGA_save(argv[3], dst, 0);

return 0;
}
/*::------------------------------------------------------------------::*/
