/*
	un outil pour combiner deux images TGA
*/

#include  <stdio.h>
#include  <string.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/

#define MIX_SEUILS	10
#define MIX_GRAY	11
#define MIX_RGB		12
#define MIX_LINES	20
#define MIX_COLUMNS	21
#define MIX_CHECKER	22
#define MIX_RANDOM	23
#define MIX_RNDRGB	24
#define MIX_POWER	25
#define MIX_POWERM	26		/* new 14 mars 2014 */
#define MIX_XOR		27		/* new 25 sept 2018 */

#define MIX_DIAGO	40

#define MIX_HSPLIT0	50		/* new 29 nov 2013 */
#define MIX_VSPLIT0	51		/* new 29 nov 2013 */

#define MIX_MINMAX	60
#define MIX_STEREO	70
#define MIX_STEREOK	71
#define MIX_STEREO3	73
#define MIX_WAOU	80
#define MIX_WAUO	81
#define MIX_CIRCLE0	84
#define MIX_CIRCLE1	85		/* new 6 juillet 2022 */
#define MIX_HDEG	100
#define MIX_VDEG	101
#define MIX_PROTO	110
#define MIX_IFNOTBLACK	200

static char no_help[] = "no help";

static char h_mix_gray[] = "param: 1..10000";
static char h_mix_rgb[]	= "params: 3*1..10000";
static char h_lico[]	= "taille offset 0";
static char h_random[]	= "param: 1..10000";
static char h_minmax[]	= "0:min 1:max";
static char h_waou[]    = "ubyte ubyte ubyte bit";
static char h_wauo[]	= "must be 0";
static char h_circle0[]	= "must be 0";
static char h_deg[] 	= "0 or 1";

mot_clef les_types[] = 
	{
	{ "mixer",	MIX_GRAY,	"i",	no_help		},
	{ "mix_gray",	MIX_GRAY,	"i",	h_mix_gray	},
	{ "mix_rgb",	MIX_RGB,	"iii",	h_mix_rgb	},
	{ "seuils",	MIX_SEUILS,	"iii",  "P in [0..255]"	},

	{ "lines",	MIX_LINES,	"iii",	h_lico		},
	{ "columns",	MIX_COLUMNS,	"iii",	h_lico		},
	{ "checker",	MIX_CHECKER,	"iiiii", no_help	},
	{ "random",	MIX_RANDOM,	"i",	h_random	},
	{ "rndrgb",	MIX_RNDRGB,	"i",	h_random	},

	{ "diago",	MIX_DIAGO,	"fii",	no_help		},
	{ "diagonale",	MIX_DIAGO,	"fii",	no_help		},

	{ "hsplit",	MIX_HSPLIT0,	"ii",	"position 0"	},
	{ "vsplit",	MIX_VSPLIT0,	"ii",	"P in pixels"	},

	{ "minmax",	MIX_MINMAX,	"i",	h_minmax	},
	{ "power",	MIX_POWER,	"",	""		},
	{ "powerm",	MIX_POWERM,	"",	""		},
	{ "xor",	MIX_XOR,	"",	""		},
	{ "waou",	MIX_WAOU,	"iiii",	h_waou		},
	{ "wauo",	MIX_WAUO,	"i",	h_wauo		},
	{ "circle0",	MIX_CIRCLE0,	"i",	h_circle0	},
	{ "circle1",	MIX_CIRCLE1,	"i",	"don't know"	},

	{ "hdeg",	MIX_HDEG,	"f",	h_deg		},
	{ "vdeg",	MIX_VDEG,	"f",	h_deg		},
	{ "ifnotblack",	MIX_IFNOTBLACK,	"",	"new mars 2007"	},
	{ "stereo",	MIX_STEREO,	"",	"rouge/vert"	},
	{ "stereok",	MIX_STEREOK,	"iii",	"rouge/bleu coefs"	},
	{ "proto", 	MIX_PROTO,	"",	"prototype"	},
	{ NULL,		0,		NULL,	NULL		}
	};

/*::------------------------------------------------------------------::*/
void usage(int flag)
{
fprintf(stderr, "* Tga Combine v 0.1.43 [%s] %s\n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);
if (flag)
	fprintf(stderr, "* compiled : " __DATE__ " *\n");

Image_print_version(0);
fputs("Usage:\n", stderr);
fputs("\ttga_combine s1.tga s2.tga MODE d.tga [PARAMS]\n", stderr);
fputs("\n", stderr);
if (flag) liste_mots_clefs(les_types, 42);
exit(0);
}
/*::------------------------------------------------------------------::*/
int combine_proto(Image_Desc *sa, Image_Desc *sb, Image_Desc *dest)
{

fprintf(stderr, "%s: %p + %p -> %p\n", __func__, sa, sb, dest);

return 42;
}
/*::------------------------------------------------------------------::*/
/*
 *	arg[1]		s1.tga
 *	arg[2]		s2.tga
 *	arg[3]		mode
 *	arg[4]		dst.tga
 */
#define FIRST_PARAM 5
int main(int argc, char *argv[])
{
Image_Desc 	*s1, *s2, *d;
int		foo;
int		idx, mode, nbarg;
char		*dstname;
int		kr, kg, kb;

dump_command_line(argc, argv, 0);

if (argc==2 && !strcmp(argv[1], "list"))	usage(1);

if (argc < 5 ) usage(0);

if ((idx=cherche_mot_clef(argv[3], les_types, &mode, &nbarg)) == -1)
	{
	fprintf(stderr, "mode '%s' inconnu\n", argv[3]);
	exit(1);
	}

#if DEBUG_LEVEL
fprintf(stderr, "%s --> idx=%d, mode=%d, %d args\n", argv[3], idx, mode, nbarg);
fprintf(stderr, "argc = %d\n", argc);
#endif
if (mode == -1)
	{
	fprintf(stderr, "mode '%s' inconnu\n", argv[3]);
	exit(5);
	}
/* analyse des paramètres */
foo = parse_parametres(argc, argv, les_types[idx].ptypes, FIRST_PARAM);
#if DEBUG_LEVEL
fprintf(stderr, "parse params -> %d\n", foo);
#endif

if ((s1 = Image_TGA_alloc_load(argv[1]))==NULL)
	{
	fprintf(stderr, "tga_combine: can't load image #1\n");
	exit(5);
	}
if (must_be_verbose())
	{
	fprintf(stderr, "%s loaded at %p\n", argv[1], s1);
	}

if ((s2 = Image_TGA_alloc_load(argv[2]))==NULL)
	{
	fprintf(stderr, "tga_combine: can't load image #2\n");
	exit(5);
	}
if (must_be_verbose())
	{
	fprintf(stderr, "%s loaded at %p\n", argv[2], s2);
	}

if ((d = Image_clone(s2, 0))==NULL)
	{
	fprintf(stderr, "tga_combine: erreur clonage destination\n");
	exit(5);
	}

switch (mode)
	{
	case MIX_SEUILS:
		foo = Image_combine_seuils(s1, s2, d, GIP(0), GIP(1), GIP(2));
		break;

	case MIX_GRAY:
		foo = Image_mix(s1, s2, d, GIP(0));
		break;

	case MIX_RGB:
		foo = Image_mix_rgb(s1, s2, d, GIP(0), GIP(1), GIP(2));
		break;

	case MIX_LINES:
		foo = Image_combine_lines(s1, s2, d, GIP(0), GIP(1), GIP(2));
		break;

	case MIX_COLUMNS:
		foo = Image_combine_columns(s1, s2, d, GIP(0), GIP(1), GIP(2));
		break;

	case MIX_CHECKER:
		foo = Image_combine_checker(s1, s2, d, GIP(0), GIP(1), GIP(2),
							GIP(3), GIP(4));	
		break;

	case MIX_RANDOM:
		foo = Image_combine_random_point(s1, s2, d, GIP(0));
		break;

	case MIX_RNDRGB:
		fprintf(stderr, "SEGFAULT, COREDUMP, what else ?\n");
		foo = Image_combine_random_rgb(s1, s2, d, GIP(0));
		break;
		
	case MIX_DIAGO:
		/* oui, bon, a quoi servent les parametres ? */
		/* 17sept2009: le 0 est le sens de la coupure, 
                   et les 1 & 2 sont 'not used' */
		foo = Image_combine_diagonale(s1, s2, d,
					GFP(0), GIP(1), GIP(2));
		break;

	case MIX_HSPLIT0:
		foo = Image_combine_Hsplit(s1, s2, d, GIP(0), GIP(1));
		break;
	case MIX_VSPLIT0:
		foo = Image_combine_Vsplit(s1, s2, d, GIP(0), GIP(1));
		break;

	case MIX_MINMAX:
		foo = Image_combine_minmax(s1, s2, d, GIP(0));
		break;

	case MIX_POWER:
		foo = Image_combine_power(s1, s2, d);
		fprintf(stderr, "combine power -> %d\n", foo);
		break;
	case MIX_POWERM:
		foo = Image_combine_power_m(s1, s2, d);
		fprintf(stderr, "combine power_m -> %d\n", foo);
		break;

	case MIX_WAOU:
		foo = Image_combine_waou(s1,s2,d,GIP(0),GIP(1),GIP(2),GIP(3));
		break;

	case MIX_WAUO:
		foo = Image_combine_wauo(s1, s2, d, GIP(0));
		break;

	case MIX_CIRCLE0:
		foo = Image_combine_cercle_flou(s1, s2, d, 0);
		break;
	case MIX_CIRCLE1:
		fprintf(stderr, "pid %d is circle trashing\n", getpid());
		foo = Image_combine_cercle_flou(s1, s2, d, 1);
		break;

/* FIXME 16 sept 2009 : je pense que hdeg et vdeg sont inverses */
	case MIX_HDEG:
		foo = Image_combine_Hdegrade(s1, s2, d, GFP(0));
		break;

	case MIX_VDEG:
		foo = Image_combine_Vdegrade(s1, s2, d, GFP(0));
		break;

	case MIX_IFNOTBLACK:
		foo = Image_combine_if_not_black(s1, s2, d);
		break;

	case MIX_XOR:
		foo = Image_XOR(s1, s2, d, 1);
		if (666==foo) fputs("#####################", stderr);
		break;

	case MIX_STEREO:
		foo = Image_combine_stereo_0(s1, s2, d);
		break;

	case MIX_STEREOK:
		kr = GIP(0);
		kg = GIP(1);
		kb = GIP(2);
		foo = Image_combine_stereo_1(s1, s2, d, kr, kg, kb);
		break;

	default:
		fprintf(stderr, "mode operation %d non implemente\n", mode);
		exit(1);
		break;
	}

if (foo)
	Image_print_error("Combination", foo);

dstname = argv[4];

foo = Image_TGA_save(dstname, d, 0);

#if DEBUG_LEVEL
fprintf(stderr, "image '%s' ecrite (%d)\n", dstname, foo);
#endif

return 0;
}
/*::------------------------------------------------------------------::*/
