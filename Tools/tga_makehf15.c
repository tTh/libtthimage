/*
		convert a TGA image to a POVRAY height_field
	    ----------------------------------------------------

    This operation is also implemented in 'povhf_tools' (dec 2009)
*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
void usage()
{
fprintf(stderr, "* tga_makehf15 v 0.0.6 [%s] (dwtfywl) tontonTh\n",
					TGA_OUTILS_VERSION);
fprintf(stderr, "Usage:\n\ttga_makehf15 source.tga hf.tga\n");
fprintf(stderr, "\tno parameters, no options... but stay tuned :)\n\n");
Image_print_version(0);
exit(5);
}
/*::------------------------------------------------------------------::*/
int main (int argc, char *argv[])
{
Image_Desc 	*img, *hf15;
int		foo;

dump_command_line(argc, argv, 0);

if (argc != 3)		usage();

if ( (img=Image_TGA_alloc_load(argv[1]))==NULL )
	{
	fprintf(stderr, "can't load '%s'\n", argv[1]);
	exit(5);
	}

if ( (hf15=Image_clone(img, 0))==NULL )
	{
	fprintf(stderr, "can't clone %p\n", img);
	exit(5);	
	}

/*
 *	et c'est quoi le param�tre qui est � 0 ?
 */
foo = Image_hf15_rgb2hf(img, hf15, 0);
Image_print_error("hf15 rgb2hf", foo);

foo = Image_TGA_save(argv[2], hf15, 0);

return 0;
}
/*::------------------------------------------------------------------::*/
