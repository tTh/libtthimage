/*
		convert a TGA image to various formats
	    -----------------------------------------------
*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/

#define		PGM0	1
#define		PGM1	2
#define		PPM0	10
#define		PHT	20
#define		BMP24	30
#define		PCX8	60
#define		PCX24	61
#define		PGMHF	80
#define		TIFF	90

mot_clef mots_clef[] =
{
{ "pgm0",	PGM0,		"c",	"param: color component"	},
{ "ppm0",	PPM0,		"",	"portable pixmap (rgb)"		},
{ "pht",	PHT,		"c",	"param: color component"	},
{ "bmp24",	BMP24,		"i",	"param must be 0"		},
{ "pcx8",	PCX8,		"i",	"plop"				},
{ "pcx24",	PCX24,		"i",	"plup"				},
{ "pgmhf",	PGMHF,		"",	"height fields for POV"		},
{ "tiff",	TIFF,		"",	""				},
{ NULL,		0,		NULL,	NULL				}
};

/*::------------------------------------------------------------------::*/

void usage(int flag)
{
fprintf(stderr, "*** tga_export v 0.0.10 [%s] %s\n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);

fprintf(stderr, "tga_export source.tga format destination.xxx [params]\n\n");

if (flag) {
	liste_mots_clefs(mots_clef, 42);
	}
exit(5);
}
/*::------------------------------------------------------------------::*/
/*
 *	argv[1]		source.tga
 *	argv[2]		format d'exportation
 *	argv[3]		destination.???
 */
#define FIRST_PARAM	4
int main (int argc, char *argv[])
{
Image_Desc 	*img;
int		foo;
int		idx, commande, nbargs;

dump_command_line(argc, argv, 0);

if (argc < 4)		usage(1);

/* recherche du type d'exportation demand� */
idx = cherche_mot_clef(argv[2], mots_clef, &commande, &nbargs);
if (idx < 0) {
	fprintf(stderr, "tga_export: format %s inconnu...\n", argv[2]);
	exit (5);
	}
if ( (argc-nbargs) != FIRST_PARAM ) {
	fprintf(stderr, "%s: bad number of parameters\n", argv[0]);
	exit(5);
	}

/* analyse des param�tres */
foo = parse_parametres(argc, argv, mots_clef[idx].ptypes, FIRST_PARAM);

if ( (img=Image_TGA_alloc_load(argv[1]))==NULL ) {
	fprintf(stderr, "tga_export: can't load '%s'\n", argv[1]);
	exit(5);
	}

switch (commande) {
	case PGM0:
		foo = Image_wr_pgm_0(argv[3], img, GCP(0));
		break;
	case PPM0:
		foo = Image_wr_ppm_0(argv[3], img, 0);
		break;
	case PHT:
		foo = Image_PHT_save_component(argv[3], img, GCP(0));
		break;
	case BMP24:
		foo = Image_BMP_save_24(argv[3], img, 0);
		break;
	case PGMHF:
		/* une fonction equivalente est dans 'povhf_tools' et
 		 * il serait bien de mutualiser le code */
		foo = Image_hf15_save_PGM(argv[3], img, "Nice try...");
		break;
	default:
		foo = 9999;
		break;
	}

if (foo) {
	fprintf(stderr, "tga_export (%s) ", argv[2]);
	Image_print_error("ecriture fichier", foo);
	exit(1);
	}

return 0;
}
/*::------------------------------------------------------------------::*/
