/*
	ce programme n'est pas pres pour le 'primtime'
	il manque une gestion correcte des arguments.
	voir aussi 'tga_tools.c'
*/


#include <stdio.h>

#include "../tthimage.h"

int main(int argc, char *argv[])
{
Tga_file_header     head;
FILE                *fp;
char                *ptr;
char                comment[257];
int                 foo;

if (argc != 2)
    {
    fprintf(stderr, "il manque le nom du fichier TGA\n");
    exit(1);
    }

if ((fp=fopen(argv[1], "rb")) == NULL)
    {
    fprintf(stderr, "erreur ouverture fichier %s\n", argv[1]);
    exit(1);
    }

fread(&head, 1, sizeof(head), fp);
if (head.text_size)
    {
    fread(comment, 1, head.text_size, fp);
    comment[head.text_size] = '\0';
    }
fclose(fp);

printf("file name           %s\n", argv[1]);

if (head.text_size)
    {
    printf("comment size        %d\n", head.text_size);
    puts(comment);
    }
else puts("no comment.");

printf("is color map ?      %d\n", head.is_color_map);
switch (head.type)
    {
    case  1:        ptr = "8-bit palette";  break;
    case  2:        ptr = "RGB";            break;
    case  3:        ptr = "Gray";	    break;
    case 10:        ptr = "rle/rvb";        break;
    case 11:	    ptr = "grey/rle";	    break; /* !!! a verifier !!! */
    default:        ptr = "???";            break;
    }
printf("type                %d     %s\n", head.type, ptr);
printf("map start           %d\n", head.map_start);
printf("map length          %d\n", head.map_length);
printf("map bits            %d\n", head.map_bits);
printf("x start             %d\n", head.x_start);
printf("y start             %d\n", head.y_start);
printf("width               %d\n", head.width);
printf("height              %d\n", head.height);
printf("nbr of bits         %d\n", head.bits);

switch (head.flags>>4)
    {
    case 0:         ptr = "NON";            break;
    case 1:         ptr = "droite-gauche";  break;
    case 2:         ptr = "haut-bas";       break;
    case 3:         ptr = "les deux";       break;
    default:        ptr = "???";            break;
    }

printf("flags               %02x    flip: %s\n", head.flags, ptr);
return 0;
}
