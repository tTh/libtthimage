/*
	----------------------------------------------
	    utilitaire pour egalizer (uh?) une image
	----------------------------------------------
	     http://la.buvette.org/devel/libimage/
	----------------------------------------------
*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
#define		EQ_STD		4
#define		EQ_GRAY		8
#define		EQ_COS01	9
#define		EQ_COS010	10
#define		EQ_2X2		12
#define		EQ_LUMIN	14
#define		EQ_SQUARE	16
#define		EQ_SQROOT	17
#define		EQ_GAMMA	20
#define		EQ_MULT		21

mot_clef mots_clef[] =
{
{ "std",	EQ_STD,		"", 	"standard method"	},
{ "rgb",	EQ_STD,		"", 	"same as 'std'"		},
{ "gray",	EQ_GRAY,	"",	"gray based"		},
{ "cos01",	EQ_COS01,	"",	"cosinus 0->1"		},
{ "cos010",	EQ_COS010,	"",	"cosinus 0-1-0"		},
{ "2x2",	EQ_2X2,		"",	"2x2 matrix"		},
{ "lumin",	EQ_LUMIN,	"i",	"param: ident is 256"	},
/* { "gamma",	EQ_GAMMA,	"d",	"not implemented"	},*/
{ "square",	EQ_SQUARE,	"",	"pix**2"		},
{ "sqroot",	EQ_SQROOT,	"",	"sqr(pix)"		},
{ "mult",	EQ_MULT,	"d",	"multiply by const"	},
{ NULL,		0,		NULL,	NULL			}
};

/*::------------------------------------------------------------------::*/
void usage()
{
fprintf(stderr, "* tga_equalize v 0.0.25 [%s]  (dwtfywl) tonton Th\n",
					TGA_OUTILS_VERSION);
fprintf(stderr, "  compiled %s at %s\n", __DATE__, __TIME__);
fprintf(stderr, "usage:\n\ttga_equalize avant.tga mode apres.tga [params]\n");
liste_mots_clefs(mots_clef, 42);
exit(5);
}
/*::------------------------------------------------------------------::*/
int main (int argc, char *argv[])
{
Image_Desc	*src, *dst;
int		foo;
int		commande, nbargs, idx;

dump_command_line(argc, argv, 0);
if (argc < 4)		usage();

idx = cherche_mot_clef(argv[2], mots_clef, &commande, &nbargs);
if (idx < 0)
	{
	fprintf(stderr, "tga_equalize: mot-clef %s inconnu...\n", argv[2]);
	exit(5);
	}
#if DEBUG_LEVEL
fprintf(stderr, "idx %d commande %d nbargs %d argc %d\n",
					idx, commande, nbargs, argc);
#endif

foo = parse_parametres(argc, argv, mots_clef[idx].ptypes, 4);

if ( (src=Image_TGA_alloc_load(argv[1])) == NULL )
	{
	fprintf(stderr, "tga_equalize: err load %s\n", argv[1]);
	exit(5);
	}

if ( (dst=Image_clone(src, 0)) == NULL )
	{
	fprintf(stderr, "no mem for image cloning\n");
	exit(5);
	}

switch (commande)
	{
	case EQ_STD:
		foo = Image_egalise_RGB(src, dst, 0);
		break;

	case EQ_GRAY:
		foo = Image_egalise_mono_0(src, dst, 0);
		break;

	case EQ_2X2:
		foo = Image_2x2_contrast(src, dst);
		break;

	case EQ_LUMIN:
		foo = Image_luminance(src, dst, GIP(0));
		break;

	case EQ_SQUARE:
		foo = Image_pix_square(src, dst, 0);
		break;

	case EQ_SQROOT:
		foo = Image_pix_sqroot(src, dst, 0);
		break;

	case EQ_COS01:
		foo = Image_egalise_cos01(src, dst, 0);
		break;

	case EQ_COS010:
		foo = Image_egalise_cos010(src, dst, 0);
		break;

	case EQ_GAMMA:
		fprintf(stderr, "no gamma func in %d\n", getpid());
		foo = FULL_NUCKED;
		break;

	case EQ_MULT:
		foo = PASTIS;
		break;


	default:
		fprintf(stderr, "unexpected command #%d\n", commande);
		foo=-1;
		break;
	}

if (foo) {
	fprintf(stderr, "%s: retour = %d, %s\n", argv[0],
				foo, Image_err2str(foo));
	}

foo = Image_TGA_save(argv[3], dst, 0);

return 0;
}
/*::------------------------------------------------------------------::*/
