/*
		...............................
		.redimensionnement d'une image.
		...............................
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>


#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
#define  HALF_SIZE	1
#define  DOUBLE_SIZE	2
#define  PERCENT	3
#define  NEWSIZE	4

mot_clef mots_clef[] =
{
{ "half",	HALF_SIZE,	"f",	"0:crude 1:interpolate"		},
{ "double",	DOUBLE_SIZE,	"f",	""				},
{ "percent",    PERCENT,	"dii",  "%%size, method, 0"		},
{ "newsize",	NEWSIZE,	"iii",	"newW newH method"		}
};
/*::------------------------------------------------------------------::*/
void usage(int flag)
{
fprintf(stderr, "* Tga Resize v 0.0.13 [%s] %s\n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);

fprintf(stderr, "usage:\n");
fprintf(stderr, "\ttga_resize source.tga methode resultat.tga [params]\n");
if (flag)
	liste_mots_clefs(mots_clef, 42);
else
	Image_print_version(0);
exit(5);
}
/*::------------------------------------------------------------------::*/
/*
 *	argv[1]		source.tga
 *	argv[2]		m�thode
 *	argv[3]		destination.tga
 */
#define  FIRST_PARAM	4
int main(int argc, char *argv[])
{
Image_Desc	*src, *dst;
int		foo, idx;
int		commande, nbargs, interpol;
double		scale, scaleX, scaleY;
int		newW, newH;

dump_command_line(argc, argv, 0);

if ( argc==2 && !strcmp(argv[1], "list") ) usage(1);
if ( argc < 4)	usage(0);

/*
 *	rechercher le mot clef dans la table
 */
idx = cherche_mot_clef(argv[2], mots_clef, &commande, &nbargs);
if (idx < 0)
	{
	fprintf(stderr, "keyword %s unknow\n", argv[1]);
	exit(5);
	}

/*
 *	d�codage des param�tres
 */
foo = parse_parametres(argc, argv, mots_clef[idx].ptypes, FIRST_PARAM);
#if DEBUG_LEVEL
fprintf(stderr, "tga_resize: ret parse parameters = %d\n", foo);
#endif

if ((src = Image_TGA_alloc_load(argv[1]))==NULL)
	{
	fprintf(stderr, "tga_resize: can't load '%s'\n", argv[1]);
	exit(5);
	}

dst = NULL;	/* secure the default case */

switch (commande)
	{
	case HALF_SIZE:
		if ( (dst=Image_MakeHalfSize(src, GFP(0))) == NULL)
			{
			fprintf(stderr, "tga_resize: half: fatal error\n");
			exit(1);
			}
		foo = 0;
		break;

	case DOUBLE_SIZE:
		if ( (dst=Image_MakeDoubleSize(src, GFP(0))) == NULL)
			{
			fprintf(stderr, "tga_resize: double: fatal error\n");
			exit(1);
			}
		foo = 0;
		break;

	case PERCENT:
		scale = GDP(0) / 100.0;
		interpol = GIP(1);
#if DEBUG_LEVEL
		printf("  scale = %f\n", scale);
		printf("  interpol = %d\n", interpol);
#endif
		if ((dst = Image_new_scale(src, scale, scale, interpol))==NULL)
			{
			fprintf(stderr, "tga_resize: percent: fatal error\n");
			exit(1);
			}
		break;

	case NEWSIZE:
		newW = GIP(0);
		newH = GIP(1);
		printf("new size: %d x %d\n", newW, newH);
		scaleX = ((double)newW / (double)src->width);
		scaleY = ((double)newH / (double)src->height);
		printf("scales:  X %f  y %f\n", scaleX, scaleY);	

		if ((dst = Image_new_scale(src, scaleX, scaleY, GIP(2)))==NULL)
			{
			fprintf(stderr, "tga_resize: newsize: fatal error\n");
			exit(1);
			}
		break;
	}

/*
 *	sauvons vite ce precieux resultat
 */
if (dst != NULL)
	foo = Image_TGA_save(argv[3], dst, 0);

return 0;
}
/*::------------------------------------------------------------------::*/
