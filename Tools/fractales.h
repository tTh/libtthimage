/*
			fractales.h
			-----------
   *....................................................*
   .        warning, this is a 'work in progress'       .
   .        don't trust prototypes for building         .
   .        serious applications...                     .
   *....................................................*


*/

void Image_fractales_print_version(int flag);

int GingerBreadMan(Image_Desc *im, int maxiter);

int Image_Random_Walker_0(Image_Desc *im, Image_Desc *dst,
						int nbpass, int nbsteps);
int Image_Julia_0_0(Image_Desc *, RGB_map *, int, double, double);
int Image_Mandelbrot_0(Image_Desc *image, int maxiter);

int Newton_0(Image_Desc *im, int maxiter);

int Lorenz_Orbits(Image_Desc *img, int iters,
				double a, double b, double c, double dt);
