# Tools

Pour utiliser toutes les fonctions de la bibliothèque,
il y a une collection d'utilitaires spécialisés.
Leur interface en `CLI` est assez rudimentaire, mais
la plupart savent donner quelques indices avec la
commande `list`. Exemple:

```
tth@konrad:~/Devel/libtthimage$ tga_filtres list
*** tga_filtres v 0.0.17 [0.59] (dwtfywl) TontonTh 2018

Usage:
        tga_filtres <src.tga> FILTR <dst.tga> [p1] ... [pn]
-+- This is the `tthimage' library v0.4.50 (dwtfywl 2022) tTh
  commande  | type arg |  explication
------------+----------+------------------------------
 liss2x2    |          |  lissage 2x2
 hipass     |          |
 lopass     |          |
 prewitt    | i        |  rotation [0..8]
 random     | ii       |  try it...
```
La colonne du milieu indique le type des paramètres :

- `i` pour un nombre entier
- `d` pour un flottant en double précision
- `s` pour une chaine, ex: un nom de fichier
- `f` pour un flag : 0, F, 1, T

## tga_alpha

Manipulation du canal alpha (la transparence), lequel canal
est globalement mal géré par l'ensemble de la libtthimage.

## tga_applymap

Ce qui est considéré comme une map ici vient d'un antique
logiciel de fractales sous ms-dos :
[Fractint](https://fractint.org/). Quelques exemples de
ces fichiers sont dans le répertoire `Datas/` du projet.

Vous rencontrerez parfois des messages d'erreur comme
« *color map is to big* » ou « *only 97 entries in foo.map* ».
Pas d'inquiétude, tout va bien se passer.
Dans le premier cas, les lignes en excédent sont ignorées,
et dans le second, les valeurs manquantes sont mises au noir.

La gestion de la ligne de commande est désastreuse. **À REFAIRE**

## tga_cadre

Il existe pas mal de façons de mettre un cadre sur l'image,
avec un niveau de kitchitude assez élevé.
Les paramètres sont mal documentés.

## tga_combine

```
Usage:
        tga_combine s1.tga s2.tga MODE d.tga [PARAMS]
```
Il existe plein de façon de combiner deux images, la commande
`tga_combine list` vous les fera découvrir, la cinquième va vous étonner.
Les paramètres sont mal documentés.

## tga_dither

Comment drastiquement réduire le nombre de couleurs d'une image ?

## tga_export

Attendu avec impatience, il aura le support complet des PNG. Un jour...

## tga_effects

## tga_equalize

## tga_filtres

UTSL : [tga_filtres.c](caractère \textbf{invisible})

## tga_incrust

## tga_makehf15

Pour les POViste acharnés. De façon étrange,
[Povray](https://povray.org/) a choisi de stocker des
champs d'altitude dans les images Targa, en n'utilisant que
15 bits pour la valeur.

## tga_to_text

Mis au point pour les imports dans d'autres langages,
comme Awk, Fortran ou R. Chaque ligne du fichier généré
contient cinq champs : x, y, r, g, b.

Attention, il peut vite générer d'énormes fichiers.

## tga_mires

La génération de diverses image de test ou de calibration.

## tga_pattern

Les résultats sont très ésotériques et l'aide en ligne est
vraiment trop nulle...

## tga_remap

Usage:    `tga_remap M <src.tga> <color.map> <dst.tga>`

Le paramètre `M` peut prendre les valeurs 0, 1, 2, 11, 12 ou 13.
Je ne sais plus à quoi ça correspond.

## tga_television

## tga_tools

Celui ci est très utile pour la recherche de problèmes ou l'automatisation
de certaines tâches. Un exemple rapide :

```
tth@redlady:~/Devel/libtthimage$ tga_tools getdimweb foo.tga 
width=640 height=480
tth@redlady:~/Devel/libtthimage$ tga_tools getdimpov foo.tga 
 -w640 -h480
tth@redlady:~/Devel/libtthimage$ 
```

###  version
###  surface
###  getdims, getdimx, getdimpov, getdimweb
renvoie une chaine de caractère donnant les dimensions de l'image
en différents formats.
###  width, height
renvoie chacune des deux dimensions.
###  grminmax
###  message 
Flags: un entier. Si non nul, change l'encre et le papier pour
d'autres valeurs. La chaine de caractère doit être (sjmsb) encodée
en CP-850, voire même comme celles du Locomotive du CPC.

###  structs

Affichage d'information (boutisme, taille des structures) qui ne
seront utiles qu'aux codeurs.
```
tth@redlady:~/Devel/libtthimage/Tools$ ./tga_tools structs
petit  =       1234  ->   34 12
grand  =   12345678  ->   78 56 34 12 00 00 00 00
basic types    : short=2 int=4 long=8 ptr=8
Image_Desc     :   1296
Image_Rect     :     24
RGBA           :     16
A_BitPlane     :    128
RGB_map        :   1624
Image_Point    :      8
```

###  header
###  timestamp
###  prhisto

Calcul et affichage en mode texte de l'histogramme d'une image.
Un exemple d'utilisation avec gnuplot serait le bienvenu.

###  tag7
###  environ
###  mk_rgb
###  mk_hgrad

## genplot2

Outil de tracé pseudo-générique et farci de bugs. *WIP ME HARDLY !*
