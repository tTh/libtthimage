/*
		TGA EXTRACTOR
		-------------
		http://www.chez.com/oulala/libimage/
*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
void usage(void)
{
fprintf(stderr, "* TGA extractor  v 0.0.12 [%s] %s \n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);
fprintf(stderr, "usage:\n");
fprintf(stderr, "      $ tga_extract source.tga resultat.tga X Y W H\n\n");
Image_print_version(0);
fprintf(stderr, "\n");
exit(1);
}
/*::------------------------------------------------------------------::*/

#define		X	0
#define		Y	1
#define		W	2
#define		H	3

int main(int argc, char *argv[])
{
Image_Desc	*image, *resultat;
int		foo, coords[4];
Image_Rect	rect;

dump_command_line(argc, argv, 0);

if ( argc != 7 ) {
	fprintf(stderr, "argc=%d\n", argc);
	usage();
	}

/* decode and check parameters */
for (foo=0; foo<4; foo++) {
	if ( sscanf(argv[foo+3], "%d", &coords[foo]) != 1 ) {
		fprintf(stderr, " '%s'  oikk?!\n", argv[foo+3]);
		usage();
		}
	}

if ( (coords[X]<0) || (coords[Y]<0) || (coords[W]<0) || (coords[H]<0) )
	{
	fprintf(stderr, "%s: no negative parameter, please.\n", argv[0]);
	exit(1);
	}
if (coords[X]>16000 || coords[Y]>16000 || coords[W]>16000 || coords[H]>16000)
	{
	fprintf(stderr, "%s: no overburned parameter, please.\n", argv[0]);
	exit(1);
	}

/* load the source image ... */
if ( (image=Image_TGA_alloc_load(argv[1])) == NULL ) {
	fprintf(stderr, "%s: no mem for input image '%s'\n", argv[0], argv[1]);
	exit(1);
	}

if (must_be_verbose()) {
	/* ... so we know source dimensions */
	fprintf(stderr, "tga_extract: src '%s': %d x %d\n",
			argv[1], image->width, image->height);
	}

if ( (resultat=Image_alloc(coords[W], coords[H], 3)) == NULL ) {
	fprintf(stderr, "%s: no mem for ouput image\n", argv[0]);
	exit(1);
	}

rect.x = coords[X];	rect.y = coords[Y];
rect.w = coords[W];	rect.h = coords[H];

foo = Image_get_rect(image, &rect, resultat, 0, 0); if (foo)
	{
	fprintf(stderr, "tga_extract: get_rect: err %d %s\n",
						foo, Image_err2str(foo));
	exit(2);
	}
if (must_be_verbose()) {
	fprintf(stderr, "tga_extract: dst '%s': %dx%d @%d,%d\n",
			argv[2], resultat->width, resultat->height,
			rect.x, rect.y);
	}

foo = Image_TGA_save(argv[2], resultat, 0);
if (foo) {
	fprintf(stderr, "tga_extract: %s save err %d %s\n",
					argv[2], foo, Image_err2str(foo));
	exit(2);
	}

return 0;
}
/*::------------------------------------------------------------------::*/
