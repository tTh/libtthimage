/*
          =I            =I                                                    
  =I=I=I  =I  =I=I=I    =I=I=I      =I=I=I        =I=I    =I=I=I      =I=I=I  
=I    =I  =I  =I    =I  =I    =I  =I    =I      =I    =I  =I    =I  =I=I      
=I    =I  =I  =I    =I  =I    =I  =I    =I      =I    =I  =I    =I      =I=I  
  =I=I=I  =I  =I=I=I    =I    =I    =I=I=I        =I=I    =I=I=I    =I=I=I    
              =I                                          =I                  
              =I                                          =I                  

*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/

#define ALPHA_ADD	1
#define ALPHA_REDUCE	2
#define ALPHA_KILL	3
#define ALPHA_SETV	4

mot_clef mots_clef[] =
{
{ "add",	ALPHA_ADD,	"i",	"value of alpha channel"	},
{ "reduce",	ALPHA_REDUCE,	"i",	"param = ?, put zero..."	},
{ "kill",	ALPHA_KILL,	"",	"kill the alpha channel"	},
{ "setv",	ALPHA_SETV,	"i",	"set alpha channel to param"	},
{ NULL,		0,		NULL,	NULL				}
};

/*::------------------------------------------------------------------::*/
void usage(int flag)
{
fprintf(stderr, "* tga_alpha v 0.0.10 [%s] %s\n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);
Image_print_version(0);

fprintf(stderr, "Usage:\n   tga_alpha src op dst\n");

if (flag)
	{
	liste_mots_clefs(mots_clef, 0);
	}

exit(5);
}
/*::------------------------------------------------------------------::*/
/*
 *	argv[1] = source image
 *	argv[2] = operation
 *	argv[3] = destination image
 */
#define FIRST_PARAM 4

int main(int argc, char *argv[])
{
Image_Desc	*src, *dst;
int		foo;
int		idx, commande, nbargs;
/* XXX RGB_map		map; */

dump_command_line(argc, argv, 0);

if (argc<3)	usage(1);

/* search operation keyword in table */
idx = cherche_mot_clef(argv[2], mots_clef, &commande, &nbargs);
if (idx < 0)
	{
	fprintf(stderr, "tga_filtres: mot-clef %s inconnu...\n", argv[2]);
	exit (5);
	}

foo = parse_parametres(argc, argv, mots_clef[idx].ptypes, FIRST_PARAM);
#if DEBUG_LEVEL
fprintf(stderr, "tga_alpha: parse params : %d\n", foo);
#endif

if ( (src=Image_TGA_alloc_load(argv[1])) == NULL )
	{
	fprintf(stderr, "tga_alpha: can't load '%s'\n", argv[1]);
	exit(1);
	}

if ( (dst=Image_clone(src, 0)) == NULL )
	{
	fprintf(stderr, "tga_alpha: can't clone %p\n", src);
	exit(1);
	}

switch (commande)
	{
	case ALPHA_ADD:
		Image_copy(src, dst);
		foo = GIP(0);
		fprintf(stderr, "valeur canal alpha %d\n", foo);
		Image_add_alpha_channel(dst, foo);
		break;
	
	case ALPHA_REDUCE:
		fprintf(stderr, "'%s' not implemented.\n", argv[2]);
		exit(1);
		break;

	case ALPHA_KILL:
		Image_copy(src, dst);
		fprintf(stderr, "jette le canal alpha\n");
		Image_kill_alpha_channel(dst);
		break;

	case ALPHA_SETV:
		Image_copy(src, dst);
		foo = Image_alpha_setvalue(dst, GIP(0));
		break;

	default:
		fprintf(stderr, "FUNCTION NOT IMPLEMENTED\n");
		break;

	}

if (foo)
	{
	fprintf(stderr, "alpha op -> %d\n", foo);
	}

foo = Image_TGA_save(argv[3], dst, 0);

return 1;
}
/*::------------------------------------------------------------------::*/
