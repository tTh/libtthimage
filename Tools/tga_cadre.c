/*
		Encadreur de Tga
		----------------

	http://la.buvette.org/devel/libimage/img-outils.html
	=======================================================
	3 Juin 2002: bon, une seule lettre pour selectionner le 
	type du cadre, c'est pas top... donc...
	12 Juin 2002: mise en place du chercheur de commande
	dont vous trouverez le source dans 'fonctions.c'
	=======================================================
*/

#include  <stdio.h>
#include  <string.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
#define CDR_A		1
#define CDR_B		2	/* cracra */
#define CDR_SWEEP	3	/* sweep */
#define CDR_D		4
#define CDR_E		5
#define CDR_f		6
#define CDR_p		7
#define CDR_P		8
#define CDR_r		9
#define CDR_BOX0	10
#define CDR_BURP0	20
#define CDR_BURP1	21
#define CDR_BURP2	22
#define CDR_BURP3	23
#define CDR_SOFT_1	30		/* new 13 mai 2009 */
#define CDR_PIXX	41
#define CDR_RASTA0	42		/* don't walk on the grass */
#define CDR_WAVE0	54
#define CDR_WAVE1	65

#define CDR_ZSND0	80
#define CDR_ZSND1	81

#define CDR_B84_A	90
#define CDR_B84_B	91
#define CDR_B84_C	92

mot_clef mots_clef[] =
{
{ "A",		CDR_A,	"", 	"use 'filet' instead"			},
{ "filet",	CDR_A,  "",     "un simple filet noir & blanc"		},
{ "B",		CDR_B,	"i", 	"use 'cracra' instead"			},
{ "cracra",	CDR_B,	"i", 	"un machin bizarre, un peu cracra"	},
{ "sweep",	CDR_SWEEP,  "iiii",	"degrade: rouge vert bleu taille" },
{ "D",		CDR_D,	"iiii",	"use 'bruit' instead"			},
{ "bruit",	CDR_D,	"iiii",	"du bruit: r g b taille"		},
{ "E",		CDR_E,	"iiii",	"cadre 'blablanux': 4 parametres"	},
{ "blablanux",	CDR_E,	"iiii",	"cadre 'blablanux': 4 parametres"	},
{ "f",		CDR_f,	"ii", 	"un simple filet en gris, 2 params"	},
{ "filetg",	CDR_f,	"ii", 	"un simple filet en gris, 2 params"	},

{ "p",		CDR_p,	"si", 	NULL					},
{ "pattern",	CDR_p,	"si", 	"cadre pattern"				},
{ "P",		CDR_P,	"si", 	NULL					},
{ "patt_degr",	CDR_P,	"si", 	"pattern degrade"			},
{ "r",		CDR_r,	"si",	NULL					},
{ "patt_rand",	CDR_r,	"si",	"pattern random"			},

{ "burp0",	CDR_BURP0,	"iii",		"cadre burp 0"		},
{ "burp1",	CDR_BURP1,	"ii",		"cadre burp 1"		},
{ "burp2",	CDR_BURP2,	"iiii",		"inversion composantes"	},
{ "burp3",	CDR_BURP3,	"iiii",		"en chantier..."	},
{ "soft1",	CDR_SOFT_1,	"",		"en chantier aussi"	},
{ "x",		CDR_PIXX,	"ii",		NULL			},
{ "pixx",	CDR_PIXX,	"ii",		"pixx me !"		},
{ "rasta0",	CDR_RASTA0, 	"i",		"largeur des bandes"	},
{ "w",		CDR_WAVE0,	"iiiii",	NULL			},
{ "wave0",	CDR_WAVE0,	"iiiii",	"des vagues..." },

{ "zsnd0",	CDR_ZSND0,	"iii",		"cadre zongo sound"	},
{ "zsnd1",	CDR_ZSND1,	"iii",		"cadre zongo sound"	},

{ "b84a",	CDR_B84_A,	"iii",		"cadre de la base 84"	},
{ "b84b",	CDR_B84_B,	"ii",		"cadre de la base 84"	},
{ "b84c",	CDR_B84_C,	"iii",		"non cadre base 84"	},

{ NULL,		0,		NULL,	NULL		}
};
/*::------------------------------------------------------------------::*/
void usage(int flag)
{
fprintf(stderr, "*** tga_cadre v 0.1.85 [%s] %s\n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);

if (flag)
	{
	Image_print_version(0);
	fprintf(stderr, "\ttga_cadre <src.tga> STYLE <dst.tga> [p1]...[p8]\n");
	liste_mots_clefs(mots_clef, 42);
#if DEBUG_LEVEL
	fprintf(stderr, "ouba rand is %d\n", rand());
#endif
	}
else
	{
	Image_print_version(1);
	fprintf(stderr, "\nUsage:\n");
	fprintf(stderr, "\ttga_cadre <src.tga> STYLE <dst.tga> [p1]...[p8]\n");
	fprintf(stderr, "\ttry 'tga_cadre list' for a list.\n");
	}

exit(5);
}
/*::------------------------------------------------------------------::*/
/*
 *	argv[1] 	type cadre
 *	argv[2] 	image source
 *	argv[3] 	image destination 
 *	argv[...] 	parametres 
 */
#define FIRST_PARAM 4

int main(int argc, char *argv[])
{
Image_Desc	*src, *dst, *pattern;
int		foo, bar;
RGBA		rgba;
int		commande, nbargs, idx;

dump_command_line(argc, argv, 0);

foo = set_new_seed(0);
#if DEBUG_LEVEL
fprintf(stderr, "%s : newseed -> %d\n", argv[0], foo);
#endif

if (argc==2 && !strcmp(argv[1], "-?"))		usage(0);
if (argc==2 && !strcmp(argv[1], "list"))	usage(1);
if (argc < 4) usage(0);

if (strlen(argv[1])==1)
	fprintf(stderr, "!!! %s : single letter arg '%s' is deprecated\n",
			   argv[0],                argv[1]);


/* recherche du type de cadre demande */
idx = cherche_mot_clef(argv[2], mots_clef, &commande, &nbargs);
if (idx < 0)
	{
	fprintf(stderr, "tga_cadre: mot-clef %s inconnu...\n", argv[2]);
	exit(5);
	}
#if DEBUG_LEVEL
fprintf(stderr, "retour %d commande %d nbargs %d argc %d\n",
					idx, commande, nbargs, argc);
#endif

if ( (argc-nbargs) != FIRST_PARAM )
	{
	fprintf(stderr, "%s: bad number of parameters\n", argv[0]);
	exit(5);
	}
/*
 *	decodage des parametres
 */
foo = parse_parametres(argc, argv, mots_clef[idx].ptypes, FIRST_PARAM);
#if DEBUG_LEVEL
fprintf(stderr, "retour de parse parameters = %d\n", foo);
#endif

/*
 *	debut du traitement des images.
 *	en premier le chargement de la source
 */
if ( (src=Image_TGA_alloc_load(argv[1])) == NULL )
	{
	fprintf(stderr, "tga_cadre: can't load \"%s\" ...\n", argv[1]);
	exit(1);
	}

/* 26 Jan 2002: en fait, est-il necessaire de cloner l'image ? */
if ( (dst=Image_clone(src, 1)) == NULL )
	{
	fprintf(stderr, "tga_cadre: can't clone %p\n", src);
	exit(1);
	}

switch (commande)
	{
	case CDR_A:	foo = Image_cadre_A(dst);		break;

	case CDR_B:	foo = Image_cadre_cracra(dst, GIP(0), 0); break;

	case CDR_SWEEP:
		foo=Image_cadre_C(dst, GIP(0), GIP(1), GIP(2), GIP(3));
		break;

	case CDR_D:
#if DEBUG_LEVEL
		printf("tga_cadre bruit : rand is %d\n", rand());
#endif
		foo=Image_cadre_bruit(dst, GIP(0), GIP(1), GIP(2), GIP(3));
		break;

	case CDR_E:
		/* aka blablanux */
		foo=Image_cadre_E(dst, GIP(0), GIP(1), GIP(2), GIP(3));
		break;
	
	case CDR_f:
		foo=Image_cadre_f(dst, GIP(0), GIP(1));
		break;

	case CDR_p:		/* pattern */
		pattern = Image_TGA_alloc_load(GSP(0));
		if (pattern==NULL)
			{
			fprintf(stderr, "pattern '%s' not found\n", GSP(0));
			exit(5);
			}
		Image_copy(src, dst);
		foo = Image_cadre_pattern_0(dst, pattern, GIP(1));
		break;
		
	case CDR_P:		/* pattern d�grad� */
		pattern = Image_TGA_alloc_load(GSP(0));
		if (pattern==NULL)
			{
			fprintf(stderr, "pattern '%s' not found\n", GSP(0));
			exit(5);
			}
		Image_copy(src, dst);
		foo = Image_cadre_pattern_1(dst, pattern, GIP(1));
		break;
		
	case CDR_r:		/* pattern random */
		pattern = Image_TGA_alloc_load(argv[4]);
		if (pattern==NULL)
			{
			fprintf(stderr, "pattern '%s' not found\n", argv[4]);
			exit(5);
			}
		Image_copy(src, dst);
		foo = Image_cadre_pattern_2(dst, pattern, GIP(1));
		break;
		
	case CDR_BURP0:
		Image_copy(src, dst);
		foo = Image_cadre_burp_0(dst, GIP(0), GIP(1), GIP(2));
		break;

	case CDR_BURP1:
		Image_copy(src, dst);
		foo = Image_cadre_burp_1(dst, GIP(0), GIP(1), 0);
		break;

	case CDR_BURP2:
		Image_copy(src, dst);
		/* a quoi correspondent les parametres ? */
		foo = Image_cadre_burp_2(dst, GIP(0), GIP(1), GIP(2), GIP(3));
		break;

	case CDR_BURP3:
		Image_copy(src, dst);
		foo = Image_cadre_burp_3(dst, GIP(0), GIP(1), GIP(2), GIP(3));
		break;

	case CDR_PIXX:
		bar = GIP(1);
		fprintf(stderr, "* p2 of pixx is %d\n", bar);	
		foo=Image_cadre2_pixx(src, dst, GIP(0), bar);
		break;

	case CDR_RASTA0:
		bar = GIP(0);
		foo = Image_cadre_rasta_0(dst, bar);
		break;

	case CDR_WAVE0:
		Image_copy(src, dst);
		rgba.r = GIP(2);
		rgba.g = GIP(3);
		rgba.b = GIP(4);
		rgba.a = 0;		/* XXX */
		foo = Image_cadre_waves_0(dst, GIP(0), GIP(1), &rgba, 12);
		break;

	case CDR_B84_A:
#if DEBUG_LEVEL
		fprintf(stderr, "You are calling a COREDUMPER !\n");
#endif
		foo = Image_cadre_b84_a(dst, GIP(0), GIP(1), GIP(2));
		break;
	case CDR_B84_B:
#if DEBUG_LEVEL
		fprintf(stderr, "You are calling a COREDUMPER !\n");
#endif
		/* XXX attention, cette fonction va peut-etre avoir
		 * un parametre supplementaire dans un proche avenir */
		foo = Image_cadre_b84_b(dst, GIP(0), GIP(1));
		break;
	case CDR_B84_C:
#if DEBUG_LEVEL
		fprintf(stderr, "You are calling a COREDUMPER !\n");
#endif
		foo = Image_cadre_b84_c(dst, GIP(0), GIP(1), GIP(2));
		break;

	case CDR_ZSND0:
		foo = Image_cadre_A(dst);		/* FIXME */
		break;

	default:
		fprintf(stderr, "*** case %d not found ***\n", commande);
		usage(1);
		break;
	}

if (foo)
	{
	fprintf(stderr, "tga_cadre: type '%s': %d / %s\n",
				argv[2], foo, Image_err2str(foo));
	}

foo = Image_TGA_save(argv[3], dst, 0);
if (foo)
	fprintf(stderr, "oups %d\n", foo);

exit (0);
}
/*::------------------------------------------------------------------::*/
