/*
	----------------------------------------------
	utilitaire pour tracer une map 'a la fractint'
	----------------------------------------------
	     http://tth.zouh.org/libimage.html   
	----------------------------------------------
*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/* ------------------------------------------------------------------ */
void usage()
{
fprintf(stderr, "* tga_plotmap v 0.1.11 [%s] %s\n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);
fprintf(stderr, "usage:\n\ttga_plotmap colors.map graphe.tga [texte]\n\n");
Image_print_version(1);
exit(5);
}

#define		CLASSIC		1
#define		PAPER		2
#define		SQUARE		3

mot_clef mots_clef[] =
{
{ "classic",	CLASSIC,	"", 	"classic"	},
{ "paper",	PAPER,		"",	"papier"	},
{ "square",	SQUARE,		"",	"square"	},
{ NULL,		0,		"",	NULL		}
};

/* ------------------------------------------------------------------ */
int main (int argc, char *argv[])
{
RGB_map		map;
int		foo;

dump_command_line(argc, argv, 0);

if ((argc!=3)&&(argc!=4))		usage();

foo = Image_load_color_Map(argv[1], "tga_plotmap", &map);
if (foo)
	{
	fprintf(stderr, "tga_plotmap, chargement '%s': %d  %s\n",
					argv[1], foo, Image_err2str(foo));
	exit(5);
	}

fprintf(stderr, "map file %s have %d colors\n", argv[1], map.nbre);

foo = Image_plot_Map(argv[2], &map, argc==4?argv[3]:argv[1]);
if (foo)
	{
	fprintf(stderr, "tga_plotmap, dessin de '%s': %d  %s\n",
					argv[2], foo, Image_err2str(foo));
	}

return 0;
}
