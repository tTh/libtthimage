/*
	 , __                               
	/|/  \                              
	 |___/ __, _|__|_  _   ,_    _  _   
	 |    /  |  |  |  |/  /  |  / |/ |  
	 |    \_/|_/|_/|_/|__/   |_/  |  |_/
                                    
*/
                                    

#include  <stdio.h>
#include  <string.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
struct type
	{
	char	*nom;
	int	code;
	int nbparams;
	};

#define REPONSE		1
#define GRAYNOISE	2
#define RGBNOISE	3
#define TEXTURE_1	21
#define TEXTURE_2	22
#define TEXTURE_3	23
#define EXPOV_0		24
#define PATT_100	30
#define PATT_101	31
#define PATT_102	32	/* new June 2003 */
#define GRAYNOISE2	40
#define RGBNOISE1	50
#define RGBNOISE2	51
#define CHECK_BW	80
#define PATT_000	90
#define PATT_001	91
#define PATT_002	92

static char no_help[] = "no help...";

mot_clef mots_clef[] = 
{
{ "graynoise",	GRAYNOISE,	"ii",	"bruit gris"	},
{ "rgbnoise",	RGBNOISE,	"ii",	"bruit RGB"	},
{ "42",		REPONSE,	"ii",	no_help },
{ "reponse",	REPONSE,	"ii",	no_help },
{ "text1",	TEXTURE_1,	"ii",	no_help },
{ "text2",	TEXTURE_2,	"iii",	no_help },
{ "expov",	EXPOV_0,	"iii",	no_help },
{ "cent",	PATT_100,	"iii", "kr kg kb"	},
{ "centun",	PATT_101,	"iii", "kr kg kb"	},
{ "centdeux",	PATT_102,	"iiii", no_help },
{ "first",	PATT_000,	"i",	"byte mask"	},
{ "second",	PATT_001,	"i",	"magic value"	},
{ "third",	PATT_002,	"i",	"magic value"	},
{ "graynoise2", GRAYNOISE2,	"ii",	no_help },
{ "rgbnoise1",  RGBNOISE1,	"iiiiii",	no_help },
{ "rgbnoise2",  RGBNOISE2,	"iiiiii",	no_help },
{ "check_bw",	CHECK_BW,	"iiii",		no_help },
{ NULL,		0,		NULL,	NULL		}
};

/*::------------------------------------------------------------------::*/
void usage(int flag)
{
fprintf(stderr, "* Tga Pattern v 0.0.28 [%s] %s\n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);
Image_print_version(flag);

fprintf(stderr, "\nUsage:\n");
fprintf(stderr, "\ttga_pattern dest.tga width height type [params]\n");

if (flag)
	liste_mots_clefs(mots_clef, 42);

exit(5);
}
/*::------------------------------------------------------------------::*/
/*
 *	argv[1]		nom du fichier resultat
 *	argv[2]		largeur
 *	argv[3]		hauteur
 *	argv[4]		type du pattern
 */
#define FIRST_PARAM  5
int main(int argc, char *argv[])
{
Image_Desc	*dst;
int		foo, width, height;
RGBA		rgba1, rgba2;
int		commande, nbargs, idx;

dump_command_line(argc, argv, 0);
srand(getpid());

if (argc==1)	usage(0);

if (argc==2 && !strcmp("list", argv[1]))
	{
	usage(1);
	}
if (argc < 5)	usage(0);

idx = cherche_mot_clef(argv[4], mots_clef, &commande, &nbargs);
if (idx < 0)
	{
	fprintf(stderr, "mot-clef %s inconnu...\n", argv[4]);
	exit(5);
	}
#if DEBUG_LEVEL
fprintf(stderr, "  idx=%d commande=%d nbargs=%d\n", idx, commande, nbargs);
#endif

foo = parse_parametres(argc, argv, mots_clef[idx].ptypes, FIRST_PARAM);
#if DEBUG_LEVEL
fprintf(stderr, "  retour parse parameters = %d\n", foo);
#endif

/* allocation et preparation de l'image */
width = atoi(argv[2]);
height = atoi(argv[3]);
if (width<1 || width>16000 || height<1 || height>16000)
	{
	fprintf(stderr, "%s: hu ho ? invalid dimension\n", argv[0]);
	exit(5);
	}

dst = Image_alloc(width, height, 3);

/*
 *	calcul de l'image patternelle.
 */
switch(commande)
	{
	case REPONSE:
		foo = Image_pattern_042(dst, GIP(0));
		break;

	case RGBNOISE:
		fprintf(stderr, "entering RGBNOISE\n");
		foo = Image_rgb_noise_0(dst, GIP(0), GIP(1));
		break;

	case GRAYNOISE:
		fprintf(stderr, "entering GRAYNOISE\n");
		foo = Image_gray_noise_0(dst, GIP(0), GIP(1));
		fprintf(stderr, "gray noise -> %d\n", foo);
		break;

	case TEXTURE_1:
		foo = Image_texture_1(dst, GIP(0), GIP(1));
		break;

	case TEXTURE_2:
		foo = Image_texture_2(dst, GIP(0), GIP(1), GIP(2));
		break;

	case EXPOV_0:
		foo = Image_pattern_100(dst, GIP(0), GIP(1), GIP(2));
		break;

	case PATT_000:
		foo = Image_pattern_000(dst, GIP(0));
		break;

	case PATT_001:
		foo = Image_pattern_001(dst, GIP(0));
		break;
	case PATT_002:
		foo = Image_pattern_002(dst, GIP(0));
		break;

	case PATT_100:
		foo = Image_pattern_100(dst, GIP(0), GIP(1), GIP(2));
		break;

	case PATT_101:
		foo = Image_pattern_101(dst, GIP(0), GIP(1), GIP(2));
		break;

	case PATT_102:
		foo = Image_pattern_102(dst, GIP(0), GIP(1), GIP(2), GIP(3));
		break;

	case GRAYNOISE2:
		foo = Image_gray_noise_2(dst, GIP(0), GIP(1));
		break;

	case RGBNOISE1:
		rgba1.r = GIP(0);	rgba2.r = GIP(1);
		rgba1.g = GIP(2);	rgba2.g = GIP(3);
		rgba1.b = GIP(4);	rgba2.b = GIP(5);
		foo = Image_rgb_noise_1(dst, &rgba1, &rgba2);
		break;

	case RGBNOISE2:
		rgba1.r = GIP(0);	rgba2.r = GIP(1);
		rgba1.g = GIP(2);	rgba2.g = GIP(3);
		rgba1.b = GIP(4);	rgba2.b = GIP(5);
		foo = Image_rgb_noise_2(dst, &rgba1, &rgba2);
		break;

	case CHECK_BW:
		rgba1.r = rgba1.g = rgba1.b = GIP(2);
		rgba2.r = rgba2.g = rgba2.b = GIP(3);
		fprintf(stderr, "CHECK_BW : encore en mise au point...\n");
		foo = Image_pattern_104(dst, GIP(0), GIP(1), &rgba1, &rgba2);
		break;

	default:
		fprintf(stderr, "type #%d not implemented\n", commande);
		exit(5);
	}

if (foo)
	Image_print_error("fabrication pattern", foo);

foo = Image_TGA_save(argv[1], dst, 0);

if (foo)
	Image_print_error("sauvegarde pattern", foo);

return 0;
}
/*::------------------------------------------------------------------::*/
