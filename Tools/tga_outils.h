/*
		tga_outils.h
		------------
*/

#include  <unistd.h>

#include  "../tthimage.h"

#define  TGA_OUTILS_VERSION   "0.62"
/*
 *	13 Dec 2001: v0.11 a cause du 'mustopen' pour les palettes.
 *	11 Fev 2002: v0.12 a cause du '-ansi' (hein Kerdeuzz, on y vient)
 *	21 Nov 2002: v0.13 a cause du parser de parametres.
 *	14 Jan 2006: v0.14 parce que je remet le truc dans le net.
 *	05 Jan 2007: v0.15 pour etre synchrone avec la doc.
 *	11 Dec 2007: v0.22 pour le parser de parametres (hexadecimal)
 *	05 May 2009: v0.36 grand nettoyage du code en chantier 
 *	27 Dec 2009: v0.42 prise en compte des champs d'altitude.
 *	06 Mar 2010: v0.43 introduction du 'df3 tool'.
 *	27 Jan 2014: V0.52 added : 'parse_rect_param'.
 *	18 Nov 2022: Vx.xx added parse_size_param
 */

#define TGA_OUTILS_COPYLEFT "(dwtfywl) TontonTh 2023"
#define TGA_WWW_SITE "http://la.buvette.org/devel/libimage/"

#define PERR(txt) fprintf(stderr, "\t| %s\n", (txt))
#define PERL fprintf(stderr, "\t+-----------------------------------------\n")

#define NOM_VAR_ENV_VERBOSE "TGA_OUTILS_VERBOSE"      /* flag ON = "yes" */
#define NOM_VAR_ENV_TIMING  "TGA_OUTILS_TIMING"       /* flag ON = "yes" */

int	must_be_verbose(void);
int     set_new_seed(int k);

/*::------------------------------------------------------------------::*/
/*
 *	structure de description d'un mot-clef
 *	--------------------------------------
 *
 *	types des parametres (chaine pointee par ptype)
 *
 *		i	entier 32 bits / char parameter
 *		s	chaine de caracteres
 *		d	double flottant
 */
typedef struct 
	{
	char	*nom;
	int	code;
	char   *ptypes;		/* type des parametres */
	char   *aide;
	/* char	*longhelp; XXX */
	} mot_clef;

#define NB_PARAMS 16		/* max number of allowed parameters */

/*
 *	structure contenant un parametre.
 */
typedef struct
	{
	char	type;
	union
		{
		int	i;
		char	*s;
		double	d;
		} p;
	} Param;

int cherche_mot_clef(char *mot, mot_clef *liste, int *pmode, int *pnbarg);

/* le flag ne sert a rien, mais il faut le mettre a 42 */
int liste_mots_clefs(mot_clef *liste, int flag);

int	get_verbosity(void);
int	set_verbosity(int level);

int	parse_int_param(char *str, int *pval, int k);
int	parse_rect_param(char *str, Image_Rect *prect, int k);
int	parse_size_param(char *str, int *pw, int *ph);

int	parse_parametres(int ac, char *av[], char *types, int prem);
void	print_parametres(void);

int	GIP(int rang);
int	GCP(int rang);
int	GIPdef(int rang, int def);
char *	GSP(int rang);
double	GDP(int rang);
int	GFP(int rang);			/* get flag */

/*::------------------------------------------------------------------::*/
/*
 *	fonctions de bavardage
 */
int dump_command_line(int argc, char *argv[], int force);

/*::------------------------------------------------------------------::*/
/*
 *	pour plus de details, for more informations
 *	http://la.buvette.org/devel/libimage/img-outils.html
 *	----------------------------------------------------
 */

