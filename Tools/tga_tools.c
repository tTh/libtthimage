/*
 *				TGA_TOOLS
 *				=========
 */

#include  <stdio.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
#define		VERSION		1
#define		SURFACE		2
#define		GETDIMS		4
#define		GETDIMX		5
#define		GETDIMPOV	6
#define		GETDIMW		7
#define		WIDTH		8
#define		HEIGHT		9
#define		MK_RGB		10	/* new 22 jan 2014 */
#define		MK_NOISE	11	/* new 07 fev 2014 */
#define		GRMINMAX	17	/* min & max in gray level */
#define		MESSAGE		20
#define		STRUCTS		21
#define		HEADER		33
#define		TIMESTAMP	34
#define		PRHISTO		35
#define		H_GRAD		36
#define		V_GRAD		37
#define		TAG7SEG0	40
#define		BIZARRE		41
#define		ENVIRON		50
/*::------------------------------------------------------------------::*/
mot_clef commandes[] = 
{
{ "version",	VERSION,	"",	"version de la libimage"	},
{ "surface",	SURFACE,	"s",	"number of pixels"		},
{ "getdims",	GETDIMS,	"s",	"dim targa '42 33'"		},
{ "getdimx",	GETDIMX,	"s",	"dim targa '42x33'"		},
{ "getdimpov",	GETDIMPOV,	"s",	"dim au format POV opt"		},
{ "getdimweb",	GETDIMW,	"s",	"dim au format HTML"		},
{ "width",	WIDTH,		"s",	"width of targa"		},
{ "height",	HEIGHT,		"s",	"height of targa"		},
{ "grminmax",	GRMINMAX,	"si",	"image.tga K"			},
{ "message", 	MESSAGE,	"sis",  "tgafname flags 'baratin bla'"	},
{ "structs",	STRUCTS,	"", 	"display sizeof structs"	},
{ "header",	HEADER,		"s",	"filename of image"		},
{ "timestamp",	TIMESTAMP,	"ss",	"filename message"		},
{ "prhisto",	PRHISTO,	"s",	"filename.tga"			},
{ "tag7",	TAG7SEG0,	"si",	"fname.tga nnn"			},
{ "environ",	ENVIRON,	"",	""				},
{ "bizarre",	BIZARRE,	"",	"portnawak..."			},
{ "mk_rgb",	MK_RGB,		"siiiii", "fname w h r g b"		},
{ "mk_hgrad",	H_GRAD,		"sii",	"fname w h"			},
{ NULL,		0,		NULL,	NULL		}
};

/*::------------------------------------------------------------------::*/
int
print_tga_dims(char *filename, int flags)
{
int	foo, width, height;

#if DEBUG_LEVEL
fprintf(stderr, "%s:%d %s ( %s, %d )\n", __FILE__, __LINE__,
			__func__, filename, flags);
#endif
foo = Image_TGA_get_dims(filename, &width, &height);
if (foo) {
	fprintf(stderr, "something is bad with %s : %d\n", filename, foo);
	Image_print_error("explanation", foo);
	return foo;
	}

foo = 0;
switch(flags) {
	case 0:
		printf("%8d %8d\n", width, height);		break;
	case 1:
		printf("%dx%d\n", width, height);		break;
	case 2:
		printf(" -w%d -h%d\n", width, height);		break;
	case 3:
		printf("width=%d height=%d\n", width, height);	break;
	case 4:
		printf("%d\n",  width);				break;
	case 5:
		printf("%d\n",  height);
			break;
	default:
		fprintf(stderr, "bad flag %d in %s\n", flags, __func__);
		foo = BAD_PARAMETER;
		break;			/* unreached ? */
	}
return foo;
}
/*::------------------------------------------------------------------::*/
int showheader(char *filename, int flag)
{
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, "Show Header '%s' %d\n", filename, flag);
#endif

foo = Image_TGA_show_header(filename, flag);

#if DEBUG_LEVEL
fprintf(stderr, "%s got %d\n", __func__, foo);
#endif

return foo;
}
/*::------------------------------------------------------------------::*/
/* new: Mon Sep 18 06:28:51 UTC 2023
	this is just a dummy wraper					*/
int make_a_H_gradient(char *fname, int w, int h)
{
int		foo;


#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %s %d %d )\n", __func__, fname, w, h);
#endif

foo = Image_plot_H_gradient(fname, w, h);
#if DEBUG_LEVEL
fprintf(stderr, "    got a %d\n", foo);
#endif

return foo;
}
/*::------------------------------------------------------------------::*/
int make_a_rgb_tga(char *fname, int w, int h, int r, int g, int b)
{
Image_Desc	*img;
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, "%s -> %s : %dx%d, rgb %d,%d,%d\n", __func__, fname, w, h,
					r, g, b);
#endif

if (w < 1 || h < 1) {
	fprintf(stderr, "%s : img dims %d %d ?\n", __func__, w, h);
	return 666;
	}
img = Image_alloc(w, h, 3);
Image_clear(img, r, g, b);
foo = Image_TGA_save(fname, img, 0);
if (foo) {
	fprintf(stderr, "%s: saving '%s' -> %d\n", __func__, fname, foo);
	return foo;
	}
Image_DeAllocate(img), free(img);

return 666;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
int timestamp(char *fnin, char *fnout, char *txt, int flag)
{
int		foo;
Image_Desc	*src;

#if DEBUG_LEVEL
fprintf(stderr, "timestamping '%s' to '%s' with '%s'\n", fnin, fnout, txt);
#endif

if ( (src=Image_TGA_alloc_load(fnin)) == NULL )	{
	fprintf(stderr, "tga_tools, load of '%s' failed in %s\n",
					fnin, __func__);
	exit(5);
	}

foo = Image_marque_timestamp(src, txt, NULL, 0);
#if DEBUG_LEVEL
if (foo)
	Image_print_error("marque timestamp --> ", foo);
#endif

Image_TGA_save(fnout, src, 0);

return foo;
}
/*::------------------------------------------------------------------::*/
int marque_message(char *fnin, char *fnout, char *txt, int flag)
{
int		foo, posx, posy;
Image_Desc	*src;
RGBA            ink, pap;

/* some sane default values */
ink.r = 255, ink.g = 255, ink.b = 100;          ink.a = 255;
pap.r =  30, pap.g =  30, pap.b = 155;          pap.a = 255;
posx = posy = 6;
if (flag) {
	ink.r = 200, ink.g = 200, ink.b = 200;          ink.a = 255;
	pap.r =   0, pap.g =   0, pap.b =   0;          pap.a = 127;
	posx = posy = 0;
	}

if (must_be_verbose()) {
	fprintf(stderr, "%s:\n  '%s' to '%s' with '%s' flg %d\n",	\
			 __func__, fnin, fnout, txt, flag);
	}

if ( (src=Image_TGA_alloc_load(fnin)) == NULL ) {
	fprintf(stderr, "tga_tools, load of '%s' failed in %s\n",
					fnin, __func__);
	exit(5);
	}

foo = Image_txt1_box_0(src, txt, posx, posy, 4, &pap, &ink, 0);
if (foo) {
	fprintf(stderr, "err txt1_box_0 = %d\n", foo);
	return foo;
	}

foo = Image_TGA_save(fnout, src, 0);
if (foo) {
	fprintf(stderr, "%s: err TGA save = %d\n", __func__, foo);
	return foo;
	}

#if DEBUG_LEVEL
fprintf(stderr, "%s : saving %s\n", __func__, fnout);
#endif

return foo;
}
/*::------------------------------------------------------------------::*/
int print_histogramme(char *filename, int flags)
{
Image_Desc	*src;
long		hr[256], hg[256], hb[256];
int		foo;

if (flags) {
	fprintf(stderr, "in %s, flags must be 0\n", __func__);
	}

if ( (src=Image_TGA_alloc_load(filename)) == NULL ) {
	fprintf(stderr, "tga_tools, chargement '%s' failed\n", filename);
	exit(5);
	}
foo = Image_histo_RGB(src, hr, hb, hg);
if (foo) {
	fprintf(stderr, "tga_tools, calcul histogramme: %d  %s\n",
						foo, Image_err2str(foo));
	exit(5);
	}

for (foo=0; foo<256; foo++) {
	printf("%3d   %8ld   %8ld   %8ld\n", foo, hr[foo], hg[foo], hb[foo]);
	}

return 0;
}
/*::------------------------------------------------------------------::*/
/* new 27 mars 2008 - avenue St Exupery */
int print_environ(void)
{
int	foo;
char	*ptr;
char	*noms[] = {
	NOM_VAR_ENV_VERBOSE, NOM_VAR_ENV_TIMING, "LIBIMAGE_PATH", 
	"TTH_CONF_DIR", "LIBIMAGE_DEBUG", "BITMAP_FONTS",
	"FIXED_SEED", ENV_DEFAULT_RGBA
	};

for (foo=0; foo<(int)(sizeof(noms)/sizeof(char *)); foo++) {
	ptr = getenv(noms[foo]);
	printf("%5d    %-25s  %s\n", foo, noms[foo], ptr);
	}

return OLL_KORRECT;		/* ? seriously ? */
}
/*::------------------------------------------------------------------::*/
int tag_7_segments_0(char *fname, int value)
{
int		foo;
Image_Desc	*src;
char		buffer[100];

#if DEBUG_LEVEL
fprintf(stderr, "%s:%s %s %d\n", __FILE__, __func__, fname, value);
#endif

if ( (src=Image_TGA_alloc_load(fname)) == NULL ) {
	fprintf(stderr, "tga_tools, load of '%s' failed in %s\n",
					fname, __func__);
	exit(5);
	}

sprintf(buffer, "%04d", value);
foo = Image_7seg_tag0(src, buffer, 0);
#if DEBUG_LEVEL
fprintf(stderr, "%s : tagging %s -> %d\n", __func__, fname, foo);
#endif
if (foo) {
	fprintf(stderr, "ERR %d IN %s\n", foo, __func__);
	abort();
	}

foo = Image_TGA_save(fname, src, 0);
#if DEBUG_LEVEL > 1
fprintf(stderr, "%s : saving %s -> %d\n", __func__, fname, foo);
#endif

return -1;
}
/*::------------------------------------------------------------------::*/
int bizarre(char *fname, int notused)
{

fprintf(stderr, ">>> %s ( %p %d )\n", __func__, fname, notused);

if (notused) {
	fprintf(stderr, "in %s notused must be zero, was %d\n", 
			    __func__,			 notused);
	return INVALID_PARAM;
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
int main (int argc, char *argv[])
{
int		foo;
int		idx, mode, nbarg;
char		* cptr;

dump_command_line(argc, argv, 0);

/* new 4 feb 2014 */
foo = set_new_seed(42);
#if DEBUG_LEVEL > 1
fprintf(stderr, "!!! set new seed -> %d\n", foo);
#endif

if (1 == argc) {
	fprintf(stderr, "* tga_tools v 0.1.33 (%s) *\n", __DATE__);
	fprintf(stderr, "usage:\n\t%s action f.tga [params]\n", argv[0]);
	liste_mots_clefs(commandes, 42);
	exit(0);
	}

if (-1 == (idx=cherche_mot_clef(argv[1], commandes, &mode, &nbarg)) ) {
	fprintf(stderr, "action '%s' inconnue.\n", argv[1]);
	exit(1);
	}
#if DEBUG_LEVEL
fprintf(stderr, "%s commande: idx=%d mode=%d nbarg=%d\n",
			argv[0], idx, mode, nbarg);
#endif

#define FIRST_PARAM	2
foo = parse_parametres(argc, argv, commandes[idx].ptypes, FIRST_PARAM);

switch(mode)
	{
	case VERSION:
		printf("Version des outils: %s  ", TGA_OUTILS_VERSION);
		printf("-  %s,  %s\n", __DATE__, __TIME__);
		printf("%s is the swiss knife for Targa files\n", argv[0]);
		Image_print_version(2);
		break;
	case GETDIMS:
		cptr = GSP(0);
#if DEBUG_LEVEL
		fprintf(stderr, "GETDIMS %s\n", cptr);
#endif
		foo = print_tga_dims(cptr, 0);
		break;
	case GETDIMX:
		cptr = GSP(0);
		foo = print_tga_dims(cptr, 1);
		break;
	case GETDIMPOV:
		cptr = GSP(0);
		foo = print_tga_dims(cptr, 2);
		break;
	case GETDIMW:
		cptr = GSP(0);
		foo = print_tga_dims(cptr, 3);
		break;
	case WIDTH:
		foo = print_tga_dims(GSP(0), 4);
		break;
	case HEIGHT:
		foo = print_tga_dims(GSP(0), 5);
		break;

	case HEADER:
		foo = showheader(GSP(0), 0);
		break;

	case PRHISTO:
		foo = print_histogramme(GSP(0), 0);
		break;
	case TIMESTAMP:
		cptr = GSP(0);
		foo = timestamp(cptr, GSP(0), GSP(1), 0);
		break;
	case MK_RGB:
		cptr = GSP(0);
		fprintf(stderr, "MK_RGB -> %s\n", cptr);
		foo = make_a_rgb_tga(cptr, GIP(1), GIP(2),
				GIP(3), GIP(4), GIP(5));
		break;
	case H_GRAD:
		cptr = GSP(0);
		fprintf(stderr, "MK_HGRAD -> %s\n", cptr);
		foo = make_a_H_gradient(cptr, GIP(1), GIP(2));
		break;
	case MK_NOISE:
		cptr = GSP(0);
		fprintf(stderr, "MK_NOISE -> %s\n", cptr);
		foo = make_a_rgb_tga(cptr, GIP(1), GIP(2),
				GIP(3), GIP(4), GIP(5));
		break;
	case TAG7SEG0:
		foo = tag_7_segments_0(GSP(0), GIP(1));
		break;

	case MESSAGE:
		cptr = GSP(0);
#if DEBUG_LEVEL
		fprintf(stderr, "marque message %p\n", cptr);
#endif
		foo = marque_message(cptr, GSP(0), GSP(2), GIP(1));
#if DEBUG_LEVEL
		fprintf(stderr, "marque message -> %d\n\n", foo);
#endif
		break;
	case STRUCTS:
#if DEBUG_LEVEL
		fprintf(stderr, "going to 'Image_print_sizeof_structs'\n");
#endif
		Image_print_sizeof_structs(NULL);
		break;
	case ENVIRON:
		foo = print_environ();
		break;
	default:
		fprintf(stderr, "Hu ho %d ?\n", mode);
		foo = FULL_NUCKED;
		break;
	}

if (foo) {
	/* fprintf(stderr, "*** at end of %s, foo was %d\n", __func__, foo); */
	}
return 0;
}
/*::------------------------------------------------------------------::*/
