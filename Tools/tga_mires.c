/*
		FABRIQUEUR DE MIRES
		===================

	 http://krabulator.free.fr/devel/libimage.html
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
#define		MIRCOL0			2
#define		MIRCOL1			3
#define		MIRCOL2			4
#define		MIRCOL3			5
#define		MIRCOL4			6
#define		DECOMP			10
#define		RGB0			14
#define		RGBSTRIP		15	/* mmmm, c'est quoi ? */
#define		GRAY0			16
#define		FNT16X24		20

mot_clef mots_clef[] =
{
{ "mircol0",	MIRCOL0,	"s",	"free text"		},
{ "mircol1",	MIRCOL1,	"s",	"texte libre"		},
{ "mircol2",	MIRCOL2,	"iis",	"dims & free text"	},
{ "mircol3",	MIRCOL3,	"iiis",	"dims, flag & free text" },
{ "mircol4",	MIRCOL4,	"iiis",	"dims, flag & free text" },
{ "decomp",	DECOMP,		"",	"mysterious operation"	},
{ "rgb0",	RGB0,		"",	""			},
{ "gray0",	GRAY0,		"i",	"param: height"		},
{ "fnt16x24",	FNT16X24,	"s",	"param: font name"	},
{ NULL,		0,		NULL,	NULL			}
};

/*::------------------------------------------------------------------::*/
void usage(int flag)
{
fprintf(stderr, "* tga_mires v 0.0.24 [%s]  (dwtfywl 2015) tonton Th\n",
					TGA_OUTILS_VERSION);
fprintf(stderr, "usage:\n\ttga_mires type image.tga [params]\n");
if (flag)
	liste_mots_clefs(mots_clef, 42);
else
	Image_print_version(0);
exit(5);
}
/*::------------------------------------------------------------------::*/
/* code selon ma methode de la rache le 25 avril 2007 */
int
mire_grise(Image_Desc *img)
{
int	x, y;

if (256 != img->width)		abort();

for (x=0; x<256; x++)
	for (y=0; y<img->height; y++)
		Image_plotRGB(img, x, y, x, x, x);

return 0;
}
/*::------------------------------------------------------------------::*/
int do_mircol_2(Image_Desc *img, char *texte)
{
int	foo;
foo = Image_mircol_2(img, texte, 0);
return foo;
}
/*::------------------------------------------------------------------::*/
int do_mircol_3(Image_Desc *img, char *texte, int mode)
{
int	foo;
fprintf(stderr, "%s : mode is %d\n", __func__, mode);
foo = Image_mircol_3(img, texte, mode);
return foo;
}
/*::------------------------------------------------------------------::*/
int do_mircol_4(Image_Desc *img, char *texte, int mode)
{
int	foo;
fprintf(stderr, "%s : mode is %d\n", __func__, mode);
foo = Image_mircol_4(img, texte, mode);
return foo;
}
/*::------------------------------------------------------------------::*/
int main (int argc, char *argv[])
{
Image_Desc	*dst;
int		foo;
int		commande, nbargs, idx;
char		*cptr, *argument;
int		width, height;

dump_command_line(argc, argv, 0);

if ( argc==2 && !strcmp(argv[1], "list") ) usage(1);
if ( argc < 3)	usage(0);

idx = cherche_mot_clef(argv[1], mots_clef, &commande, &nbargs);
if (idx < 0)
	{
	fprintf(stderr, "tga_mires: mot-clef %s inconnu...\n", argv[1]);
	exit(5);
	}
#if DEBUG_LEVEL
fprintf(stderr, "idx %d commande %d nbargs %d argc %d\n",
					idx, commande, nbargs, argc);
#endif

foo = parse_parametres(argc, argv, mots_clef[idx].ptypes, 3);
#if DEBUG_LEVEL
print_parametres();
#endif

dst = NULL;

switch (commande)
	{
	case MIRCOL0:
		dst = Image_alloc(512, 512, 3);
		foo = Image_mircol_0(dst, GSP(0), 0);
		break;		
	case MIRCOL1:
		dst = Image_alloc(512, 512, 3);
		foo = Image_mircol_1(dst, GSP(0), 0);
		break;
	case MIRCOL2:
		width = GIP(0);
		height = GIP(1);
		fprintf(stderr, "mircol2 sur %dx%d\n", width, height);
		dst = Image_alloc(width, height, 3);
		foo = do_mircol_2(dst, GSP(2));
		break;		
	case MIRCOL3:
		width = GIP(0);
		height = GIP(1);
#if DEBUG_LEVEL
		fprintf(stderr, "mircol3 sur %dx%d\n", width, height);
#endif
		dst = Image_alloc(width, height, 3);
		foo = do_mircol_3(dst, GSP(3), GIP(2));
		break;		
	case MIRCOL4:
		width = GIP(0);
		height = GIP(1);
		fprintf(stderr, "mircol4 sur %dx%d\n", width, height);
		dst = Image_alloc(width, height, 3);
		foo = do_mircol_3(dst, GSP(3), GIP(2));
		break;		
	case RGB0:
		dst = Image_alloc(256, 256, 3);
		foo = Image_mirRGB_0(dst, 0);
		break;
	case GRAY0:
		foo = GIP(0);
		fprintf(stderr, "hauteur = %d\n", foo);
		dst = Image_alloc(256, foo, 3);
		foo = mire_grise(dst);
		break;
	case FNT16X24:
#if DEBUG_LEVEL
		fprintf(stderr, "generation image fonte 16x24\n");
#endif
		cptr = argv[2];
		argument = GSP(0);
		fprintf(stderr, "image fonte 16x24 %s -> %s\n",
						cptr, argument);
		foo = Image_t16x24_chars_map(cptr, argument, 0);
		break;
	default:
		fprintf(stderr, "method not implemented\n");
		foo=-1;
		break;
	}

if (foo)
	fprintf(stderr, "retour = %d, %s\n", foo, Image_err2str(foo));

if (dst != NULL)
	{
	foo = Image_TGA_save(argv[2], dst, 0);
	Image_DeAllocate(dst);
	}

#if DEBUG_LEVEL
puts("");
#endif

return 0;
}
/*::------------------------------------------------------------------::*/
