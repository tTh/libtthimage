/*
 # #    #  ####  #####  #    #  ####  #####   ##   ##### #  ####  #    # 
 # ##   # #    # #    # #    # #        #    #  #    #   # #    # ##   # 
 # # #  # #      #    # #    #  ####    #   #    #   #   # #    # # #  # 
 # #  # # #      #####  #    #      #   #   ######   #   # #    # #  # # 
 # #   ## #    # #   #  #    # #    #   #   #    #   #   # #    # #   ## 
 # #    #  ####  #    #  ####   ####    #   #    #   #   #  ####  #    # 
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/

#define		ICRU_POKE	2
#define		ICRU_POKEMIX	3
#define		ICRU_POKEALPHA	4
#define		ICRU_POKESCALE	10
#define		ICRU_RGBXOR	20
#define		ICRU_RECT	30
#define		ICRU_RECT2	32

mot_clef mots_clef[] =
{
{ "poke",	ICRU_POKE,	"ii",	"poke it hardly"		},
{ "pokemix",	ICRU_POKEMIX,	"iii",	"ponderate it"			},
{ "pokealpha",	ICRU_POKEALPHA,	"ii",	"use the alpha channel"		},
{ "pokescale",	ICRU_POKESCALE,	"ii%",	"scale before poking"		},
{ "rgbxor",	ICRU_RGBXOR,	"ii",	"exclusive OR of poked pix"	},
{ "rect",	ICRU_RECT,	"iir",	"only a part of 'what'"		},
{ "rect2",	ICRU_RECT2,	"iir",	"only a part of 'what'"		},
{ NULL,		0,		"WTF",	NULL				},
};

/*::------------------------------------------------------------------::*/
int usage(int flag)
{
fprintf(stderr, "* TGA incrustator  v 0.1.3 [%s] %s \n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);
fprintf(stderr, "  compiled %s at %s\n", __DATE__, __TIME__);

fprintf(stderr, "usage:\n\ttga_incrust <orig.tga> <insert.tga> MODE <out.tga>\n");

if (flag) {
	Image_print_version(0);
	liste_mots_clefs(mots_clef, 42);
	}

exit(0);
}

/*::------------------------------------------------------------------::*/
/*
 *	argv[1] 	image originale
 *	argv[2] 	image a incruster
 *	argv[3] 	type d'incrustation
 *	argv[4] 	image destination
*/                                                                    
#define FIRST_PARAM	5

int main(int argc, char *argv[])
{
Image_Desc	*where, *what;
int		foo, idx;
int		xpos, ypos;
int		commande, nbargs;

dump_command_line(argc, argv, 0);

if (argc==2 && !strcmp(argv[1], "-?"))		usage(0);
if (argc==2 && !strcmp(argv[1], "list"))	usage(1);
if (argc < 4) usage(1);

#if DEBUG_LEVEL
if (must_be_verbose())
	{
	fprintf(stderr, "********************************\n");
	fprintf(stderr, "*   EXPERIMENTAL CORE DUMPER   *\n");
	fprintf(stderr, "********************************\n");
	}
#endif

idx = cherche_mot_clef(argv[3], mots_clef, &commande, &nbargs);
if (idx < 0) {
	fprintf(stderr, "tga_incrust: mot-clef '%s' inconnu...\n", argv[3]);
	exit (5);
	}
if ( (argc-nbargs) != FIRST_PARAM ) {
	fprintf(stderr, "%s: bad number of parameters\n", argv[0]);
#if DEBUG_LEVEL
	fprintf(stderr, "argc=%d nbargs=%d FIRST_PARAM=%d\n",
				argc, nbargs, FIRST_PARAM);
#endif
	exit(5);
	}

/* analyse des parametres */
idx = 0;
foo = parse_parametres(argc, argv, mots_clef[idx].ptypes, FIRST_PARAM);
#if DEBUG_LEVEL
fprintf(stderr, "parse params -> %d\n", foo);
#endif

if (NULL==(where=Image_TGA_alloc_load(argv[1]))) {
	fprintf(stderr, "can't load %s\n", argv[1]);
	exit(2);
	}

if (NULL==(what=Image_TGA_alloc_load(argv[2]))) {
	fprintf(stderr, "can't load %s\n", argv[2]);
	exit(2);
	}

switch (commande)
	{
	case ICRU_POKE:
		xpos = GIP(0);		ypos = GIP(1);
		foo = Image_overlay(what, where, xpos, ypos);
		if (foo) fprintf(stderr, "overlay -> %d\n", foo);
		break;

	default:
		fprintf(stderr, "%d command not implemented\n", commande);
		exit(1);
	}

Image_DeAllocate(what);			free(what);

foo = Image_TGA_save(argv[4], where, 0);
if (foo) {
	fprintf(stderr, "save -> %d\n", foo);
	}
Image_DeAllocate(where);		free(where);

return 0;
}
