/*
       _____    _    __     ___     _             
      |_   _|__| | __\ \   / (_)___(_) ___  _ __  
        | |/ _ \ |/ _ \ \ / /| / __| |/ _ \| '_ \ 
        | |  __/ |  __/\ V / | \__ \ | (_) | | | |
        |_|\___|_|\___| \_/  |_|___/_|\___/|_| |_|
                                            
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
#define TV_GRINK	2
#define TV_GROK		4
#define TV_GRUUD	6
#define TV_GRIIIZ	8
#define TV_PIX0		10
#define TV_PIX1		11
#define TV_MINITEL	12
#define TV_OLD		20
#define TV_HISTORY	21		/* wtf ? */
#define TV_TRILIGNE	22
#define TV_PROTO	30

mot_clef mots_clef[] =
{
{ "grink",	TV_GRINK,	"i",	"colors are bipolars"		},
{ "grok",	TV_GROK,	"i",	"..."				},
{ "gruud",	TV_GRUUD,	"i",	"..."				},
{ "griiiz",	TV_GRIIIZ,	"i",	"parameter must be 0"		},
{ "oldstyle",	TV_OLD,		"iii",  "..."				},
{ "pix0",	TV_PIX0,	"iii",	"func not finished"		},
{ "pix1",	TV_PIX1,	"iii",	"func not finished"		},
{ "triligne",	TV_TRILIGNE,	"f",	"flag : rand or modulo"		},
{ "cplus0",	TV_PROTO,	"i",    "premier cplus"			},
{ "proto",	TV_PROTO,	"i",	"prototype canal+"		}
};
/*::------------------------------------------------------------------::*/
void usage(int flag)
{
fprintf(stderr, "* tga_television v 0.0.25 [%s] %s\n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);
fprintf(stderr, "  compiled %s at %s\n", __DATE__, __TIME__);
fprintf(stderr, "Usage:\n\ttga_television <src.tga> method <dst.tga> [params]\n");
Image_print_version(0);
if (flag) { liste_mots_clefs(mots_clef, 42); }

exit(5);
}
/*::------------------------------------------------------------------::*/

int main(int argc, char *argv[])
{
Image_Desc	*src, *dst;
int		foo, mode;
int		par[NB_PARAMS], nb_int_par, value;
int		commande, nbargs;

dump_command_line(argc, argv, 0);
if (argc==2 && !strcmp(argv[1], "-?"))		usage(0);
if (argc==2 && !strcmp(argv[1], "list"))	usage(1);
if (argc < 4) usage(0);

srand(getpid());

if ( (src=Image_TGA_alloc_load(argv[1])) == NULL )
	{
	fprintf(stderr, "can't load image file '%s'\n", argv[1]);
	exit(1);
	}

if ( (dst=Image_clone(src, 0))==NULL )
	{
	fprintf(stderr, "can't clone %p, exiting...\n", src);
	exit(2);
	}

/*
 *	rechercher le mot clef dans la table
 */
mode = cherche_mot_clef(argv[2], mots_clef, &commande, &nbargs);
if (mode < 0)
	{
	fprintf(stderr, "%s : mot-clef %s inconnu\n", argv[0], argv[2]);
	exit(5);
	}

/*
 *	decoder les arguments
 */
for (foo=0; foo<NB_PARAMS; foo++)	par[foo]=0;
nb_int_par = 0;
for (foo=4; foo<argc; foo++)
	{
	if ( sscanf(argv[foo], "%d", &value)==1 )
		{
		par[foo-4] = value;
		nb_int_par++;
		}
	}

switch (commande)
	{
	case TV_GRINK:
		/* third parameter is a boolean */
		foo = Image_TV_grink(src, dst, par[0]);
		break;

	case TV_GROK:
		foo = Image_TV_grok(src, dst, par[0]);
		break;

	case TV_GRUUD:
		foo = Image_TV_gruud(src, dst, par[0]);
		break;

	case TV_GRIIIZ:
		foo = Image_TV_griiiz(src, dst, par[0]);
		break;

	case TV_OLD:
		foo = Image_TV_old(src, dst, par[0], par[1], par[2]);
		break;

	case TV_PIX0:
		foo = Image_TV_pix_0(src, dst, par[0], par[1], par[2]);
		break;

	case TV_PIX1:
		foo = Image_TV_pix_1(src, dst, par[0], par[1], par[2]);
		break;

	case TV_TRILIGNE:
		foo = Image_TV_triligne(src, dst, par[0]);
		break;

	case TV_PROTO:		/* effet porno canal+ */
		foo = Image_TV_cplus_v1(src, dst, par[0]);
		break;


	default: 
		fprintf(stderr, "commande %d inconnue\n", commande);
		break;
	}

if (foo && must_be_verbose())
	{
	fprintf(stderr, "%s : process error : %s\n",
			 argv[0], Image_err2str(foo));
	}

foo = Image_TGA_save(argv[3], dst, 0);
if (foo)
	{
	fprintf(stderr, "%s: TGA_save: err %d %s\n",
			argv[0], foo, Image_err2str(foo));
	exit(1);
	}

exit (0);
}
/*::------------------------------------------------------------------::*/
