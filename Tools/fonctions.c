/*
		T G A _ O U T I L S
		-------------------

	fonctions communes a tous les outils - new 11 juin 2002

*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

/* non portable but useful function */
char *strdup(const char *s);

#define IN_FONCTION
#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/

int cherche_mot_clef(char *mot, mot_clef *liste, int *pmode, int *pnbarg)
{
int		idx = 0;

idx = 0;
while (liste[idx].nom != NULL)
	{
#if DEBUG_LEVEL > 1
	fprintf(stderr, "check %12s  %8s\n",	liste[idx].nom,
						liste[idx].ptypes);
#endif
	if ( ! strcmp(liste[idx].nom, mot) )
		{
		*pmode = liste[idx].code;
		*pnbarg = strlen(liste[idx].ptypes);	
#if DEBUG_LEVEL > 1
		fprintf(stderr, "found! mode %d nbpar %d\n", *pmode, *pnbarg);
		sleep(1);
#endif
		return idx;
		}
	idx++;
	}
#if DEBUG_LEVEL > 1
fprintf(stderr, "%s : %s not found\n", __func__, mot);
#endif

return -1;
}
/*::------------------------------------------------------------------::*/
int liste_mots_clefs(mot_clef *liste, int flag)
{
mot_clef	*pmc;

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s ( %p %d )\n", __func__, liste, flag);
#endif

if (42 != flag) {
	fprintf(stderr, "UH ? flag is not the universal answer ?\n");
	}

pmc = liste;

printf("  commande  | type arg |  explication\n");
printf("------------+----------+------------------------------------\n");
while (pmc->nom != NULL)
	{
	if (pmc->aide != NULL)
		printf(" %-10s | %-8s |  %s\n",
			pmc->nom, pmc->ptypes, pmc->aide);
	pmc++;
	}
printf("------------+----------+------------------------------------\n");

return 0;
}
/*::------------------------------------------------------------------::*/
/*
 * INPUT	str	string to scan for an integer
 * 		pval	pointer to the return value
 * 		k	* not used *
 *
 * OUTPUT	1	successfuly make a conversion
 * 		0	strange error occured :(
 */
int	parse_int_param(char *str, int *pval, int k)
{
long	lfoo;
int	val;

if (k)	fprintf(stderr, "%s: k %d\n", __func__, k);

val = (int)(lfoo = strtol(str, NULL, 0));

#if DEBUG_LEVEL
fprintf(stderr, "%s: %s: '%s'  ->  %ld  ->  %d\n",
		__FILE__, __func__, str, lfoo, val);
#endif

*pval = val;

return 1;
}

/*::------------------------------------------------------------------::*/
/*							Thu 17 Nov 2022
 *	input           str      a string like '640x480'
 *			pw, ph   pointers to result
 *
 *	output          0 on ok, INVALID_PARAM on error
 */
int parse_size_param(char *str, int *pw, int *ph)
{
int		tw, th, foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s'  %p  %p )\n", __func__, 
						str, pw, ph);
#endif

foo = sscanf(str, "%dx%d", &tw, &th);
if (2 != foo) {
	fprintf(stderr, "%s: fail %d on '%s'\n", __func__, foo, str);
	return INVALID_PARAM;
	}
/* fprintf(stderr, "   %d  %d\n", tw, th); */
*pw = tw;	*ph = th;
return 0;
}
/*::------------------------------------------------------------------::*/
/*	input:		str	string to scan for four csv int
			prect	rectangle struct who need datas
			k	* not used *
	output		1	all correct
			2	invalid string
27 janvier 2014
*/
int	parse_rect_param(char *str, Image_Rect *prect, int k)
{
char		*cptr;
int		idx, foo, val;

#if DEBUG_LEVEL
fprintf(stderr, "%s '%s' to %p\n", __func__, str, prect);
#endif

if (k) fprintf(stderr, "%s : k is %d, but must be 0\n", __func__, k);

memset(prect, 0, sizeof(Image_Rect));

cptr = strdup(str);
#if DEBUG_LEVEL
fprintf(stderr, "copie dans cptr %p\n", cptr);
#endif

idx = 0;
cptr = strtok(cptr, ",");
while (NULL != cptr)	{
	fprintf(stderr, "%3d %p = '%s'\n", idx, cptr, cptr);
	foo = sscanf(cptr, "%d", &val);
	if (1 != foo) {
		fprintf(stderr, "%s: err sscanf on '%s'\n", __func__, cptr);
		exit(1);
		}
	/*
	 *   no error check ?
	 */
	fprintf(stderr, "val = %d\n", val);
	switch (idx) {
		case 0:		prect->x = val;		break;
		case 1:		prect->y = val;		break;
		case 2:		prect->w = val;		break;
		case 3:		prect->h = val;		break;
		}
	cptr = strtok(NULL, ",");
	idx++;
	}

free(cptr);

return 0;
}
/*::------------------------------------------------------------------::*/

static Param		params[NB_PARAMS];

int parse_parametres(int argc, char *argv[], char *types, int prem)
{
int	foo, idxt;
int	entier;
double	flottant;
char	*cptr;

#if DEBUG_LEVEL 
fprintf(stderr, "Parse params: types '%s' debut %d\n", types, prem);
#endif

/*
 *	init de la table des paramètres.
 */
for (foo=0; foo<NB_PARAMS; foo++) {
	params[foo].type = '?';
	}

for (foo=prem, idxt=0; foo<argc; foo++, idxt++)
	{
#if DEBUG_LEVEL
	fprintf(stderr, "\t%d/%d\t%c\t%s\n", foo, idxt, types[idxt], argv[foo]);
#endif
	switch (types[idxt])
	{
	case 'c':
		if (strlen(argv[foo])!=1)
			{
			fprintf(stderr, "bad length of char param\n");
			}
		else
			{
#if DEBUG_LEVEL
			fprintf(stderr, "char param = %d\n", *argv[foo]);
#endif
			params[idxt].type = 'c';
			params[idxt].p.i = *argv[foo];
			}
		break;
	case 'i':
		/*
 		 * En fait, ici, il serait bon de parser aussi les
 		 * parametres donnes en hexadecimal.
 		 */
		if (parse_int_param(argv[foo], &entier, 0)==1)
			{
			params[idxt].type = 'i';
			params[idxt].p.i = entier;
			}
		else
			{
			fprintf(stderr, "ERR parse int param %s\n", argv[foo]);
			exit(5);
			}
		break;

	case 's':
#if DEBUG_LEVEL
		fprintf(stderr, "%s: case 's' : %p\n", __func__, argv[foo]);
#endif
		if ((cptr=strdup(argv[foo]))!=NULL)	
			{
			params[idxt].type = 's';
			params[idxt].p.s = cptr;
			}
		else
			{
			fprintf(stderr, "ERR parse param %s\n", argv[foo]);
			exit(5);
			}
		break;

	case 'd':
		/* the 'd' is for 'double precision', not for 'decimal' */
		if (1 == sscanf(argv[foo], "%lf", &flottant))
			{
			params[idxt].type = 'd';
			params[idxt].p.d = flottant;
#if DEBUG_LEVEL
			fprintf(stderr, "at foo=%d, val is %f %g\n",
						foo,	flottant, flottant);
#endif
			}
		else
			{
			fprintf(stderr,"ERR parse float param %s\n",argv[foo]);
			exit(5);
			}
		break;

	case 'f':
		/* the 'f' is for 'flag', not for 'float' */
		switch(argv[foo][0])
			{
			case '0': case 'f': case 'F': case 'n': case 'N':
				params[idxt].type = 'f';
				params[idxt].p.i = 0;
				break;
			case '1': case 't': case 'T': case 'y': case 'Y':
				params[idxt].type = 'f';
				params[idxt].p.i = 1;
				break;
			default:
				fprintf(stderr, "bad flag %s\n", argv[foo]);
				exit(5);
			}
		break;

	case 'R':
		fprintf(stderr, "'Rect' parser not implemented\n");
		exit(6);

	default:
		fprintf(stderr, "partype #%d '%c' invalid\n",
					idxt, types[idxt]);
		exit(5);
		}
	}

return 0;
}
/*::------------------------------------------------------------------::*/
/*	affichage de la liste des parametres trouves			*/

void print_parametres(void)
{
int	foo;
printf("   +------+-----+\n");
printf("   | parameters |\n");
printf("   +------+-----+\n");
for (foo=0; foo<NB_PARAMS; foo++)
	{
	printf("   |  %2d  |  %c  |\n", foo, params[foo].type);
	}
printf("   +------+-----+\n");
fflush(stdout);
}

/*::------------------------------------------------------------------::*/
/*
 *	fonctions de récuperation d'un paramètre.
 */

int	GIP(int rang)
{
if (rang<0 || params[rang].type!='i') {
	fprintf(stderr, "erreur GIP %d\n", rang);
	exit(5);
	}
return params[rang].p.i;
}

int	GCP(int rang)
{
if (rang<0 || params[rang].type!='c') {
	fprintf(stderr, "erreur GCP %d\n", rang);
	exit(5);
	}
return params[rang].p.i;
}

/* get an 'int' parameter with a default value */
int	GIPdef(int rang, int def)
{
#if DEBUG_LEVEL
fprintf(stderr, "Warning !   %s is not tested\n", __func__);
#endif
if (rang<0 || params[rang].type!='i') {
	return def;
	}
return params[rang].p.i;
}

char *	GSP(int rang)
{
#if DEBUG_LEVEL > 1
fprintf(stderr, "GetStringParameter(%d)\n", rang);
#endif
if (rang<0 || params[rang].type!='s') {
	fprintf(stderr, "erreur GSP %d\n", rang);
	exit(5);
	}
#if DEBUG_LEVEL > 1
fprintf(stderr, "GetStringParameter -> %s\n", params[rang].p.s);
#endif
return params[rang].p.s;
}

double	GDP(int rang)
{
if (rang<0 || params[rang].type!='d') {
	fprintf(stderr, "erreur GDP %d\n", rang);
	exit(5);
	}
return params[rang].p.d;
}

int	GFP(int rang)
{
if (rang<0 || params[rang].type!='f') {
	fprintf(stderr, "erreur GFP %d\n", rang);
	exit(5);
	}
return params[rang].p.i;
}

/*::------------------------------------------------------------------::*/
static int verboselevel;

int must_be_verbose(void)
{
char	*envvar;
envvar = getenv(NOM_VAR_ENV_VERBOSE);
if ((envvar!=NULL) && !strcmp(envvar, "yes"))
	{
#if DEBUG_LEVEL > 1
	fprintf(stderr, "HA HA HA ! pid:%d MUST BE VERBOSE !\n", getpid());
#endif
	return 1;
	}
return 0;
}

/*		new Fri 18 Nov 2022 11:48:06 PM CET			*/

int set_verbosity(int level)
{
int	tmplevel;

tmplevel = verboselevel;
verboselevel = level;

return tmplevel;
}
int get_verbosity(void)
{
return verboselevel;
}
/*::------------------------------------------------------------------::*/
int dump_command_line(int argc, char *argv[], int force)
{
char	*envvar;
int	flag=0, foo;

#if DEBUG_LEVEL
fprintf(stderr, "%s : %s, argc=%d\n", __func__, argv[0], argc);
#endif

envvar = getenv(NOM_VAR_ENV_VERBOSE);
if ( (envvar!=NULL) && !strcmp(envvar, "yes") )		flag = 1;

if (flag || force) {
	fprintf(stderr, "TGA(%d) %d arg:", getpid(), argc);
	for (foo=0; foo<argc; foo++)
		fprintf(stderr, " %s", argv[foo]);
	fprintf(stderr, "\n");
	}
return flag;
}
/*::------------------------------------------------------------------::*/
int	set_new_seed(int k)
{
char		*ptr;
long		seed;

if (NULL==(ptr=getenv("FIXED_SEED"))) {
	/* no fixed seed in context, doing semi-random */
	srand(getpid() + k);
#if DEBUG_LEVEL
	fprintf(stderr, "no FIXED_SEED, first rand is number : %d\n", rand());
#endif
	}
else	{
	/* try to parse the env var */
	seed = strtol(ptr, NULL, 0);
	srand(seed);
	}
	
return rand();
}
/*::------------------------------------------------------------------::*/
