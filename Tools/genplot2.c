/*
	Bon, c'est tres bien tout c,a mais il faudrait
	faire une page de man pour expliquer comment 
	ce machin fonctionne...
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <getopt.h>

#include "tga_outils.h"

#define XMIN 0
#define YMIN 0
#define XMAX 800		/* constant from a 'dddd' */
#define YMAX 600		/* constant from a 'dddd' */

/*::------------------------------------------------------------------::*/
static 	Image_Desc	*image;
static	RGB_map		map;
static	int		curX, curY;
static  int		verbeux;
/*::------------------------------------------------------------------::*/
int initgr(int largeur, int hauteur)
{
int foo, r, v, b, dummy;

if (NULL==(image=Image_alloc(largeur, hauteur, 3))) {
	fprintf(stderr, "hu hu, 'man addswap' :)\n");
	exit(3);
	}

if (verbeux) fprintf(stderr, "Genplot2: initgr  %d  %d\n", largeur, hauteur);
for (foo=0; foo<8; foo++) {
#if DEBUG_LEVEL
	printf("\tPal(%d) = ", foo);
#endif
	r = foo & 1 ? 255 : 0;
	v = foo & 2 ? 255 : 0;
	b = foo & 4 ? 255 : 0;
#if DEBUG_LEVEL
	printf("%02X %02X %02X\n", r, v, b);
#endif
	map.red[foo] = r;
	map.green[foo] = v;
	map.blue[foo] = b;
	}
map.nbre = 8;

return 0;
}
/*::------------------------------------------------------------------::*/
int move(int x, int y)
{
#if DEBUG_LEVEL
fprintf(stderr, "\tMOVE  %5d  %5d\n", x, y); 
#endif
curX = x;	curY = y;
return 0;
}
/*::------------------------------------------------------------------::*/
int draw(int x, int y, int color)
{
RGBA	rgba;
int	idx;

#if DEBUG_LEVEL
fprintf(stderr, "\tdraw  %5d  %5d   to   %5d  %5d\n", curX, curY, x, y);
#endif

idx = color % 8;
if (idx != color) {
	fprintf(stderr, "%s %s : color %d out of range\n",
					__FILE__, __func__, color);
	}
rgba.r = map.red[color];
rgba.g = map.green[color];
rgba.b = map.blue[color];

/* new Mon 19 Dec 2022 12:53:32 PM CET
   check for special cases: eg horizontal lines
*/
if (y == curY) {
	Image_H_line(image, curX, x, y, &rgba);
	}
else	{
	Image_draw_line(image, curX, curY, x, y, &rgba);
	}
curX = x;	curY = y;
return 0;
}
/*::------------------------------------------------------------------::*/
int endgr(char *filename)
{

if (verbeux) fprintf(stderr, "genplot2 is saving to '%s'\n", filename);
Image_TGA_save(filename, image, 0);

return 0;
}
/*::------------------------------------------------------------------::*/
void help_me(char *proggy)
{
fprintf(stderr, "%s   build  %s  %s\n", proggy, __DATE__, __TIME__);

puts("options:");
puts("\t-v\t\tdo some blablage");
puts("\t-s WxH\t\tset image size");
}
/*::------------------------------------------------------------------::*/
int main(int argc, char *argv[])
{
char		*filename, *image;
FILE		*fp;
int		outw, outh;			/* size of output pic */
double		x, y, xmin, ymin, xmax, ymax;
double		fx, fy, f, X, Y;
double		xC, yC, XC, YC, c1, c2;
int		v, nbp, opt, foo;

/* setting some default values */

outw = XMAX;			outh = YMAX;
filename = "a.scratch";		image = "image.tga";

/*---------- processing command line arguments */

while ((opt = getopt(argc, argv, "hs:v")) != -1) {
	switch (opt) {
		case 'h':
			help_me(argv[0]);
			exit(0);
		case 's':			/* size of output pic */
			foo = parse_size_param(optarg, &outw, &outh);
			break;
		case 'v':
			verbeux = 1;
			break;
		default:
			fprintf(stderr, "invalid opt %d\n", opt);
			exit(1);
			break;
		}
	}
if (argc<=optind)	filename = "a.scratch";
else			filename = argv[optind];

if (argc<=optind+1)	image = "image.tga";
else			image = argv[optind+1];

/*----------- giving to the yuser some useless informations --- */
#if DEBUG_LEVEL
fprintf(stderr, "*** Genplot2 v 1.0.10 (dwtfywl) 1995,2010,2022 TontonTh \n");
#endif
if (verbeux) {
	fprintf(stderr, "argc %d optind %d  file '%s' image '%s'\n",
			argc, optind, filename, image);
	fprintf(stderr, "    picsize : %d %d\n", outw, outh);
	}

/*----------- opening input file and getting MIN and MAX values */

if ((fp = fopen(filename, "r"))==NULL) {
	perror("fichier d'entree");
	exit(1);
	}

nbp = 0;
xmin =  9999999.99;		xmax = -9999999.99;
ymin =  9999999.99;		ymax = -9999999.99;
while ( fscanf(fp, "%lf %lf %d", &x, &y, &v) == 3 ) {
	nbp++;

	if (x > xmax)		xmax = x;
	if (x < xmin)		xmin = x;
	if (y > ymax)		ymax = y;
	if (y < ymin)		ymin = y;
	}
fclose(fp);

if (nbp == 0)
	{
	fprintf(stderr, "omg, I'v found _ZERO_ points for plotting...\n");
	exit(2);
	}
if (nbp == 1)
	{
	fprintf(stderr, "omg, I can't do any job with only one point.\n");
	exit(2);
	}


fprintf(stderr, "Genplot2: found %d points\n", nbp);

/*---------- computing coefficients (temporary hack !-) */

fx = (outw-XMIN-1)/(xmax-xmin);
fy = (outh-YMIN-1)/(ymax-ymin);
#if DEBUG_LEVEL
fprintf(stderr, "fc = %12f     fy = %12f\n", fx, fy);
#endif
f = (fx<fy?fx:fy);

xC = 0.5*(xmin+xmax);		yC = 0.5*(ymin+ymax);
XC = 0.5*(XMIN+outw);		YC = 0.5*(YMIN+outh);

c1 = XC-f*xC;			c2 = YC-f*yC;

#if DEBUG_LEVEL
fprintf(stderr, "c1 = %12f     c2 = %12f\n", c1, c2);
#endif

/*------------- and now, plotting the image ! */
initgr(outw, outh);

fp = fopen(filename, "r");
puts("");
while ( fscanf(fp, "%lf %lf %d", &x, &y, &v) == 3 ) {
#if DEBUG_LEVEL
	fprintf(stderr, "%12f %12f %d\n", x, y, v);
#endif
	X = f*x+c1;	Y = f*y+c2;
	
	if (v < 0)	move(X, Y);
	else		draw(X, Y, v);
	}

endgr(image);

return 0;
}

