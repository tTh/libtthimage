/*
	utilitaire pour plotter les histogrammes d'une image.
	-----------------------------------------------------
*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "tga_outils.h"

void usage()
{
fprintf(stderr, "*** tga_plothisto v 0.1.3 [%s] %s\n", 
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);
fprintf(stderr, "\nusage: tga_plothisto image.tga histo.tga [texte]\n\n");
Image_print_version(0);
exit(5);
}

int main (int argc, char *argv[])
{
Image_Desc	*src;
long		hr[256], hg[256], hb[256];
int		foo;
char		*texte;

dump_command_line(argc, argv, 0);

if (argc < 3)
	{
	/*fprintf(stderr, "argc = %d\n", argc);*/
	usage();
	}

if ( (src=Image_TGA_alloc_load(argv[1])) == NULL )
	{
	fprintf(stderr, "tga_plothisto, chargement '%s' failed\n", argv[1]);
	exit(5);
	}

foo = Image_histo_RGB(src, hr, hb, hg);
if (foo)
	{
	fprintf(stderr, "tga_plothisto, calcul histogramme: %d  %s\n",
						foo, Image_err2str(foo));
	exit(5);
	}

if (argc == 3)		texte = argv[1];
else			texte = argv[3];

foo = Image_plot_histo(argv[2], hr, hg, hb, texte);
if (foo)
	{
	fprintf(stderr, "tga_plothisto, dessin de '%s': %d  %s\n",
					argv[2], foo, Image_err2str(foo));
	exit(5);
	}

return 0;
}
