/*
		FRACTALES
		---------

	Il reste beaucoup de travail sur cet outil:

	 - plasma
	 - popcorn
	 - newton: oct 2003, c'est en route. janv 2010 pas fini.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include  "tga_outils.h"
#include  "fractales.h"

/*::------------------------------------------------------------------::*/
#define		MANDEL0		1
#define		JULIA00		2
#define		GINGER		3
#define		NEWTON0		10
#define		NEWTON1		11
#define		LORENZ0		12

mot_clef mots_clef[] =
{
{ "ginger",	GINGER,		"ii",	"width & height"	},
{ "julia00",	JULIA00,	"ii",	"width & height"	},
{ "mandel0",	MANDEL0,	"ii",	"width & height"	},
{ "newton0",	NEWTON0,	"ii",	"width & height"	},
{ "lorenz0",    LORENZ0,	"ii",   "W & H"			},
{ NULL,		0,		NULL,	NULL			}
};

/*::------------------------------------------------------------------::*/
void usage(int flag)
{
Image_fractales_print_version(flag);
fprintf(stderr, "usage:\n\ttga_fractales type nom.tga [params]\n");
if (flag)
	{
	Image_print_version(1);
	liste_mots_clefs(mots_clef, 42);
	}
exit(1);
}
/*::------------------------------------------------------------------::*/
/*
 *	argv[1]:	type fractale
 *	argv[2]:	nom fichier.tga
 */
#define FIRST_PAR  3
int main (int argc, char *argv[])
{
Image_Desc	*dst;
int		foo, width, height;
int		commande, nbargs, idx;
RGB_map		map;

dump_command_line(argc, argv, 0);
if ( argc==2 && !strcmp(argv[1], "list") ) usage(1);
if ( argc < 3 )		usage(0);

idx = cherche_mot_clef(argv[1], mots_clef, &commande, &nbargs);
if (idx < 0)
	{
	fprintf(stderr, "tga_fractales: mot-clef %s inconnu...\n", argv[1]);
	exit(5);
	}
#if DEBUG_LEVEL
fprintf(stderr, "code commande %d\n", commande);
#endif

foo = parse_parametres(argc, argv, mots_clef[idx].ptypes, FIRST_PAR);

dst = NULL;			/* gcc warning STFU */

switch (commande)
	{
	case MANDEL0:
		width = GIP(0);		height = GIP(1);
		printf("mandelbrot 0: %dx%d\n", width, height);
		dst = Image_alloc(width, height, 3);
		foo = Image_Mandelbrot_0(dst, 300);
		Image_print_error("mandelbrot 0", foo);		
		break;
	case JULIA00:
		width = GIP(0);		height = GIP(1);
		foo = Image_load_color_Map("volcano.map", "plop", &map);
		Image_print_error("julia00: loadmap", foo);
		dst = Image_alloc(width, height, 3);
		Image_Julia_0_0(dst, &map, 3000, 0.333, 0.333);
		Image_print_error("julia00: calcul", foo);
		break;
	case GINGER:
		width = GIP(0);		height = GIP(1);
		printf("ginger bread man: %dx%d\n", width, height);
		dst = Image_alloc(width, height, 3);
		foo = GingerBreadMan(dst, 300);
		Image_print_error("GingerBreadMan", foo);		
		break;
	case NEWTON0:
		width = GIP(0);		height = GIP(1);
		printf("newton 0: %dx%d\n", width, height);
		dst = Image_alloc(width, height, 3);
		foo = Newton_0(dst, 42);
		Image_print_error("proto Newton 0", foo);		
		break;
	case LORENZ0:
		width = GIP(0);		height = GIP(1);
		printf("lorenz 0: %dx%d\n", width, height);
		dst = Image_alloc(width, height, 3);
		foo = Lorenz_Orbits(dst, 3000, 5.0, 15.0, 1.0, 0.02);
		Image_print_error("lorenz 0", foo);		
		break;
	default:
		fprintf(stderr, "Cette fractale n'est pas disponible.\n");
		exit(1);
		break;
	}

foo = Image_TGA_save(argv[2], dst, 0);

return 0;
}
/*::------------------------------------------------------------------::*/
