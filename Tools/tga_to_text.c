/*
 *		TGA to text
 *		===========
 * new: Mon Feb 26 23:21:21 UTC 2024
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
/*
 *	Print a textual form of a picture, so you can read it
 *	with a software who can't read TGA files.
 *	Exemples: Fortran or Basic code, R, the statistic
 *	software. And maybe you cant write a shell script
 *	to convert from TGA to PNM :)
 */
int printf_this_picture(Image_Desc *pic, int flag)
{
int		x, y,	r, g, b;

if (flag) {
	/* needed for easy import in Rstats */
	printf("    X     Y      R    G    B\n");
	}

for (y=0; y<pic->height; y++) {
	for (x=0; x<pic->width; x++) {
		printf("%5d %5d    ", x, y);
		Image_getRGB(pic, x, y, &r, &g, &b);
		printf("%3d %3d %3d\n", r, g, b);
		}
	}		

return 0;
}
/*::------------------------------------------------------------------::*/
int main (int argc, char *argv[])
{
Image_Desc	*src;
int		foo;

if (2!=argc) {
	fprintf(stderr, "%s need a filename\n", argv[0]);
	exit(2);
	}

if ((src = Image_TGA_alloc_load(argv[1]))==NULL) {
	fprintf(stderr, "TGA to text: can't load image %s\n", argv[1]);
	exit(5);
	}

foo = printf_this_picture(src, 0);
if (foo) {
	fprintf(stderr, "printf_this_picture -> %d\n", foo);
	}

return 0;
}
/*::------------------------------------------------------------------::*/

