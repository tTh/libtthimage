/*
		2x2 contrast enhancer
		---------------------

		:::::::::::::::::::::::::::::::::::::::::
		:::::: NEED ACCURATE DOCUMENTATION ::::::
		:::::::::::::::::::::::::::::::::::::::::
*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
void usage(void)
{
fprintf(stderr, "* tga_2x2contrast v 0.18 [%s] %s\n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);
fprintf(stderr, "Usage:\n\ttga_2x2contrast <src.tga> <dst.tga>\n");
Image_print_version(0);
exit(5);
}
/*::------------------------------------------------------------------::*/

int main(int argc, char *argv[])
{
Image_Desc	*src, *dst;
int		foo;

dump_command_line(argc, argv, 0);

if (argc != 3)
	usage();

if ( (src=Image_TGA_alloc_load(argv[1])) == NULL )
	{
	fprintf(stderr, "tga_2x2contrast: can't load \"%s\" ...\n", argv[2]);
	exit(1);
	}

if ( (dst=Image_clone(src, 1)) == NULL )
	{
	exit(1);
	}

foo = Image_2x2_contrast(src, dst);

if (foo)
	{
	fprintf(stderr, "tga_2x2contrast: %d / %s\n", foo, Image_err2str(foo));
	}

foo = Image_TGA_save(argv[2], dst, 0);

exit (0);
}
/*::------------------------------------------------------------------::*/
