#!/bin/bash

set -e

make genplot2

cat > a.scratch << __EOT__
  0   0   0
320   0   2
320 240   2
  0 240   2
  0   0   2
320 240   7
__EOT__

./genplot2 -v -s 640x480 a.scratch foo.tga