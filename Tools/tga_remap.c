/*
            _                                                 
           | |_ __ _  __ _     _ __ ___ _ __ ___   __ _ _ __  
           | __/ _` |/ _` |   | '__/ _ \ '_ ` _ \ / _` | '_ \ 
           | || (_| | (_| |   | | |  __/ | | | | | (_| | |_) |
            \__\__, |\__,_|___|_|  \___|_| |_| |_|\__,_| .__/ 
               |___/     |_____|                       |_|    

*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <time.h>

#include  "tga_outils.h"

/*::------------------------------------------------------------------::*/
void usage(int flag)
{
fprintf(stderr, "* tga_remap v 0.1.21 [%s] %s\n",
				TGA_OUTILS_VERSION, TGA_OUTILS_COPYLEFT);
fprintf(stderr, "Usage:    tga_remap M <src.tga> <color.map> <dst.tga>\n");
fprintf(stderr, "          tga_remap -? for more infos...\n");
if (flag)
	{
	PERL;
	PERR("valid values for 'M'");
	PERR("   0   minimal distance");
	PERR("   1   min of R or G or B dist");
	PERR("   2   ???");
	PERR("   11  minimal dist for R");
	PERR("   12  minimal dist for G");
	PERR("   13  minimal dist for B");
	PERL;
	Image_print_version(2);
	}
else
	{
	Image_print_version(0);
	}
exit(5);
}
/*::------------------------------------------------------------------::*/
int main(int argc, char *argv[])
{
Image_Desc	*src, *dst;
int		foo, type;
RGB_map		map;

dump_command_line(argc, argv, 0);

if (argc==2 && !strcmp(argv[1], "-?"))	usage(1);

if (argc != 5) usage(0);

if ( (src=Image_TGA_alloc_load(argv[2])) == NULL )
	{
	fprintf(stderr, "can't load '%s'\n", argv[2]);
	exit(1);
	}

if ( (foo=Image_load_color_Map(argv[3], "map", &map)) )
	{
	fprintf(stderr, "%s: %s: err %d '%s'\n", argv[0], argv[3], 
					foo, Image_err2str(foo));
	exit(1);
	}

if (must_be_verbose())
	{
	fprintf(stderr, "Il y a %d couleurs dans la palette '%s'\n",
			map.nbre, argv[3]);
	}

/*
 * que faire si le nombre de couleurs dans la palette est pas bon ??? 
 */

if ( (dst=Image_clone(src, 0))==NULL )
	{
	fprintf(stderr, "can't clone %p, exiting...\n", src);
	exit(2);
	}

/* ---------- determiner le type de remap demande */
if ( (sscanf(argv[1], "%d", &type)) != 1 )	type = 0;

#if DEBUG_LEVEL
t_debut = time(NULL);
#endif

if ( (foo=Image_colors_2_Map(src, dst, &map, type)) )
	{
	fprintf(stderr, "%s: color->map: erreur %d: %s\n", argv[0],  
					foo, Image_err2str(foo));
	/* Oulala ! c'est pas un peu brutal ? */
	exit(1);
	}

#if DEBUG_LEVEL
t_fin = time(NULL);
fprintf(stderr, "   remap : %ld secondes\n", t_fin-t_debut);
#endif

foo = Image_TGA_save(argv[4], dst, 0);
if (foo)
	{
	fprintf(stderr, "%s: TGA_save: err %d %s\n",
			argv[0], foo, Image_err2str(foo));
	exit(1);
	}

exit (0);
}
/*::------------------------------------------------------------------::*/
