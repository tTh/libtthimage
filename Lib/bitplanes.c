/*
 *                ESSAIS SUR LES BITPLANES
 *
 * new 12 janvier 2009 - avenue St Exupery - dans un etat de grosse fatigue
 * reprise 17 mars 2010 - meme endroit - epuisement de classe "irpP"
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <unistd.h>

#include  "../tthimage.h"

#define VERSION_STRING "7 juiller 2022"

/*::------------------------------------------------------------------::*/
/*:: private variables */
static		int	next_id;
/*::------------------------------------------------------------------::*/
int Image_dump_BP_head(A_BitPlane *bp, char *txt, int flag)
{

printf("-----------: BitPlane %s ----\n", txt);
printf("@bp        : %p\n", bp);
printf("magic      : $%08x\n", bp->magic);
printf("size       : %d x %d\n", bp->width, bp->height);
printf("id         : o%o\n", bp->id);
printf("name       : %s\n", bp->name);
printf("tagname    : $%02x\n", bp->tagname);
printf("@plane     : %p\n", bp->plane);
printf("reserved   : %d\n", bp->reserved);

if (flag)	puts("  and  --> \\FLAG/");

puts(".");

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
A_BitPlane * Image_give_me_a_BP(int w, int h)
{
A_BitPlane	*bp;
int		taille;
uint8_t		*pbits;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d %d )\n", __func__, w, h);
#endif

/* is there a sanity check on the dimensions ? */
if ( (w<1) || (h<1) ) {
	fprintf(stderr, "w %d or h %d is bad\n", w, h);
	return NULL;
	}

if ( NULL==(bp=calloc(sizeof(A_BitPlane), 1)) ) {
	perror("getting memory for a bitmap header");
	return NULL;
	}
#if DEBUG_LEVEL
fprintf(stderr, "head malloc(%d) -> %p\n", sizeof(A_BitPlane), bp);
#endif

taille = (w * h) / 8;
if ( NULL == (pbits=calloc(taille, 1)) ) {
	perror("getting a plane");
	return NULL;
	}
#if DEBUG_LEVEL
fprintf(stderr, "plane malloc(%d) -> %p\n", taille, pbits);
#endif

/* set some default values */
bp->magic = MAGIC_BIT_PLANE;
bp->width = w;
bp->height = h;
if (0==next_id)	next_id = getpid();
bp->id = next_id++;
bp->r0 = bp->g0 = bp->b0 = 0;
bp->r1 = bp->g1 = bp->b1 = 255;
bp->plane = pbits;
strcpy(bp->name, "(noname)");
bp->reserved = 0;

return bp;
}
/*::------------------------------------------------------------------::*/
int Image_free_that_BP(A_BitPlane *bp, int k)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d )\n", __func__, bp, k);
#endif

if (bp->magic != MAGIC_BIT_PLANE) {
	fprintf(stderr, "%s : %p is not a bitplane descriptor\n", 
				__func__, bp);
#if FORCE_ABORT
	abort();
#endif
	return 666;
}

free(bp->plane);
#if DEBUG_LEVEL
fprintf(stderr, "    bp->plane %p freed\n", bp->plane);
#endif
bp->plane = NULL;
bp->magic = 0;

free(bp);

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
int Image_BP_setname(A_BitPlane *bp, const char *name)
{
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p '%s' )\n", __func__, bp, name);
#endif

if (MAGIC_BIT_PLANE != bp->magic) {
	fprintf(stderr, "%s : %p bad magic\n", __func__, bp);
	return BAD_MAGIC;
	}
if (IMG_OBJNAME_LEN <= strlen(name)) {
	fprintf(stderr, "%s : name too long\n", __func__);
	return FULL_NUCKED;
	}
strcpy(bp->name, name);

return UNKNOW_ERROR;
}
/*::------------------------------------------------------------------::*/
int Image_BP_clear(A_BitPlane *bp, int bit)
{
int	bitsize, foo;

#if DEBUG_LEVEL
fprintf(stderr, "%s (( %p ))\n", __func__, bp);
#endif

(void)bit;

if (MAGIC_BIT_PLANE != bp->magic) {
	fprintf(stderr, "%s : %p bad magic\n", __func__, bp);
	return BAD_MAGIC;
	}
bitsize = bp->height * bp->width / 8;
#if DEBUG_LEVEL
fprintf(stderr, "%d x %d -> %d\n", bp->width, bp->height, bitsize);
#endif

for (foo=0; foo<bitsize; foo++) {
	/*
	 *	clear something here
	 */
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Image_BP_plot(A_BitPlane *bp, int x, int y, int bit)
{
int	offbyte, offbit;

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s ( %p %4d %4d    %c )\n", __func__, bp, x, y,
						(bit&1) ? 'X' : '.');
#endif

/* we have to do some sanity checks */
if (MAGIC_BIT_PLANE != bp->magic) {
	fprintf(stderr, "%s : %p bad magic\n", __func__, bp);
	return BAD_MAGIC;
	}
if ( (x<0) || (y<0) || (x>=bp->width) || (y>=bp->height) ) {
	fprintf(stderr, "%s : %d,%d out of pic\n", __func__, x, y);
	return OUT_OF_IMAGE;
	}

offbyte = ((bp->width*y) + x) / 8;
offbit = x % 8;
#if DEBUG_LEVEL > 1
fprintf(stderr, "   offbyte %d, offbit %d\n", offbyte, offbit);
fprintf(stderr, "   avant 0x%02x\n", bp->plane[offbyte]);
#endif

if (bit)		bp->plane[offbyte] |= (1 << offbit);
else			bp->plane[offbyte] &= ~(1 << offbit);

#if DEBUG_LEVEL > 1
fprintf(stderr, "   apres 0x%02x\n", bp->plane[offbyte]);
#endif

return UNKNOW_ERROR;
}
/*::------------------------------------------------------------------::*/
int Image_BP_pget(A_BitPlane *bp, int x, int y, int *pbit)
{
int	offbyte, offbit;

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s ( %p %4d %4d    %p )\n", __func__, bp, x, y, pbit);
#endif

/* we have to do some sanity checks */
if (MAGIC_BIT_PLANE != bp->magic) {
	fprintf(stderr, "%s : %p bad magic\n", __func__, bp);
	return BAD_MAGIC;
	}
if ( (x<0) || (y<0) || (x>bp->width) || (y>bp->height) )
	{
	fprintf(stderr, "%s : %d,%d out of pic\n", __func__, x, y);
	return OUT_OF_IMAGE;
	}

offbyte = ((bp->width*y) + x) / 8;
offbit = x % 8;
#if DEBUG_LEVEL > 1
fprintf(stderr, "   offbyte %d, offbit %d\n", offbyte, offbit);
#endif

if (bp->plane[offbyte] & (1 << offbit))
	*pbit = 1;
else
	*pbit = 0;

return UNKNOW_ERROR;
}
/*::------------------------------------------------------------------::*/
int	Image_BP_to_pbm(A_BitPlane *bp, char *fname, int k)
{
FILE	*fp;
int	x, y, v;
int	chariot;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p '%s' 0x%03x )\n", __func__, bp, fname, k);
#endif

if (NULL==(fp=fopen(fname, "w")))
	{
	fprintf(stderr, "fucked in %s on file '%s'\n", __func__, fname);
	return FULL_NUCKED;
	}

fprintf(fp, "P1\n%d %d\n", bp->width, bp->height);
fprintf(fp, "# written by libtthimage v %s\n# bitplane module '%s'\n",
			IMAGE_VERSION_STRING, VERSION_STRING);	

chariot = 0;
for (y=0; y<bp->height; y++) {
	for (x=0; x<bp->width; x++) {
		Image_BP_pget(bp, x, y, &v);
		if (v)		fputc('1', fp);
		else		fputc('0', fp);
		if (chariot++ < 74)
			fputc(' ', fp);
		else	{
			fputc('\n', fp);	chariot=0;
			}
		}
	}

if (k) fputs("\n# gruiked by tth\n", fp);

fclose(fp);

return UNKNOW_ERROR;
}
/*::------------------------------------------------------------------::*/
