/*
 *		ANAMORPHOSES
 *
 *	new 18 octobre 2003
 */
#include   <stdio.h>
#include   <stdlib.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
int
Image_anmo_X(Image_Desc *src, Image_Desc *dst)
{
int		x, y;
double		dnormx, dnormy;

fprintf(stderr, ">>> %s: %p -> %p\n", __func__, src, dst);

/*
 * we scan the destination picture
 */
for (y=0; y<dst->height; y++) {
	dnormy = (double)y/(double)dst->height;
	for (x=0; x<dst->width; x++) {
		dnormx = (double)x/(double)dst->width;


		}
	}

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
/*
 *	warning: this is a test function. prototype may change 
 *		 in a near futur.
 */
int
Image_anmo_grille(Image_Desc *dst)
{

if (dst->magic != MAGIC_OF_IMAGE)
	{
	fprintf(stderr, "Grille Anamorphose: need Dead Beef!\n");
	return NOT_AN_IMAGE_DESC;
	}

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
/*
 *	warning: this is a test function. prototype may change 
 *		 in a not so near futur.
 */

#define STAT	1

int
Image_anmo_essai_0(char *nomtga, int w, int h, int flgtxt)
{
Image_Desc	*image;
int		x, y, x2, y2;
double		dx, dy;
double		angle;
int		xorg, yorg;
double		xamp, yamp;
#if STAT
long		nb_in, nb_out;
#endif

if (NULL==(image = Image_alloc(w, h, 3))) {
	fprintf(stderr, "%s %s : Abend: no mem\n", __FILE__, __func__);
	exit(5);
	}

fprintf(stderr, "--> %s  %s  %s\n", __FILE__, __DATE__, __TIME__);

/* déterminer les parametres de la deformation */
xorg = w / 2;		yorg = h - 10;
xamp = 1.0;		yamp = 1.0;

#if STAT
nb_in = nb_out = 0L;
#endif

/*
 * we scan all the destination picture...
 */
for (x=0; x<w; x++)
	{
	dx = (double)x;
	angle = (dx/(double)w * (M_PI/1)) + M_PI;

	for (y=0; y<h; y++)
		{
		dy = (double)y;

		x2 = (int)(cos(angle) * (dy*xamp)) + xorg;
		y2 = (int)(sin(angle) * (dy*yamp)) + yorg;

		if (x2>=0 && y2>=0 && x2<w-1 && y2<h-1)
			{
			/* Image_plotRGB(image, x2, y2, x*2, 200, y*2); */
	    		(image->Rpix[y])[x] = x2*2;
	    		(image->Gpix[y])[x] = 200;
	    		(image->Bpix[y])[x] = y2*2;
#if STAT
			nb_in++;
#endif
			}
#if STAT
		else
			{
			nb_out++;
			}
#endif
		}
	}

#if STAT
fprintf(stderr, "    %ld in, %ld out\n", nb_in, nb_out);
#endif

Image_cadre_A(image);

if (flgtxt)
	{
	Image_marque_timestamp(image, "essai anamorphose", NULL, 1);
	}

Image_TGA_save(nomtga, image, 0);

Image_DeAllocate(image);	free(image);

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
