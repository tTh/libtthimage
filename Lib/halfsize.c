
/*
                    halfsize.c
		------------------
	
	see also: scale.c doublesz.c

*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	Fabrication d'une image moiti� de l'image source.
 *
 *	cette fonction alloue une _nouvelle_ image
 *                                ==========
 *	m�thodes:
 *		0: primitive.
 *		1: semblant d'interpolation.
 *
 */
Image_Desc *
Image_MakeHalfSize(Image_Desc *image, int methode)
{
int		largeur, hauteur, x, y, xx, yy;
Image_Desc	*demi;

largeur = image->width / 2;     hauteur = image->height / 2;

if (image->type != IMAGE_RGB)
	{
	fprintf(stderr, "Image HalfSize: bad image type %d\n", image->type);
	return NULL;
	}

#if DEBUG_LEVEL
fprintf(stderr, "MakeHalfSize: from %dx%d, method %d  -> %d x %d\n",
		image->width, image->height, methode, largeur, hauteur);
#endif

demi = Image_alloc(largeur, hauteur, image->type);
if ( demi == NULL )
	{
	fprintf(stderr, "Image make half size: no memory\n");
	exit(5);
	}

/*
 *	ne faudrait-il pas transmettre certains des attributs de
 *	l'image originale � la nouvelle image ?
 */
demi->modified = 0;
demi->errmsg = image->errmsg;

for (y=0; y<hauteur; y++)
    {
    yy = y * 2;
    for (x=0; x<largeur; x++)
        {
        xx = x * 2;
        switch(methode)
            {
            case 0:
                (demi->Rpix[y])[x] = (image->Rpix[yy])[xx];
                (demi->Gpix[y])[x] = (image->Gpix[yy])[xx];
                (demi->Bpix[y])[x] = (image->Bpix[yy])[xx];
                break;

	    case 1:
                (demi->Rpix[y])[x] = (	 (image->Rpix[yy])[xx]   +
					 (image->Rpix[yy+1])[xx] +
					 (image->Rpix[yy])[xx+1] +
					 (image->Rpix[yy+1])[xx+1] ) / 4;
           
                (demi->Gpix[y])[x] = (	 (image->Gpix[yy])[xx]   +
					 (image->Gpix[yy+1])[xx] +
					 (image->Gpix[yy])[xx+1] +
					 (image->Gpix[yy+1])[xx+1] ) / 4;
           
                (demi->Bpix[y])[x] = (	 (image->Bpix[yy])[xx]   +
					 (image->Bpix[yy+1])[xx] +
					 (image->Bpix[yy])[xx+1] +
					 (image->Bpix[yy+1])[xx+1] ) / 4;
           
		break;

	    default:
		/*
		 * et ici, un message d'erreur ?
		 */
		break;
            }
        }
    }

demi->modified = 1;

return demi;		/* demi is a pointer to an Image_Desc ! */
}
/*::------------------------------------------------------------------::*/
