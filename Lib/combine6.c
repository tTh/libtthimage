/*
                    combine6.c
		    ----------
*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/* nouveau 28 fevrier 2014 / ave StExupery */
int Image_combine_power(Image_Desc *s1, Image_Desc *s2, Image_Desc *d)
{
int		x, y;
int		foo;

if ( (foo=Image_compare_desc(s1, s2)) )
	{
	fprintf(stderr, "%s: sources are differents (%d)\n", __func__, foo);
	return foo;
	}

#define D1(pix) ( ((double)pix)/256.0 )

for (y=0; y<s1->height; y++)
	{
	for (x=0; x<s1->width; x++)
		{
		d->Rpix[y][x] = (int)(pow( D1(s1->Rpix[y][x]), 
				     D1(s2->Rpix[y][x]) ) * 255.0);
		d->Gpix[y][x] = (int)(pow( D1(s1->Gpix[y][x]), 
				     D1(s2->Gpix[y][x]) ) * 255.0);
		d->Bpix[y][x] = (int)(pow( D1(s1->Gpix[y][x]), 
				     D1(s2->Bpix[y][x]) ) * 255.0);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* nouveau 13 mars 2014 / Mixart-Myrys */
int Image_combine_power_m(Image_Desc *s1, Image_Desc *s2, Image_Desc *d)
{
int		x, y;
int		foo;
double		v;

#if DEBUG_LEVEL
fprintf(stderr, "---> %s ( %p %p %p )\n", __func__, s1, s2, d);
#endif

if ( (foo=Image_compare_desc(s1, s2)) )
	{
	fprintf(stderr, "%s: sources are differents (%d)\n", __func__, foo);
	return foo;
	}

for (y=0; y<s1->height; y++)
	{
	for (x=0; x<s1->width; x++)
		{
		v = (double)(s2->Rpix[y][x] + s2->Gpix[y][x] + s2->Bpix[y][x]);
		v /= (256.0 * 3.0);
		d->Rpix[y][x] = (int)(pow( D1(s1->Rpix[y][x]), v) * 255.0);
		d->Gpix[y][x] = (int)(pow( D1(s1->Gpix[y][x]), v) * 255.0);
		d->Bpix[y][x] = (int)(pow( D1(s1->Bpix[y][x]), v) * 255.0);
		}
	}
		
d->modified = 3;

#if DEBUG_LEVEL
fprintf(stderr, "fin %s\n", __func__);
#endif

return 666;
}
/*::------------------------------------------------------------------::*/
/*
 * XOR entre images, nouveau 25 septembre 2018, dans le  DD2
 * pour le timelapse de streetpack...
 */

int Image_XOR(Image_Desc *s1, Image_Desc *s2, Image_Desc *d, int k)
{
int		foo, x, y;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p %d)\n", __func__, s1, s2, d, k);
#endif

if ( (foo=Image_compare_desc(s1, s2)) )
	{
	fprintf(stderr, "%s: sources are differents (%d)\n", __func__, foo);
	return foo;
	}

if ( (foo=Image_compare_desc(s1, d)) )
	{
	fprintf(stderr, "%s: source & destinationa are differents (%d)\n",
				__func__, foo);
	return foo;
	}

for (y=0; y<s1->height; y++)
	{
	for (x=0; x<s1->width; x++)
		{
		d->Rpix[y][x] = s1->Rpix[y][x] ^ s2->Rpix[y][x];
		d->Gpix[y][x] = s1->Gpix[y][x] ^ s2->Gpix[y][x];
		d->Bpix[y][x] = s1->Bpix[y][x] ^ s2->Bpix[y][x];
		}
	}

d->modified = 3;

return OLL_KORRECT;
}
