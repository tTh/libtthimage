#!/bin/bash

set -e

SRC="testtga.c essais.c operat.c image.c effects.c tele_2.c calculs.c"
SRC="$SRC dither.c television.c rgbmask.c sobel4.c tga.c patterns*.c"
echo $SRC ; echo

FLOW="cflow.txt"

cflow --depth=5 -o $FLOW -m main --number $SRC

less $FLOW
