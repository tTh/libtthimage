/*
		imprime.c
		---------
*/

#include  <stdio.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_print_histo_0(long *histo, char *fname, char *texte)
{
int	foo;

printf("** %s / %s **\n", texte, fname);

for (foo=0; foo<256; foo++) {
	printf("%7ld ", histo[foo]);
	if ((foo % 8)==0)
		printf("\n");
	}
printf("\n");

return FULL_NUCKED;
}
/*
 *	je sais m�me plus pourquoi j'ai commenc� cette fonction :)
 */
/*::------------------------------------------------------------------::*/

void Image_print_sizeof_structs(char *texte)
{
unsigned short		petit;
unsigned long		grand;
unsigned char		*ucptr;
int			foo;

if (NULL != texte) {
	printf("============= %s ============\n", texte);
	}

/* examen du boutisme (ah ah ah) */
petit = 0x1234;			printf("petit  =   %8x  ->  ", petit);
ucptr = (unsigned char *)&petit;
for (foo=0; foo<(int)sizeof(petit); foo++)
	printf(" %02x", ucptr[foo]);
printf("\n");

/* trop kikoolol, le boutisme */
grand = 0x12345678;
printf("grand  =   %lx  ->  ", grand);
ucptr = (unsigned char *)&grand;
for (foo=0; foo<sizeof(grand); foo++)
	printf(" %02x", ucptr[foo]);
printf("\n");

/* examen des types de base */
printf("basic types    : short=%ld int=%ld long=%ld ptr=%ld\n",
		sizeof(short), sizeof(int), sizeof(long), sizeof(void *));

/* examen des tructures de la libtthimage */
printf("Image_Desc     :  %5ld\n", sizeof(Image_Desc)); 
printf("Image_Rect     :  %5ld\n", sizeof(Image_Rect)); 
printf("RGBA           :  %5ld\n", sizeof(RGBA)); 
printf("A_BitPlane     :  %5ld\n", sizeof(A_BitPlane)); 
printf("RGB_map        :  %5ld\n", sizeof(RGB_map)); 
printf("Image_Point    :  %5ld\n", sizeof(Image_Point)); 

}
/*::------------------------------------------------------------------::*/
