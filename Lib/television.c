/*
			television.c
			------------
*/

#include  <stdio.h>
#include  <math.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_TV_grink(Image_Desc *src, Image_Desc *dst, int yo)
{
int	foo;
int	x, y, r, g, b;
int	resteY;

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

resteY = src->height % 3;
#if DEBUG_LEVEL
fprintf(stderr, "Image TV grink: height %d reste Y %d\n", src->height, resteY);
#endif
resteY /= 2;		/* half for the top, half for the bottom */

for (y=resteY; y<src->height-resteY-1; y+=3) {
	for (x=0; x<src->width; x++) {
		r =   ( Image_R_pixel(src, x, y) +
			Image_R_pixel(src, x, y+1) +
			Image_R_pixel(src, x, y+2) ) / 3;
		g =   ( Image_G_pixel(src, x, y) +
			Image_G_pixel(src, x, y+1) +
			Image_G_pixel(src, x, y+2) ) / 3;
		b =   ( Image_B_pixel(src, x, y) +
			Image_B_pixel(src, x, y+1) +
			Image_B_pixel(src, x, y+2) ) / 3;
		Image_plotRGB(dst, x,   y, r, g, b);
		if (yo) {
			Image_plotRGB(dst, x, y+1, r, b, g);
			Image_plotRGB(dst, x, y+2, r, g, b);
			}
		else	{
			Image_plotRGB(dst, x, y+2, r, b, g);
			Image_plotRGB(dst, x, y+1, r, g, b);
			}
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
int Image_TV_grok(Image_Desc *src, Image_Desc *dst, int yo)
{
int	foo, x, y;
int	r, g, b;

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		Image_getRGB(src, x, y, &r, &g, &b);
		if (yo)
			Image_plotRGB(dst, x, y, r&0xa0, g&0xa0, b&0xa0);
		else
			Image_plotRGB(dst, x, y, r&0xf0, g&0xf0, b&0xf0);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* new 8 jan 2002
 *	le parametre 'yo' ne sert a rien et doit etre mis a 0
 */
int Image_TV_gruud(Image_Desc *src, Image_Desc *dst, int yo)
{
int	x, y, i, j;
int	r, g, b, rc, gc, bc;
int	foo;

if (yo != 0) {
    	fprintf(stderr, "%s: yo (%d) _must_ be 0\n", __func__, yo);
	return INVALID_PARAM;
	}

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "%s: images are differents %d\n",__func__,  foo);
    return foo;
    }

for (y=0; y<src->height-2; y+=3) {
	for (x=0; x<src->width-2; x+=3) {
		rc = gc = bc = 0;
		for (i=0; i<3; i++) {
			for (j=0; j<3; j++) {
				Image_getRGB(src, x+i, y+j, &r, &g, &b);
				rc += r;
				gc += g;
				bc += b;
				}
			}

		rc /= 9; gc /= 9; bc /= 9;
		for (i=0; i<3; i++) {
			Image_plotRGB(dst, x,   y+i, rc, 0, 0);
			Image_plotRGB(dst, x+1, y+i, 0, gc, 0);
			Image_plotRGB(dst, x+2, y+i, 0, 0, bc);
			}
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/

#define GRIIIZ_SIZE 20

/*
 *	le parametre 'yo' ne sert e rien et doit etre mis a 0
 */
int Image_TV_griiiz(Image_Desc *src, Image_Desc *dst, int yo)
{
int	foo, r, g, b, cr, cg, cb;
int	x, y, i, j, ox, oy;

int	mr[GRIIIZ_SIZE][GRIIIZ_SIZE] = {
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 1, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 1, 4, 9, 9, 9, 9, 9, 9, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 1, 4, 9, 9, 9, 9, 9, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 1, 4, 9, 9, 9, 9, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 1, 4, 7, 7, 9, 4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 1, 4, 7, 7, 4, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 1, 4, 4, 4, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	};

int	mg[GRIIIZ_SIZE][GRIIIZ_SIZE] = {
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 1 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 5, 5, 5, 5, 5, 5, 4, 3, 1 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 6, 6, 6, 6, 6, 6, 6, 3, 1 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 5, 7, 8, 9, 9, 8, 7, 3, 1 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 7, 8, 9, 9, 9, 7, 3, 1 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 5, 7, 8, 9, 9, 7, 3, 1 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 5, 7, 7, 8, 7, 3, 1 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 5, 7, 7, 7, 3, 1 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 5, 6, 6, 3, 1 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	};

int	mb[GRIIIZ_SIZE][GRIIIZ_SIZE] = {
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 2, 4, 5, 7, 4, 2, 2, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 2, 4, 7, 9, 9, 4, 4, 2, 2, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 2, 4, 0, 9, 9, 9, 9, 4, 4, 2, 2, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 2, 5, 7, 8, 9, 9, 9, 9, 9, 7, 5, 2, 1, 0, 0, 0 },
	{ 0, 0, 0, 2, 4, 6, 6, 6, 7, 7, 7, 7, 7, 6, 5, 4, 2, 1, 0, 0 },
	{ 0, 0, 2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 3, 3, 2, 1, 0 },
	{ 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	};

int	kr, kg, kb;

if (yo != 0) {
    	fprintf(stderr, "%s: yo (%d) _must_ be 0\n", __func__, yo);
	return INVALID_PARAM;
	}

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

ox = (src->width % GRIIIZ_SIZE) / 2;
oy = (src->height % GRIIIZ_SIZE) / 2;
#if DEBUG_LEVEL
fprintf(stderr, "%s : X %d-%d Y %d-%d\n", __func__,
					src->width, ox, src->height, oy);
#endif

kr = kg = kb = 0;
for (i=0; i<GRIIIZ_SIZE; i++) {
	for (j=0; j<GRIIIZ_SIZE; j++) {
		kr += mr[i][j];
		kg += mg[i][j];
		kb += mb[i][j];
		}
	}
#if DEBUG_LEVEL
fprintf(stderr, "%s: kr %d kg %d kb %d\n", __func__, kr, kg, kb);
#endif

for (x=ox; x<src->width-(ox+1); x+=GRIIIZ_SIZE) {
    for (y=oy; y<src->height-(oy+1); y+=GRIIIZ_SIZE) {
	cr = cb = cg = 0;
	for (i=0; i<GRIIIZ_SIZE; i++) {
	    for (j=0; j<GRIIIZ_SIZE; j++) {
		Image_getRGB(src, x+i, y+j, &r, &g, &b);
		cr += r;
		cg += g;
		cb += b;
		}
            }

	for (i=0; i<GRIIIZ_SIZE; i++) {
	    for (j=0; j<GRIIIZ_SIZE; j++) {
		r = (cr*mr[j][i] / kr) ;
		g = (cg*mg[j][i] / kg) ;
		b = (cb*mb[j][i] / kb) ; /* XXX / (GRIIIZ_SIZE*GRIIIZ_SIZE); */
		Image_plotRGB(dst, x+i, y+j, r, g, b);
		}
	    }
	}
    }

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
