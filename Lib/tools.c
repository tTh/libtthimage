/*
            TOOLS.C         outils de mise au point
			    (debugging tools)
*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <string.h>
#include   <time.h>
#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	WARNING! this function is not re-entrant!
 */
void Image_pacifier(char *texte, int div)
{
static int	compteur;
int		foo;
char		batons[] = "|/-\\";

if ( texte != NULL )	printf("%s : ", texte);
else			printf("wait ");
if (div<1)	div=1;
foo = (compteur++) / div;
printf("%c\r", batons[foo&0x03]);	fflush(stdout);
}
/*::------------------------------------------------------------------::*/
/*
 *	I think that the 'chrono' system is full broken
 *
 *  You have to look at 'functions.c' for more timing functions.
 *
 */
#define NB_CHRONOS	42

static struct {
	int	in_use;
	time_t	debut;
	time_t	fin;
	} chronos[NB_CHRONOS];

int Image_start_chrono(char *texte, int num)
{
if (num< 0 || num>= NB_CHRONOS)	{
	return BAD_CHRONO;
	}

if (NULL != texte) {
	fprintf(stderr, ">>> start chrono(%d) %s\n", num, texte);
	}

time(&chronos[num].debut);
chronos[num].in_use = 1;
return OLL_KORRECT;
}

long Image_stop_chrono(char *texte, int num)
{
long delta_t;

if (num < 0 || num >= NB_CHRONOS) {
	return -1L;
	}
time(&chronos[num].fin);
chronos[num].in_use = 0;
delta_t = chronos[num].fin - chronos[num].debut; 
if (texte != NULL)
	fprintf(stderr, ">>> chrono(%d):  %s: %ld seconds\n", num, texte, delta_t);

return delta_t;
}
/*::------------------------------------------------------------------::*/
int Image_dump_colormap(RGB_map *map, int verbose)
{
int foo;

if (map == NULL) 	{
	fprintf(stderr, "%s: 'map' ptr is NULL\n", __func__);
	return NULL_DESCRIPTOR;
	}

printf("+---------------------------\n");
printf("| nom de la palette  :  %s\n", map->name);
printf("| nombre de couleurs :  %d\n", map->nbre);
printf("+---------------------------\n");

if (verbose) {
	for (foo=0; foo<map->nbre; foo++) {
		printf("%3d:   %3d  %3d  %3d\n", foo,
				map->red[foo], map->green[foo], map->blue[foo]);
		}
	}

return 0;
}
/*::------------------------------------------------------------------::*/
int Image_dump_descriptor(Image_Desc *im, char *text)
{
fflush(stdout);
printf("/-------- Descriptor Dump (lib v=%s) -----------\n",
                                         IMAGE_VERSION_STRING);

printf("|          why text:   %s\n", text);
printf("|      main pointer:   %p\n", im);
if ( NULL == im )     {
    printf("!\t\tthis is a null descriptor, good luck, Sir...\n");
    return  NULL_DESCRIPTOR;
    }
else {
    printf("|              name:   %s\n", im->name);
    printf("|           comment:   %s\n", im->comment);
    printf("|             magic:   0x%08lx\n", im->magic);
    printf("|        dimensions:   %d  x  %d\n", im->width, im->height);
    printf("|      type/nb_plan:   %d (%s), %d\n", im->type,
						Image_type2str(im->type),
						im->nb_planes);
    printf("|   planes pointers:   %p %p %p %p\n",
                                im->Rpix, im->Gpix, im->Bpix, im->Apix);

    printf("|  errmsg/modified :   %d / %d\n", im->errmsg, im->modified);
    printf("|       xy fenetre :   %15f  %15f\n", im->fx, im->fy);
    printf("|       wh fenetre :   %15f  %15f\n", im->fw, im->fh);
    }
printf("\\-----------------------------------------------------------\n");
fflush(stdout);
return 0;
}
/*::------------------------------------------------------------------::*/
int Image_dump_rect(Image_Rect *rect, char *texte, int flag)
{
printf("Rect:  '%s'\n\t%5d %5d  %5d %5d   $%04lx\n", texte,
			rect->x, rect->y, rect->w, rect->h, rect->reserved);
if (flag) {
	printf(" lower right    %5d %5d\n", rect->x+rect->w, rect->y+rect->h);
	printf(" surface     %8ld\n", (long)rect->w * (long)rect->h);
	}
fflush(stdout);

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/* new 8 octobre 2009 */
static void ligne_char(char c, int t)
{
int	foo;
for (foo=0; foo<t; foo++)
	putc(c, stderr);
putc('\n', stderr);
}
static void ligne_space(char c, int t)
{
int	foo;
putc(c, stderr);
for (foo=0; foo<t-2; foo++)
	putc(' ', stderr);
putc(c, stderr);
putc('\n', stderr);
}

void Image_warning(char caractere, char *lig1, char *lig2, int k)
{
int	len1, len2, len;

len1 = len2 = 0;
if (NULL != lig1)		len1 = strlen(lig1);
if (NULL != lig2)		len2 = strlen(lig2);
if (len2 > len1)		len = len2;
else				len = len1;

#if DEBUG_LEVEL
fprintf(stderr, "%s : largeur = %d\n", __func__, len);
fprintf(stderr, "%s : ligne 1 = %s\n", __func__, lig1);
fprintf(stderr, "%s : ligne 2 = %s\n", __func__, lig2);
#endif

ligne_char(caractere, len+10+k);
ligne_space(caractere, len+10+k);
ligne_char(caractere, len+10+k);

}
/*::------------------------------------------------------------------::*/
int Image_print_rgba(char *txt, RGBA *rgba, int hexa)
{
char	*fmtd = "%-20s: RGBA %4d %4d %4d %4d\n";
char	*fmth = "%-20s: 0xRGBA %02x %02x %02x %02x\n";
FILE	*fp;

if (*txt == '-') {
	fp = stdout;
	txt++;			/* skip the '-' */
	}
else	{
	fp = stderr;
	}
fprintf(fp, hexa ? fmth : fmtd, txt, rgba->r, rgba->g, rgba->b, rgba->a);
fflush(fp);

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/* affichage d'un descripteur de DF3 - new 28feb2008 - ave St Exupery */
int Image_print_DF3head(DensityF3Head *h, char *txt, int flag)
{
int	retval = 0;
int	foo;

if (NULL == h) {
	fprintf(stderr, "%s: null pointer\n", __func__);
	return 666;
	}
if (h->control != MAGIC_OF_DF3)	{
	fprintf(stderr, "%s: bad magic value\n", __func__);
	return 666;
	}

printf("+--- DF3head @ %p  -- %s -- %c\n", h, txt, flag ? 'X' : 'O');
printf("|     magic: %08lx\n",		h->control);
printf("|      name: %s\n", 		h->name);
printf("|      dims: %d %d %d\n",	h->xdim, h->ydim, h->zdim);


foo = h->nbrbytes;
printf("| nbr bytes: %d ",	foo);
if ( (foo!=1) && (foo!=2) && (foo!=4) )	{
	printf("**ERROR**\n");
	retval++;
	}
else	{
	printf("\n");
	}

printf("|     flags: %08x\n",	h->flags);
printf("|     scale: %.g\n",	h->scale);
printf("|  datas @ : %p\n",	h->datas);
printf("+------------------------------------\n");

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
int Image_Overlay_Palette(Image_Desc *im, int xo, int yo)
{
int     x, y, xx, yy, code;
int     r, g, b;

/* No boundchecking here ? */

for (y=0; y<128; y++) {
    yy = y + yo;
    if ( (yy >= 0) && (yy<im->height) ) {
        for (x=0; x<256; x++) {
            xx = x + xo;
            if ( (xx >= 0) && (xx<im->width) )  {
                code = (y<<8) | x;

                r = (code & 0x1f) * 4;
                b = ((code>>5) & 0x1f) * 4;
                g = ((code>>10) & 0x1f) * 4;

                Image_plotRGB(im, xx, yy, r, g, b);
                }
            }
        }
    }

im->modified = 1;
return 0;
}
/*::------------------------------------------------------------------::*/
/*
 *	this function need more security controls
 *				see also: mircol.c
 */
int Image_fabrique_une_mire(Image_Desc *im, RGB_map *map)
{
int     foo, bar;

#if DEBUG_LEVEL
fprintf(stderr, "%s : %p %p\n", __func__, im, map);
#endif
if ( (im->width < 320) || (im->width<200) )
    return IMAGE_TOO_SMALL;

if (NULL != map) {
	fprintf(stderr, "%s:%s map must be null\n", __FILE__, __func__);
	exit(5);
	}
Image_Overlay_Palette(im, 50, 10);

for (foo=0; foo<256; foo++) {
    for (bar=0; bar<10; bar++) {
        Image_plotRGB(im, foo, bar, foo, foo, foo);
        Image_plotRGB(im, foo, 190+bar, 256-foo, 256-foo, 256-foo);
        }
    }

im->modified = 1;
return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	This func make a grid over an image, and need a lot of work.
 */
int Image_Grille(Image_Desc *im, int stepx, int stepy, int rgb)
{
int	foo, bar;
int	r, g, b;

if (rgb < 0) {
	r = rand() % 256;
	g = rand() % 256;
	b = rand() % 256;
	}
else {
	r = g = b = rgb;
	}

for (foo=0; foo<im->width; foo+=stepx) {
	for (bar=0; bar<im->height; bar++)
		Image_plotRGB(im, foo, bar, r, g, b);	
	}
	
for (foo=0; foo<im->height; foo+=stepy)	{
	for (bar=0; bar<im->width; bar++)
		Image_plotRGB(im, bar, foo, r, g, b);	
	}

im->modified = 1;
return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
int Image_print_minmax(Image_Desc *img)
{
int	res[8], foo;

printf("         R     G     B     A\n");
foo = Image_minmax_RGB(img, res);
if (foo) {
	fprintf(stderr, "in %s, minmax rgb -> %d\n", __func__, foo);
	}
printf("min   %4d  %4d  %4d  %4d\n", res[0], res[2], res[4], res[6]);
printf("max   %4d  %4d  %4d  %4d\n", res[1], res[3], res[5], res[7]);
return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
int Image_xy_inside(Image_Desc *img, int x, int y)
{
/*
 * 1er mars 2010 : this func need a g77 binding
 *                                  ???
 */
if ( (x<0) || (y<0) || (x>=img->width) || (y>=img->height) ) {
#if DEBUG_LEVEL > 1
	fprintf(stderr, "%s : %p  %6d  %6d --> FAIL\n", __func__, img, x, y);
#endif
	return 0;
	}

return 1;
}
/*::------------------------------------------------------------------::*/
