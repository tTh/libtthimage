/*
                    combine2.c
		    mixages...
		    ----------
*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
      Le coefficient K va de 0 a 10000 !
*/
int Image_mix(Image_Desc *a, Image_Desc *b, Image_Desc *c, int k)
{
int     x, y;
int     ra, rb, mk;
uint8_t	*pra, *prb, *prk, *pga, *pgb, *pgk, *pba, *pbb, *pbk;
int	foo;

if ( (foo=Image_compare_desc(a, b)) ||
     (foo=Image_compare_desc(a, c)) )
    {
    fprintf(stderr, "%s: images differentes %s\n", __func__, Image_err2str(foo));
    fprintf(stderr, "  a %dx%d b %dx%d c %dx%d\n", 
				a->width, a->height,
				b->width, b->height,
				c->width, c->height);
    return IMG_ARE_DIFFERENT;
    }

#if DEBUG_LEVEL > 1
fprintf(stderr, "Mixing:  %p and %p to %p,  k=%d\n", a, b, c, k);
#endif

mk = 10000 - k;
for (y=0; y<a->height; y++) {
    pra = a->Rpix[y];   prb = b->Rpix[y];   prk = c->Rpix[y];
    pga = a->Gpix[y];   pgb = b->Gpix[y];   pgk = c->Gpix[y];
    pba = a->Bpix[y];   pbb = b->Bpix[y];   pbk = c->Bpix[y];

    for (x=0; x<a->width; x++) {
        ra = pra[x];
        rb = prb[x];
        prk[x] = ((ra * k) + (rb * mk)) / 10000;

        ra = pga[x];
        rb = pgb[x];
        pgk[x] = ((ra * k) + (rb * mk)) / 10000;

        ra = pba[x];
        rb = pbb[x];
        pbk[x] = ((ra * k) + (rb * mk)) / 10000;
        }
    }

return 0;
}
/*::------------------------------------------------------------------::*/
/*							new 6 nov 2001
      Les coefficients K[rgb] vont de 0 a 10000 !
*/
int Image_mix_rgb(Image_Desc *a, Image_Desc *b, Image_Desc *c,
						int kr, int kg, int kb)
{
int	foo, x, y;
uint8_t	*pra, *prb, *prk, *pga, *pgb, *pgk, *pba, *pbb, *pbk;
int	ra, rb, mkr, mkg, mkb;

if ( (foo=Image_compare_desc(a, b)) ||
     (foo=Image_compare_desc(a, c)) )
    {
    fprintf(stderr, "Image mix: images differentes %s\n", Image_err2str(foo));
    fprintf(stderr, "  a %dx%d b %dx%d c %dx%d\n", 
				a->width, a->height,
				b->width, b->height,
				c->width, c->height);
    return IMG_ARE_DIFFERENT;
    }

mkr = 10000 - kr;
mkg = 10000 - kg;
mkb = 10000 - kb;
for (y=0; y<a->height; y++)
    {
    pra = a->Rpix[y];   prb = b->Rpix[y];   prk = c->Rpix[y];
    pga = a->Gpix[y];   pgb = b->Gpix[y];   pgk = c->Gpix[y];
    pba = a->Bpix[y];   pbb = b->Bpix[y];   pbk = c->Bpix[y];

    for (x=0; x<a->width; x++)
        {
        ra = pra[x];
        rb = prb[x];
        prk[x] = ((ra * kr) + (rb * mkr)) / 10000;

        ra = pga[x];
        rb = pgb[x];
        pgk[x] = ((ra * kg) + (rb * mkg)) / 10000;

        ra = pba[x];
        rb = pbb[x];
        pbk[x] = ((ra * kb) + (rb * mkb)) / 10000;

        }
    }

return 0;
}
/*::------------------------------------------------------------------::*/
/*
 *	les deux images sources (a & b) et l'image de destination (d)
 *	doivent avoir les m�mes largeur et hauteur. quand � l'image
 *	de 'ratio', ses dimensions peuvent �tre diff�rentes.
 *
 *	XXX mettre en place un test unitaire !!!
 */
int
Image_trimix(Image_Desc *a, Image_Desc *b, Image_Desc *c, Image_Desc *d, char w)
{
int	x, y, rv, gv, bv, cx, cy;
int	ra, ga, ba, rb, gb, bb, rc, gc, bc;

fprintf(stderr, "Image tri mix (%02X) : NO SANITY CONTROL\n", w);

/*
 *	YES, I know that i can optimize this func for speed.
 *	I do that when a got a zen-er life. Sorry. man vi, man gcc.
 */
for (y=0; y<a->height; y++)
	{
	cy = y % c->height;
	for (x=0; x<a->width; x++)
		{
		cx = x % c->width;
		Image_getRGB(a,  x,  y, &ra, &ga, &ba);
		Image_getRGB(b,  x,  y, &rb, &gb, &bb);
		Image_getRGB(c, cx, cy, &rc, &gc, &bc);

		rv = (ra * rb) + (rc * (256-rb));
		gv = (ga * gb) + (gc * (256-gb));
		bv = (ba * bb) + (bc * (256-bb));

		(d->Rpix[y])[x] = rv/256;
		(d->Gpix[y])[x] = gv/256;
		(d->Bpix[y])[x] = bv/256;
		}
	}

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
