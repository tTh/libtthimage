/*
		bitblt.c
		--------

	Dans ce module, il y a �normement d'optimisations de vitesse
	� faire. Je le sais. Mais mon neurone est en surcharge...
	-------------------------------------------------------
	voir aussi: insert.c

*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	la structure 'z' decrit les pixels sources (dimensions et position)
 *	et les parametres x et y disent ou recopier les pixels dans l'image
 *	de destination.
 */
int Image_get_rect(Image_Desc *s, Image_Rect *z, Image_Desc *d, int x, int y)
{
int	xfoo, yfoo;				/* oh! des 'foo' 2D :-) */
int	xs, ys;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p  %d %d )\n", __func__, s, z, d, x, y);
#endif

/*
 *			NEED MORE EXPLANATION XXX !
 */
#if DEBUG_LEVEL > 1
fprintf(stderr, "GET RECT src  %p   %4d, %4d, %4d, %4d\n",
				s, z->x, z->y, z->w, z->h);
fprintf(stderr, "         dst  %p   %4d, %4d\n", d, x, y);
#endif

if ( (z->w > d->width) || (z->h > d->height) ) {
	fprintf(stderr, "%s: oups, bad rect\n", __func__);
	return 666;
	}

for (yfoo=0; yfoo<z->h; yfoo++)	{
	ys = yfoo + z->y;
	if (ys<0 || ys>s->height-1)
		continue;
	for (xfoo=0; xfoo<z->w; xfoo++)	{
		xs = xfoo + z->x;
		if (xs<0 || xs>s->width-1)
			continue;

		Image_pixel_copy(s, xs, ys, d, x+xfoo, y+yfoo);
		}
	}
return 0;
}
/*::------------------------------------------------------------------::*/
/*
 *	2023-09-30: je comprend rien a ce truc !
 *
 *	recopie d'un rectangle d'une image source par dessus
 *	une image destination.
 */
int Image_put_rect(Image_Desc *s, Image_Rect *z, Image_Desc *d, int x, int y)
{
int	xfoo, yfoo;		
int	xs, ys, xd, yd;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p  %d %d )\n", __func__, s, z, d, x, y);
#endif

#if DEBUG_LEVEL > 1
fprintf(stderr, "PUT RECT src  %p   %4d, %4d, %4d, %4d\n",
			s, z->x, z->y, z->w, z->h);
fprintf(stderr, "         dst  %p   %4d, %4d\n", d, x, y);
#endif

for (yfoo=0; yfoo<z->h; yfoo++)	{
	ys = yfoo + z->y;
	if (ys<0 || ys>s->height-1)
		continue;

	yd = yfoo + y;
	if (yd<0 || yd>d->height-1)
		continue;

	for (xfoo=0; xfoo<z->w; xfoo++)	{
		xs = xfoo + z->x;
		if (xs<0 || xs>s->width-1)
			continue;

		xd = xfoo + x;
		if (xd<0 || xd>d->width-1)
			continue;

		Image_pixel_copy(s, xs, ys, d, xd, yd);
		}
	}

return FUNC_IS_ALPHA;		/* AH BRAVO */
}

/*::------------------------------------------------------------------::*/
/*
 *  coredumper dlmkt !
 */
int Image_copy_rect(Image_Desc *s, Image_Rect *z, Image_Desc *d, int x, int y)
{
int	xs, ys, xd, yd, xx, yy;
int	foo;

fprintf(stderr, ">>> %s ( %p %p %p  %d %d )\n", __func__, s, z, d, x, y );

if (d->magic != MAGIC_OF_IMAGE)	{
	fprintf(stderr, "%s: need Dead Beef\n", __func__);
	return  NOT_AN_IMAGE_DESC;
	}

foo = 0;
for (yy=0; yy<z->h; yy++) {
	ys = yy + z->y;
	if (ys<0 || ys>=s->height)
		continue;

	yd = y + yy;
	if (yd<0 || yd>=d->height)
		continue;

	for (xx=0; xx<z->w; xx++) {
		xs = xx + z->x;
		if (xs<0 || xs>=s->width)
			continue;

		xd = x + xx;
		if (xd<0 || xd>=d->width)
			continue;

		Image_pixel_copy(s, xs, ys, d, xd, yd);
		foo++;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	nouveau 11 jan 00	cree a la rache pour extraitre des images
 *				256x256 afin de tester FFTW :-)
 *
 *		23 dec 01	found a coredump if src image is too small
 *
 */
Image_Desc *Image_create_subimg(Image_Desc *src, Image_Rect *r, int gray)
{
Image_Desc *clone;
int x,y,xs, ys;

fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__, src, r, gray);

/*
 *	sanities controls...
 */
if (src->type != IMAGE_RGB) {
	fprintf(stderr, "'%s' work only on RGB images\n", __func__);
	exit(5);
	}
if ( (r->x<0) || (r->y<0) ) {
	fprintf(stderr, "create_subimg, rect(%d,%d,%d,%d) iznotgoud\n",
					r->x, r->y, r->w, r->h);
	exit(5);
	}
/*	MUST CHECK THAT SOURCE IMAGE IS BIG ENOUGH !	*/

/*
 *	now, we allocate the new image...
 */
clone = Image_alloc(r->w, r->h, src->type);
if (clone == NULL) {
	fprintf(stderr, "CreateSubImage: can't alloc memory...\n");
	exit(5);
	}

for (x=0; x<r->w; x++) {
	xs = x + r->x;
	for (y=0; y<r->h; y++) {
		ys = y + r->y;
		/*printf("dst %4d %4d     ", x, y); */
		/*printf("src %4d %4d     ", xs, ys); */

		if (ys<0 || ys>src->width) {
			Image_plotRGB(clone, x, y, gray, gray, gray);
			}
		else	{
			/* XXX calling this func is a nasty cpu sucker,
			 * so we have to go to a nasty optimize trick */
			/* XXX Image_pixel_copy(src, xs, ys, clone, x, y); */
			(clone->Rpix[y])[x] = (src->Rpix[ys])[xs];
			(clone->Gpix[y])[x] = (src->Gpix[ys])[xs];
			(clone->Bpix[y])[x] = (src->Bpix[ys])[xs];
			}
		}
	}

return clone;
}
/*::------------------------------------------------------------------::*/
/*
http://highwire.atari-users.net/cgi-bin/cvsweb/~checkout~lib/gemlib/rc_intersect.c?rev=1.2
 */
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
int
rc_intersect (const Image_Rect * r1, Image_Rect * r2)
{
int		tx, ty, tw, th, ret;

tx = max (r2->x, r1->x);
tw = min (r2->x + r2->w, r1->x + r1->w) - tx;
	
ret = (0 < tw);
if (ret)
	{
	ty = max (r2->y, r1->y);
	th = min (r2->y + r2->h, r1->y + r1->h) - ty;
		
	ret = (0 < th);
	if (ret)
		{
		r2->x = tx;
		r2->y = ty;
		r2->w = tw;
		r2->h = th;
		}
	}
	
return ret;
}
/*::------------------------------------------------------------------::*/

