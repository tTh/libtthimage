/*
		detect2.c
		---------
*/

#include  <stdio.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *      Warning: this function can be very expensive in memory use
 */
int
Image_pix2ptl(Image_Desc *img, int seuil, Image_PtList *ptl)
{
int		foo, val;
int		x, y;
long		nbre;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d %p )\n", __func__, img, seuilr, ptl);
#endif

nbre = 0;
for (y=0; y<img->height; y++)
	{
	for (x=0; x<img->width; x++)
		{
		val = Image_R_pixel(img, x, y);
		if (val > seuil)
			{
			Image_ptl_add(ptl, x, y, val, 0);
			nbre++;
			}
		}
	}

foo = Image_ptl_write("pixels.ptl", ptl);

#if DEBUG_LEVEL
fprintf(stderr, "pix2ptl: %ld pixels\n", nbre);
#endif

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/

