/*
                    doublesz.c
		----------------
	see also: scale.c halfsize.c
*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	cette fonction alloue une _nouvelle_ image
 *
 *	m�thodes:
 *		0: primitive.
 *		1: semblant d'interpolation.
 *
 */
Image_Desc *
Image_MakeDoubleSize(Image_Desc *image, int methode)
{
int		largeur, hauteur, x, y, xx, yy;
Image_Desc	*sortie;
int		v1, v2, v3, v4;

largeur = image->width * 2;
hauteur = image->height * 2;
sortie = Image_alloc(largeur, hauteur, image->type);
#if DEBUG_LEVEL
fprintf(stderr, "Image double size: %d %d -> %p\n", largeur, hauteur, sortie);
#endif
if ( sortie == NULL )
	{
	fprintf(stderr, "Image make double size: no memory\n");
	exit(5);
	}

/*
 *	transfert de certains attributs...
 */
sortie->modified = 0;
sortie->errmsg = image->errmsg;		/* XXX pointeur fou ? */

if (methode == 0)
	{
	for (x=0; x<image->width; x++)
		{
		xx = x * 2;
		for (y=0; y<image->height; y++)
			{
			yy = y * 2;
			Image_pixel_copy(image, x, y, sortie, xx, yy);
			Image_pixel_copy(image, x, y, sortie, xx+1, yy);
			Image_pixel_copy(image, x, y, sortie, xx, yy+1);
			Image_pixel_copy(image, x, y, sortie, xx+1, yy+1);
			}
		}
	}
else
	{
	for (x=0; x<image->width-1; x++)
		{
		xx = x * 2;
		for (y=0; y<image->height-1; y++)
			{
			yy = y * 2;

			v1 = (image->Rpix[y])[x];
			v2 = (image->Rpix[y])[x+1];
			v3 = (image->Rpix[y+1])[x];
			v4 = (image->Rpix[y+1])[x+1];
			(sortie->Rpix[yy])[xx] = v1;
			(sortie->Rpix[yy])[xx+1] = (v1+v2)/2;
			(sortie->Rpix[yy+1])[xx] = (v1+v3)/2;
			(sortie->Rpix[yy+1])[xx+1] = (v1+v2+v3+v4)/4;

			v1 = (image->Gpix[y])[x];
			v2 = (image->Gpix[y])[x+1];
			v3 = (image->Gpix[y+1])[x];
			v4 = (image->Gpix[y+1])[x+1];
			(sortie->Gpix[yy])[xx] = v1;
			(sortie->Gpix[yy])[xx+1] = (v1+v2)/2;
			(sortie->Gpix[yy+1])[xx] = (v1+v3)/2;
			(sortie->Gpix[yy+1])[xx+1] = (v1+v2+v3+v4)/4;

			v1 = (image->Bpix[y])[x];
			v2 = (image->Bpix[y])[x+1];
			v3 = (image->Bpix[y+1])[x];
			v4 = (image->Bpix[y+1])[x+1];
			(sortie->Bpix[yy])[xx] = v1;
			(sortie->Bpix[yy])[xx+1] = (v1+v2)/2;
			(sortie->Bpix[yy+1])[xx] = (v1+v3)/2;
			(sortie->Bpix[yy+1])[xx+1] = (v1+v2+v3+v4)/4;
			}
		}  /* finbo x */

	}	/* endif */

sortie->modified = 0;		

return sortie;
}
/*::------------------------------------------------------------------::*/
Image_Desc *
Image_MakeDoubleSize_H(Image_Desc *image, int methode)
{
Image_Desc	*sortie;
int		largeur, x, y, r, g, b;
int		r2, g2, b2;

#if DEBUG_LEVEL
fprintf(stderr, "*** %s is experimental. method: %d\n", __func__, methode);
#endif

largeur = image->width * 2;
fprintf(stderr, "%s ---> %d x %d\n", __func__, largeur, image->height);

sortie = Image_alloc(largeur, image->height, image->type);
if ( sortie == NULL )
	{
	fprintf(stderr, "Image make double size H: no memory\n");
	exit(5);
	}

#if DEBUG_LEVEL
Image_dump_descriptor(sortie, "double largeur");
#endif

switch (methode)
	{
	case 0:
	for (x=0; x<image->width; x++)
		{
		for (y=0; y<image->height; y++)
			{
			Image_getRGB(image, x, y, &r, &g, &b);
			Image_plotRGB(sortie, x, y, r, g, b);
			Image_plotRGB(sortie, x+1, y, r, g, b);
			}
		}
	break;

	case 1:
	break;
	}

sortie->modified = 1;

return sortie;
}
/*::------------------------------------------------------------------::*/
