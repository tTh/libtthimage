/*
	ce fichier contient diverses fonctions pour tester
	les trucs et puis les machins.

	en general, c'est des fonctions que je met ici et 
	elles sont appelees par le main dans 'testtga.c'.

	NOTE: il faudrait verifier si les liberations de 
	memoire sont bien faites...
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <math.h>
#include  <string.h>

#include  "../tthimage.h"
#include  "essais.h"

/*		==============================				*/
int essai_draw_paint_rect(char *outga)
{
Image_Desc		*img;
Image_Rect		rect;
int			foo, x, y;

fprintf(stderr, ">>> %s ( '%s' )\n", __func__, outga);

rect.x = 8;		rect.y = 8;
rect.w = 6;		rect.h = 6;

if (NULL==(img=Image_alloc(64, 32, 3))) {
	abort();
	}

for (x=0; x<img->width; x+=2) {
	for (y=0; y<img->height; y+=2) {
		Image_plotRGB(img, x, y, 200, 200, 200);
		}
	}

foo = Image_draw_rect(img, &rect, 192, 128, 64);
if (foo) {
	fprintf(stderr, "%s: err %d draw rect\n", __func__, foo);
	return foo;
	}

rect.x = 24;
foo = Image_paint_rect(img, &rect, 64, 128, 192);
if (foo) {
	fprintf(stderr, "%s: err %d paint rect\n", __func__, foo);
	return foo;
	}

foo = Image_TGA_save(outga, img, 0);
if (foo) {
	fprintf(stderr, "%s: err %d save '%s'\n", __func__, foo, outga);
	return foo;
	}

return OLL_KORRECT;
}

/*::------------------------------------------------------------------::*/
/* nouveau vacances de fevrier 2010 */
int Essayer_les_alphas(char *fname, int k)
{
Image_Desc	*src, *dst;
int		foo;

printf("=========== %s ( '%s' %d ) ===============\n",
				__func__, fname, k);

if (NULL==(src=Image_TGA_alloc_load(fname))) {
	fprintf(stderr, "%s : omg, '%s' is not readable.\n", __func__, fname);
	exit(1);
	}
if (NULL==(dst=Image_clone(src, 0))) {
	fprintf(stderr, "%s : omg, %p is not clonable.\n", __func__, src);
	exit(5);
	}

foo = Image_tampon_alpha_0(src, NULL, dst);
if (foo) Image_print_error(fname, foo);

Image_TGA_save("Pictures/aaaa-tampon.tga", dst, 0);

Image_DeAllocate(dst);		free(dst);
Image_DeAllocate(src);		free(src);

return 42;
}
/*::------------------------------------------------------------------::*/
int Manipuler_les_couleurs(char *fname, int k)
{
Image_Desc	*src, *dst;
int		foo;

printf("=========== %s ( '%s' %d ) ===============\n",
				__func__, fname, k);

if (NULL==(src=Image_TGA_alloc_load(fname))) {
	fprintf(stderr, "%s : omg, '%s' is not readable.\n", __func__, fname);
	exit(1);
	}
if (NULL==(dst=Image_clone(src, 0))) {
	fprintf(stderr, "%s : omg, %p is not clonable.\n", __func__, src);
	exit(5);
	}

/* OK, les deux images doivent etre en place */
fprintf(stderr, "trying %p -> %p\n", src, dst);

foo = Image_BiColor_0(src, dst, 0);
Image_print_error("BiColor 0", foo);

foo = Image_BiColor_1(src, dst, 0, 1);
Image_print_error("BiColor 1", foo);

return WTF_OSEF;
}
/*::------------------------------------------------------------------::*/
int Essai_luminance(char *srcname, int k)
{
Image_Desc	*src, *dst;
int		foo;

fprintf(stderr, "=========== %s ( %s %d )\n", __func__, srcname, k);

if (NULL==(src=Image_TGA_alloc_load(srcname))) {
	fprintf(stderr, "%s : oups, '%s' n'est pas lisible\n",
				__func__, srcname);
	exit(1);
	}
/* on va creer le receptacle des horreurs */
if (NULL==(dst=Image_clone(src, 0))) {
	exit(5);
	}

/* que doit valoir le troisieme parametre ? */
foo = Image_luminance(src, dst, -50);
Image_TGA_save("Pictures/aaaa-luminance--50.tga", dst, 0);
foo = Image_luminance(src, dst, 128);
Image_TGA_save("Pictures/aaaa-luminance-128.tga", dst, 0);
foo = Image_luminance(src, dst, 200);
Image_TGA_save("Pictures/aaaa-luminance-200.tga", dst, 0);
foo = Image_luminance(src, dst, 500);
if (foo) Image_print_error((char *)__func__, foo);

Image_TGA_save("Pictures/aaaa-luminance-500.tga", dst, 0);

Image_DeAllocate(dst);	free(dst);
Image_DeAllocate(src);	free(src);

return 0;
}
/*::------------------------------------------------------------------::*/
int Essais_plot_Map(int k)
{
RGB_map		map;
int		foo;


printf("=========== %s ( %d ) ===============\n",
				__func__, k);

foo = Image_load_color_Map("volcano.map", "Volcano Map", &map);
Image_print_error("chargement Volcano", foo);
foo = Image_plot_square_Map("Pictures/aaaa-sqrmap-volcano.tga", &map, "yo le monde", 0);
Image_print_error("plot square Map", foo);

foo = Image_load_color_Map("neon.map", "Neon Map", &map);
Image_print_error("chargement Neon", foo);
foo = Image_plot_square_Map("Pictures/aaaa-sqrmap-neon.tga", &map, "yo le monde", 0);
Image_print_error("plot square Map", foo);

foo = Image_load_color_Map("primaires.map", "Primaire Map", &map);
Image_print_error("chargement Primaire", foo);
foo = Image_plot_square_Map("Pictures/aaaa-sqrmap-primaires.tga", &map, "yo !", 0);
Image_print_error("plot square Map", foo);

foo = Image_load_color_Map("reduce.map", "reduce Map", &map);
Image_print_error("chargement reduce", foo);
foo = Image_plot_square_Map("Pictures/aaaa-sqrmap-reduce.tga", &map, "yo !", 0);
Image_print_error("plot square Map", foo);

return 0;
}
/*::------------------------------------------------------------------::*/
/*
 * 	raw16bits.c - new 22 septembre 2009 - Petit London
 */
int Essai_raw16bits(char *srcname, int k)
{

printf("=========== %s ( '%s' %d ) ===============\n",
				__func__, srcname, k);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/* new 21 septembre 2009 - essai des reductions chromatiques 		*/
int Essai_color_2_map(char *srcname, int k)
{
Image_Desc	*src, *dst;
int		foo, idx;
char		fname[200];

printf("============== %s ( '%s' %d ) ==================\n",
				__func__, srcname, k);

if (NULL==(src=Image_TGA_alloc_load(srcname))) {
	fprintf(stderr, "%s : oups, '%s' n'est pas lisible\n",
				__func__, srcname);
	exit(1);
	}
/* on va creer le receptacle des horreurs */
if (NULL==(dst=Image_clone(src, 0))) {
	fprintf(stderr, "%s: err clone\n", __func__);
	exit(5);
	}

/* ah bravo ! enfin un test unitaire appelable
 * depuis le test unitaire */
for (idx=0; idx<20; idx++) {
	foo = Image_essai_col_reduce(src, dst, 133, idx);
	printf("le retour de l'essai_col_reduce %d est %d\n", idx, foo);
	if (OLL_KORRECT==foo) {
		sprintf(fname, "aaaa-colredux-%02d.tga", idx);
		Image_TGA_save(fname, dst, 0);
		}
	}

Image_DeAllocate(dst);	free(dst);
Image_DeAllocate(src);	free(src);

return 666;
}
/*::------------------------------------------------------------------::*/
/* nouveau 8 septembre 2009 - essai des operations etranges sur le gris */
int Essai_des_gray_ops(char *srcname, int k)
{

printf("=================  %s ( '%s' %d ) =============\n",
				__func__, srcname, k);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/* nouveau 1 septembre 2009 - ave StExupery */
int Essai_des_mires(char *texte, int ka, int kb)
{
Image_Desc	*img, *tmp;
int		foo;

printf("================ %s ( '%s' %d %d ) ==============\n",
				__func__, texte, ka, kb);

if (NULL==(img=Image_alloc(800, 480, IMAGE_RGB))) {
	fprintf(stderr, "%s : FAIL no memory\n", __func__);
	abort();
	}

foo = Image_mircol_2(img, texte, 0);
Image_print_error("mircol 2", foo);
Image_TGA_save("Pictures/aaaa-mircol-2.tga", img, 0);

tmp = Image_clone(img, 0);
Image_decompose(img, tmp, 0);
Image_TGA_save("Pictures/aaaa-mircol-2-dec.tga", tmp, 0);
Image_DeAllocate(tmp);	free(tmp);

printf("%s -> mircol_3 0\n", __func__);
foo = Image_mircol_3(img, texte, 0);
Image_print_error("mircol 3 0", foo);
Image_TGA_save("Pictures/aaaa-mircol-3-0.tga", img, 0);
printf("%s -> mircol_3 0\n", __func__);
foo = Image_mircol_3(img, texte, 1);
Image_print_error("mircol 3 1", foo);
Image_TGA_save("Pictures/aaaa-mircol-3-1.tga", img, 0);
printf("%s -> mircol_3 2\n", __func__);
foo = Image_mircol_3(img, texte, 2);
Image_print_error("mircol 3 2", foo);
Image_TGA_save("Pictures/aaaa-mircol-3-2.tga", img, 0);

return -1;
}
/*::------------------------------------------------------------------::*/
int Essai_des_lut15bits(char *srcname, int k)
{
int		foo;

printf("=================  %s ( '%s' %d ) =============\n",
				__func__, srcname, k);

foo = Image_lut15_essai(srcname, "aaaa-hf15-lut-0.tga", 0);
Image_print_error("lut 15, 0 = ", foo);

foo = Image_lut15_essai(srcname, "aaaa-hf15-lut-1.tga", 1);
Image_print_error("lut 15, 1 = ", foo);

return 42;
}
/*::------------------------------------------------------------------::*/
/* nouveau - 8 novembre 2009 */
int Essai_des_bitblt(char *srcname, int k)
{
Image_Desc 	*src, *dst;
Image_Rect	rectA, rectB;

printf("=================  %s ( '%s' %d ) =============\n",
				__func__, srcname, k);

if ( NULL == (src=Image_TGA_alloc_load(srcname)) ) {
	fprintf(stderr, "%s : *PAN* sur %s (not found ?)\n", __func__, srcname);
	exit(1);
	}

fprintf(stderr, "source %dx%d\n", src->width, src->height);

dst = Image_clone(src, 0);

rectA.w = src->width;
rectA.h = src->height;

Image_DeAllocate(src);	free(src);
Image_DeAllocate(dst);	free(dst);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Essai_des_gadgrect(char *srcname, int k)
{
/* ce truc est nouveau, du 4 fevrier 2009 */
Image_Desc 	*image;
Image_Rect	coin;
int		foo;

printf("=================  %s ( '%s' %d ) =============\n",
				__func__, srcname, k);

if ( NULL == (image=Image_TGA_alloc_load(srcname)) )
	{
	fprintf(stderr, "%s : Bang sur %s (not found ?)\n", __func__, srcname);
	exit(1);
	}

coin.x = image->width / 3;	coin.y = image->height / 3;
coin.w = image->width / 5;	coin.h = image->height / 7;
Image_dump_rect(&coin, "le coin(pan)", 0);
foo = Image_gadrct_Hsweep_0(image, &coin, 0);

Image_TGA_save("Pictures/aaaa-Hsweep0.tga", image, 0);
Image_calc_plot_histo(image, "Pictures/aaaa-lol-mdr.tga");

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Essai_des_zooms(char *srcname, int k)
{
Image_Desc	*origine, *nucked;
int		foo;

printf("=================  %s ( '%s' %d ) =============\n",
				__func__, srcname, k);

if ( NULL == (origine=Image_TGA_alloc_load(srcname)) ) {
	fprintf(stderr, "%s : pas de '%s' ici\n", __func__, srcname);
	exit(1);
	}
if ( NULL == (nucked=Image_clone(origine, 1)) ) {
	fprintf(stderr, "%s : FailClone of %p\n", __func__, origine);
	exit(1);
	}

foo = Image_essai_zoom(origine, nucked, 1.18, 0.42, 0);
if (foo) {
	fprintf(stderr, "In %s, essai zoom -> %d\n", __func__, foo);
	}
Image_TGA_save("Pictures/aaaa-zoom.tga", nucked, 0);

Image_DeAllocate(nucked);	free(nucked);
Image_DeAllocate(origine);	free(origine);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Essai_des_distances(char *srcname, int nbre, int flag)
{
Image_Desc	*src, *dst;
int		foo;
double		dr, dg, db;

printf("============ %s ( '%s' %d %d ) ============\n", __func__,
					srcname, nbre, flag);

if ( NULL == (src=Image_TGA_alloc_load(srcname)) ) {
	fprintf(stderr, "%s : Bang sur %s\n", __func__, srcname);
	exit(1);
	}
#if DEBUG_LEVEL
fprintf(stderr, "%s : img @ %p\n", __func__, src);
#endif
if ( NULL == (dst=Image_clone(src, 1)) ) {
	fprintf(stderr, "%s : FailClone of %p\n", __func__, src);
	exit(1);
	}

dr = dg = db = 0.0;

for (foo=0; foo<nbre; foo++)
	{
	foo = Image_distance_0(src, dst, &dr, &dg, &db);
	printf("%5d : %g %g %g\n", foo, dr, dg, db);
	}

Image_DeAllocate(src);	free(src);
Image_DeAllocate(dst);	free(dst);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Essai_des_7_segments(char *srcname, int flag)
{
Image_Desc	*origine, *imgtmp;
int		foo;
char		buffer[100];

printf("=========== essai 7 segments sur %s =========\n", srcname);

if ( NULL == (origine=Image_TGA_alloc_load(srcname)) )
	{
	fprintf(stderr, "7seg : Bang sur %s\n", srcname);
	exit(1);
	}
printf("image originale @ %p\n", origine);


if (flag & 1)
	{
	imgtmp = Image_clone(origine, 1);
	foo = Image_7_segments(imgtmp, 9, 0);
	printf("%s : image 7 segments -> %d\n", __FILE__, foo);
	Image_TGA_save("Pictures/aaaa-7seg.tga", imgtmp, 0);
	Image_DeAllocate(imgtmp);	free(imgtmp);
	}
 
if (flag & 2)
	{
	imgtmp = Image_clone(origine, 1);
	sprintf(buffer, "%04d", 42);
	foo = Image_7seg_tag0(imgtmp, buffer, 0);
	printf("%s : image 7 seg tag0 -> %d\n", __FILE__, foo);
	Image_TGA_save("Pictures/aaaa-7seg-tag0.tga", imgtmp, 0);
	Image_DeAllocate(imgtmp);	free(imgtmp);
	}

Image_DeAllocate(origine);	free(origine);

printf("fin essai 7 segs\n");

return 7;
}
/*::------------------------------------------------------------------::*/
/*	les essais de cadres sont partis dans essais2.c			*/
/*::------------------------------------------------------------------::*/
int Filtre_Directionnel(char *srcname, int flags)
{
int		rotule, foo;
Image_Desc	*origine, *imgtmp;
char		chaine[100];
int		filtre[11];
Image_Rect      rect;

static	int	datas_sobel[] =
	{
	1,	2,	1,
	0,	0,	0,
	-1,	-2,	-1,
	1,	128
	};
static	int	datas_prewitt[] =
	{
	1,	1,	1,
	0,	0,	0,
	-1,	-1,	-1,
	1,	128
	};
#if 0
/* WHAT THE HELL is this data thing ? */
static struct offxy
	{ int	x, y; } offsets[] = {
	{ 1, 0 },
	{ 2, 0 },
	{ 2, 1 },
	{ 2, 2 },
	{ 1, 2 },
	{ 0, 2 },
	{ 0, 1 },
	{ 0, 0 }
	};
#endif

printf("========= Filtrage directionnel sur %s =======\n", srcname);

if ( NULL == (origine=Image_TGA_alloc_load(srcname)) ) {
	fprintf(stderr, "%s : Bang sur %s\n", __func__, srcname);
	exit(1);
	}
printf("image originale @ %p\n", origine);

imgtmp = Image_clone(origine, 1);

if (flags)	memcpy(filtre, datas_prewitt, sizeof(filtre));
else		memcpy(filtre, datas_sobel, sizeof(filtre));

for (rotule=0; rotule<8; rotule++)
	{
	foo = Image_convolueur_2(origine, imgtmp, filtre, "rgb");
	printf("=== Passe %d retour %d\n", rotule, foo);

	Image_raz_sides(imgtmp);

	sprintf(chaine, "Pictures/aaaa_fildir_%x.tga", rotule);
	Image_TGA_save(chaine, imgtmp, 0);

	/* de la scorie ?
	rect.x = offsets[rotule].x * origine->width;
	rect.y = offsets[rotule].y * origine->height;
	Image_dump_rect(&rect, "filtre direct", 0);
	*/

	Image_rotate_filtre(filtre);
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/* nouveau 31 aout 2008 - ave St Exupery */
int Test_des_trucs_2x2(char *srcname, int k)
{
int		foo;
Image_Desc	*origine, *imgtmp;

printf("===== LES TRUCS 2 x 2 ===[ %s ]=[ %d ]=====\n", srcname, k);

if ( NULL == (origine=Image_TGA_alloc_load(srcname)) )
	{
	fprintf(stderr, "%s : Bang sur %s\n", __func__, srcname);
	exit(1);
	}
printf("image originale @ %p\n", origine);

imgtmp = Image_clone(origine, 1);
if (NULL == imgtmp) {
	fprintf(stderr, "fail in %s - %s\n", __func__, __FILE__);
	abort();
	}
foo = Image_2x2_contours_0(imgtmp);
Image_TGA_save("Pictures/aaaa-2x2-a.tga", imgtmp, 0);

Image_seuil_RGB(origine, imgtmp, 64, 128, 192);
foo = Image_2x2_contours_0(imgtmp);
Image_TGA_save("Pictures/aaaa-2x2-b.tga", imgtmp, 0);

Image_AutoSeuilRGB(origine, imgtmp);
foo = Image_2x2_contours_0(imgtmp);
Image_TGA_save("Pictures/aaaa-2x2-c.tga", imgtmp, 0);

Image_DeAllocate(imgtmp);  free(imgtmp);
Image_DeAllocate(origine);  free(origine);

return 42;
}
/*::------------------------------------------------------------------::*/
/* new 14 septembre 2008 /o\ 51 ans... */
int Test_des_warpings(char *srcname, int k)
{
Image_Desc	*src, *dst;
int		foo, ax, ay;

printf("====== essais Warping sur %s (k=%d) ====\n", srcname, k);

if (NULL == (src = Image_TGA_alloc_load(srcname)) ) {
	fprintf(stderr, "Err loading '%s'\n", srcname);
	exit(5);
	}
dst = Image_clone(src, 0);

foo = Image_warp_essai_0(src, dst, 33.5, 100, 100);
Image_TGA_save("warp-essai-0.tga", dst, 0);

foo = Image_center_rotate(src, dst, 21.5);
Image_TGA_save("warp-center-rotate.tga", dst, 0);

ax = (src->width * 2) / 3;
ay = (src->height * 2) / 3;
foo = Image_shift_xy(src, dst, ax, ay);
Image_TGA_save("warp-shift-xy.tga", dst, 0);
foo = Image_shift_x(src, dst, ax);
Image_TGA_save("warp-shift-x.tga", dst, 0);
foo = Image_shift_y(src, dst, ay);
Image_TGA_save("warp-shift-y.tga", dst, 0);

Image_DeAllocate(src);  free(src);
Image_DeAllocate(dst);  free(dst);

return 42;
}
/*::------------------------------------------------------------------::*/
/* new 7 avril 2007, essai des mosaiques */
int Test_des_mosaiques(char *srcname, int k)
{
Image_Desc	*src, *dst;
int		foo;

fprintf(stderr, "============== %s : k  %d ======\n", __func__, k);

if (NULL == (src = Image_TGA_alloc_load(srcname)) )
	{
	fprintf(stderr, "%s : Err loading '%s'\n", __func__, srcname);
	exit(5);
	}
dst = Image_clone(src, 0);

foo = Image_mosaic_0(src, dst, 6, 6, 0);
Image_TGA_save("Pictures/aaaa-mosaic-0.tga", dst, 0);
foo = Image_mosaic_simple(src, dst);
Image_TGA_save("Pictures/aaaa-mosaic-s.tga", dst, 0);

Image_DeAllocate(src);	free(src);
Image_DeAllocate(dst);	free(dst);

return 42;
}
/* quelque part, il doit aussi y avoir un test des grilles */

/*::------------------------------------------------------------------::*/
/* new 11 decembre 2007 - ave St Exupery */
static double fonction(int x, int y, int param)
{
double 	value;
if (param)
	value = 0.333333 + sin((double)x) + cos((double)y);
else
	value = 0.333333 + sin((double)x) * cos((double)y);
return value;
}
int Test_hf15_synth_fromfunc(char *fname)
{
Image_Desc 	*image;
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, "in %s, param '%s' not used ?\n", __func__, fname);
#endif

image = Image_alloc(256, 256, 3);

foo = Image_hf15_synth_fromfunc0(image, 33, fonction);
fprintf(stderr, "Synth from func %d\n", foo);
Image_TGA_save("Pictures/aaaa-synthfunc.tga", image, 0);

return 0;
}
/*::------------------------------------------------------------------::*/
int Test_hf15_synth_fromPtl(char *iname, char *oname)
{
Image_Desc	*src, *dst;
Image_PtList	*ptl;
Image_Rect	boite;
int		x, y, h, foo;
int		r, g, b;

printf("======= Synth from PointList ===========\n");

if (NULL == (src = Image_TGA_alloc_load(iname)) )
	{
	fprintf(stderr, "%s: Err loading '%s'\n", __func__, iname);
	exit(5);
	}
dst = Image_clone(src, 0);
ptl = Image_ptl_alloc(300, "pix2ptl");
printf("point list @ %p\n", ptl);

for (foo=0; foo<6000; foo++)
	{
	x = rand() % (src->width - 5);
	y = rand() % (src->height - 5);
	Image_getRGB(src, x, y, &r, &g, &b);
	h = r + g + b;
	Image_ptl_add(ptl, x, y, h, 0);
	}

Image_ptl_dump(ptl, "apres randomz");
memset(&boite, 0, sizeof(Image_Rect));
foo = Image_ptl_boundingbox(ptl, &boite);
Image_print_error("ptl bounding box", foo);
Image_dump_rect(&boite, "ptl bounding box", 1);

foo = Image_hf15_synth_1(dst, ptl);
Image_TGA_save("Pictures/aaaa-synthptl.tga", dst, 0);

Image_DeAllocate(src);	free(src);
Image_DeAllocate(dst);	free(dst);

Image_ptl_kill(ptl, __func__);

return 0;
}
/*::------------------------------------------------------------------::*/
int Test_funcs_hf15(char *srcname, int flag)
{
Image_Desc	*src, *dst;
int		foo;
int		minimum, maximum;

fprintf(stderr, "entering ================>> %s\n", __func__);

if (NULL == (src = Image_TGA_alloc_load(srcname)) )
	{
	fprintf(stderr, "Error loading '%s'\n", srcname);
	exit(5);
	}

if (flag)
	{
	fprintf(stderr, "mmmm flag is %02x, need some debug...\n", flag);
	}

fprintf(stderr, "picture '%s' : %d x %d\n", srcname, src->width, src->height);

foo = Image_hf15_rgb2hf(src, src, 0);
printf("rgb2hf -> %d\n", foo);
Image_hf15_save_PGM("Pictures/aaaa-hf15-src.pgm", src, "from 'essais.c'");

dst = Image_clone(src, 0);
Image_TGA_save("Pictures/aaaa-clone-hf15.tga", dst, 0);

foo = Image_plot_histo_hf15(src, "aaaa-histo-src.tga", "from essais.c", 0, 0);
fprintf(stderr, "retour plottage de l'histo = %d\n", foo);

foo = Image_hf15_sqrt(src, dst);
foo = Image_plot_histo_hf15(dst, "aaaa-histo-sqrt.tga", "from essais.c", 0, 0);
Image_hf15_save_PGM("Pictures/aaaa-hf15-sqrt.pgm", src, "from 'essais.c'");
fprintf(stderr, "Ok, done %d sqrt\n", foo);

Image_copy(src, dst);
foo = Image_hf15_noise_2(dst, 42, 5, 0, 0);
Image_TGA_save("Pictures/aaaa-hf15-noise2.tga", dst, 0);

Image_copy(src, dst);
foo = Image_hf15_noise_3(dst, 42.42, 0);
Image_TGA_save("Pictures/aaaa-hf15-noise3.tga", dst, 0);

Image_copy(src, dst); 
foo = Image_hf15_rgb2hf(dst, dst, 0);
/* calculer le min et le max AVANT */
Image_hf15_calc_minmax(dst, "Avant le sweep", &minimum, &maximum);
foo = Image_hf15_sweep(dst, 42, 400, 0);
/* calculer le min et le max APRES */
Image_hf15_calc_minmax(dst, "Apres le sweep", &minimum, &maximum);

Image_TGA_save("Pictures/aaaa-hf15-sweep-c.tga", dst, 0);
Image_hf15_save_PGM("Pictures/aaaa-hf15-sweep-c.pgm", dst, "from essais.c");

Image_DeAllocate(src);	free(src);
Image_DeAllocate(dst);	free(dst);
return IMAGE_FAIL;
}
/*::------------------------------------------------------------------::*/
int Test_vignettes(char *srcname, int k)
{
Image_Desc	*src, *dst;
int		foo;

if (NULL == (src = Image_TGA_alloc_load(srcname)) )
	{
	fprintf(stderr, "Err loading '%s'\n", srcname);
	exit(5);
	}
dst = Image_clone(src, 0);

foo = Image_vignetize_x_0(src, dst, k);
Image_print_error("vignetize_x_0", foo);

Image_TGA_save("Pictures/aaaa-vignetize_x0.tga", dst, 0);

Image_DeAllocate(src);	free(src);
Image_DeAllocate(dst);	free(dst);

return 42;
}
/*::------------------------------------------------------------------::*/
int Test_Classif(char *srcname, int k)
{
Image_Desc	*src, *dst;
int		foo;

fprintf(stderr, "============= %s sur %s =======\n", __func__, srcname);

#define DC  50
static Une_Classe_Sph classs[] = 
	{
	{ 127, 127, 127,   DC,   127, 127, 127 },
	{ 255, 255, 255,   DC,   255, 255, 255 },
	{ 255,   0,   0,   DC,   255,   0,   0 },
	{   0,   0, 255,   DC,     0,   0, 255 },
	{   0, 255,   0,   DC,     0, 255,   0 }
	};

if (NULL == (src = Image_TGA_alloc_load(srcname)) ) {
	fprintf(stderr, "Err loading '%s'\n", srcname);
	exit(5);
	}
dst = Image_clone(src, 0);

foo = Image_classif_0(src, dst);
Image_print_error("essai classif 0", foo);
Image_TGA_save("aaaa-classif0.tga", dst, 0);

foo = Image_classif_1(src, dst, classs, 5, 0);
Image_print_error("essai classif 1", foo);
Image_TGA_save("aaaa-classif1.tga", dst, 0);

Image_DeAllocate(src);	free(src);
Image_DeAllocate(dst);	free(dst);

return 42;
}
/*::------------------------------------------------------------------::*/
/* new 24 avril 2007, essais sur le module rgbmask.c			*/
int Test_RGBmask(char *srcname)
{
Image_Desc *source, *image ;
int		foo;

fprintf(stderr, "=============== %s on %s ==============\n", __func__, srcname);


source = Image_TGA_alloc_load(srcname);
image = Image_clone(source, 0);

foo = Image_rgbmask_H(source, image, 0);
Image_TGA_save("aaaa-rgbmaskH.tga", image, 0);
foo = Image_rgbmask_V(source, image, 0);
Image_TGA_save("aaaa-rgbmaskV.tga", image, 0);
foo = Image_rgbmask_2(source, image, 0);
Image_TGA_save("aaaa-rgbmask2.tga", image, 0);
foo = Image_rgbmask_R(source, image, 0);
Image_TGA_save("aaaa-rgbmaskR.tga", image, 0);

Image_DeAllocate(source);	free(source);
Image_DeAllocate(image);	free(image);


return  42;
}
/*::------------------------------------------------------------------::*/
/* new 9 avr 2007, tests du module col_xyz.c				*/

void Test_rgb2xyz(void)
{
int	r, g, b, foo;
double	dx, dy, dz;
double	minx, maxx, miny, maxy, minz, maxz;

minx = miny = minz =  999999.9;
maxx = maxy = maxz = -999999.9;

for (r=0; r<256; r+=1)
	{
	for (g=0; g<256; g+=1)
		{
		for (b=0; b<256; b+=1)
			{
			foo = Image_pix_rgb2xyz_d(r, g, b, &dx, &dy, &dz);
			if (dx > maxx)		maxx = dx;
			if (dx < minx)		minx = dx;
			if (dy > maxy)		maxy = dy;
			if (dy < miny)		miny = dy;
			if (dz > maxz)		maxz = dz;
			if (dz < minz)		minz = dz;
			}
		}
	}
fprintf(stderr, "fin test rgb2xyz\n");
fprintf(stderr, "%12.6f     X  %12.6f\n", minx, maxx);
fprintf(stderr, "%12.6f     Y  %12.6f\n", miny, maxy);
fprintf(stderr, "%12.6f     Z  %12.6f\n", minz, maxz);
}
/*::------------------------------------------------------------------::*/
/* added 24 mai 2009, because I need a reliable timestamp utility for
 * my big POV project, "Parking Maudit".
 */
int Essai_des_marquages(char *srcname, int nombre, char *texte)
{
Image_Desc	*src, *dst;
int		foo;

if (NULL == (src = Image_TGA_alloc_load(srcname)) )
	{
	fprintf(stderr, "Err in %s when loading '%s'\n", __func__, srcname);
	exit(5);
	}

printf("le nombre est %d\n", nombre);

dst = Image_clone(src, 1);

fprintf(stderr, ">>>> %s : src (%s) %p dst %p\n", __func__, srcname, src, dst);

foo = Image_marque_timestamp(dst, texte, NULL, 0);
Image_TGA_save("Pictures/aaaa-timestamp-0.tga", dst, 0);

Image_copy(src, dst);
foo = Image_marque_timestamp(dst, texte, NULL, 1);
Image_TGA_save("Pictures/aaaa-timestamp-1.tga", dst, 0);

return 42;
}
/*::------------------------------------------------------------------::*/
void Test_double_size(void)
{
Image_Desc *src, *dst;

src = Image_alloc(50, 50, 3);
Image_clear(src, 0, 0, 0);

Image_marque_0(src, 128);

Image_plotRGB(src, 11, 2, 255, 255, 255);
Image_plotRGB(src, 12, 2, 255, 255, 255);
Image_plotRGB(src, 13, 2, 255, 255, 255);

dst = Image_MakeDoubleSize(src, 1);
Image_TGA_save("Pictures/aaaa_dblsz.tga", dst, 0);
Image_DeAllocate(dst);	free(dst);

dst = Image_MakeDoubleSize_H(src, 0);
Image_TGA_save("Pictures/aaaa_dblszh0.tga", dst, 0);
Image_DeAllocate(dst);	free(dst);
dst = Image_MakeDoubleSize_H(src, 1);
Image_TGA_save("Pictures/aaaa_dblszh1.tga", dst, 0);
Image_DeAllocate(dst);	free(dst);

Image_DeAllocate(src);	free(src);
}
/*::------------------------------------------------------------------::*/
/* new, around Hallowen 2009 */
int Test_new_scale(char *fnamesrc, int k)
{
Image_Desc *src, *dst;
int		foo;

#define WDST 512
#define HDST 512

fprintf(stderr, "============= %s ( '%s' ) ================\n",
				__func__, fnamesrc);
src = Image_TGA_alloc_load(fnamesrc);
if (NULL == src)
	{
	fprintf(stderr, "? %s non existantz ?\n", fnamesrc);
	abort();
	}
Image_marque_0(src, 255);
Image_TGA_save("Pictures/aaaa_new_scale_0.tga", src, 0);

dst = Image_alloc(WDST, HDST, 3);
foo = Image_scale_bilinear(src, dst, 0);
printf("retour scale bilinear -> %d\n", foo);
foo = Image_TGA_save("Pictures/aaaa_new_scale_bilin.tga", dst, 0);
#if DEBUG_LEVEL
fprintf(stderr, "saving in %s -> %d\n", __func__, foo);
#endif
Image_DeAllocate(dst);	free(dst);

dst = Image_alloc(WDST, HDST, 3);
foo = Image_scale_simple(src, dst, 0);
printf("retour scale simple -> %d\n", foo);
Image_TGA_save("Pictures/aaaa_new_scale_simple.tga", dst, 0);

Image_DeAllocate(dst);	free(dst);
Image_DeAllocate(src);	free(src);

return 0;
}
/*::------------------------------------------------------------------::*/
/*
 *	2023-09-30 this function need more work
 */
int Test_copie_de_rectangles(char *nomsource)
{
Image_Desc *source, *image, *finale;
int	foo, w, h, x, y;
Image_Rect rect;
char	*cptr;

printf("======= test des copies de rectangles ========\n");

source = Image_TGA_alloc_load(nomsource);
if (NULL==source) {
	fprintf(stderr, "fail on '%s'\n", nomsource);
	return -1;
	}
printf("'%s' loaded : %dx%d\n", nomsource, source->width, source->height);

cptr = "wou.tga";
image = Image_TGA_alloc_load(cptr);
if (NULL==image) {
	fprintf(stderr, "fail on '%s'\n", cptr);
	Image_DeAllocate(source);	free(source);
	return -1;
	}

finale = Image_clone(source, 1);
rect.x = 50;	rect.y = 50;
rect.h = 100;	rect.w = 160;
foo = Image_copy_rect(image, &rect, finale, 10, 10);
Image_print_error("Image copy rect", foo);

Image_TGA_save("Pictures/aaaa_copyrect.tga", finale, 0);
Image_DeAllocate(image);	free(image);

/* essai de la fonction 'Image_overlay' */
w = source->width / 4;
h = source->height / 4;

image = Image_alloc(w, h, 3);
Image_pattern_000(image, 0x55);
Image_copy(source, finale);
/* orly bad code here */
for (x=-w; x<=w; x+=w)
	{
	for (y=-h; y<=h; y+=h)
		{
		fprintf(stderr, "    *** overlaying to %d, %d\n", x, y);
		foo = Image_overlay(source, finale, x, 10);
		Image_print_error("Image overlay", foo);
		}
	}
Image_TGA_save("Pictures/aaaa_overlay.tga", finale, 0);

Image_copy(source, finale);
foo = Image_overlay_mix(source, finale, 30, 40, 5000);
Image_TGA_save("Pictures/aaaa_overlay_mix.tga", finale, 0);

Image_DeAllocate(image);	free(image);
Image_DeAllocate(source);	free(source);
Image_DeAllocate(finale);	free(finale);

return 0;
}
/*::------------------------------------------------------------------::*/
/* new: Sat Aug 10 15:32:01 UTC 2024 */
int Essai_Televisions(char *nomsource, int k)
{
Image_Desc	*image, *resultat;
int		foo;

fprintf(stderr, "================ Televisions %s %d ========\n", nomsource, k);


image = Image_TGA_alloc_load(nomsource);
printf("%s : image loaded %p\n", __func__, image);
resultat = Image_clone(image, 0);
printf("%s : image cloned %p\n", __func__, resultat);

foo = Image_TV_old(image, resultat, 127, 127, 127);
printf("\tTV old -> %d\n", foo);
Image_TGA_save("aaaa_tv_old.tga", resultat, 0);

foo = Image_TV_pix_0(image, resultat, 7, 7, 0);
printf("\tTV pix 0 -> %d\n", foo);
Image_TGA_save("aaaa_tv_pix_0.tga", resultat, 0);

foo = Image_TV_cplus_v1(image, resultat, 7);
printf("\tTV cplus v1 -> %d\n", foo);
Image_TGA_save("aaaa_tv_cplusV1.tga", resultat, 0);


Image_DeAllocate(image);	free(image);
Image_DeAllocate(resultat);	free(resultat);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Test_Egalisations(char *nomsource, int k)
{
Image_Desc *image, *deuxieme, *troisieme;
int     x, y;

fprintf(stderr, "================ Egalisations %s %d =====\n", nomsource, k);

image = Image_TGA_alloc_load(nomsource);
printf("image loaded %p\n", image);

deuxieme = Image_clone(image, 0);
printf("image 2 cloned %p\n", deuxieme);
Image_egalise_mono_0(image, deuxieme, 0);

troisieme = Image_clone(image, 0);
printf("image 3 cloned %p\n", troisieme);
Image_egalise_RGB(image, troisieme, 0);

for (x=0; x<image->width; x++) {
	if (x < image->width/3) {
		for (y=0; y<image->height; y++) {
			Image_pixel_copy(image, x, y, troisieme, x, y);
			}
		}
	else
	if (x > image->width/3*2) {
		for (y=0; y<image->height; y++) {
			Image_pixel_copy(deuxieme, x, y, troisieme, x, y);
			}
		}

#if DEBUG_LEVEL > 2
	printf("%4d\r", x);	fflush(stdout);
#endif
	}

printf("saving resultat...\n");
Image_marque_1(troisieme, "Egalisations", 0);
Image_TGA_save("aaaa_egal.tga", troisieme, 0);
Image_DeAllocate(image);	free(image);
Image_DeAllocate(deuxieme);	free(deuxieme);
Image_DeAllocate(troisieme);	free(troisieme);

return 42;
}
/*::------------------------------------------------------------------::*/
int Test_Dithering(char *nomsource, int k)
{
Image_Desc	*source, *image, *deuxieme, *finale;
Image_Rect	rect;
int		foo, wf, hf;
int		x2, y2;
RGBA		ink, pap;

fprintf(stderr, "============== Dithering on %s (%d) =======\n", nomsource, k);

source = Image_TGA_alloc_load(nomsource);
if (NULL == source) {
	fprintf(stderr, "%s:%s err load %s\n", __FILE__, __func__, nomsource);
	exit(5);
	}
#if DEBUG_LEVEL
Image_dump_descriptor(source, "just after load");
#endif

/*
 * est-ce bien necessaire ?
 */
/* Image_egalise_RGB(source, source, 0); */
wf = source->width;		hf = (source->height) * 3;
x2 = source->width / 2;		y2 = source->height / 2;
rect.w = x2;			rect.h = y2;
rect.x = 0;			rect.y = 0;

ink.r = 255, ink.g = 255, ink.b = 100;		ink.a = 255;
pap.r =  30, pap.g =  30, pap.b = 155;		pap.a = 255;

finale = Image_alloc(wf, hf, 3);
if (NULL == finale) {
	fprintf(stderr, "%s:%s:%d err alloc image\n",
			__FILE__, __func__, __LINE__);
	exit(5);
	}
Image_clear(finale, 0, 200, 0);
printf("image finale %dx%d allocated at %p\n", wf, hf, finale);

image = Image_MakeHalfSize(source, 0);
if (NULL == image) {
	fprintf(stderr, "%s:%s:%d err halfsize image\n",
			__FILE__, __func__, __LINE__);
	exit(5);
	}
printf("%s: image Half at %p\n", __func__, image);

Image_DeAllocate(source);	free(source);
Image_copy_rect(image, &rect, finale, 0, 0);

deuxieme = Image_clone(image, 0);
printf("image 2 cloned %p\n\n", deuxieme);

foo = Image_dither_Bayer_0(image, deuxieme, 255);
printf("retour Dither_Bayer_0 = %d\n", foo);
Image_copy_rect(deuxieme, &rect, finale, x2, 0);
Image_txt1_box_0(finale, "Dither_Bayer_0", x2, 0, 4, &pap, &ink, 0);

foo = Image_dither_seuil_random(image, deuxieme, 255);
printf("retour Seuil_Random = %d\n", foo);
Image_copy_rect(deuxieme, &rect, finale, x2, y2);
Image_txt1_box_0(finale, "Seuil_Random", x2, y2, 4, &pap, &ink, 0);

foo = Image_dither_simple_error(image, deuxieme, 255);
printf("retour Dither_SimpleError = %d\n", foo);
Image_copy_rect(deuxieme, &rect, finale, 0, y2);
Image_txt1_box_0(finale, "Simple_Error", 0, y2, 4, &pap, &ink, 0);

foo = Image_dither_double_seuil(image, deuxieme, 100, 200, 255);
printf("retour Dither_DoubleSeuil = %d\n", foo);
Image_copy_rect(deuxieme, &rect, finale, 0, y2*2);
Image_txt1_box_0(finale, "DoubleSeuil", 0, y2*2, 4, &pap, &ink, 0);

foo = Image_dither_3x3_0(image, deuxieme, 127);
printf("retour Dither_3x3_0 = %d\n", foo);
Image_copy_rect(deuxieme, &rect, finale, x2, y2*2);
Image_txt1_box_0(finale, "3x3 0", x2, y2*2, 4, &pap, &ink, 0);

foo = Image_dither_3x3_1(image, deuxieme, 255);
printf("retour Dither_3x3_1 = %d\n", foo);
Image_copy_rect(deuxieme, &rect, finale, 0, y2*3);
Image_txt1_box_0(finale, "3x3 1", 0, y2*3, 4, &pap, &ink, 0);

foo = Image_dither_3x3_2(image, deuxieme, 127);
printf("retour Dither_3x3_2 = %d\n", foo);
Image_copy_rect(deuxieme, &rect, finale, x2, y2*3);
Image_txt1_box_0(finale, "3x3 2", x2, y2*3, 4, &pap, &ink, 0);

foo = Image_dither_bayer8x8rnd(image, deuxieme, 5000, 0);
printf("retour Dither bayer8x8rnd = %d\n", foo);
Image_copy_rect(deuxieme, &rect, finale, x2, y2*4);
Image_txt1_box_0(finale, "bayer8xrnd rgb", x2, y2*4, 4, &pap, &ink, 0);

foo = Image_dither_bayer8x8rnd(image, deuxieme, 5000, 1);
printf("retour Dither bayer8x8rnd = %d\n", foo);
Image_copy_rect(deuxieme, &rect, finale, 0, y2*4);
Image_txt1_box_0(finale, "bayer8x8rndgrey", 0, y2*4, 4, &pap, &ink, 0);

printf("++++++ test dither\n");
Image_dump_descriptor(finale, "just before save");
Image_TGA_save("aaaa_dithering.tga", finale, 0);
Image_DeAllocate(image);	free(image);
Image_DeAllocate(deuxieme);	free(deuxieme);
Image_DeAllocate(finale);	free(finale);
printf("++++++ fin essais dither\n");

return 0;
}
/*::------------------------------------------------------------------::*/
/*
 * 	test des marquages 
 */
int Test_des_marquages(char *srname, int k1, int k2)
{

/*
 *		please insert useless code here
 */

return -1;
}
/*::------------------------------------------------------------------::*/
/* new 21 juillet 2007 - ave St Exupery */
int Test_Effects_A(char *nomsource, int flag)
{
Image_Desc *source, *image, *deuxieme, *finale;
int	foo;
int	x, y, x2, y2;
RGBA		ink, pap;

printf("=== %s %s === %s === %d ===\n", __FILE__, __func__, nomsource, flag);

if (flag) {
	fprintf(stderr, "%s: flag is %d ?\n", __func__, flag);
	}

ink.r = 255, ink.g = 255, ink.b = 100;		ink.a = 255;
pap.r =  30, pap.g =  30, pap.b = 155;		pap.a = 255;

source = Image_TGA_alloc_load(nomsource);
if (NULL==source) {
	fprintf(stderr, "%s : il manque '%s'\n", __func__, nomsource);
	exit(1);
	}
printf("test effect (%d): image loaded %p\n", flag, source);
/*
 * est-ce bien necessaire ?
 */
/* non Image_egalise_RGB(source, source, 0); XXX */

x2 = source->width / 2;		y2 = source->height / 2;

finale = Image_clone(source, 0);
printf("image finale cloned %p\n", finale);
image = Image_MakeHalfSize(source, 0);
Image_DeAllocate(source);	free(source);

deuxieme = Image_clone(image, 0);
printf("image 2 cloned %p\n\n", deuxieme);

	/* first shoot */
foo = Image_water(image, deuxieme, 15);
if (foo) fprintf(stderr, "water -> %d\n", foo);
for (x=0; x<x2; x++)
	for (y=0; y<y2; y++)
		Image_pixel_copy(deuxieme, x, y, finale, x, y);
Image_txt1_box_0(finale, "Water", 0, 0, 4, &pap, &ink, 0);

foo = Image_noise(image, deuxieme, 42);
for (x=0; x<x2; x++)
	for (y=0; y<y2; y++)
		Image_pixel_copy(deuxieme, x, y, finale, x+x2, y);
Image_txt1_box_0(finale, "noise", x2, 0, 4, &pap, &ink, 0);

foo = Image_center_rotate(image, deuxieme, 23.6);
for (x=0; x<x2; x++)
	for (y=0; y<y2; y++)
		Image_pixel_copy(deuxieme, x, y, finale, x, y+y2);
Image_txt1_box_0(finale, "center rotate", 0, y2, 4, &pap, &ink, 0);

foo = Image_effect_x_5(image, deuxieme, 126, 127, 127);
for (x=0; x<x2; x++)
	for (y=0; y<y2; y++)
		Image_pixel_copy(deuxieme, x, y, finale, x+x2, y+y2);
Image_txt1_box_0(finale, "effect x 5", x2, y2, 4, &pap, &ink, 0);

Image_TGA_save("Pictures/aaaa_effects_a.tga", finale, 0);

	/* second shoot */
foo = Image_scratch(image, deuxieme, 15);
for (x=0; x<x2; x++)
	for (y=0; y<y2; y++)
		Image_pixel_copy(deuxieme, x, y, finale, x, y);
Image_txt1_box_0(finale, "Scratch", 0, 0, 4, &pap, &ink, 0);

foo = Image_effect_x_2(image, deuxieme, 126, 127, 127);
for (x=0; x<x2; x++)
	for (y=0; y<y2; y++)
		Image_pixel_copy(deuxieme, x, y, finale, x+x2, y+y2);
Image_txt1_box_0(finale, "effect x 2", x2, y2, 4, &pap, &ink, 0);

foo = Image_swap_nibbles(image, deuxieme);
for (x=0; x<x2; x++)
	for (y=0; y<y2; y++)
		Image_pixel_copy(deuxieme, x, y, finale, x, y+y2);
Image_txt1_box_0(finale, "swap nibbles", 0, y2, 4, &pap, &ink, 0);

Image_TGA_save("Pictures/aaaa_effects_b.tga", finale, 0);

Image_DeAllocate(image);	free(image);
Image_DeAllocate(deuxieme);	free(deuxieme);
Image_DeAllocate(finale);	free(finale);

return 42;
}
/*::------------------------------------------------------------------::*/
int Test_des_tamppools(char *imgname, int param)
{
void	*ptr;
int	foo;

printf("=== test des tamppools === %s === %d ===\n", imgname, param);

Image_Pool_infos(0);

ptr = Image_Pool_create("pour un essai", 10, 320, 200);
foo = Image_Pool_describe(ptr, "pour voir...", 0);
foo = Image_Pool_destroy(ptr, "Kill Bill !!!", 0);

Image_Pool_infos(1);

return 42;
}
/*::------------------------------------------------------------------::*/
int Test_des_filtres(char *srcname, int k)
{
Image_Desc	*src, *dst;
int		foo;

printf("=============== Filtres =====[ %s ]====[ %d ]====\n", srcname, k);

src = Image_TGA_alloc_load(srcname);
if (NULL == src) {
	fprintf(stderr, "%s can't load %s\n", __func__, srcname);
	return 666;
	}
dst = Image_clone(src, 1);
if (NULL == dst) {
	fprintf(stderr, "mem failure in %s\n", __func__);
	abort();
	}

foo = Image_lissage_3x3(src, dst);
Image_TGA_save("filtre-liss3x3.tga", dst, 0);
if (foo) { fprintf(stderr, "%s: liss3x3   -> %d\n", __func__, foo); }
foo = Image_filtre_Prewitt(src, dst, 5);
Image_TGA_save("filtre-prewitt-5.tga", dst, 0);
foo = Image_filtre_passe_haut(src, dst);
Image_TGA_save("filtre-passe-haut.tga", dst, 0);

/*
foo = Image_filtre_Sobel_4(src, dst, 0);
Image_TGA_save("filtre-sobel-4.tga", dst, 0);
*/

Image_DeAllocate(src);	free(src);
Image_DeAllocate(dst);	free(dst);

return 42;
}
/*::------------------------------------------------------------------::*/
/*
 * the parameter 'prefix' will be prepended to the generated filenames.
 * you can use somthing like "Pictures/foo"
 */
void Test_des_patterns(char *prefix, int foo, int bar)
{
Image_Desc	*image;
char		filename[1000];
int		retc;

printf("============ test des patterns ================\n");
printf("%s:%s: prefix [%s], foo %d, bar %d\n",
			__FILE__, __func__, prefix, foo, bar);

image = Image_alloc(320, 200, 3);

strcpy(filename, prefix);	strcat(filename, "_000.tga");
fprintf(stderr, "--> %s\n", filename);
retc = Image_pattern_000(image, 100);
Image_print_error(filename, retc);
Image_TGA_save(filename, image, 0);

strcpy(filename, prefix);	strcat(filename, "_001.tga");
fprintf(stderr, "--> %s\n", filename);
retc = Image_pattern_001(image, 0);
Image_print_error(filename, retc);
Image_TGA_save(filename, image, 0);

strcpy(filename, prefix);	strcat(filename, "_002.tga");
fprintf(stderr, "--> %s\n", filename);
retc = Image_pattern_002(image, 0);
Image_print_error(filename, retc);
Image_TGA_save(filename, image, 0);

strcpy(filename, prefix);	strcat(filename, "_005.tga");
fprintf(stderr, "--> %s\n", filename);
retc = Image_pattern_005(image, 0);
Image_print_error(filename, retc);
Image_TGA_save(filename, image, 0);

strcpy(filename, prefix);	strcat(filename, "_042.tga");
fprintf(stderr, "--> %s\n", filename);
retc = Image_pattern_042(image, 0);
Image_print_error(filename, retc);
Image_TGA_save(filename, image, 0);

fprintf(stderr, "===== %s : la serie des 10x ====\n", __func__);

strcpy(filename, prefix);	strcat(filename, "_100.tga");
retc = Image_pattern_100(image, 0, 0, 0);
Image_print_error(filename, retc);
Image_TGA_save(filename, image, 0);

strcpy(filename, prefix);	strcat(filename, "_101.tga");
retc = Image_pattern_101(image, 0, 0, 0);
Image_print_error(filename, retc);
Image_TGA_save(filename, image, 0);
}
/*::------------------------------------------------------------------::*/
/* test des pixelizzzations - 14 octobre 2007 - avenue Sy Exupery
 *
 */
int Test_des_pixeliz(char *srcname, int flags)
{
Image_Desc	*src, *dst;
int		foo;

printf("\nPIXELIZ flags= $%04x\n", flags);
src = Image_TGA_alloc_load(srcname);
printf("test pixeliz: image loaded %p\n", src);
dst = Image_clone(src, 0);
printf("test pixeliz:      clone @ %p\n", dst);

foo = Image_pixeliz_X(src, dst);
printf("test pixeliz: X %d\n", foo);
Image_TGA_save("Pictures/aaaa_pixelX.tga", dst, 0);

foo = Image_pixeliz_Y(src, dst);
printf("test pixeliz: Y %d\n", foo);
Image_TGA_save("Pictures/aaaa_pixelY.tga", dst, 0);

Image_DeAllocate(src);	free(src);
Image_DeAllocate(dst);	free(dst);


return -1;
}
/*::------------------------------------------------------------------::*/
/*		==============================				*/

void essai_gradients(void)
{
int		foo;

foo = Image_plot_H_gradient("foo.tga", 640, 200);
fprintf(stderr, "plot h gradient -> %d\n", foo);
foo = Image_plot_V_gradient("foo.tga", 900, 200);
fprintf(stderr, "plot v gradient -> %d\n", foo);
}
/*		==============================				*/
/*::------------------------------------------------------------------::*/

