/*
                    combine3.c
		    ----------
*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	un kludge en chantier. programmation � la 'Gruiik' en vue.
 */
int Image_combine_waou(Image_Desc *s1, Image_Desc *s2, Image_Desc *dst,
					int a, int b, int c, int d)
{
int	x, y, foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p %d : %d %d (%d) )\n",
			 __func__, s1, s2, dst, a, b, c, d);
#endif

if ( (foo=Image_compare_desc(s1, s2)) ) {
    fprintf(stderr, "%s : sources are differents %d\n", __func__, foo);
    return foo;
    }

if ( (foo=Image_compare_desc(s1, dst)) ) {
    fprintf(stderr, "%s : destination bad dims %d\n", __func__, foo);
    return foo;
    }

for (y=0; y<dst->height; y++) {
	for (x=0; x<dst->width; x++) {
		if (d)	{
			/* canonical function */
			(dst->Rpix[y])[x] = (x & (s1->Rpix[y])[x]) | a;
			(dst->Gpix[y])[x] = x | b;
			(dst->Bpix[y])[x] = (y & (s2->Rpix[y])[x]) | c;
			}
		else	{
			/* new 9 mars 2010 - ave St Ex */
			(dst->Rpix[y])[x] = (x & (s1->Rpix[y])[x]) ^ a;
			(dst->Gpix[y])[x] = x ^ b;
			(dst->Bpix[y])[x] = (y & (s2->Gpix[y])[x]) ^ c;
			}
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	un kludge en chantier. programmation � la 'Gruiik' en vue.
 */ 
int Image_combine_wauo(Image_Desc *s1, Image_Desc *s2, Image_Desc *dst,
						int flag)
{
int	x, y, foo;

if ( flag )
	fprintf(stderr, "Combine Wauo: flag = %d ?\n", flag);

if ( (foo=Image_compare_desc(s1, s2)) ) {
    fprintf(stderr, "Combine Wauo: sources are differents %d\n", foo);
    return foo;
    }

if ( (foo=Image_compare_desc(s1, dst)) ) {
    fprintf(stderr, "Combine Wauo: destination bad dims %d\n", foo);
    return foo;
    }

for (y=1; y<dst->height-1; y++) {
	for (x=1; x<dst->width-1; x++) {
		(dst->Rpix[y])[x] = (s1->Rpix[y][x] + s2->Rpix[y][x])/2;

		if (s1->Rpix[y][x] > s2->Rpix[y][x]) {	
			(dst->Gpix[y])[x] = (s1->Gpix[y])[x];
			(dst->Bpix[y])[x] = (s2->Bpix[y])[x];
			}
		else	{	
			(dst->Gpix[y])[x] = (s2->Gpix[y])[x];
			(dst->Bpix[y])[x] = (s1->Bpix[y])[x];
			}
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *   threshold values (sr, sg, sb) are in the pix range [0..255]
 */
int Image_combine_seuils(Image_Desc *s1, Image_Desc *s2, Image_Desc *dst,
						int sr, int sb, int sg)
{
int	x, y, foo;
int	r, g, b;

if ( (foo=Image_compare_desc(s1, s2)) ) {
    fprintf(stderr, "Combine Seuils: sources are differents %d\n", foo);
    return foo;
    }

if ( (foo=Image_compare_desc(s1, dst)) ) {
    fprintf(stderr, "Combine Seuils: destination bad dims %d\n", foo);
    return foo;
    }

sr*=2;	sg*=2;	sb*=2;

for (y=0; y<dst->height; y++) {
	for (x=0; x<dst->width; x++) {
		if ( (s1->Rpix[y][x] + s2->Rpix[y][x]) > sr )
			r = s1->Rpix[y][x];
		else
			r = s2->Rpix[y][x];

		if ( (s1->Gpix[y][x] + s2->Gpix[y][x]) > sg )
			g = s1->Gpix[y][x];
		else
			g = s2->Gpix[y][x];

		if ( (s1->Bpix[y][x] + s2->Bpix[y][x]) > sb )
			b = s1->Bpix[y][x];
		else
			b = s2->Bpix[y][x];
 
		(dst->Rpix[y])[x] = r;
		(dst->Gpix[y])[x] = g;
		(dst->Bpix[y])[x] = b;
		}
	}

dst->modified = 1;
return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
