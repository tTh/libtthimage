/*
                    marques.c
		________________
	Routines pour deposer des marques sur des images.
	Nouveau: 12 Nov 2001.
*/

#include   <stdio.h>
#include   <string.h>
#include   <math.h>
#include   <time.h>
#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	dessine deux lignes grises sur les diagonales de l'image
 */
int
Image_marque_0(Image_Desc *img, int val)
{
int	foo;
RGBA	rgba;

rgba.r = rgba.g = rgba.b = val;
rgba.a = 0;	/* goof value ? */
foo = Image_draw_line(img, 0, 0, img->width-1, img->height-1, &rgba);
foo = Image_draw_line(img, 0, img->height-1, img->width-1, 0, &rgba);

return 0;
}
/*::------------------------------------------------------------------::*/
/*
 *	place un label dans le coin en haut � gauche de l'image.
 */
int
Image_marque_1(Image_Desc *img, char *texte, int flags)
{
RGBA		papier, encre;
int		len;

if (flags)
	{
	fprintf(stderr, "*** %s 'flags' must be ZERO ***\n", __func__);
	}

len = strlen(texte);
if (len < 1)	return FULL_NUCKED;

papier.r = 255;		papier.g = 255;		papier.b = 255;
encre.r = 220;		encre.g = 100;		encre.b = 20;
papier.a = encre.a = 255;

Image_trace_chaine_1(img, texte, 2, 2, "8x8thin", &papier, &encre);
Image_trace_chaine_1(img, IMAGE_VERSION_STRING, 2, 11, "8x8thin",
							&papier, &encre);
img->modified = 1;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/

/* FORK OF : Image_marque_0						*/

int
Image_marque_2(Image_Desc *img, char *txt,  RGBA *rgba, int k1, int k2)
{
int	foo;

fprintf(stderr, "%s : %p '%s' %p  %d %d\n", __func__, img, txt, rgba, k1, k2);

foo = Image_draw_line(img, 0, 0, img->width-1, img->height-1, rgba);
foo = Image_draw_line(img, 0, img->height-1, img->width-1, 0, rgba);

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
/*
 *	Marquage avec la date et l'heure.
 *	utilisation des flags:
 *		bit 0:		trace le cadre
 */
int
Image_marque_timestamp(Image_Desc *img, char *texte, RGBA *rgba, int flags)
{
RGBA		papier, encre;
int		foo, l1, l2;
time_t		temps;
char		*ptr;
Image_Rect	rect;

if (NULL==texte)
	{
	texte = "<kikoolol>";
	fprintf(stderr, "%s : using default text : '%s'\n", __func__, texte);
	}

l1 = strlen(texte);
temps = time(NULL); ptr = ctime(&temps);
l2 = strlen(ptr)-1;
if (l2 >= 0)
	ptr[l2] = '\0';

foo = (l1>l2) ? l1 : l2;

#if DEBUG_LEVEL
fprintf(stderr, "%s: L=%d T=%ld\n", __func__, foo, temps);
fprintf(stderr, "%s: '%s' %s\n", __func__, texte, ptr);
#endif

papier.r = papier.g = papier.b = papier.a = 0;
if (NULL == rgba) {
	encre.r = encre.g = encre.b = 90, encre.a = 255;
	rgba = &encre;
	}
	
if (flags & 1) {
	rect.x = rect.y = 3;
	rect.h = 11;	rect.w = l1*8+2;
	Image_paint_rect(img, &rect, 255, 255, 255);
	}
Image_trace_chaine_1(img, texte, 5, 6, "libimage.fonte",  &papier, rgba);

if (flags & 1) {
	rect.x = 3;	rect.y = 16;
	rect.h = 11;	rect.w = l2*8+2;
	Image_paint_rect(img, &rect, 255, 255, 255);
	}

Image_trace_chaine_1(img, ptr, 5, 18, NULL, &papier, rgba);

img->modified = 1;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 * Pour centrer la grille, il faut jouer sur 'ox' et 'oy' ?
 */
int Image_grille(Image_Desc *im, int stx, int ox, int sty, int oy, RGBA *ink)
{
int		x, y, foo;
RGBA		encre;

/* avoid an infinite loop */
if ( (stx<1) || (sty<1) ) {
	fprintf(stderr, "%s : steps must be > 0\n", __func__);
	return BAD_PARAMETER;
	}

if (ink == NULL) {
	fprintf(stderr, "%s is using default color values\n", __func__);
	encre.r = encre.g = encre.b = encre.a = 0;
	ink = &encre;
	}

#if DEBUG_LEVEL
fprintf(stderr, "%s   X: %d %d   Y: %d %d\n", __func__, ox, stx, oy, sty); 
#endif

for (x=ox; x<im->width; x+=stx)	{
	for (foo=0; foo<im->height; foo++)
		Image_plotRGB(im, x, foo, ink->r, ink->g, ink->b);	
	}
	
for (y=oy; y<im->height; y+=sty) {
	for (foo=0; foo<im->width; foo++) {
		Image_plotRGB(im, foo, y, ink->r, ink->g, ink->b);	
		}
	}

im->modified = 1;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 * Pour centrer la grille, il faut jouer sur 'ox' et 'oy' ?
 */
int Image_grille_xor(Image_Desc *im, int stx, int ox, int sty, int oy, RGBA *ink)
{
int		x, y, foo;
RGBA		encre;

if (ink == NULL) {
	fprintf(stderr, "%s is using default XOR values\n", __func__);
	encre.r = encre.g = encre.b = encre.a = 0xff;
	ink = &encre;
	}

#if DEBUG_LEVEL
fprintf(stderr, "%s   X %d %d   Y %d %d\n", __func__, ox, stx, oy, sty); 
#endif

for (x=ox; x<im->width; x+=stx)	{
	for (foo=0; foo<im->height; foo++) {
		Image_XOR_pixel(im, x, foo, ink->r, ink->g, ink->b);	
		}
	}
	
for (y=oy; y<im->height; y+=sty) {
	for (foo=0; foo<im->width; foo++) {
		Image_XOR_pixel(im, foo, y, ink->r, ink->g, ink->b);	
		}
	}

im->modified = 1;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/

