/*
 *		mosaic.c	des mosaiques :)
 */

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
      <mode rengaine>
      Et la doc, elle est o� ?
      </mode>
      <mode #linuxfr>
		DTC
      </mode>
*/
/*::------------------------------------------------------------------::*/
int
Image_mosaic_0(Image_Desc *src, Image_Desc *dst, int sx, int sy, int flg)
{
int		w, h, foo;
Image_Desc	*carre1, *carre2;
Image_Rect	rect;

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s(%p, %p, %d, %d, $%x)\n", __func__, src, dst, sx, sy, flg);
#endif

if ( (carre1=Image_alloc(sx, sy, 3)) == NULL )
	{
	fprintf(stderr, "mosaic 0: pas de memoire pour le carre 1\n");
	exit(5);
	}
if ( (carre2=Image_alloc(sx, sy, 3)) == NULL )
	{
	fprintf(stderr, "mosaic 0: pas de memoire pour le carre 2\n");
	exit(5);
	}

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s: flag is %d\n", __func__, flg);
#endif

rect.w = sx;	rect.h = sy;

for (h=0; h<src->height; h+=sy)
    {
    for (w=0; w<src->width; w+=sx)
	{
	rect.x = w;	rect.y = h;
	foo = Image_get_rect(src, &rect, carre1, 0, 0);
	Image_egalise_mono_0(carre1, carre2, 0);
	rect.x = 0;	rect.y = 0;
	foo = Image_put_rect(carre2, &rect, dst, w, h);
	}
    }

Image_DeAllocate(carre1);		free(carre1);
Image_DeAllocate(carre2);		free(carre2);

return FUNC_NOT_FINISH;
}

/*::------------------------------------------------------------------::*/
#define STEP 32
int
Image_mosaic_simple(Image_Desc *src, Image_Desc *dst)
{
int		w, h, foo;
Image_Desc	*carre, *carre2;
Image_Rect	rect;

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s ( %p, %p )\n", __func__, src, dst);
#endif

if ( (carre=Image_alloc(STEP, STEP, 3)) == NULL )
	{
	fprintf(stderr, "mosaique simple: pas de memoire pour le carre 1\n");
	exit(5);
	}
if ( (carre2=Image_alloc(STEP, STEP, 3)) == NULL )
	{
	fprintf(stderr, "mosaique simple: pas de memoire pour le carre 2\n");
	exit(5);
	}

rect.w = STEP;	rect.h = STEP;

for (h=0; h<src->height; h+=STEP)
    {
    for (w=0; w<src->width; w+=STEP)
	{
	/*
	 *    on extrait le petit carre
	 */
	Image_clear(carre, 0, 0, 0);
	rect.x = w;	rect.y = h;
	foo = Image_get_rect(src, &rect, carre, 0, 0);

	Image_egalise_mono_0(carre, carre2, 0);

	/*
	 *    et hop, on met le petit carre dans la destination
	 */
	rect.x = 0;	rect.y = 0;
	foo = Image_put_rect(carre2, &rect, dst, w, h);

	}
    }

Image_DeAllocate(carre);		free(carre);
Image_DeAllocate(carre2);		free(carre2);

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
void Image_mosaic_prctrl(IMAGE_MOSAIC_CTRL *ctrl, char *txt)
{
printf("     src:   %p\n", ctrl->src);
printf("     dst:   %p\n", ctrl->dst);
printf("   steps:   %d %d\n", ctrl->stepx, ctrl->stepy);
}
/*::------------------------------------------------------------------::*/
int
Image_mosaic_iterateur(IMAGE_MOSAIC_CTRL *ctrl, int yo)
{

printf("================= MoZaic IteraTor =====================\n");

/*
 *	Janvier 2003:
 *		je ne sais toujours pas quoi mettre ici.
 *	Juin 2008:
 *		C'est toujours en standby :)
 */
Image_mosaic_prctrl(ctrl, "iterateur");

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
