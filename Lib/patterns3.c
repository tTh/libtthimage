/*
                    patterns3.c
		------------------
	see also 'patterns.c' & 'patterns2.c'

*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <unistd.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 * c'est quasiment impossible de documenter cette fonction.
 */
int Image_pattern_100(Image_Desc *dst, int kr, int kg, int kb)
{
int	x, y, r,g, b;

fprintf(stderr, ">>> %s ( %p  %d %d %d )\n", __func__,
				dst, kr, kg, kb);

for (x=0; x<dst->width; x++) {
	for (y=0; y<dst->height; y++) {
		r = abs(x+y-(dst->width/3)) * kr;
		g = abs(x-y-(dst->height/3)) * kg;
		b = (rand() % 42) + kb;

		Image_plotRGB(dst, x, y, r, g, b);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int Image_pattern_101(Image_Desc *dst, int kr, int kg, int kb)
{
int	x, y, r,g, b;

fprintf(stderr, ">>> %s ( %p  %d %d %d )\n", __func__,
				dst, kr, kg, kb);

if (kr == 0)		kr = 1;
if (kg == 0)		kg = 1;
if (kb == 0)		kb = 1;

for (x=0; x<dst->width; x++) {
	for (y=0; y<dst->height; y++) {
		if (x < dst->width/2)
			{ r=x/kr; g=y/kg; b=x/kb; }
		else
			{ r=y/kr; g=x/kg; b=y/kb; }
		Image_plotRGB(dst, x, y, r, g, b);
		}
	}
return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int Image_pattern_102(Image_Desc *dst, int param, int kr, int kg, int kb)
{
int	x, y, r,g, b;

for (x=0; x<dst->width; x++) {
	for (y=0; y<dst->height; y++) {
		r = x ^ kr;
		g = y ^ kg;
		b = (x+y+param) ^ kb;
		Image_plotRGB(dst, x, y, r, g, b);
		}
	}

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
/* hop, 31 mars 2008, une petite evolution stochastique de base...
 * */
int Image_pattern_103(Image_Desc *dst, int param, int kr, int kg, int kb)
{
int		x, y, r, g, b;
double		dx, dy;

#if DEBUG_LEVEL
fprintf(stderr, "%s: acting on image %p\n", __func__, dst);
#endif

for (x=0; x<dst->width; x++) {
	dx = (double)x / (double)dst->width;
	for (y=0; y<dst->height; y++) {
		dy = (double)x / (double)dst->height;
		r = (int)(256.0 * sqrt(dx * dy));		/* XXX */
		g = b = rand() & 0xf0;
		Image_plotRGB(dst, x, y, r, g, b);
		}
	}

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
/*
 *	new 10 juillet 2003
 */
int
Image_pattern_104(Image_Desc *dst, int sx, int sy, RGBA *a, RGBA *b)
{
int	x, y, fx, fy;

for (x=0; x<dst->width; x++)
	{
	fx = x % sx;
	for (y=0; y<dst->height; y++)
		{
		fy = y % sy;

		if (fx==1 && fy==1)
			{
			Image_plotRGB(dst, x, y, a->r, a->g, a->b);
			}
		else
			{
			Image_plotRGB(dst, x, y, b->r, b->g, b->b);
			}
		}
#if DEBUG_LEVEL > 1
	fprintf(stderr, "%05d\r", x);
#endif
	}

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
