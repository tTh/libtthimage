/*
                Zooms sur les images
		--------------------
	new 31 janvier 2009 - avenue Saint Exupery
*/

#include  <stdio.h>
#include  <string.h>
#include  <stdlib.h>
#include  <math.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*		private variables					*/
/*::------------------------------------------------------------------::*/
/*		private functions					*/
/*::------------------------------------------------------------------::*/
int Image_essai_zoom(Image_Desc *src, Image_Desc *dst,
				double kx, double ky, int flags)
{
int		xd, yd, xs, ys;
int		rd, gd, bd;
double		dxs, dys;		/* destination float position */
#if DEBUG_LEVEL
long		hit, mess;
#endif

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p  %p      %f %f   0x%x )\n", __func__,
			src, dst,  kx, ky, flags);
#endif

if (flags) {
	fprintf(stderr, "in %s, flags muste be 0x0000\n", __func__);
	return WRONG_FLAG;
	}

/* initialisation de variables de debug/comptage */
#if DEBUG_LEVEL
hit = mess = 0L;
#endif

for (xd=0; xd<dst->width; xd++)	{
	dxs = (double)xd * kx;
	for (yd=0; yd<dst->height; yd++) {
		rd = gd = bd = 42;

		dys = (double)yd * ky;		/* kikoo lol */
		xs = (int)dxs;
		ys = (int)dys;

		if ( Image_xy_inside(src, xs, ys) ) {
			Image_getRGB(src, xs, ys, &rd, &gd, &bd);
#if DEBUG_LEVEL
			hit++;
#endif			
			}
#if DEBUG_LEVEL
		else { mess++; }
#endif
		Image_plotRGB(dst, xd, yd, rd, gd, bd);
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "%s -> hit=%ld mess=%ld\n", __func__, hit, mess);
#endif

return BAD_ZOOM;
}
/*::------------------------------------------------------------------::*/
/*
 *     Very old non-working code get an update on Thu 24 Sep 2022
 *
 *	We have two versions:
 *		zoom_000 without interpolation
 *		zoom_001 with a pseudo bi-linear interpolation
 *	Public intercace:
 *		Image_zoom_extract(src, dst, rect, flags)
 */

/*	this is a semi-private function 				*/
 
int Image_zoom_000(Image_Desc *src, Image_Desc *dst, Image_Rect *rect,
			float kx, float ky)
{
int		xd, yd, xs, ys, r, g, b;
float		ftmp;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p  %p )\n", __func__, src, dst, rect);
#endif

for (yd=0; yd<dst->height; yd++) {
	ftmp = ((float)yd) / ky;
	ys = (int) (ftmp);
	ys += rect->y;
/*
	fprintf(stderr, "  yd %5d  ->  %8.3f  ys %5d\n", yd, ftmp, ys);
*/
	for (xd=0; xd<dst->width; xd++)	{
		xs = (int) ((float)xd / kx);
		xs += rect->x;

		if ( Image_xy_inside(src, xs, ys) )
			{ Image_getRGB(src, xs, ys, &r, &g, &b); }
		else
			{ r = g = b = 127; }
		Image_plotRGB(dst, xd, yd, r, g, b);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*									*/
/*	nouveau Fri 23 Sep 2022 02:27:00 PM CEST			*/
/*	this is a semi-private function 				*/
/*									*/

int Image_zoom_001(Image_Desc *src, Image_Desc *dst, Image_Rect *rect,
			float kx, float ky)
{
int		xd, yd, xs, ys, r, g, b;
double		dxs, dys;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p  %p  %.3f %.3f)\n", __func__,
				src, dst, rect, kx, ky);
#endif

for (yd=0; yd<dst->height; yd++) {
	dys = (double)rect->y + (((double)yd) / ky);
	ys = (int)dys;
	for (xd=0; xd<dst->width; xd++) {
		dxs = (double)rect->x + ((double)xd / kx);
		xs = (int)dxs;
		if ( Image_xy_inside(src, xs, ys) )
			{
			/* see 'scale.c' for this func */
			Image_getpix_bilin(src, dxs, dys, &r, &g, &b);
			}
		else
			{ r = g = b = 127; }
		Image_plotRGB(dst, xd, yd, r, g, b);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*									*/
/*	nouveau Fri 23 Sep 2022 02:16:55 PM CEST			*/
/*									*/
int Image_zoom_extract(Image_Desc *src, Image_Desc *dst, Image_Rect *rect,
								int flags)
{
int			foo;
float			kx, ky;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p  %p 0x%x)\n", __func__, src, dst, rect, flags);
#endif

kx = (float)dst->width     /  (float)rect->w;
ky = (float)dst->height    /  (float)rect->h;

#if DEBUG_LEVEL
fprintf(stderr, "    kx %8.3f   ky %8.3f\n", kx, ky);
#endif

/* this is ugly code ! */
if (flags)
	{ foo = Image_zoom_001(src, dst, rect, kx, ky); }
else
	{ foo = Image_zoom_000(src, dst, rect, kx, ky); }

if (foo) {
	fprintf(stderr, "got %d in %s\n", foo, __func__);
	return foo;
	}

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
