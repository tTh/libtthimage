/*
		+---------------------------------------+
		|    Effets speciaux sur les images	|
		|          deuxieme module		|
		+---------------------------------------+
			Thierry Boudet <oulala@chez.com>
    vous pouvez aussi aller regarder 'effects.c' et 'combine.c' 
*/

#include  <stdio.h>
#include  <unistd.h>
#include  <stdlib.h>
#include  <math.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	la parametre 'table' est un tableau de 6 double. d'accord, 
 *	mais quel est donc son contenu ?
 */
int Image_sinwave_0( Image_Desc *source, Image_Desc * but, double *table )
{
#if DEBUG_LEVEL > 1
int	foo;
#endif
int     x, y, xb, yb, r, g, b;
long    outside = 0;
double  fx, fy, cx, cy;

#if DEBUG_LEVEL > 1
Image_dump_descriptor(source, "SinWave 0: source");
#endif
#if DEBUG_LEVEL > 1
fprintf(stderr, "-- In %s, table is:\n", __func__);
for (foo=0; foo<6; foo++)
    fprintf(stderr, "\t%4d   -->  %12.6f\n", foo, table[foo]);
#endif

cx = (2.0 * M_PI) / (double)source->width;
cy = (2.0 * M_PI) / (double)source->height;

for (x=0; x<source->width; x++) {
    fx = (double)x + (table[1] * sin(table[4] + (cx * table[0] * (double)x)));
    xb = floor(fx + 0.5);

    for (y=0; y<source->height; y++) {
        fy = (double)y + (table[3] * sin(table[5] + cy * table[2] * (double)y));
        yb = floor(fy + 0.5);

        if ( xb>0 && yb>0 && xb<but->width && yb<but->height ) {
            r = (source->Rpix[yb])[xb];
            g = (source->Gpix[yb])[xb];
            b = (source->Bpix[yb])[xb];

	    (but->Rpix[y])[x] = r;
	    (but->Gpix[y])[x] = g;
	    (but->Bpix[y])[x] = b;
            }
        else { outside++; }
        }
    }

#if DEBUG_LEVEL
if (outside)
    fprintf(stderr, "%s: outside = %ld\n", __func__, outside);
#endif

but->modified   = 1;	
return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	11 Octobre 1999
 *	Bon, sur celui-ci, je vais essayer d'expliquer...
 *
 *	Les parametres sont en deux groupes de 3 dans la table.
 *	mais franchement, j'arrive plus a savoir a quoi ils servent.
 */
int Image_sinwave_1( Image_Desc *source, Image_Desc * but, double *table )
{
int		foo, x, y, xb, yb, r, g, b;
long		outside = 0;
double  	fx, fy, dx, dy;
int		ox, oy;
Image_Desc	*buffer;

if ( (foo=Image_compare_desc(source, but)) )
    {
    fprintf(stderr, "Image SinWave 1: images are differents %d\n", foo);
    return foo;
    }

if ((buffer = Image_clone(source, 0)) == NULL )
	{
	fprintf(stderr, "Image SinWave 1: no mem for buffer\n");
	return BUFFER_NO_MEM;
	}

for (y=0; y<source->height; y++)
	{
	fy = (double)y / (double)source->height * M_PI;
	dy = sin(fy*table[0]) * table[1];
	oy = (int)(dy);
	for (x=0; x<source->width; x++)
		{
		xb = x + oy;
		if (xb >=0 && xb<source->width)
			{
            		r = (source->Rpix[y])[xb];
            		g = (source->Gpix[y])[xb];
            		b = (source->Bpix[y])[xb];
			}
		else
			{
			r = g = b = 0;
			outside++;
			}
		(buffer->Rpix[y])[x] = r;
		(buffer->Gpix[y])[x] = g;
		(buffer->Bpix[y])[x] = b;
		}
	}

for (x=0; x<source->width; x++)
	{
	fx = (double)x / (double)source->width * M_PI;
	dx = sin(fx*table[3]) * table[4];
	ox = (int)(dx);
	for (y=0; y<source->height; y++)
		{
		yb = y + ox;
		if (yb >=0 && yb<source->height)
			{
            		r = (buffer->Rpix[yb])[x];
            		g = (buffer->Gpix[yb])[x];
            		b = (buffer->Bpix[yb])[x];
			}
		else
			{
			r = g = b = 0;
			outside++;
			}
		(but->Rpix[y])[x] = r;
		(but->Gpix[y])[x] = g;
		(but->Bpix[y])[x] = b;
		}
	}
Image_DeAllocate(buffer);	free(buffer);		

but->modified   = 1;	

#if DEBUG_LEVEL > 1
Image_dump_descriptor(but, "SinWave 1: destination");
#endif

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/* new 10 janvier 2009 - avenue St Exupery */
int Image_sinwave_2(Image_Desc *source, Image_Desc *but, double table[])
{
int		foo;

if ( (foo=Image_compare_desc(source, but)) ) {
    fprintf(stderr, "Image SinWave 2: images are differents %d\n", foo);
    return foo;
    }

printf("    in %s now\n", __func__);

for (foo=0; foo<6; foo++)
	{
	printf("     %d     %g\n", foo, table[foo]);
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Image_degouline_0(Image_Desc *source, Image_Desc *but, int k1, int k2)
{
int		x, y;
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">> %s ( %p %p  %d %d )\n", __func__, source, but, k1, k2);
#endif
fprintf(stderr, "Degouline %d %d\n", k1, k2);

if ( (foo=Image_compare_desc(source, but)) ) {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

for (x=0; x<source->width; x++)
	{

	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/*
 *		bon sang, mais ya personne qui va se plonger dans ce code
 *		100% crade, et en sortir une doc (et des exemples) un peu
 *		utilisables ?
 */
/*::------------------------------------------------------------------::*/
