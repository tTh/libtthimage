/*
                    turtle.c
		---------------
*/

#include   <stdio.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_turtle_infos(Image_Desc *img, int flags)
{

#if DEBUG_LEVEL
fprintf(stderr, "Image turtle infos: flags = %04x\n", flags);
#endif

if ( flags & 1 ) {
	Image_dump_descriptor(img, "turtle infos");
	}

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
/*
 *	changer la couleur de l'encre de la tortue.
 *
 *	Q: pourquoi le canal alpha n'est pas pris en compte ?
 *	A: parce que :)
 */
int Image_turtle_setcolors(Image_Desc *img, int r, int g, int b)
{
img->rt = r;	img->gt = g;	img->bt = b;

img->at = 0;	/* ? bonne valeur ? */

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
int Image_turtle_move(Image_Desc *img, double xt, double yt)
{

fprintf(stderr, ">>> %s ( %p   %f %f )\n", __func__, img, xt, yt);

img->xt = xt;
img->yt = yt;

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
int Image_turtle_draw(Image_Desc *img, double xt, double yt)
{
int	x1, y1, x2, y2;
RGBA	color = { 200, 200, 200, 0, 0, 0 };

fprintf(stderr, ">>> %s ( %p   %f %f )\n", __func__, img, xt, yt);

x1 = (int)img->xt, y1 = (int)img->yt;
x2 = (int)xt     , y2 = (int)yt;

Image_draw_line(img, x1, y1, x2, y2, &color);

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
