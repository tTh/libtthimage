/*
		primitives graphiques deuxieme module (circa 2002)
		--------------------------------------------------

*/

#include  <stdio.h>
#include  <unistd.h>
#include  <stdlib.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
#ifndef min
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#endif
/*::------------------------------------------------------------------::*/
/*
 *	the easy one.
 */
int Image_paint_A_rect(Image_Desc *img, Image_Rect *rect, RGBA *rgba)
{
int	xd, yd, xf, yf, x, y;
int	r, g, b, a0, a1, nbre;

xd = max(0, rect->x);
yd = max(0, rect->y);
xf = min((img->width-1), (rect->x+rect->w));
yf = min((img->height-1), (rect->y+rect->h));

#if DEBUG_LEVEL
fprintf(stderr, "Paint Alpha Rect: %d <X< %d   %d <Y< %d\n", xd, xf, yd, yf);
Image_print_rgba("encre alpha", rgba, 0);
#endif

a0 = 255 - rgba->a;
a1 = rgba->a;

/* printf("a0 = %d    a1 = %d\n", a0, a1); */

nbre = 0;

for (x=xd; x<xf; x++) {
	for (y=yd; y<yf; y++) {
		Image_getRGB(img, x, y, &r, &g, &b);
		/*
		 * ok, compute now...
		 */
		r = ( r*a0 + rgba->r*a1 ) / 255;
		g = ( g*a0 + rgba->g*a1 ) / 255;
		b = ( b*a0 + rgba->b*a1 ) / 255;
		Image_plotRGB(img, x, y, r, g, b);
		nbre++;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	new 15 Janvier 2003.
 *	premiere utilisation: l'ecriture des titres des 
 *		image de l'expovsition.
 */
int Image_fade_A_rect(Image_Desc *img, Image_Rect *rect, int alpha)
{
int	xd, yd, xf, yf, x, y;
int	r, g, b ;

xd = max(0, rect->x);
yd = max(0, rect->y);
xf = min((img->width-1), (rect->x+rect->w));
yf = min((img->height-1), (rect->y+rect->h));

#if DEBUG_LEVEL
fprintf(stderr, "Fade Alpha Rect:  %d < X < %d     %d < Y < %d\n",
				xd, xf, yd, yf);
fprintf(stderr, "  coefficient alpha = %d\n", alpha);
#endif

for (x=xd; x<xf; x++) {
	for (y=yd; y<yf; y++) {
		Image_getRGB(img, x, y, &r, &g, &b);
		/*
		 * ok, compute now...
		 */
		r = ( r*alpha ) / 255;
		g = ( g*alpha ) / 255;
		b = ( b*alpha ) / 255;
		Image_plotRGB(img, x, y, r, g, b);
		}
	}

return FUNC_IS_BETA;		/* need unit testing */
}
/*::------------------------------------------------------------------::*/
/* nouveau vacances de fevrier 2010 */
/*
 * bon, une fonction pour tamponner une image quelconque avec une
 * image alpha, il serait temps de la mettre en place, nom ?
 */
int Image_tampon_alpha_0(Image_Desc *src, Image_Desc *tampon, Image_Desc *dst)
{
int	x, y, rs, gs, bs, rt, gt, bt;
int	rd, gd, bd, clean_tampon;

clean_tampon = 0;
/* if the 'tampon' is not defined, we have to imagine one */
if (NULL==tampon) {
	tampon = Image_clone(src, 0);
	fprintf(stderr, "fake tampon @ %p\n", tampon);
	Image_mirror(src, tampon, 0);
	clean_tampon = 1;
	}

for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		Image_getRGB(src, x, y, &rs, &gs, &bs);
		Image_getRGB(tampon, x, y, &rt, &gt, &bt);
		rd = (rs * rt) / 256;
		}
	}

if (clean_tampon) {
	fprintf(stderr, "clean %p in %s\n", tampon, __func__);
	Image_DeAllocate(tampon);	free(tampon);
	}

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
