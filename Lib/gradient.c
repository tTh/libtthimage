/*
			gradient.c
			----------

	new: 	Sun Sep 17 20:47:57 UTC 2023

*/

#include  <stdio.h>
#include  <string.h>
#include  <stdlib.h>
#include  <math.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_plot_H_gradient(char *fname, int w, int h)
{
Image_Desc		*img;
int			x, y, pix, foo;
float			fval;

if ( (img = Image_alloc(w, h, IMAGE_RGB)) == NULL )	{
#if DEBUG_LEVEL
	fprintf(stderr, "%s: alloc of img failed, file=%s\n", __func__, fname);
#endif
	return IMAGE_NO_MEM;
	}

for (x=0; x<w; x++) {
	fval = (float)x / (float)(w-1);
	pix = (int)(fval * 255.0);
#if DEBUG_LEVEL > 1
	printf("%5d    %6.3f    %5d\n", x, fval, pix);
#endif
	for (y=0; y<h; y++) {
		(img->Rpix[y])[x] = pix;
		(img->Gpix[y])[x] = pix;
		(img->Bpix[y])[x] = pix;
		}
	}

foo = Image_TGA_save(fname, img, 0);
if (foo) {
	fprintf(stderr, "%s: err write to '%s'\n", __func__, fname);
	return FULL_NUCKED;
	}

Image_DeAllocate(img);	free(img);

return FUNC_IS_BETA;
}

/*::------------------------------------------------------------------::*/
int Image_plot_V_gradient(char *fname, int w, int h)
{

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
