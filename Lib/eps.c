/*

		Encapsulated Postscrip
		----------------------

	26 Jan 2002: Functions for exporting images to EPS format,
	for use in printed version of the story "Life in Slashie".

	--> see pnmtops manpage ?

*/

#include  <stdio.h>
#include  <time.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *		really don't know how to do that ;-)
 */
int
Image_EPS_save_0(char *nom, Image_Desc *img)
{
FILE	*fp;
time_t	temps;

fprintf(stderr, "Image EPS save 0: %p -> %s\n", img, nom);

if ( (fp=fopen(nom, "w")) == NULL )
	{
	fprintf(stderr, "EPS save 0: err fopen %s\n", nom);
	return FILE_CREATE_ERR;
	}

fputs("%!PS-Adobe-2.0 EPSF-1.2\n%%\n", fp);
fprintf(fp, "%%%% Generator: libtthimage v %s\n", IMAGE_VERSION_STRING);
time(&temps);
fprintf(fp, "%%%% Date: %s\n", ctime(&temps));
fclose(fp);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/*
 *		really don't know how to do that ;-)
 */
/*::------------------------------------------------------------------::*/
