/*
                pov_hf15f.c
                ===========
	traitement des bords des height-fields.
	---------------------------------------
	new 3 octobre 2009 - avenue StExupery
*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"        

/*::------------------------------------------------------------------::*/
/*	Abaissement graduel des bords d'un height field 		*/
/*	Inspiration : 'Image_cadre_C'					*/
/* TODO : build a real unit test */
/* TODO : expliquer les parametres */

int Image_hf15_sweep(Image_Desc *im, int hfin, int taille, int k)
{
int		x, y, x2, y2;
float		fhfin, fx, fy, coef, d;
int		hit, mess, h, h2;

#if DEBUG_LEVEL 
fprintf(stderr, "%s : img=%p   hfin=%d   taille=%d   k=%d\n",
				__func__, im, hfin, taille, k);
fprintf(stderr, "   pic dims : %d x %d\n", im->width, im->height);
#endif

if (taille) {
	fprintf(stderr, "%s : taille = %d, wtf?\n", __func__, taille);
	}

/* this is really a MESS ! */
fhfin = (float)(hfin*hfin);
x2 = im->width / 2;
y2 = im->height / 2;

#if DEBUG_LEVEL
fprintf(stderr, "   fhfin=%f      x2=%d  y2=%d\n", fhfin, x2, y2);
#endif

hit = mess = 0;

for (y=0; y<im->height; y++)
	{
	/*
	 *	distance au bord HAUT BAS le plus proche
	 */
	if (y < y2)	fy = (float)y;
	else		fy = (float)((im->height - y)-1);
	fy = fy*fy;

	for (x=0; x<im->width; x++)
		{
		/*
		 *	distance au bord DROITE GAUCHE le plus proche
		 */
		if (x < x2)	fx = (float)x;
		else		fx = (float)((im->width - x)-1);
		fx = fx*fx;

		/* XXX  un peu plus d'explications ici */
		if (fx < fy)	d = fx;
		else		d = fy;

		if (d < fhfin)
			{
			coef = d / fhfin;
			h = Image_hf15_height(im, x, y);
			h2 = (int)(((float)h*coef) + ((float)hfin*(1.0-coef)));
#if DEBUG_LEVEL > 1
			printf("__sweep__ %5d %d\n", h, h2);
#endif
			if ( h<0 || h>32767)
				{
				fprintf(stderr, "%s fail %d %d\n",
							__func__, x, y);
				abort();
				}
			Image_hf15_plot(im, x, y, h2);
			hit ++;
			}
		else
			{
			mess ++;
			}
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "      hit %d     mess %d\n", hit, mess);
#endif

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Image_hf15_getmin(Image_Desc *s1, Image_Desc *s2, Image_Desc *d)
{
int	x, y, h1, h2;

for (y=0; y<d->height; y++)
	{
	for (x=0; x<d->width; x++)
		{
		h1 = Image_hf15_height(s1, x, y);
		h2 = Image_hf15_height(s2, x, y);
		if (h1 < h2)
			Image_hf15_plot(d, x, y, h1);
		else
			Image_hf15_plot(d, x, y, h2);
		}
	}
return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Image_hf15_getmax(Image_Desc *s1, Image_Desc *s2, Image_Desc *d)
{
int	x, y, h1, h2;

for (y=0; y<d->height; y++)
	{
	for (x=0; x<d->width; x++)
		{
		h1 = Image_hf15_height(s1, x, y);
		h2 = Image_hf15_height(s2, x, y);
		if (h1 > h2)
			Image_hf15_plot(d, x, y, h1);
		else
			Image_hf15_plot(d, x, y, h2);
		}
	}
return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/

