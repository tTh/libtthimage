/*
                pov_hf15c.c
                ===========

	Mixing operations beetween two height-fields.
	---------------------------------------------
*/

#include  <stdio.h>
#include  <string.h>
#include  <math.h>
#include  <stdlib.h>

#include  "../tthimage.h"        

/*::------------------------------------------------------------------::*/
/*
 *  new 10 D�cembre 2001
 *
 *     Le coefficient K va de 0 a 10000 !
 */
int Image_hf15_mix(Image_Desc *s1, Image_Desc *s2, Image_Desc *dst, int k)
{
int	foo, x, y, h1, h2, hd;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s: k = %d\n", __func__, k);
#endif

if ( (foo=Image_compare_desc(s1, s2)) ) {
	fprintf(stderr, "hf15 mix: err on sources: %s\n", Image_err2str(foo));
	return foo;
	}
if ( (foo=Image_compare_desc(s1, dst)) ) {
	fprintf(stderr, "hf15 mix: err on dest: %s\n", Image_err2str(foo));
	return foo;
	}

for (y=0; y<s1->height; y++) {
	for (x=0; x<s1->width; x++) {
		h1 = Image_hf15_height(s1, x, y);
		h2 = Image_hf15_height(s2, x, y);
		hd = ((h1*k) + (h2*(10000-k))) / 10000;
		Image_hf15_plot(dst, x, y, hd);
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "Image hf15 mix: done.\n");
#endif

dst->modified = 1;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *  new 5 Janvier 2003
 *	il y a probablement des problemes d'overflow ici.
 */
int Image_hf15_mult_i(Image_Desc *s1, Image_Desc *s2, Image_Desc *dst)
{
int	foo, x, y, h1, h2, hd;

if ( (foo=Image_compare_desc(s1, s2)) ) {
	fprintf(stderr, "hf15 mult i: err on srces: %s\n", Image_err2str(foo));
	return foo;
	}
if ( (foo=Image_compare_desc(s1, dst)) ) {
	fprintf(stderr, "hf15 mult: err on dest: %s\n", Image_err2str(foo));
	return foo;
	}

for (y=0; y<s1->height; y++) {
	for (x=0; x<s1->width; x++) {
		h1 = Image_hf15_height(s1, x, y);
		h2 = Image_hf15_height(s2, x, y);
		hd = (h1 * h2) / 32768;
		Image_hf15_plot(dst, x, y, hd);
		}
	}

dst->modified = 1;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* nouveau 17 novembre 2009 - avenue St Exupery */
int Image_hf15_mult_k(Image_Desc *s, Image_Desc *d, float dk)
{
int	foo, x, y, h1, h2;

if ( (foo=Image_compare_desc(s, d)) ) {
	fprintf(stderr, "hf15 mult k: error: %s\n", Image_err2str(foo));
	return foo;
	}

for (y=0; y<s->height; y++) {
	for (x=0; x<s->width; x++) {
		h1 = Image_hf15_height(s, x, y);
		h2 = (int)( (float)h1 * dk );
		Image_hf15_plot(d, x, y, h2);
		}
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/* nouveau Juin 2008 - avenue St Exupery */
int Image_hf15_div_i(Image_Desc *s1, Image_Desc *s2, Image_Desc *dst)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p )\n", __func__, s1, s2, dst);
#endif

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/* nouveau Juin 2008 - avenue St Exupery */
int
Image_hf15_sqrt(Image_Desc *src, Image_Desc *dst)
{
int		x, y, h;
int		foo;
float		rh, mean_a;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p , %p )\n", __func__, src, dst);
#endif

if ( (foo=Image_compare_desc(src, dst)) ) {
	fprintf(stderr, "hf15 sqrt: (failed) : %s\n", Image_err2str(foo));
	return foo;
	}

mean_a = 0.0;

for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		h = Image_hf15_height(src, x, y);
		if (h < 0) {
			fprintf(stderr, "WTF ! %s  %p  %d,%d   %d\n",
						__func__,
						src, x, y, h);
#if FORCE_ABORT
			fflush(stdout);
			abort();
#endif
			}
		rh = (float)h / 32767.0;
		mean_a += rh;				/* c'est quoi ? */
		h  = (int)(sqrt(rh) * 32767.0);
		Image_hf15_plot(dst, x, y, h);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* nouveau Novembre 2008 - avenue St Exupery */
int Image_hf15_pow(Image_Desc *src, Image_Desc *dst, double p0w44)
{
int		x, y, h;
int		foo;
/* hu ho 27 jan 2010 :  pourquoi on ne calcule pas en double precision ? */
float		rh;
float		minh, maxh;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p , %p, %g )\n", __func__, src, dst, p0w44);
#endif

if ( (foo=Image_compare_desc(src, dst)) ) {
	fprintf(stderr, "%s : (fail) : %s\n", __func__, Image_err2str(foo));
	return foo;
	}

minh =  9999999.99;
maxh = -9999999.99;
for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		h = Image_hf15_height(src, x, y);
		rh = (float)h / 32768.0;
		rh = pow(rh, p0w44);
		if (rh < minh)		minh = rh;
		if (rh > maxh)		maxh = rh;
		h  = (int)(rh * 32767.0);
		Image_hf15_plot(dst, x, y, h);
		}
	}
fprintf(stderr, "%s -> %f %f\n", __func__, minh, maxh);

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int Image_hf15_exp1(Image_Desc *src, Image_Desc *dst)
{
int		foo, x, y, h;
/*
 * on va calculer en double precision, parce que je suis un dino et
 * que ma machine de dev ne connait pas 'expf' 
 */
double		rh;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p , %p )\n", __func__, src, dst);
#endif

if ( (foo=Image_compare_desc(src, dst)) ) {
	fprintf(stderr, "%s : (fail) : %s\n", __func__, Image_err2str(foo));
	return foo;
	}

for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		h = Image_hf15_height(src, x, y);
		rh = (double)h / 32768.0;
		rh = exp(rh) - 1.0;

		h  = (int)(rh * 32767.0);
		Image_hf15_plot(dst, x, y, h);
		}
	}

return FUNC_IS_ALPHA;
}

int Image_hf15_exp2(Image_Desc *src, Image_Desc *dst)
{
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p  %p )\n", __func__, src, dst);
#endif
return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/

