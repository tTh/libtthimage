/*
                    patterns.c
		------------------
*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <math.h>
#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_pattern_000(Image_Desc *img, int foo)
{
int	x, y, xx, yy, r, g, b;

#if DEBUG_LEVEL
fprintf(stderr, "%s : foo = %d\n", __func__, foo);
#endif

for (x=0; x<img->width; x++) {
	xx = (x * 256) / img->width;
	for (y=0; y<img->height; y++) {
		yy = (y * 256) / img->height;
		r = xx & yy & foo;
		g = ((xx * yy) & 0xf8 );
		b = xx ^ yy;
		Image_plotRGB(img, x, y, r, g, b);
		}
	}
/*
   sometime, a newbie ask me: "why use intermediate vars: r, g, and b ?"
   my answer was: "clarity of code, small scarab�e".
*/ 
return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int Image_pattern_001(Image_Desc *img, int foo)
{
int	x, y;

#if DEBUG_LEVEL
fprintf(stderr, "%s : foo is %d\n", __func__, foo);
#endif

for (x=0; x<img->width; x++)
	for (y=0; y<img->height; y++)
		Image_plotRGB(img, x, y, x^y^foo, (x*y)>>4, x&y);

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int Image_pattern_002(Image_Desc *img, int foo)
{
int	x, y, x2, y2;

#if DEBUG_LEVEL
fprintf(stderr, "Pattern 002: %d\n", foo);
#endif

for (x=0; x<img->width; x++) {
    x2 = (x * 256) / img->width;
    for (y=0; y<img->height; y++) {
	y2 = (y * 256) / img->height;
	Image_plotRGB(img, x, y, (x2/2)&(y2/2)&foo, (x2*y2)>>3, x2^y2);
	}
    }

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int Image_pattern_003(Image_Desc *img, int foo)
{
int	x, x2, y, y2;

#if DEBUG_LEVEL
fprintf(stderr, "Pattern 003: foo is %d\n", foo);
#endif

/* added some ugly code the 28 february 2008 */
for (x=0; x<img->width; x++) {
    x2 = (x * 256) / img->width;
    for (y=0; y<img->height; y++) {
	y2 = (y * 256) / img->height;
	Image_plotRGB(img, x, y, (x2/4)&(y2/4)&foo, (x2*y2)>>5, x2^y2);
	}
    }

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
int Image_pattern_004(Image_Desc *img, int a, int b, int c, int d)
{
#if DEBUG_LEVEL
fprintf(stderr, "Pattern 004: %d, %d, %d, %d\n", a, b, c, d);
#endif

fprintf(stderr, "NO CODE HERE\n");

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
int Image_pattern_005(Image_Desc *img, RGB_map *map)
{
int		x, y;
double		fx, fy;
int		idx;

if (NULL == map) {
	fprintf(stderr, "in %s:%s, map is NULL\n",
				__FILE__, __func__);
	return NULL_POINTER;
	}

for (y=0; y<img->height; y++) {
	fy = (double)y;
	for (x=0; x<img->width; x++) {
		fx = (double)x;
		
		idx = (int)(127.0+(sin(fx)*cos(fy)*254.99));
		idx &= 0xff;

		(img->Gpix[y])[x] = map->green[idx];
		(img->Bpix[y])[x] = map->blue[idx];
		}
	}

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
int Image_pattern_042(Image_Desc *img, int foo)
{
int	x, y, r, g, b;

#if DEBUG_LEVEL
fprintf(stderr, "Pattern 042: threshold level is %d\n", foo);
#endif

r = g = b = 42;
for (x=0; x<img->width; x++) {
	for (y=0; y<img->height; y++) {
		r += rand() % 4;
		if (r > foo)	r = 0;

		g += rand() % 4;
		if (g > foo)	g = 0;

		b += rand() % 4;
		if (b > foo)	b = 0;

		/* Image_plotRGB(img, x, y, r, g, b); */
		(img->Rpix[y])[x] = r;
		(img->Gpix[y])[x] = g;
		(img->Bpix[y])[x] = b;
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
