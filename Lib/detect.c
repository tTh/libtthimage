/*
		detect.c
		--------
*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	this function detect all the pixels who the value is
 *	over the values of all his 8 neight-machin.
 */
int
Image_detect_tops(Image_Desc *src, Image_Desc *dst, int flags)
{
int	x, y, foo, top, r, g, b, rm, gm, bm;
static  struct
	{
	int	x, y;
	} d[] =
	{
	{  -1, -1  },
	{   0, -1  },
	{   1, -1  },
	{  -1,  0  },
	{   1,  0  },
	{  -1,  1  },
	{   0,  1  },
	{   1,  1  } 
	};

if ( (foo=Image_compare_desc(src, dst)) )
	{
	fprintf(stderr, "Detect Tops: erreur: %s\n", Image_err2str(foo));
	return foo;
	}

Image_clear(dst, 0, 0, 0);
for (y=1; y<src->height-1; y++)
	{
	for (x=1; x<src->width-1; x++)
		{
		top = 0;
		for (foo=0; foo<8; foo++)
			{
			r = Image_R_pixel(src, x+d[foo].x, y+d[foo].y);
			if (r > top)	top = r;
			}
		r = Image_R_pixel(src, x, y);
		if (r > top)	rm = 255;
		else		rm = 0;

		top = 0;
		for (foo=0; foo<8; foo++)
			{
			g = Image_G_pixel(src, x+d[foo].x, y+d[foo].y);
			if (g > top)	top = g;
			}
		g = Image_G_pixel(src, x, y);
		if (g > top)	gm = 255;
		else		gm = 0;

		top = 0;
		for (foo=0; foo<8; foo++)
			{
			b = Image_B_pixel(src, x+d[foo].x, y+d[foo].y);
			if (b > top)	top = b;
			}
		b = Image_B_pixel(src, x, y);
		if (b > top)	bm = 255;
		else		bm = 0;

		Image_plotRGB(dst, x, y, rm, gm, bm);
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "\tok?\n");
#endif

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	on cherche les endroits ou le niveau de gris est 'plat'.
 *
 *	le parametre 'r1' n'est pas utilise et doit etre mis a 0
 */
int
Image_detect_flat_gray(Image_Desc *src, Image_Desc *dst, int param, int r1)
{
int	x, y, foo, r, g, b, gris, mini, maxi;
static  struct
	{
	int	x, y;
	} d[] =
	{
	{  -1, -1  },
	{   0, -1  },
	{   1, -1  },
	{  -1,  0  },
	{   1,  0  },
	{  -1,  1  },
	{   0,  1  },
	{   1,  1  } 
	};

if ( (foo=Image_compare_desc(src, dst)) )
	{
	fprintf(stderr, "Detect Flat Gray: erreur: %s\n", Image_err2str(foo));
	return foo;
	}

if (0 != r1)
	{
	fprintf(stderr, "%s : ah ah r1 is %d, a bad value\n", __func__, r1);
	}

for (y=1; y<src->height-1; y++)
	{
	for (x=1; x<src->width-1; x++)
		{
		mini = 999999;
		maxi = -42;
		for (foo=0; foo<8; foo++)
			{
			Image_getRGB(src, x+d[foo].x, y+d[foo].y, &r, &g, &b);
			gris = r + g + b;
			if	(gris > maxi)		maxi = gris;
			else if	(gris < mini)		mini = gris;
			}
		if (maxi-mini < param)
			Image_plotRGB(dst, x, y, 255, 255, 255);
		else
			Image_plotRGB(dst, x, y, 0, 0, 0);
		}
	}
/*
 *	hop, en finale, on efface les bords, et on
 *	marque l'image "modifiee". 
 */
(void)Image_raz_sides(dst);
dst->modified = 1;

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/

