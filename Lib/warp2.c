/*
                    warp2.c
		---------------
*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <math.h>
#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/* 
 *	Bon, �a marche avec des d�placements positifs, mais pour
 *	revenir en arri�re, on fait comment ?
 */
int
Image_shift_xy(Image_Desc *src, Image_Desc *dst, int ox, int oy)
{
int	x, y;
int	x2, y2, foo;

#if DEBUG_LEVEL
fprintf(stderr, "*** %s: work in progress...\n", __func__);
fprintf(stderr, "src=%p dst=%p ox=%d oy=%d\n", src, dst, ox, oy);
#endif

if ( src == dst )
	{
	fprintf(stderr, "Image shift xy: can't overwrite, sorry\n");
	return IMG_OVERWRITE;
	}

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

if ( (ox<0) || (ox>=src->width) )
	{
	fprintf(stderr, "Image shift xy: param ox %d out of range\n", ox);
	return INVALID_PARAM;
	}
if ( (oy<0) || (oy>=src->height) )
	{
	fprintf(stderr, "Image shift xy: param oy %d out of range\n", oy);
	return INVALID_PARAM;
	}

for (y=0; y<dst->height; y++)
	{
	y2 = (y+oy) % src->height;
	for (x=0; x<dst->width; x++)
		{
		x2 = (x+ox) % src->width;
		Image_pixel_copy(src, x, y, dst, x2, y2);
		}
	}

return FUNC_IS_BETA;		/* et elle coredumpe ? */
}
/*::------------------------------------------------------------------::*/
/* OPTIMIZED FOR SPEED */
int Image_shift_x(Image_Desc *src, Image_Desc *dst, int ox)
{
unsigned char	*buffer;
int		foo, line;

#if DEBUG_LEVEL
fprintf(stderr, "*** %s : work in progress...\n", __func__);
fprintf(stderr, "%p -> sX(%d) -> %p\n", src, ox, dst);
#endif

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

if (NULL==(buffer=malloc(src->width))) {
	fprintf(stderr, "%s: malloc(%d) fail.\n", __func__, src->width);
#if MUST_ABORT
	abort();
#endif
	exit(1);
	}

for (line=0; line<src->width; line++) {
	
	/* do something here */

	}

free(buffer);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/* OPTIMIZED FOR SPEED */
int Image_shift_y(Image_Desc *src, Image_Desc *dst, int oy)
{
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, "*** %s : work in progress...\n", __func__);
fprintf(stderr, "%p -> sY(%d) -> %p\n", src, oy, dst);
#endif

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/*
 *		voir aussi les modules scale.c et warp[0|1|3].c
 */
/*::------------------------------------------------------------------::*/
