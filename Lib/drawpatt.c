/*
 *	DRAWPATT.C
 *	==========
 *
 *	new 1st July 2003
 *
 */

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	ATTENTION CETTE FONCTION COREDUMPE SI LE RECTANGLE 
 *	DEPASSE SUR UN BORD DE L'IMAGE !!!
 */
int
Image_draw_rect_from_patt(Image_Desc *img, Image_Rect *rect, Image_Desc *patt)
{
int	x, y, xp, yp, xm, ym;
int	r, g, b;

#if DEBUG_LEVEL
fprintf(stderr, ">>>> %s\n", __func__);
#endif

if ( patt == NULL )
	{
	fprintf(stderr, "Badaboum!\n");
	exit(5);
	}

for (x=0; x<rect->w; x++)
	{
	xp = x % patt->width;
	if (xp > img->width)
		{
		continue;
		}

	for (y=0; y<rect->h; y++)
		{
		yp = y % patt->height;
		if (yp > img->height)
			{
			continue;
			}
		Image_getRGB(patt, xp, yp, &r, &g, &b);
		Image_plotRGB(img, x+rect->x, y+rect->y, r, g, b);
		}
	}

return FUNC_NOT_FINISH;
}
/*
 *	ATTENTION CETTE FONCTION COREDUMPE SI LE RECTANGLE 
 *	DEPASSE SUR UN BORD DE L'IMAGE !!!
 */
/*::------------------------------------------------------------------::*/
