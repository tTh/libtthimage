/*
		conversion of numeric codes to text messages
		--------------------------------------------
*/

#include   <stdio.h>
#include   <stdlib.h>		/* for abort() */
#include   <time.h>
#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
char * Image_type2str(int type)
{
char	*pstr;

switch (type) {
	case IMAGE_RGB:		pstr = "RGB";		break;
	case IMAGE_RGBA:	pstr = "RGBA";		break;
	case IMAGE_GRAY:	pstr = "Gray";		break;
	case IMAGE_PAL:		pstr = "Palet";		break;
	case IMAGE_NONE:	pstr = "(none)";	break;
	case IMAGE_ALPHA:	pstr = "alpha";		break;
	case IMAGE_PORTNAWAK:	pstr = "portnawak";	break;
	default:		pstr = "Uh?";		break;
	}

return pstr;
}
/*::------------------------------------------------------------------::*/
char * Image_err2str(int codeerr)
{
char	*pstr;
static	char chaine[100];

switch (codeerr) {
	case -1:		pstr = "error: invalid error";	break;
	case OLL_KORRECT:	pstr = "no error";		break;
	case 42:		pstr = "Universal answer";	break;
	case 406:		pstr = "Mort de Peur :)";	break;
	case 666:		pstr = "The devil is coming";	break;

	case DIFFERENT_WIDTH:	pstr = "different width";	break;
	case DIFFERENT_HEIGHT:	pstr = "different height";	break;
	case NOT_AN_IMAGE_DESC:	pstr = "not an image desc";	break;
	case IMAGE_NO_MEM:	pstr = "no mem for image";	break;
	case IMAGE_TOO_SMALL:	pstr = "image too small";	break;
	case UNKNOW_ERROR:	pstr = "unknow error";		break;
	case STRING_ERROR:	pstr = "string error";		break;
	case IMAGE_BAD_TYPE:	pstr = "image bad type";	break;
	case STRING_TOO_LONG:	pstr = "string too long";	break;
	case NULL_DESCRIPTOR:	pstr = "null descriptor";	break;
	case VERY_STRANGE:	pstr = "very strange error";	break;
	case DIVISOR_IS_ZERO:	pstr = "zero divide, reboot universe"; break;
	case FULL_NUCKED:	pstr = "...ahem, yu'r nucked";	break;
	case BUFFER_NO_MEM:	pstr = "no memory for buffer";	break;
	case FILE_NOT_FOUND:	pstr = "file not found";	break;
	case OUT_OF_IMAGE:	pstr = "out of image";		break;
	case WRONG_CHANNEL:	pstr = "wrong channel";		break;
	case BAD_COLOR_NUMBER:	pstr = "bad color number";	break;
	case MAP_TOO_BIG:	pstr = "color map is too big";	break;
	case SRC_TOO_SMALL:	pstr = "Src is too small";	break;
	case DST_TOO_SMALL:	pstr = "Dst is too small";	break;
	case BAD_MAGIC:		pstr = "Bad magic number";	break;
	case IMG_OVERWRITE:	pstr = "Dst overwrite Src";	break;
	case INVALID_PARAM:	pstr = "Invalid parameter";	break;
	case RECT_TOO_SMALL:	pstr = "Rect is too small";	break;
	case NO_ALPHA_CHANNEL:	pstr = "_no_ alpha channel";	break;
	case NO_BETA_CHANNEL:	pstr = "rah: no beta channel";	break;
	case FILE_CREATE_ERR:	pstr = "err on file creation";	break;
	case WRONG_FLAG:	pstr = "wrong flag value";	break;
	case NOT_3D_IMG:	pstr = "not a 3d valid img";	break;
	case INVALID_RGB:	pstr = "invalid RGB values";	break;
	case TOO_MUCH_VALUES:	pstr = "too much values";	break;
	case INVALID_PT_LIST:	pstr = "invalid points list";	break;
	case INVALID_POINT:	pstr = "invalid point";		break;
	case NULL_POINTER:	pstr = "null pointer";		break;
	case PIXEL_OVERFLOW:	pstr = "pixel overflow";	break;
	case PIXEL_UNDERFLOW:	pstr = "pixel underflow";	break;
	case INVALID_GAMMA:	pstr = "invalid gamma";		break;
	case INVALID_HF15:	pstr = "invalid HF 15";		break;
	case BAD_FILE_NAME:	pstr = "bad file name";		break;
	case EMPTY_COL_MAP:	pstr = "empty ColMap";		break;
	case BAD_CHRONO:	pstr = "bad chrono number";	break;
	case IMAGE_BAD_DIMS:	pstr = "bad image dimensions";	break;
	case PASTIS:		pstr = "*** Apero Time ***";	break;
	case BAD_OPERATOR:	pstr = "bad operator";		break;
	case BAD_FILTER:	pstr = "Ugly Filter !";		break;
	case WTF_OSEF:		pstr = "omigod, wtf ?...";	break;
	case BASIC_IO_RD_ERR:	pstr = "BasicIO, Read err";	break;
	case BASIC_IO_WR_ERR:	pstr = "BasicIO, Write err";	break;

	case FP_GARBAGE:	pstr = "garbage in floating point"; break;
	case IMG_ARE_DIFFERENT: pstr = "img are differents";	break;
	case BAD_ZOOM:		pstr = "hu ho, bad zoom ?";	break;
	case BAD_GRAY_OP:	pstr = "bad gray operation";	break;
	case ERR_GARDRECT:	pstr = "err on a gadgrect";	break;

	case FUNC_IS_ALPHA:	pstr = "func is ALPH@#;~:~.";	break;
	case FUNC_IS_BETA:	pstr = "function in BETA test";	break;
	case FUNC_IS_XPER:	pstr = "experimental function";	break;
	case FUNC_COREDUMP:	pstr = "codedumping";		break;
	case FUNC_NOT_FINISH:	pstr = "function not finish";	break;
	case IMAGE_FAIL:	pstr = " }{ EPIC FAIL }{ ";	break;

	case BAD_CADRE:		pstr = "I haz a bad 'cadre'";	break;

	case 9999:		pstr = "FORTRAN ro><ore :)";	break;

	default:
		sprintf(chaine, "error code %d is unknow", codeerr);
		pstr = chaine;
#if DEBUG_LEVEL
		fprintf(stderr, "%s: WTF ? Error %d unknow ?\n",
				__func__,	codeerr);
#if FORCE_ABORT
		abort();
#endif
#endif
		break;
	}
return pstr;
}
/*::------------------------------------------------------------------::*/
/* new 14 Feb 2000							*/
void Image_print_error(char * texte, int err)
{
FILE	*fp;
char	*errtxt;

errtxt = Image_err2str(err);

if (*texte == '-') {
	fp = stdout;
	texte++;			/* skip the '-' */
	}
else	{
	fp = stderr;
	}
fprintf(fp, "%s '%s': %d, %s\n", err ? "Error :" : "Waouh !",
					texte, err, errtxt);
fflush(fp);
}
/*::------------------------------------------------------------------::*/
