/*
 *		classif.c		novembre 2002
 *		---------		-------------
 */

#include  <stdio.h>
#include  <math.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	A vrai dire, je ne sais pas trop comment traiter ce genre
 *	de truc. Il va falloir que je me documente serieusement.
 */

static Une_Classe_Sph classes[] = 
{
{   64,  64,  64,      50,	128, 128, 128	},
{   64, 192,  64,      50,	128, 255, 128	},
{   64,  64, 192,      50,	128, 128, 255 	},
{   64, 192, 192,      50,	255, 255, 255 	},

{  192,  64,  64,      50,	255, 128, 128 	},
{  192, 192,  64,      50,	255, 255, 128 	},
{  192,  64, 192,      50,	255, 128, 255 	},
{  192, 192, 192,      50,	255, 255, 255 	} 

};

#define NBC  (sizeof(classes)/sizeof(Une_Classe_Sph))

#define CUB(x)  ((x)*(x))

/*::------------------------------------------------------------------::*/
int
Image_classif_0(Image_Desc *src, Image_Desc *dst)
{
int	foo, x, y, z, r, g, b, plotted;
long	nbrplot;

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "Image classif 0: images are differents %d\n", foo);
    return foo;
    }

#if DEBUG_LEVEL
fprintf(stderr, "Classif 0: nombre de classes = %d\n", NBC);
#endif

nbrplot = 0;
for (y=0; y<src->height; y++)
    {
    for (x=0; x<src->width; x++)
	{
	Image_getRGB(src, x, y, &r, &g, &b);

	plotted = 0;
	for (foo=0; foo<(int)NBC; foo++)
	    {
	    z = CUB(r-classes[foo].rc) +
		CUB(g-classes[foo].gc) +
		CUB(b-classes[foo].bc);
            
  	    if (z < CUB(classes[foo].rad))
		{
		Image_plotRGB(dst, x, y,
			classes[foo].r, classes[foo].g, classes[foo].b);
		plotted = 1;
		nbrplot++;
		break;
		}
	    }

	if ( ! plotted ) Image_plotRGB(dst, x, y, 0, 0, 0);

	}	/* fin for x */
    }

dst->modified   = 1;

#if DEBUG_LEVEL
fprintf(stderr, " plotted: %ld / %d : %4.2f %%\n", nbrplot,
	src->width*src->height,
	((float)nbrplot)/(float)(src->width*src->height) * 100.0);
#endif

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* new 14 mai 2007, avenue St Exupery */
int
Image_classif_1(Image_Desc *src, Image_Desc *dst,
			Une_Classe_Sph *classes, int nbrclasses, int flag)
{
int	foo, x, y, z, r, g, b, plotted;
long	nbrplot;

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "Image classif 1: images are differents %d\n", foo);
    return foo;
    }

#if DEBUG_LEVEL
fprintf(stderr, "   Classif 1: nombre de classes = %d\n", nbrclasses);
fprintf(stderr, "   Classif 1: flag is %d\n", flag);
#endif

nbrplot = 0;
for (y=0; y<src->height; y++)
    {
    for (x=0; x<src->width; x++)
	{
	Image_getRGB(src, x, y, &r, &g, &b);

	plotted = 0;
	for (foo=0; foo<nbrclasses; foo++)
	    {
	    z = CUB(r-classes[foo].rc) +
		CUB(g-classes[foo].gc) +
		CUB(b-classes[foo].bc);
            
  	    if (z < CUB(classes[foo].rad))
		{
		if (flag)
		    Image_plotRGB(dst, x, y,
			    classes[foo].r, classes[foo].g, classes[foo].b);
                else
		    Image_plotRGB(dst, x, y, r, g, b);
		
		plotted = 1;
		nbrplot++;
		break;
		}
	    }	/* fin for classe */

	if ( ! plotted ) Image_plotRGB(dst, x, y, 0, 0, 0);
	}	/* fin for x */
    }

dst->modified   = 1;

#if DEBUG_LEVEL
fprintf(stderr, "   plotted: %ld / %d\n", nbrplot, src->width*src->height);
#endif

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
int Image_display_classes(Une_Classe_Sph *cla, int nbr, char *texte, int flags)
{
int	foo;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p  %4d  $%04x %s )\n", __func__, cla, nbr, flags, texte);
#endif

if (flags) {
	fprintf(stderr, "%s --> flags 0x%04X !\n", __func__, flags);
	/* return error ? */
	}

for (foo=0; foo<nbr; foo++) {
	printf("%3d  '%-20s'  ", foo, texte);
	printf("%5d  %5d  %5d  ", cla[foo].rc, cla[foo].gc, cla[foo].bc) ;
	puts("");
	}
puts("-- ");			/* #include <sigrot.h> */

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/

