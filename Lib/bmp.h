/*
 *		header file for BMP functions
 *		-----------------------------
 *	'tthimage.h' must be included before this file.
 *
 */

#pragma pack(1)		/* est-ce encore utile ? */

typedef struct
	{
	char		id[2];
	long		filesize;
	uint16_t		reserved[2];
	long		headerSize;
	long		infoSize;
	long		width;
	long		height;
	short		planes;
	short		bits;
	long		compression;
	long		SizeImage;
	long		xpixpermeter;
	long		ypixpermeter;
	long		clrused;
	long		clrimportant;
	} BMPHEAD;

#pragma pack()

