/*
 *	essais sur le format BMP
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 * Ther was a nasty bug in the 'save' function, staying here from the
 * previous century. Try to fix it in October 2009 
 */
int essai_largeur(int k)
{
int		w;
Image_Desc 	*img;
char		filename[100], chaine[110];

fprintf(stderr, "k is %d in %s\n", k, __func__);

Image_load_fnt8x8("libimage.fonte", NULL, 0);

for (w=160; w<180; w++)
	{
	sprintf(filename, "tmp/aaaa-%03d-%d.bmp", w, w&3);
	fprintf(stderr, "---- building '%s'\n", filename);

	img = Image_alloc(w, 100, 3);
	if (NULL==img)
		{ exit(1); }
	Image_pattern_001(img, 100);
	Image_marque_0(img, 0);
	sprintf(chaine, " w = %3d  %d ", w,  w&3);
	Image_trace_chaine_1(img, chaine, 14, 14, NULL, NULL, NULL);
	Image_BMP_save_24(filename, img, 0);	

	snprintf(chaine, 100, "display %s", filename);
	fprintf(stderr, "running [%s]\n", chaine);
	system(chaine);

	Image_DeAllocate(img);	free(img);
	}
return 0;
}
/*::------------------------------------------------------------------::*/
int main(int argc, char *argv[])
{
int	foo;

Image_print_version(1);
foo = essai_largeur(20);

return 0;
}
/*::------------------------------------------------------------------::*/
