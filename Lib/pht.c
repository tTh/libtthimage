/*

        le retour du format PHT !-)
	===========================

	Et je remercie grandemant Guy Marcenac pour toute l'aide
	qu'il m'a apporte a la grande epoque du COPAM.

	------------------------------------------

	mais c'est quoi le format PHT ?

		z) Il y a fort longtemps, je travaillais dans une
		z) boite de traitement d'images medicales, et je
		z) me suis retrouve proprietaire d'une Micropix.
		z) Carte d'acquisition d'images 512x256x64.
		z) J'ai beaucoup joue avec, et j'ai stocke des
		z) centaines d'images sur disque. Au format PHT.
		z) Format strictement proprietaire, mais dont tous
		z) les secrets sont devoiles ici .;-)

	------------------------------------------

	il me semble que dans le temps du Copam, on avait aussi un
	format de fichier KP0 ? -oui, je m'en souviens...
	
	------------------------------------------

  NEW: 12 Sept 2001.
	je commence a modifier pour que ce soit independant du
	boutisme, et les premiers essais seront fait sur Sparc.
*/

#include  <stdio.h>
#include  <unistd.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int
Image_PHT_save_component(char *name, Image_Desc *img, char color)
{
FILE                *fp;
uint8_t             **planeptr;
int                 foo;

/* security fixes, because I make buggy soft around PHT files */
if (name == NULL)
	{
	fprintf(stderr, "PHT save componant: name ptr is null\n");
	return BAD_FILE_NAME;
	}
if (img == NULL)
	{
	return NULL_DESCRIPTOR;
	}

planeptr = NULL;

switch (color)
    {
    case 'R':       case 'r':
        planeptr = img->Rpix;       break;

    case 'G':       case 'g':
        planeptr = img->Gpix;       break;

    case 'B':       case 'b':
        planeptr = img->Bpix;       break;

    case 'A':       case 'a':           /* canal alpha (transparence) */
        planeptr = img->Apix;       break;

    default:
        fprintf(stderr, "save image in PHT: bad composante [%c]\n", color);
	return WRONG_CHANNEL;
        break;
    }

if (planeptr == NULL)  
	{
	fprintf(stderr, "save PHT: planeptr for '%c' is NULL", color);
	return WRONG_CHANNEL;
	}

if ( (fp=fopen(name, "wb")) == NULL )
	{
	fprintf(stderr, "save PHT: err fopen");
	return FILE_CREATE_ERR;
	}

/*
	euh, ya pas un probleme [big/little]endien ici ?
	- ben si...
	et caisse con fait ?
	- on cherche a le regler...
	euh, comment ? faudrait acheter une Sparc et une SGI...

dims[0] = img->width;       dims[1] = img->height;
fwrite(dims, sizeof(dims), 1, fp);

	NoN! il faut regarder le module "basic_io.c" :)
*/
Image_basicIO_write_I_short(fp, img->width);
Image_basicIO_write_I_short(fp, img->height);

for (foo=0; foo<img->height; foo++)
    {
    fwrite(planeptr[foo], img->width, 1, fp);
    }

fclose(fp);
return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	Ah, nostalgie, quand tu nous tiens...
 */
Image_Desc *
Image_load_PHT(char *fname)
{
FILE                *fp;
uint16_t	    width, height;
int		    foo;

if ( (fp=fopen(fname, "rb")) == NULL )
	{
	fprintf(stderr, "load PHT: err fopen %s", fname);
	return NULL;
	}

foo  = Image_basicIO_read_I_short(fp, &width);
foo += Image_basicIO_read_I_short(fp, &height);

fprintf(stderr, "load PHT: width %d, height %d\n", width, height);

fclose(fp);

return NULL;
}
/*::------------------------------------------------------------------::*/
