/*
 *		essais de filtres adaptatifs
 *		----------------------------
 */

#include  <stdio.h>
#include  <math.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int
Image_filtadapt_0(Image_Desc *src, Image_Desc *dst, int param)
{
int	x, y, foo;
int	r, g, b;

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "%s : images are differents %d\n", __func__, foo);
    return foo;
    }

for (y=1; y<src->height-1; y++) {
	for (x=1; x<src->width-1; x++) {
		Image_getRGB(src, x, y, &r, &g, &b);

		/* ICI IL MANQUE DU CODE */

		Image_plotRGB(dst, x, y, r, g, b);
		}
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int
Image_filtadapt_1(Image_Desc *src, Image_Desc *dst, int param)
{
int	x, y, foo;
int	r, g, b;

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "%s : images are differents %d\n", __func__, foo);
    return foo;
    }

for (y=1; y<src->height-1; y++) {
	for (x=1; x<src->width-1; x++) {
		Image_getRGB(src, x, y, &r, &g, &b);

		/* ICI IL MANQUE DU CODE */

		Image_plotRGB(dst, x, y, r, g, b);
		}
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int
Image_filtadapt_2(Image_Desc *src, Image_Desc *dst, int param)
{
int	foo;

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "Filtre adapt 2: images are differents %d\n", foo);
    return foo;
    }

fprintf(stderr, "%s:%d missing code for '%s'\n", __FILE__, __LINE__, __func__);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int
Image_filtadapt_3(Image_Desc *src, Image_Desc *dst, int param)
{
int	x, y, foo;

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "Filtre adapt 3: images are differents %d\n", foo);
    return foo;
    }

fprintf(stderr, "%s:%d missing code for '%s'\n", __FILE__, __LINE__, __func__);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
