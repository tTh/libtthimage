/*
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	+                                                         +
	+	Octree Color Quantization                         +
	+	-------------------------                         +
	+                                                         +
	+  http://www.cubic.org/~submissive/sorcerer/octree.htm   +
	+                                                         +
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/

#include  <stdio.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	bon, yapluka, fokon.
 */
int
Image_octree_0(Image_Desc *src, Image_Desc *dst)
{

#if DEBUG
fprintf(stderr, ">>> %s ( %p %p )\n", __func__, src, dst);
#endif

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int
Image_octree_1(Image_Desc *src, Image_Desc *dst)
{

#if DEBUG
fprintf(stderr, ">>> %s ( %p %p )\n", __func__, src, dst);
#endif

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
