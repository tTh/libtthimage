/*
 *      extractbits.c
 *				new 11 mars 2014
 */

#include  <stdio.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_extrbits_0(Image_Desc  *src, Image_Desc *dst, int rs, int gs, int bs)
{
int		x, y, foo;
int		mr, mg, mb, r, g, b;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p %p   %d %d %d )\n", __func__, src, dst, rs, gs, bs);
#endif

if ( rs<0 || gs<0 || bs<0 ) {
	fprintf(stderr, "%s : no negativ shift\n", __func__);
	return BAD_PARAMETER;
	}

if ( rs>8 || gs>8 || bs>8 ) {
	fprintf(stderr, "%s : no shift > 7\n", __func__);
	return BAD_PARAMETER;
	}

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

mr = 1 << rs;
mg = 1 << gs;
mb = 1 << bs;

#if DEBUG_LEVEL
fprintf(stderr, "%s masques = 0x%02x 0X%02x 0x%02x\n", __func__, mr, mg, mb);
#endif

for (y=0; y<dst->height; y++)
	{
	for (x=0; x<dst->width; x++)
		{
        	r = ((src->Rpix[y])[x] & mr) ? 255 : 0;
		dst->Rpix[y][x] = r;
        	g = ((src->Gpix[y])[x] & mg) ? 255 : 0;
		dst->Gpix[y][x] = g;
        	b = ((src->Bpix[y])[x] & mb) ? 255 : 0;
		dst->Bpix[y][x] = b;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
