/*
		+[
			S=T=R=E=R=E=O
		]+
*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	premiere fonction, assez primitive, mais qui marche.
 */
int Image_combine_stereo_0(Image_Desc *gauche, Image_Desc *droite,
						Image_Desc *stereo)
{
int		x, y, foo;
int		rouge, vert;

if ( (foo=Image_compare_desc(gauche, droite)) ) {
	fprintf(stderr, "Combine Stereo 0: sources are differents (%d)\n", foo);
	return foo;
	}

if ( (foo=Image_compare_desc(gauche, stereo)) ) {
	fprintf(stderr, "Combine Stereo 0: dest have a bad dim  (%d)\n", foo);
	return foo;
	}

for (y=0; y<gauche->height; y++) {
	for (x=0; x<droite->width; x++)
		{
		rouge = ( Image_R_pixel(droite, x, y)+
			  Image_G_pixel(droite, x, y)+
			  Image_B_pixel(droite, x, y) ) / 3;
		vert =  ( Image_R_pixel(gauche, x, y)+
			  Image_G_pixel(gauche, x, y)+
			  Image_B_pixel(gauche, x, y) ) / 3;
		Image_plotRGB(stereo, x, y, rouge, vert, 0);
		}
	}

stereo->modified = 1;

return OLL_KORRECT;
}

/*::------------------------------------------------------------------::*/
/*
 *	celui-ci semble le meme que le '_0', mais il va probablement
 *	evoluer... Et il serait vraiment bien que je trouve de la doc
 *	sur les differents parametres chromatiques.
 *	=============================================================
 *	En fait (30 janv 2008, ave StEx) au lieu de faire du Rouge/Vert
 *	il va faire du Rouge/Bleu. A la demande de Ker2x.
 */
int Image_combine_stereo_1(Image_Desc *s1, Image_Desc *s2, Image_Desc *d,
					int kr, int kg, int kb)
{
int		x, y, foo, cumul;
int		rouge, bleu;

if ( (foo=Image_compare_desc(s1, s2)) )	{
	fprintf(stderr, "Combine Stereo 1: sources are differents (%d)\n", foo);
	return foo;
	}
if ( (foo=Image_compare_desc(s1, d)) ) {
	fprintf(stderr, "Combine Stereo 1: dest have a bad dim  (%d)\n", foo);
	return foo;
	}

cumul = kr + kg + kb;
#if DEBUG_LEVEL
fprintf(stderr, "Combine Stereo 1: coefs are %d %d %d\n", kr, kg, kb);
fprintf(stderr, "Combine Stereo 1: cumul is %d\n", cumul);
#endif

for (y=0; y<s1->height; y++) {
	for (x=0; x<s1->width; x++) {
		rouge =   Image_R_pixel(s2, x, y) * kr	+
			  Image_G_pixel(s2, x, y) * kg	+
			  Image_B_pixel(s2, x, y) * kb;
		rouge /= cumul;
		bleu =    Image_R_pixel(s1, x, y) * kr	+
			  Image_G_pixel(s1, x, y) * kg	+
			  Image_B_pixel(s1, x, y) * kb;
		bleu /= cumul;
		Image_plotRGB(d, x, y, rouge, 0, bleu);
		}
	}

d->modified = 1;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	Et pour celui-ci, il faudrait trouver une maniere de definir
 *	les composantes pour l'image destination, afin de tenir
 *	compte des filtres qui seront dans les lunettes...
 */
int
Image_combine_stereo_2(Image_Desc *s1, Image_Desc *s2, Image_Desc *d,
					char cr, char cl)
{
/* XXX
int		x, y, foo;
int		rouge, vert, bleu;
XXX */

fprintf(stderr, ">>> %s ( %p %p %p '%c' '%c' )\n", __func__,
			s1, s2, d, cr, cl);

fprintf(stderr, "%s: %s missing, sorry.\n", __FILE__, __func__);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
