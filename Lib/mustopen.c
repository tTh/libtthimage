/*
		MUST OPEN THIS FILE
		___________________

	14 mai 2007: ya un frameworque de test pour ces trucs ?
*/

#include  <stdio.h>
#include  <sys/types.h>
#include  <sys/stat.h>
#include  <fcntl.h>
#include  <string.h>
#include  <stdlib.h>

#ifdef NEED_ALLOCA_H
#include  <alloca.h> 
#endif

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	parameter 'type' is reserved, and must be 0 for now.
 *	return value is -2 whe the file can't be opened.
 */
int  Image_must_open(char *filename, int mode, int type)
{
int	fd;
char	 *path, *tmp;
int	l1, l2;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' '%s' %d )\n", __func__, filename, mode, type);
#endif

if (type != 0)
	{
	fprintf(stderr, "Image must Open: type (%d) must be 0\n", type);
	}

if ( (fd=open(filename, mode)) >= 0)
	{
	return fd;
	}

if ( (path=getenv(ENV_LIBIMAGE_PATH)) != NULL)
	{
#if DEBUG_LEVEL > 1
	fprintf(stderr, "LIBIMAGE_PATH is '%s'\n", path);
#endif
	l1 = strlen(path);
	l2 = strlen(filename);
	if ( (tmp=alloca(l1+l2+5)) == NULL )
		{
		fprintf(stderr, "%s : no more mem available\n", __func__);
		exit(5);
		}
	strcpy(tmp, path);
	if ( path[l1-1] != '/' )	strcat(tmp, "/");
	strcat(tmp, filename);
#if DEBUG_LEVEL > 1
	fprintf(stderr, "Image must Open: --> '%s'\n", tmp);
#endif
	if ( (fd=open(tmp, mode)) > 0 )
		{
#if DEBUG_LEVEL > 1
		fprintf(stderr, "found at '%s'\n", tmp);
#endif
		return fd;
		}
	}

/*
	Ahem, trying with the hardcoded path to shared file
	of libtthimage. Hop this help.
*/
path = SHAREDIR "/";	/* don't forget the trailing '/' */

#if DEBUG_LEVEL > 1
fprintf(stderr, "*** trying standard place '%s'\n", path);
#endif

l1 = strlen(path);
l2 = strlen(filename);
if ( (tmp=alloca(l1+l2+5)) == NULL )
	{
	fprintf(stderr, "Image must Open: KatasTroph on 'share'\n");
	exit(5);
	}
strcpy(tmp, path);
strcat(tmp, filename);
#if DEBUG_LEVEL > 1
fprintf(stderr, "Image must Open: --> %s\n", tmp);
#endif
if ( (fd=open(tmp, mode)) > 0 )
	{
#if DEBUG_LEVEL > 1
	fprintf(stderr, "found at '%s'\n", tmp);
#endif
	return fd;
	}

#if DEBUG_LEVEL
fprintf(stderr, "Image must Open: file '%s' not found, sorry...\n", filename);
#endif

return -2;		/* mmm, a magic number ? */
}
/*::------------------------------------------------------------------::*/
/*
 *	parameter 'type' is reserved, and must be 0 for now.
 */
FILE * Image_must_fopen(char *filename, char *mode, int type)
{
FILE	 *fp;
char	 *path, *tmp;
int	l1, l2;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' '%s' %d )\n", __func__, filename, mode, type);
#endif

/* filter for the reserved parameter */
if (type != 0)
	{
	fprintf(stderr, "Image_must_fopen: type (%d) must be 0\n", type);
	}

if ( (fp=fopen(filename, mode)) != NULL )
	{
#if DEBUG_LEVEL > 1
	fprintf(stderr, "Image_must_fopen: found '%s' in current dir.\n",
						filename);
#endif
	return fp;
	}

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s: searching for '%s'\n", __func__, filename);
#endif

if ( (path=getenv(ENV_LIBIMAGE_PATH))!=NULL)
	{
#if DEBUG_LEVEL > 1
	fprintf(stderr, "LIBIMAGE_PATH is '%s'\n", path);
#endif
	l1 = strlen(path);
	l2 = strlen(filename);
	if ( (tmp=alloca(l1+l2+5)) == NULL )
		{
		fprintf(stderr, "Image must fopen: KatasTroph\n");
		exit(5);
		}
	strcpy(tmp, path);
	if ( path[l1-1] != '/' )	strcat(tmp, "/");
	strcat(tmp, filename);
#if DEBUG_LEVEL > 1
	fprintf(stderr, "Image must fopen: --> '%s'\n", tmp);
#endif
	if ( (fp=fopen(tmp, mode)) != NULL )
		{
#if DEBUG_LEVEL > 1
		fprintf(stderr, "found at '%s'\n", tmp);
#endif
		return fp;
		}
	}
#if DEBUG_LEVEL > 1
else
	{
	fprintf(stderr, "Image must fopen: LIBIMAGE_PATH is not set ?\n");
	}
#endif

/*
 *	Ahem, trying with the hardcoded path to shared file
 *	of libtthimage. Hope this help.
 */
path = SHAREDIR "/";	/* don't forget the trailing '/' */

#if DEBUG_LEVEL > 1
fprintf(stderr, "* trying standard place '%s'\n", path);
#endif

l1 = strlen(path);
l2 = strlen(filename);
if ( (tmp=alloca(l1+l2+5)) == NULL )
	{
	fprintf(stderr, "Image must fopen: KatasTroph on 'share'\n");
	exit(5);
	}
strcpy(tmp, path);
strcat(tmp, filename);
#if DEBUG_LEVEL > 1
fprintf(stderr, "Image must fopen: -> %s\n", tmp);
#endif
if ( (fp=fopen(tmp, mode)) != NULL )
	{
#if DEBUG_LEVEL > 1
	fprintf(stderr, "found at '%s'\n", tmp);
#endif
	return fp;
	}

#if DEBUG_LEVEL
fprintf(stderr, "Image must fopen: file '%s' not found, sorry...\n", filename);
#endif

return NULL;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/

