/*
 *	quadpics.c - jeux sur les symetries - juin 2014 a Myrys
 */

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *  fonction de test, ne pas utiliser, le wrapper est un
 *  peu plus bas.
 */

int Image_quadpics_proto_0(Image_Desc *img, int k)
{
Image_Desc	*tmp;
int		x, y, r, g, b;
int		dims;

#if DEBUG_LEVEL
fprintf(stderr, ">>   %s ( %p %d )\n", __func__, img, k);
#endif

/* duplicate the image */
if (NULL==(tmp=Image_clone(img, 0)))
	{
	fprintf(stderr, "in %s, no mem for cloning %p\n", __func__, img);
#if FORCE_ABORT
	abort();
#endif
	return IMAGE_NO_MEM; 
	}

dims = img->width / 2;

#if DEBUG_LEVEL
fprintf(stderr, "   dim/2 -> %d\n", dims);
#endif
for (y=0; y<dims; y++)
	{
	for (x=0; x<dims; x++)
		{
		/* first, grab the value */
		r = img->Rpix[y*2][x*2];
		g = img->Gpix[y*2][x*2];
		b = img->Bpix[y*2][x*2];

		/* upper left  */
		tmp->Rpix[y][x] = r;
		tmp->Gpix[y][x] = g;
		tmp->Bpix[y][x] = b;

		/* upper right */
		tmp->Rpix[y][dims+dims-x-1] = r;
		tmp->Gpix[y][dims+dims-x-1] = g;
		tmp->Bpix[y][dims+dims-x-1] = b;

		/* lower left  */
		tmp->Rpix[dims+dims-y-1][x] = r;
		tmp->Gpix[dims+dims-y-1][x] = g;
		tmp->Bpix[dims+dims-y-1][x] = b;

		/* lower right */
		tmp->Rpix[dims+dims-y-1][dims+dims-x-1] = r;
		tmp->Gpix[dims+dims-y-1][dims+dims-x-1] = g;
		tmp->Bpix[dims+dims-y-1][dims+dims-x-1] = b;

		}
	}

/* copy tmp pics to caller's img */
Image_copy(tmp, img);
/* kill duplicate */
Image_DeAllocate(tmp);	free(tmp);

return 666;
}
/*::------------------------------------------------------------------::*/
int Image_quadpics_proto_1(Image_Desc *img, Image_Desc *dst)
{
int		x, y, r, g, b;
int		dims;

#if DEBUG_LEVEL
fprintf(stderr, ">>   %s ( %p %p )\n", __func__, img, dst);
#endif

dims = img->width / 2;

#if DEBUG_LEVEL
fprintf(stderr, "   dim/2 -> %d\n", dims);
#endif
for (y=0; y<dims; y++)
	{
	for (x=0; x<dims; x++)
		{
		/* first, grab the value */
		r = img->Rpix[y*2][x*2];
		g = img->Gpix[y*2][x*2];
		b = img->Bpix[y*2][x*2];

		/* upper left  */
		dst->Rpix[y][x] = r;
		dst->Gpix[y][x] = g;
		dst->Bpix[y][x] = b;

		/* upper right  */
		dst->Rpix[x][y+dims] = r;
		dst->Gpix[x][y+dims] = 0;
		dst->Bpix[x][y+dims] = 0;

		/* lower leftt  */
		dst->Rpix[x+dims][y] = r;
		dst->Gpix[x+dims][y] = g;
		dst->Bpix[x+dims][y] = b;
		}
	}

return 666;
}
/*::------------------------------------------------------------------::*/
/*
 *	wrapper for simplest functions
 */
int Image_quadsym_simple(Image_Desc *src, Image_Desc *dst, int mode)
{
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p  %d )\n", __func__, src, dst, mode);
#endif

/*
 *	check validity of both images
 */
if ( src->width != src->height )
	{
	fprintf(stderr, "in %s, source img %p is not square.\n", __func__, src);
#if FORCE_ABORT
	abort();
#endif
	return UNKNOW_ERROR; 
	}
if ( foo=Image_compare_desc(src, dst) )
	{
	fprintf(stderr, "err dims %d\n", foo);
	return foo;
	}

/* all things all right ? Ok, go compute 4picz */

foo = UNKNOW_ERROR;		/* assume broken code */

switch (mode)
	{
	case -1:
		fprintf(stderr, "Magic Cow enabled\n");
		break;

	case 0:
		Image_copy(src,dst);
		foo = Image_quadpics_proto_0(dst, mode);
		break;

	case 1:
		Image_copy(src,dst);
		foo = Image_quadpics_proto_1(src, dst);
		break;

	default:
		fprintf(stderr, "Magic Cow disabled\n");
		break;
 
	}

#if DEBUG_LEVEL
fprintf(stderr, "in %s, action return code was %d\n", __func__, foo);
#endif

return 666;
}
/*::------------------------------------------------------------------::*/

