/*
 * cadres84.c
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <math.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_cadre_b84_a(Image_Desc *img, int parX, int parY, int k)
{
int	h, w, x, y, foo;

h = img->height;	w = img->width;

#if DEBUG_LEVEL
fprintf(stderr, "%s: %p  px=%d py=%d  k=%d\n", __func__, img, parX, parY, k);
#endif

if (k) {
	fprintf(stderr, "%s: k (%d) muste be 0\n", __func__, k);
	}

for (y=0; y<h; y++)
	{
	for (foo=0; foo<parX; foo++)
		{
		Image_plot_channel(img, 'r', foo, y, k);
		Image_plot_channel(img, 'g', foo, y, 256-k);
		Image_plot_channel(img, 'b', (w-foo)-1, y, k);
		Image_plot_channel(img, 'r', (w-foo)-1, y, 256-k);
		}
	}

return BAD_CADRE;
}
/*::------------------------------------------------------------------::*/
/* mise en place 9 avril 2010 */
int Image_cadre_b84_b(Image_Desc *img, int k0, int k1)
{
int		x, y, r, g, b;
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p %d %d )\n", __func__, img, k0, k1);
#endif

if (k0) {
	fprintf(stderr, "%s: k0 (%d) muste be 0\n", __func__, k0);
	}
if (k1) {
	fprintf(stderr, "%s: k1 (%d) muste be 0\n", __func__, k1);
	}

for (x=0; x<img->width; x++)
	{
	for (foo=0; foo<k1; foo++)
		{
		y = 0 + foo;
		Image_getRGB(img, x, y, &r, &g, &b);
		if (rand() & 0x1180)
			{
			r = (r/2) + 127; g = b / 3;
			}
		else
			{
			r ^= rand(); g ^= rand(); b ^= rand();
			}
		Image_plotRGB(img, x, y, r, g, b);

		y = img->height - (foo+1);
		Image_getRGB(img, x, y, &r, &g, &b);
		if (rand() & 0x1180)
			{
			r = (r/2) + 127; g = b / 3;
			}
		else
			{
			r ^= rand(); g ^= rand(); b ^= rand();
			}
		Image_plotRGB(img, x, y, r, g, b);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int Image_cadre_b84_c(Image_Desc *img, int k0, int k1, int k2)
{
int	h, w, x, y, foo;

#if DEBUG_LEVEL
fprintf(stderr, "%s: %p %d * %d %d\n", __func__, img, k0, k1, k2);
#endif

fprintf(stderr, "   +------------------------------+\n");
fprintf(stderr, "   | SOFTWARE FAILURE - TASK HELD |\n");
fprintf(stderr, "   | FINISH  ALL DISK  ACTIVITIES |\n");
fprintf(stderr, "   +------------------------------+\n");

return BAD_CADRE;
}
/*::------------------------------------------------------------------::*/

