/*
			cadres2.c
			=========
	more specific 'cadres' around images.
	work in progress, you have to wait (a long time ?)
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	je sais pas trop encore quoi faire, l�...
 * 	probablement poser l'image sur un fond avec une ombre en
 *	dessous...
 */
int
Image_cadre2_soft_1(Image_Desc *src, Image_Desc *dst, RGBA *fond, RGBA *cadre,
				int larg, int v1, int v2, int v3, int v4)
{
int		foo;
Image_Rect	rect;

(void)cadre;  (void)larg;

#if DEBUG_LEVEL
fprintf(stderr, "Cadre2 soft 1: work in progress...\n");
#endif

/*
 *	ze kontrol !
 */
if (src == dst)
	{
	fprintf(stderr, "Cadre2 soft 1: source==destination\n");
	return IMG_OVERWRITE;
	}

foo = Image_clear(dst, fond->r, fond->g, fond->b);
#if DEBUG_LEVEL
fprintf(stderr, "cadre2 soft 1: clear: %d\n", foo);
#endif

/* -------- on copie la partie de l'image source */
fprintf(stderr, "Cadre2 soft 1: copy image source\n");
rect.x = v1;	rect.y = v2;
rect.w = src->width;
rect.h = src->height;
#if DEBUG_LEVEL > 1
Image_dump_rect(&rect, "cadre2 soft 1", 0);
Image_TGA_save("aaaa-a.tga", src, 0);
#endif

foo = Image_copy_rect(src, &rect, dst, rect.x, rect.y);
#if DEBUG_LEVEL
fprintf(stderr, "Cadre2 soft 1: retour copy rect = %d pixels\n", foo);
#endif

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
/*
 *	fonction en chantier: coredump probable
 */
int
Image_cadre2_pixx(Image_Desc *src, Image_Desc *dst, int p1, int p2)
{
int	x, y, foo;
int	r, g, b;
int	p;

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s : %s : is a wip (%d,%d)\n", __FILE__, __func__, p1, p2);
#endif

if ( p1==0 || p2==0 )
	{
	fprintf(stderr, "Cadres 2: pixx: no zero parameters, please.\n");
	return DIVISOR_IS_ZERO;
	}

if ( (p1 > src->width/3) || (p1 > src->height/3) )
	{
	fprintf(stderr, "Cadres 2: pixx: p1 %d out of range.\n", p1);
	return INVALID_PARAM;
	}

for (x=0; x<src->width; x++)
	{
	foo = rand() % p1;
	for (y=0; y<foo; y++)
		{
		Image_getRGB(src, x, y, &r, &g, &b);
		r ^= (rand()%p2);
		g ^= (rand()%p2);
		b ^= (rand()%p2);

		(dst->Rpix[y])[x] = r;
		(dst->Gpix[y])[x] = g;
		(dst->Bpix[y])[x] = b;
		}
	foo = rand() % p1;
	for (y=0; y<foo; y++)
		{
		Image_getRGB(src, x, (src->height-1)-y, &r, &g, &b);
		r ^= (rand()%p2);
		g ^= (rand()%p2);
		b ^= (rand()%p2);

		p = (dst->height-1)-y;
		(dst->Rpix[p])[x] = r;
		(dst->Gpix[p])[x] = g;
		(dst->Bpix[p])[x] = b;
		}
	}

for (y=0; y<src->height; y++)
	{
	foo = rand() % p1;
	for (x=0; x<foo; x++)
		{
		Image_getRGB(src, x, y, &r, &g, &b);
		r ^= (rand()%p2);
		g ^= (rand()%p2);
		b ^= (rand()%p2);

		(dst->Rpix[y])[x] = r;
		(dst->Gpix[y])[x] = g;
		(dst->Bpix[y])[x] = b;
		}
	foo = rand() % p1;
	for (x=0; x<foo; x++)
		{
		Image_getRGB(src, (dst->width-1)-x, y, &r, &g, &b);
		r ^= (rand()%p2);
		g ^= (rand()%p2);
		b ^= (rand()%p2);

		p = (dst->width-1)-x;
		(dst->Rpix[y])[p] = r;
		(dst->Gpix[y])[p] = g;
		(dst->Bpix[y])[p] = b;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	this func is a derivative of the _original_ cadre function
 *	named 'Image_cadre_A' and located in file 'cadres.c'
 */
int
Image_cadre_f(Image_Desc *im, int a, int b)
{
int	x, y;

for (x=0; x<im->width; x++)
	{
	(im->Rpix[0])[x] = a;
	(im->Gpix[0])[x] = a;
	(im->Bpix[0])[x] = a;
	Image_plotRGB(im, x, im->height-1, a, a, a);
	}
for (y=0; y<im->height; y++)
	{
	(im->Rpix[y])[0] = a;
	(im->Gpix[y])[0] = a;
	(im->Bpix[y])[0] = a;
	Image_plotRGB(im, im->width-1, y, a, a, a);
	}
for (x=1; x<im->width-1; x++)
	{
	(im->Rpix[1])[x] = b;
	(im->Gpix[1])[x] = b;
	(im->Bpix[1])[x] = b;
	Image_plotRGB(im, x, im->height-2, b, b, b);
	}
for (y=1; y<im->height-1; y++)
	{
	(im->Rpix[y])[1] = b;
	(im->Gpix[y])[1] = b;
	(im->Bpix[y])[1] = b;
	Image_plotRGB(im, im->width-2, y, b, b, b);
	}
return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
