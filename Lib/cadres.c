/*
	fabrication de cadres dans des images.
	--------------------------------------
	new 24 Janvier 2000
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <math.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	l'epaisseur de ce cadre est de DEUX pixels, et cette valeur
 *	ne doit pas changer, car d'autres fonctions en dependent.
 */
int Image_cadre_A(Image_Desc *im)
{
int	x, y;
int	a, b;

a = 0;	b = 255;

for (x=0; x<im->width; x++) {
	(im->Rpix[0])[x] = a;
	(im->Gpix[0])[x] = a;
	(im->Bpix[0])[x] = a;
	Image_plotRGB(im, x, im->height-1, a, a, a);
	}
for (y=0; y<im->height; y++) {
	(im->Rpix[y])[0] = a;
	(im->Gpix[y])[0] = a;
	(im->Bpix[y])[0] = a;
	Image_plotRGB(im, im->width-1, y, a, a, a);
	}
for (x=1; x<im->width-1; x++) {
	(im->Rpix[1])[x] = b;
	(im->Gpix[1])[x] = b;
	(im->Bpix[1])[x] = b;
	Image_plotRGB(im, x, im->height-2, b, b, b);
	}
for (y=1; y<im->height-1; y++) {
	(im->Rpix[y])[1] = b;
	(im->Gpix[y])[1] = b;
	(im->Bpix[y])[1] = b;
	Image_plotRGB(im, im->width-2, y, b, b, b);
	}
return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 * 30 septembre 2008: Image_cadre_B devient Image_cadre_cracra :)
 */
int Image_cadre_cracra(Image_Desc *im, int largeur, int k)
{
int	x, y, r, g, b;

if (im->width < (largeur+5) || im->height < (largeur+5))
	return IMAGE_TOO_SMALL;

#if DEBUG_LEVEL
fprintf(stderr, "%s: k is $%04x\n", __func__, k);
#endif

for (y=0; y<im->height; y++) {
	for (x=0; x<largeur; x++) {
		r = rand() % (10 + x * 20);
		g = rand() % (11 + x * 10);
		b = rand() % (10 + x * 12);

		(im->Rpix[y])[x] = r;
		(im->Gpix[y])[x] = g;
		(im->Bpix[y])[x] = b;
		}
	for (x=0; x<largeur; x++) {
		r = rand() % (10 + (largeur-x) * 20);
		g = rand() % (10 + (largeur-x) * 10);
		b = rand() % (10 + (largeur-x) * 12);
		Image_plotRGB(im, x+im->width-largeur, y, r, g, b);
		}
	}

for (x=0; x<im->width; x++) {
	for (y=0; y<largeur; y++) {
		r = rand() % (10 + y * 10);
		g = rand() % (10 + y * 12);
		b = rand() % (10 + y * 20);

		(im->Rpix[y])[x] = r;
		(im->Gpix[y])[x] = g;
		(im->Bpix[y])[x] = b;
		}
	for (y=0; y<largeur; y++) {
		r = rand() % (10 + (largeur-y) * 10);
		g = rand() % (10 + (largeur-y) * 12);
		b = rand() % (10 + (largeur-y) * 20);

		(im->Rpix[y+im->height-largeur])[x] = r;
		(im->Gpix[y+im->height-largeur])[x] = g;
		(im->Bpix[y+im->height-largeur])[x] = b;
		}
	}

return OLL_KORRECT;		/* OK, this func is really finishd */
}
/*::------------------------------------------------------------------::*/
/*
 * killing this func - 30 sept 2008 - ave St Exupery
 */
int	Image_cadre_B(Image_Desc *im, int larg)
{
int	foo;
fprintf(stderr, "****** Image_cadre_B is obsolete ******\n");
fprintf(stderr, "****** Use Image_cadre_cracra !! ******\n");
foo = Image_cadre_cracra(im, larg, 0);

return foo;
}
/*::------------------------------------------------------------------::*/
/*
 *	--------------------------------------------------
 *	3 octobre 2009 : il faudra la renommer pour etre	XXX
 *	en accord avec 'tga_cadre' qui la nomme "sweep".	XXX
 */
int Image_cadre_C(Image_Desc *im, int r, int g, int b, int n)
{
int	x, y, x2, y2, ir, ig, ib;
float	fx, fy;
float	d, fn, coef;

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s :  %d  %d  %d  /  %d\n", __func__, r, g, b, n);
#endif

fn = (float)(n*n);
x2 = im->width / 2;
y2 = im->height / 2;

for (y=0; y<im->height; y++) {
	/*
	 *	distance au bord HAUT BAS le plus proche
	 */
	if (y < y2)	fy = (float)y;
	else		fy = (float)((im->height - y)-1);
	fy = fy*fy;

	for (x=0; x<im->width; x++) {
		/*
		 *	distance au bord DROITE GAUCHE le plus proche
		 */
		if (x < x2)	fx = (float)x;
		else		fx = (float)((im->width - x)-1);
		fx = fx*fx;

		/* XXX  un peu plus d'explications ici */
		if (fx < fy)	d = fx;
		else		d = fy;

		if (d < fn) {
			coef = d / fn;
			ir = (im->Rpix[y])[x];
			ig = (im->Gpix[y])[x];
			ib = (im->Bpix[y])[x];

			ir=(int)(((float)ir * coef) + ((float)r * (1.0-coef)));
			ig=(int)(((float)ig * coef) + ((float)g * (1.0-coef)));
			ib=(int)(((float)ib * coef) + ((float)b * (1.0-coef)));
			/* Image_plotRGB(im, x, y, ir, ig, ib); */
			(im->Rpix[y])[x] = ir;
			(im->Gpix[y])[x] = ig;
			(im->Bpix[y])[x] = ib;
			}
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *    this function was the successor of the 'C' cadre. Need a rename.
 */
int Image_cadre_bruit(Image_Desc *im, int r, int g, int b, int n)
{
int	x, y, x2, y2, ir, ig, ib;
float	fx, fy;
float	d, fn, level;

#if DEBUG_LEVEL
fprintf(stderr, "%s : %d %d %d / %d\n", __func__, r, g, b, n);
#endif

#if DEBUG_LEVEL
fprintf(stderr, "   %s :  current random is %d\n", __func__, rand());
#endif

fn = (float)(n*n);
x2 = im->width / 2;
y2 = im->height / 2;

for (y=0; y<im->height; y++) {
	/*
	 *	distance au bord HAUT BAS le plus proche
	 */
	if (y < y2)	fy = (float)y;
	else		fy = (float)((im->height - y)-1);
	fy = fy*fy;

	for (x=0; x<im->width; x++) {
		/*
		 *	distance au bord DROITE GAUCHE le plus proche
		 */
		if (x < x2)	fx = (float)x;
		else		fx = (float)((im->width - x)-1);
		fx = fx*fx;

		if (fx < fy)	d = fx;
		else		d = fy;

		if (d < fn) {
			level = (int)((d / fn) * 1000.0);
			if ( (rand()%1000) < level ) {
				Image_getRGB(im, x, y, &ir, &ig, &ib);	
				}
			else	{
				ir = r;    ig = g;    ib = b;
				}
			/* Image_plotRGB(im, x, y, ir, ig, ib); */
			(im->Rpix[y])[x] = ir;
			(im->Gpix[y])[x] = ig;
			(im->Bpix[y])[x] = ib;
			}
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/* inventee pour blablanux :) */
int Image_cadre_E(Image_Desc *im, int v1, int v2, int v3, int dim)
{
int	foo, bar;
Image_Rect rect;

for (foo=0; foo<dim; foo++) {
	bar = 1 << (foo%8);
#if DEBUG_LEVEL > 1
	fprintf(stderr, "cadre E: foo is %d, bar is 0x%x\n", foo, bar);
#endif
	rect.x = foo;
	rect.y = foo;
	rect.w = im->width - foo*2;
	rect.h = im->height - foo*2;
	Image_draw_rect(im, &rect, v1^bar, v2^bar, v3^bar);
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*						5 Fev 2001
 *		pas de d�grad�
 */
int Image_cadre_pattern_0(Image_Desc *im, Image_Desc *pat, int larg)
{
int	x, y, r, g, b;

/*
 *	Tiens, je la supposais plus facile a ecrire...
 */
for (y=0; y<im->height; y++) {
	for (x=0; x<larg; x++) {
		Image_getRGB(pat, x%pat->width, y%pat->height, &r, &g, &b);
		/* Image_plotRGB(im, x, y, r, g, b); */
		(im->Rpix[y])[x] = r;
		(im->Gpix[y])[x] = g;
		(im->Bpix[y])[x] = b;
		}
	for (x=im->width-larg; x<im->width; x++) {
		Image_getRGB(pat, x%pat->width, y%pat->height, &r, &g, &b);
		/* Image_plotRGB(im, x, y, r, g, b); */
		(im->Rpix[y])[x] = r;
		(im->Gpix[y])[x] = g;
		(im->Bpix[y])[x] = b;
		}
	}

for (x=0; x<im->width; x++) {
	for (y=0; y<larg; y++) {
		Image_getRGB(pat, x%pat->width, y%pat->height, &r, &g, &b);
		/* Image_plotRGB(im, x, y, r, g, b); */
		(im->Rpix[y])[x] = r;
		(im->Gpix[y])[x] = g;
		(im->Bpix[y])[x] = b;
		}
	for (y=im->height-larg; y<im->height; y++) {
		Image_getRGB(pat, x%pat->width, y%pat->height, &r, &g, &b);
		/* Image_plotRGB(im, x, y, r, g, b); */
		(im->Rpix[y])[x] = r;
		(im->Gpix[y])[x] = g;
		(im->Bpix[y])[x] = b;
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*						12 Feb 2001
 *		d�grad� proportionnel � la distance 
 */
int Image_cadre_pattern_1(Image_Desc *im, Image_Desc *pat, int n)
{
int	x, y, x2, y2, ir, ig, ib, jr, jg, jb, r, g, b;
float	fx, fy;
float	d, fn, level;
int	lmax;

fn = (float)(n*n);
fx = fy = 0.0;				/* warning shutter */
x2 = im->width / 2;
y2 = im->height / 2;

#if DEBUG_LEVEL
fprintf(stderr, "*** %s: fn = %f\n", __func__, fn);
#endif

lmax = 0;
for (y=0; y<im->height; y++) {
	/*
	 *	distance au bord HAUT BAS le plus proche
	 */
	if (y < y2)	fy = (float)y;
	else		fy = (float)((im->height - y)-1);
	fy = fy*fy;

	for (x=0; x<im->width; x++) {

		/*
		 *	distance au bord DROITE GAUCHE le plus proche
		 */
		if (x < x2)	fx = (float)x;
		else		fx = (float)((im->width - x)-1);
		fx = fx*fx;

		/*
		 *	we take the smallest of the two distances
		 */
		if (fx < fy)	d = fx;
		else		d = fy;

		if (d < fn) {
			level = (int)(100 * d / fn);
			if (level > lmax) {
				lmax = level;
				}

			Image_getRGB(im, x, y, &ir, &ig, &ib);	
			Image_getRGB(pat, x%pat->width, y%pat->height,
								&jr, &jg, &jb);	

			r = (ir * level) + (jr * (100-level));
			g = (ig * level) + (jg * (100-level));
			b = (ib * level) + (jb * (100-level));
			/* Image_plotRGB(im, x, y, r/100, g/100, b/100); */
			(im->Rpix[y])[x] = r / 100;
			(im->Gpix[y])[x] = g / 100;
			(im->Bpix[y])[x] = b / 100;
			}
		}
	}
#if DEBUG_LEVEL
fprintf(stderr, "cadre pattern 1 : lmax = %d\n", lmax);
#endif
return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
int Image_cadre_pattern_2(Image_Desc *im, Image_Desc *pat, int n)
{
int	x, y, x2, y2, ir, ig, ib;
int	xp, yp;
float	fx, fy;
float	d, fn, level;
int	lmax;

fn = (float)(n*n);
fx = fy = 0.0;				/* warning shutter */
x2 = im->width / 2;
y2 = im->height / 2;

#if DEBUG_LEVEL
fprintf(stderr, "*** cadre pat 2 : fn = %f\n", fn);
fprintf(stderr, "*** cadre pat 2 : pat: %d x %d\n", pat->width, pat->height);
#endif

lmax = 0;
for (y=0; y<im->height; y++) {
	/* distance au bord HAUT BAS le plus proche */
	if (y < y2)	fy = (float)y;
	else		fy = (float)((im->height - y)-1);
	fy = fy*fy;

	for (x=0; x<im->width; x++) {
		/* distance au bord DROITE GAUCHE le plus proche */
		if (x < x2)	fx = (float)x;
		else		fx = (float)((im->width - x)-1);
		fx = fx*fx;
		/* we take the smallest of the two distances */
		if (fx < fy)	d = fx;
		else		d = fy;

		if (d < fn) {
			level = (int)((d / fn) * 1000.0);
			if ( (rand()%1000) < level ) {
				Image_getRGB(im, x, y, &ir, &ig, &ib);	
				}
			else {
				xp = x % pat->width;
				yp = y % pat->height;
				Image_getRGB(pat, xp, yp, &ir, &ig, &ib);	
				}
			/* Image_plotRGB(im, x, y, ir, ig, ib); */
			(im->Rpix[y])[x] = ir;
			(im->Gpix[y])[x] = ig;
			(im->Bpix[y])[x] = ib;
			}
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*				nouveau : Mon 12 Sep 2022 10:47:18 PM CEST
 */
int Image_cadre_std2(Image_Desc *im, int r, int g, int b, int nx, int ny)
{
int			x, y;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p   %d %d %d   %d %d )\n", __func__,
			im,	r, g, b,   nx, ny);
#endif

for (y=0; y<im->height; y++) {
	for (x=0; x<nx; x++) {
		(im->Rpix[y])[x] = r;
		(im->Gpix[y])[x] = g;
		(im->Bpix[y])[x] = b;
		}
	for (x=0; x<nx; x++) {
		(im->Rpix[y])[im->width-x-1] = r;
		(im->Gpix[y])[im->width-x-1] = g;
		(im->Bpix[y])[im->width-x-1] = b;
		}
	}

for (x=0; x<im->width; x++) {
	for (y=0; y<ny; y++) {
		(im->Rpix[y])[x] = r;
		(im->Gpix[y])[x] = g;
		(im->Bpix[y])[x] = b;
		}
	for (y=0; y<ny; y++) {
		(im->Rpix[y+im->height-ny])[x] = r;
		(im->Gpix[y+im->height-ny])[x] = g;
		(im->Bpix[y+im->height-ny])[x] = b;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*		et maintenant ? on peut aller voir 'cadres2.c' ?	*/
/*::------------------------------------------------------------------::*/
