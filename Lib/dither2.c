/*
		dither2.c
		---------
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/* ya pas un probleme d'offset dans ces tables ? XXX */
static
int	dx[]= {	0, 0, 0,
		1, 1, 1,
		2, 2, 2	};
static
int	dy[]= {	0, 1, 2,
		0, 1, 2,
		0, 1, 2	};

/*::------------------------------------------------------------------::*/
/*
 *	I don't know how to do that ;) but I try hardly ;)
 */
int
Image_dither_3x3_0(Image_Desc *s, Image_Desc *d, int uh)
{
int	x, y, r, g, b;
int	foo, sr, sg, sb;

#if DEBUG_LEVEL
fprintf(stderr, "* %s  le seuil est %d\n", __func__, uh);
#endif

if ( (foo=Image_compare_desc(s, d)) )
    {
    fprintf(stderr, "dither 3x3 0: images are differents %d\n", foo);
    return foo;
    }

#if DEBUG_LEVEL > 2
Image_TGA_save("/tmp/aaaa_debug_a.tga", s, 0);
#endif

/* XXX Image_clear(d, 0, 0, 0); */

for (y=0; y<s->height-3; y+=3)
	{
	for (x=0; x<s->width-3; x+=3)
		{
		/* compute the average of pixels in a 3x3 field */
		sr = sg = sb = 0;
		for (foo=0; foo<9; foo++)
			{
			sr += (s->Rpix[y+dy[foo]])[x+dx[foo]];
			sg += (s->Gpix[y+dy[foo]])[x+dx[foo]];
			sb += (s->Bpix[y+dy[foo]])[x+dx[foo]];
			}
		r = sr / 9;
		g = sg / 9;
		b = sb / 9;

		/* plot all point, with colors based on average values */

		(d->Rpix[y])   [x]   = r<uh ? 0 : 255;	
		(d->Rpix[y+1]) [x]   = r<uh ? 0 : 255;	
		(d->Rpix[y])   [x+1] = r<uh ? 0 : 255;	
		(d->Rpix[y+1]) [x+1] = r<uh ? 0 : 255;	

		(d->Gpix[y])   [x+1] = g<uh ? 0 : 255;	
		(d->Gpix[y+1]) [x+1] = g<uh ? 0 : 255;	
		(d->Gpix[y])   [x+2] = g<uh ? 0 : 255;	
		(d->Gpix[y+1]) [x+2] = g<uh ? 0 : 255;	

		(d->Bpix[y+1]) [x+1] = b<uh ? 0 : 255;	
		(d->Bpix[y+2]) [x]   = b<uh ? 0 : 255;	
		(d->Bpix[y+2]) [x+2] = b<uh ? 0 : 255;	
		(d->Bpix[y+2]) [x+3] = b<uh ? 0 : 255;	
		}
	}


#if DEBUG_LEVEL > 2
Image_TGA_save("/tmp/aaaa_debug_b.tga", d, 0);
#endif

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	28 mars 2003...
 *
 *	parameter 'uh' is not used.
 */
int
Image_dither_3x3_1(Image_Desc *s, Image_Desc *d, int uh)
{
int	foo, x, y;
int	sr, sg, sb;

if ( (foo=Image_compare_desc(s, d)) )
    {
    fprintf(stderr, "dither 3x3 1: images are differents %d\n", foo);
    return foo;
    }

#if DEBUG_LEVEL
fprintf(stderr, "* Image_dither_3x3_1 (%s) is experimental\n", __FILE__);
#endif

/* XXX Image_clear(d, 0, 0, 0); */

for (y=0; y<s->height-3; y+=3)
	{
	for (x=0; x<s->width-3; x+=3)
		{
		/* compute the average of pixels in a 3x3 field */
		sr = sg = sb = 0;
		for (foo=0; foo<9; foo++)
			{
			sr += (s->Rpix[y+dy[foo]])[x+dx[foo]];
			sg += (s->Gpix[y+dy[foo]])[x+dx[foo]];
			sb += (s->Bpix[y+dy[foo]])[x+dx[foo]];
			}

		for (foo=0; foo<sr/255; foo++)
			{
			if (foo>=9)
				{
				fprintf(stderr, "overflow\n");
				return 666;
				}
			(d->Rpix[y+dy[foo]])[x+dx[foo]] = 255;
			}
		for (foo=0; foo<sg/255; foo++)
			{
			if (foo>=9)
				{
				fprintf(stderr, "overflow\n");
				return 666;
				}
			(d->Gpix[y+dy[foo]])[x+dx[foo]] = 255;
			}
		for (foo=0; foo<sb/255; foo++)
			{
			if (foo>=9)
				{
				fprintf(stderr, "overflow\n");
				return 666;
				}
			(d->Bpix[y+dy[foo]])[x+dx[foo]] = 255;
			}
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int
Image_dither_3x3_2(Image_Desc *s, Image_Desc *d, int uh)
{
int	foo;

if ( (foo=Image_compare_desc(s, d)) )
    {
    fprintf(stderr, "dither 3x3 2: images are differents %d\n", foo);
    return foo;
    }

/* XXX Image_clear(d, 0, 0, 0); */

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
int
Image_dither_3x3_3(Image_Desc *s, Image_Desc *d, int uh)
{
int	foo;

if ( (foo=Image_compare_desc(s, d)) )
    {
    fprintf(stderr, "dither 3x3 3: images are differents %d\n", foo);
    return foo;
    }

/* XXX Image_clear(d, 0, 0, 0); */

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
