/*
		text1.c
		-------
*/

#include  <stdio.h>
#include  <string.h>
#include  <unistd.h>
#include  <stdlib.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	parameter 'flags' is not used (reserved), must be 0.
 */
int
Image_txt1_box_0(Image_Desc *img, char *txt, int x, int y, int w, 
			RGBA *paper, RGBA *ink, int flags)
{
int		foo, lstr;
Image_Rect	rect;

if (flags != 0)
	{
	fprintf(stderr, "%s : wrong flags: 0x%x\n", __func__, flags);
	return WRONG_FLAG;
	}
if (w < 0)
	{
	fprintf(stderr, "Ahem, w (%d) is < 0 ?\n", w);
	}

lstr = strlen(txt);

#if DEBUG_LEVEL > 1
fprintf(stderr, "Txt1 box 0: taille texte: %d\n", lstr);
Image_print_rgba("Papier", paper, 0);
Image_print_rgba("Encre",  ink, 0);
#endif

rect.x = x;	rect.y = y;
rect.h = 8 + (w*2);
rect.w = (lstr*8) + (w*2);

foo = Image_paint_rect(img, &rect, paper->r, paper->g, paper->b);
if (foo)
	Image_print_error("txt box 0: paint rect: ", foo);

foo = Image_trace_chaine_1(img, txt, x+w, y+w, "libimage.fonte", paper, ink);
if (foo)
	Image_print_error("txt box 0: trace chaine 0: ", foo);

foo = Image_draw_rect(img, &rect, ink->r, ink->g, ink->b);
if (foo)
	Image_print_error("txt box 0: draw rect: ", foo);

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* new --> Sun Feb  2 20:12:07 CET 2014   */
/*
 *	Don't forget to load a font, eg :
 *	Image_load_fnt8x8("8x8thin", NULL, 0);
 */
int	Image_txt1_big_0(Image_Desc *im, char *txt, int x, int y,
						int xf, int yf)
{
int		foo, idx, len;
int		xpos;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p \"%s\" %d %d %d %d )\n", __func__, im,
				txt, x, y, xf, yf);
#endif

len = strlen(txt);
#if DEBUG_LEVEL
fprintf(stderr, "  len = %d\n", len);
#endif

xpos = x;
for (idx=0; idx<len; idx++)
	{
#if DEBUG_LEVEL > 2
	fprintf(stderr, "    idx %3d   ch 0x%02x  x %d\n",
				idx, txt[idx], xpos);
#endif
	foo = Image_trace_big_char_0(im, xpos, y, txt[idx], xf, yf);
	if (foo) {
		fprintf(stderr, "Image_trace_big_char_0 -> %d\n", foo);
		}
	xpos += (xf * 8);
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
