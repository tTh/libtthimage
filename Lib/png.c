/*

		------ libtthimage ------
	PNG aka "portable network graphics"s.

*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <string.h>
#include  <png.h>
#include  <zlib.h>

#undef DEBUG_LEVEL
#define DEBUG_LEVEL 1

#include  "../tthimage.h"

/*
 * 		http://www.libpng.org/pub/png/pngbook.html
 */
/*::------------------------------------------------------------------::*/
void write_png_version_info(void)
{
fprintf(stderr, "   Compiled with libpng %s; using libpng %s.\n",
				PNG_LIBPNG_VER_STRING, png_libpng_ver);
fprintf(stderr, "   Compiled with zlib %s; using zlib %s.\n",
				ZLIB_VERSION, zlib_version);
}
/*::------------------------------------------------------------------::*/
Image_Desc * Image_PNG_alloc_load(char *fname, int k)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, fname, k);
write_png_version_info();
#endif

return NULL;
}
/*::------------------------------------------------------------------::*/
/*
 *			http://zarb.org/~gc/html/libpng.html
 *			https://gist.github.com/niw/5963798
 */
int Image_save_as_PNG(Image_Desc *img, char *fname, int p1, int p2)
{
FILE			*fp;
int			x, y, foo;
png_structp		png;
png_infop		info;
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p '%s' %d %d )\n", __func__, img, fname, p1, p2);
write_png_version_info();
#endif

fp = fopen(fname, "wb");
if (NULL == fp) {
	perror(fname);
	return BASIC_IO_WR_ERR;
	}

png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

info = png_create_info_struct(png);

if (setjmp(png_jmpbuf(png))) abort();

png_init_io(png, fp);


png_set_IHDR(	png,
		info,
		img->width, img->height,
		8,
		PNG_COLOR_TYPE_RGBA,
		PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT,
		PNG_FILTER_TYPE_DEFAULT			);

png_write_info(png, info);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
