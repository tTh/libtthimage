/*
		alpha2.c
		--------
	Various transparency operations, aka "alpha channel ops"

*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int	Image_alpha_setvalue(Image_Desc *dst, int v_alpha)
{
int	y;

#if DEBUG_LEVEL
fprintf(stderr, "%s : %d -> %p\n", __func__, v_alpha, dst);
#endif

if (dst->type != IMAGE_RGBA)
	{
	fprintf(stderr, "%s : picz %p haz no alpha\n", __func__, dst);
#if MUST_ABORT
	abort();
#endif
	return NO_ALPHA_CHANNEL;
	}

for (y=0; y<dst->height; y++)
	{
	memset(dst->Apix[y], v_alpha, dst->width);
	}
dst->modified   = 1;
return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	values of r,g,b pixels are scaled by alpha value.
 */
int
Image_alpha_reduce(Image_Desc *src, Image_Desc *dst, int yo)
{
int	foo, x, y;
int	r, g, b, a;

#if DEBUG_LEVEL
fprintf(stderr, "Image: alpha reduce: %p -> %p (yo=%d)\n", src, dst, yo);
#endif

if (src->type != IMAGE_RGBA)
    {
    fprintf(stderr, "Image: alpha reduce: %p is not RGBA", src);
    return NO_ALPHA_CHANNEL;
    }

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "Image: alpha reduce: images are differents %d\n", foo);
    return foo;
    }

for (y=0; y<src->height; y++)
	{
	for (x=0; x<src->width; x++)
		{
		Image_getRGBA(src, x, y, &r, &g, &b, &a);
		r = (r * a) / 256;
		g = (g * a) / 256;
		b = (b * a) / 256;
		Image_plotRGB(dst, x, y, r, g, b);
		}
	}

dst->modified   = 1;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	this fonction need more explanations :(
 */
int
Image_poke_alpha_from_rgb(Image_Desc *src, Image_Desc *dst, 
					int fr, int fg, int fb, int flag)
{
int	foo, x, y, r, g, b, a0, a1, a;

a = 0;		/* warning shutdown */

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "Poke alpha from rgb: images are differents %d\n", foo);
    return foo;
    }

if ( dst->Apix == NULL )
    {
    fprintf(stderr, "poke alpha from rgb: dest image have NO alpha channel\n");
    return NO_ALPHA_CHANNEL;
    }

a0 = flag ? 0 : 255;
a1 = flag ? 255 : 0;

#if DEBUG_LEVEL
fprintf(stderr, "For rgb = %d,%d,%d  alpha wil be %d\n", fr, fg, fb, a);
#endif

for (y=0; y<src->height; y++)
	{
	for (x=0; x<src->width; x++)
		{
		Image_getRGB(src, x, y, &r, &g, &b);

		if (r==fr && g==fg && b==fb)	a = a0;
		else				a = a1;

		Image_plotRGBA(dst, x, y, r, g, b, a);
		}
	}

dst->modified   = 1;

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/

