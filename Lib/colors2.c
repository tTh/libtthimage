/*
                colors2.c
		---------		new 18 jan 2010
*/

#include    <stdio.h>
#include    <stdlib.h>		/* for rand() */
#include    <time.h>
#include    <string.h>
#include    <math.h>

#ifdef NEED_ALLOCA_H
#include    <alloca.h> 
#endif

#include    "../tthimage.h"

/*::------------------------------------------------------------------::*/
/* nouveau 20 janvier 2010 */
int
Image_AutoSeuilGray(Image_Desc *s, Image_Desc *d, int *ps)
{
long	*histo, cumul;
int	foo, x, y, r, g, b, seuil;
long	surf2;

#define NB_SLOTS	(256*3)
if((histo=(long *)alloca(NB_SLOTS*sizeof(long))) == NULL)
	return BUFFER_NO_MEM;
 
for (foo=0; foo<NB_SLOTS; foo++)	histo[foo] = 0L;

for (y=0; y<s->height; y++)
	{
	for (x=0; x<s->width; x++)
		{
		Image_getRGB(s, x, y, &r, &g, &b);
		cumul = (r + g + b);
		if (cumul > NB_SLOTS)
			{
			fprintf(stderr, "%s : cumul is %ld\n",
						__func__, cumul);
#if FORCE_ABORT
			abort();
#endif
			return FULL_NUCKED;
			}
		histo[cumul]++;
		}
	}

surf2 = (s->width*s->height)/2;	
#if DEBUG_LEVEL
fprintf(stderr, "surface : %ld\n", surf2);
#endif

cumul = 0L;
seuil = 0;
for (foo=0; foo<NB_SLOTS; foo++)
	{
	cumul += histo[foo];
	if (cumul < surf2)
		seuil = foo;
#if DEBUG_LEVEL > 1
	printf("asg %4d %8ld %8ld\n", foo, histo[foo], cumul);
#endif
	}

*ps = seuil;		/* give a nice value to our caller */

#if DEBUG_LEVEL
fprintf(stderr, "%s :  seuil is %d\n", __func__, seuil);
#endif

for (y=0; y<s->height; y++)
	{
	for (x=0; x<s->width; x++)
		{
		Image_getRGB(s, x, y, &r, &g, &b);
		cumul = (r + g + b);
		if (cumul < seuil)
			r = g = b = 0;
		else
			r = g = b = 255;
		Image_plotRGB(d, x, y, r, g, b);
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "end of %s\n", __func__);
#endif

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/* nouveau 18 janvier 2010 */
/* parameter k is currently unused */
int Image_BiColor_0(Image_Desc *src, Image_Desc *dst, int k)
{
Image_Desc	*tmp;
int		foo, bar;
int		x, y, r, g, b, seuil;
int		rb, gb, bb;		/* suffix b -> binary */
int		rd, gd, bd;		/* suffix d -> destination */
#if DEBUG_LEVEL
int		hit, mess;
#endif

#if DEBUG_LEVEL
fprintf(stderr, "$$$ %s ( %p %p %d )\n", __func__, src, dst, k);
#endif

/* calculer le(s) seuil(s) de binarisation */
tmp = Image_clone(src, 0);
if (NULL==tmp)
	{
	fprintf(stderr, "can't clone in %s\n", __func__);
	abort();		/* FIXME */
	}

foo = Image_AutoSeuilGray(src, tmp, &seuil);
fprintf(stderr, "seuil auto -> %d, v=%d\n", foo, seuil);

#if DEBUG_LEVEL > 1
Image_TGA_save("aaaa-bicolor0-a.tga", tmp, 0);
fprintf(stderr, "Ah ah, %s, pic 'a' saved\n", __func__);
#endif

#if DEBUG_LEVEL
hit = mess = 0;
#endif

/* and now, this is the big loop over all our pixels */
for (y=0; y<src->height; y++)
	{
	for (x=0; x<src->width; x++)
		{
		/* lecture de la source */
		Image_getRGB(src, x, y, &r, &g, &b);

		/* lecture du masque binaire XXX */
		Image_getRGB(tmp, x, y, &rb, &gb, &bb);

		/* construction de l'image destination */
		rd = gd = bd = 0;

		if (0 != rb)
			{
#if DEBUG_LEVEL
			hit++;
#endif
			rd = r;		
			gd = (g*3) / 2;
			}
		else
			{
#if DEBUG_LEVEL
			mess++;
#endif
			gd = g;
			bd = (b*3) / 2;
			}
		
		Image_plotRGB(dst, x, y, rd, gd, bd);
		}
	}

#if DEBUG_LEVEL > 1
Image_TGA_save("aaaa-bicolor0-b.tga", dst, 0);
fprintf(stderr, "Ah ah, %s, pic 'b' saved\n", __func__);
#endif

#if DEBUG_LEVEL
fprintf(stderr, "hit: %d  mess:%d\n", hit, mess);
fprintf(stderr, "$$$ end of %s\n", __func__);
#endif

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* nouveau 23 janvier 2010 */
/* parameters ka & kb are currently unused */
int Image_BiColor_1(Image_Desc *src, Image_Desc *dst, int ka, int kb)
{
Image_Desc		*tmp;
int			foo, seuil;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p %p %d %d )\n", __func__, src, dst, ka, kb);
#endif

/* calculer le(s) seuil(s) de binarisation */
tmp = Image_clone(src, 0);
if (NULL==tmp)
	{
	fprintf(stderr, "can't clone in %s\n", __func__);
	abort();		/* FIXME */
	}
foo = Image_AutoSeuilGray(src, tmp, &seuil);
fprintf(stderr, "in %s, seuil auto -> %d, v=%d\n", __func__, foo, seuil);


return WTF_OSEF;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/

