/*
 * Routines de test de machins de la LibImage de tTh
 *
 *            Bien entendu, pas la moindre explication ici :)
 */

int Essai_Extractbits(char *fname, int k);
int Essai_des_Quadpics(char *fsrc, int k);
int Essai_des_drawings(char *fname, int k);
int Essai_des_Combines(char *fname, int k);
int Essai_de_la_Recursion(char *fname, int k);
int Essai_des_Glitches(char *fsrc, int k);
int 	Essai_anamorphoses(char *fsrc, double dk, int k);

int     Essai_des_bitplanes(char *txt, int w, int h);
int	Essayer_les_alphas(char *fname, int k);
int     Essai_des_bitplanes(char *txt, int w, int h);

int Manipuler_les_couleurs(char *fname, int k);
int Essai_luminance(char *scrname, int k);
int Essais_plot_Map(int k);
int Essai_raw16bits(char *srcname, int k);
int Essai_color_2_map(char *srcname, int k);
int Essai_des_gray_ops(char *srcname, int k);
int Essai_des_Contrastes(char *, int);
int Essai_des_lut15bits(char *srcname, int k);
int Essai_des_mires(char *, int, int);
int Essai_des_zooms(char *srcname, int k);

int Test_Classif(char *srcname, int k);

int Essai_des_7_segments(char *srcname, int flag);
int Essai_des_distances(char *srcname, int nbre, int flag);
int Essai_des_cadres(char *srcname, int flags);
int Filtre_Directionnel(char *srcname, int flags);

int Test_des_trucs_2x2(char *srcname, int k);
int Test_des_warpings(char *srcname, int k);
int Test_des_mosaiques(char *srcname, int k);

int Test_hf15_synth_fromfunc(char *fname);
int Test_hf15_synth_fromPtl(char *fname, char *oname);
int Test_funcs_hf15(char *mntname, int flag);
int Test_des_df3(char *txt, int k);

int Test_vignettes(char *srcname, int k);

int Test_classif(char *srcname, int k);
int Test_RGBmask(char *srcname);
void Test_rgb2xyz(void);

int  Essai_des_marquages(char *srcname, int nombre, char *texte);
void Test_double_size(void);
int Test_new_scale(char *fnamesrc, int coef);

int  Test_copie_de_rectangles(char *nomsource);
int  Test_Egalisations(char *nomsource, int k);
int  Test_Dithering(char *nomsource, int k);
int  Test_des_marquages(char *srname, int k1, int k2);
int  Test_Effects_A(char *nomsource, int flag);

int Test_des_tamppools(char *imgname, int param);
int Essai_export_palettes(char *basename);

int Test_des_filtres(char *srcname, int k);

void Test_des_patterns(char *prefix, int foo, int bar);
int Test_des_pixeliz(char *srcname, int flags);

int Essai_des_bitblt(char *srcname, int k);
int Essai_des_gadgrect(char *srcname, int k);

int Essai_Televisions(char *source, int k);

/* dans essais2;c */
int Essai_des_big_chars(char *fname, int k);
int Essai_des_jauges(char *fname, int k);


/*
 * Le programme principal est 'testtga.c' mais il serait bien de faire
 * une fonction qui enchaine tous les Tests et tous les Essais afin
 * de pouvoir facilement valgrinder le tout :)
 */

