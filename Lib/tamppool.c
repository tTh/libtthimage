/*
 *	tamppool.c	new 21 mars 2007 - ave St Exupery
 *	-------------------------------------------------
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  "../tthimage.h"
/*::------------------------------------------------------------------::*/
#define TAMPPOOL_VERSION	"0.0.3"
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
void
Image_Pool_infos(int flag)
{
printf("*** Images Pools v %s,  %s\n", TAMPPOOL_VERSION, __DATE__);

if (flag)
	{
	printf("   taille des noms: %d\n", IMG_OBJNAME_LEN);
	
	}
}
/*::------------------------------------------------------------------::*/
void  *
Image_Pool_create(char *name, int nbre, int width, int height)
{
fprintf(stderr, "Image Pool Create '%s', %d, %d x %d\n",
				name, nbre, width, height);

return NULL;
}
/*::------------------------------------------------------------------::*/
int
Image_Pool_describe(void *ptr, char *text, int flags)
{

printf("+p+  Image Pool describe, explanation '%s'\n", text);
printf("+p+                  descriptor is at %p\n", ptr);
if (flags)
	printf("+p+       flag is not zero\n");

if (NULL == ptr)
	{
	fprintf(stderr, "Image Pool descriptor is NULL\n");
	return NULL_DESCRIPTOR;	
	}

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
int
Image_Pool_destroy(void *ptr, char *text, int flags)
{
fprintf(stderr, "Image Pool %p Destroy '%s'\n", ptr, text);

if (flags)
	{
	fprintf(stderr, "%s: flag = %d\n", __func__, flags);
	}

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
