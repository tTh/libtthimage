/*
                    patterns4.c
		    ------------------
		    some random noises
	
	see also 'patterns.c' & 'patterns2.c'

*/

#include   <stdio.h>
#include   <stdlib.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	bruit monochrome
 */
int
Image_gray_noise_0(Image_Desc *dst, int low, int high)
{
int	x, y, v, delta;

delta = high - low;

if (0 == delta) {
	fprintf(stderr, "%s: bad low/high parameters %d %d <- delta is 0\n",
			__func__,			low, high);
	return DIVISOR_IS_ZERO;
	}

for (x=0; x<dst->width; x++)
	{
	for (y=0; y<dst->height; y++)
		{
		v = (rand() % delta) + low;
		(dst->Rpix[y])[x] = v;
		(dst->Gpix[y])[x] = v;
		(dst->Bpix[y])[x] = v;
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
int
Image_rgb_noise_0(Image_Desc *dst, int low, int high)
{
int	x, y, r, g, b, delta;

delta = high - low;

if (0 == delta) {
	fprintf(stderr, "%s: bad low/high parameters %d %d <- delta is 0\n",
			__func__,			low, high);
	return DIVISOR_IS_ZERO;
	}

for (x=0; x<dst->width; x++)
	{
	for (y=0; y<dst->height; y++)
		{
		r = (rand() % delta) + low;
		g = (rand() % delta) + low;
		b = (rand() % delta) + low;
		(dst->Rpix[y])[x] = r;
		(dst->Gpix[y])[x] = g;
		(dst->Bpix[y])[x] = b;
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
int
Image_rgba_noise_0(Image_Desc *dst, int low, int high)
{
int	x, y, r, g, b, a, delta;

if (dst->type != IMAGE_RGBA)
	{
	fprintf(stderr, "RGBA noise 0: image is not RGBA (%d)\n", dst->type);
	return IMAGE_BAD_TYPE;
	}

if (dst->Apix == NULL)
	{
	fprintf(stderr, "RGBA noise 0: %p: no alpha buffer ?\n", dst);
	return NO_ALPHA_CHANNEL;
	}

delta = high - low;

for (x=0; x<dst->width; x++)
	{
	for (y=0; y<dst->height; y++)
		{
		r = (rand() % delta) + low;
		g = (rand() % delta) + low;
		b = (rand() % delta) + low;
		a = (rand() % delta) + low;
		Image_plotRGBA(dst, x, y, r, g, b, a);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* new 21 mars 2002 - need more doc
 */
int
Image_rgb_noise_1(Image_Desc *dst, RGBA *low, RGBA *high)
{
int	minr, ming, minb;
int	maxr, maxg, maxb;
int	dltr, dltg, dltb;
int	x, y, r, g, b;

#if DEBUG_LEVEL
Image_print_rgba("rgb noise 1: low", low, 0);
Image_print_rgba("rgb noise 1: high", high, 0);
#endif

minr = (low->r < high->r) ? low->r : high->r;
ming = (low->g < high->g) ? low->g : high->g;
minb = (low->b < high->b) ? low->b : high->b;
maxr = (low->r > high->r) ? low->r : high->r;
maxg = (low->g > high->g) ? low->g : high->g;
maxb = (low->b > high->b) ? low->b : high->b;
dltr = maxr - minr;
dltg = maxg - ming;
dltb = maxb - minb;

#if DEBUG_LEVEL
printf("\n");
printf("rgb noise 1: min    %3d  %3d  %3d\n", minr, ming, minb);
printf("rgb noise 1: max    %3d  %3d  %3d\n", maxr, maxg, maxb);
printf("rgb noise 1: delta  %3d  %3d  %3d\n", dltr, dltg, dltb);
#endif

if ( dltr==0 || dltg==0 || dltb==0 )
	{
	fprintf(stderr, "Rgb noise 1: a delta is zero...\n");
	return DIVISOR_IS_ZERO;
	}

for (x=0; x<dst->width; x++)
	{
	for (y=0; y<dst->height; y++)
		{
		r = (rand() % dltr) + minr;
		g = (rand() % dltg) + ming;
		b = (rand() % dltb) + minb;
		(dst->Rpix[y])[x] = r;
		(dst->Gpix[y])[x] = g;
		(dst->Bpix[y])[x] = b;
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	pour avoir une courbe de r�partition en cloche, j'utilise
 *	l'algo du "lancement de deux d�s", mais ce n'est probablement
 *	pas tr�s rigoureux :)
 */
int
Image_gray_noise_2(Image_Desc *dst, int low, int high)
{
int	x, y, v, delta;

delta = (high - low) / 2;

#if DEBUG_LEVEL
fprintf(stderr, "Gray noise 2: low=%d high=%d delta=%d\n", low, high, delta);
#endif

for (x=0; x<dst->width; x++)
	{
	for (y=0; y<dst->height; y++)
		{
		v = (rand()%delta) + (rand()%delta) + low;
		(dst->Rpix[y])[x] = v;
		(dst->Gpix[y])[x] = v;
		(dst->Bpix[y])[x] = v;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	hop, clonage du commentaire de la func du dessus :)
 *
 *	pour avoir une courbe de r�partition en cloche, j'utilise
 *	l'algo du "lancement de deux d�s", mais ce n'est probablement
 *	pas tr�s rigoureux :)
 */
int
Image_rgb_noise_2(Image_Desc *dst, RGBA *low, RGBA *high)
{
int	x, y, r, g, b;
int	minr, ming, minb;
int	maxr, maxg, maxb;
int	dltr, dltg, dltb;

#if DEBUG_LEVEL
Image_print_rgba("rgb noise 2: low", low, 0);
Image_print_rgba("rgb noise 2: high", high, 0);
#endif

minr = (low->r < high->r) ? low->r : high->r;
ming = (low->g < high->g) ? low->g : high->g;
minb = (low->b < high->b) ? low->b : high->b;
maxr = (low->r > high->r) ? low->r : high->r;
maxg = (low->g > high->g) ? low->g : high->g;
maxb = (low->b > high->b) ? low->b : high->b;
dltr = maxr - minr;
dltg = maxg - ming;
dltb = maxb - minb;

#if DEBUG_LEVEL
printf("\n");
printf("rgb noise 2: min    %3d  %3d  %3d\n", minr, ming, minb);
printf("rgb noise 2: max    %3d  %3d  %3d\n", maxr, maxg, maxb);
printf("rgb noise 2: delta  %3d  %3d  %3d\n", dltr, dltg, dltb);
#endif

for (x=0; x<dst->width; x++)
	{
	for (y=0; y<dst->height; y++)
		{
		r = (rand()%dltr) + (rand()%dltr) + minr;
		g = (rand()%dltg) + (rand()%dltg) + ming;
		b = (rand()%dltb) + (rand()%dltb) + minb;
		(dst->Rpix[y])[x] = r;
		(dst->Gpix[y])[x] = g;
		(dst->Bpix[y])[x] = b;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
