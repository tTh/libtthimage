/*
			cadres4.c
			=========
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <math.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	fonction en chantier -> coredump probable
 *	je ne sais pas a quoi va servir le parametre 'flags' ...
 *   7 nov 2007:
 *      le flag a disparu, il contient maintenant la valeur de
 *      la composante verte. lol.
 */
int Image_cadre_burp_0(Image_Desc *img, int p1, int p2, int gval)
{
int	x, y, zz, v;


#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d %d 0x%02x )\n", __func__, img, p1, p2, gval);
#endif

for (x=0; x<img->width; x++) {
	v = (x * 256) / img->width;
	for (zz=0; zz<p1; zz++) {
		y = zz;
		(img->Rpix[y])[x] = 0;
		(img->Gpix[y])[x] = gval;
		(img->Bpix[y])[x] = v;

		y = img->height-zz-1;
		(img->Rpix[y])[x] = 0;
		(img->Gpix[y])[x] = gval;
		(img->Bpix[y])[x] = 255-v;
		}
	}

for (y=p1; y<img->height-p1; y++) {
	v = (y * 256) / img->height;
	for (zz=0; zz<p2; zz++)	{
		x = zz;
		(img->Rpix[y])[x] = 0;
		(img->Gpix[y])[x] = gval;
		(img->Bpix[y])[x] = v;

		x = img->width-zz-1;
		(img->Rpix[y])[x] = 0;
		(img->Gpix[y])[x] = gval;
		(img->Bpix[y])[x] = 255-v;
		}
	}

img->modified = 1;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	13 Juin 2002: grosse colere, je vais faire du codage a la Gruuik.
 *	1er Juillet 2022: la colere est toujours la, mais ce n'est pas la
 *	meme qu'il y a vingt ans...
 */
int Image_cadre_burp_1(Image_Desc *img, int p1, int p2, int flags)
{
int	x, xx, y, yy, v;

#define TOPV   170

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d %d $%x )\n", __func__, img, p1, p2, flags);
#endif

for (x=0; x<img->width; x++) {
	v = (x * 256) / img->width;
#if DEBUG_LEVEL > 1
	printf("%s : debug %d %d\n", __func__, x, v);
#endif
	for (yy=0; yy<p1; yy++)	{
		y = yy;
		(img->Rpix[y])[x] = TOPV-v;
		(img->Gpix[y])[x] = TOPV;
		(img->Bpix[y])[x] = v;

		y = img->height-yy-1;
		(img->Rpix[y])[x] = v;
		(img->Gpix[y])[x] = TOPV;
		(img->Bpix[y])[x] = TOPV-v;
		}
	}

for (y=p1; y<img->height-p1; y++) {
	v = (y * 256) / img->height;
	for (xx=0; xx<p2; xx++)	{
		x = xx;
		(img->Rpix[y])[x] = 0;
		(img->Gpix[y])[x] = TOPV;
		(img->Bpix[y])[x] = v;

		x = img->width-xx-1;
		(img->Rpix[y])[x] = 0;
		(img->Gpix[y])[x] = TOPV;
		(img->Bpix[y])[x] = TOPV-v;
		}
	}

if (flags & 1) {
	fprintf(stderr, "plop from %s\n", __func__);
	}

#undef TOPV				/* molly guard */

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 * new 10 decembre 2007 - ave St Exupery - fait pour l'affichage web
 * des exemples des objets POV de tTh
 *
 */
int Image_cadre_burp_2(Image_Desc *img, int taille, int pr, int pg, int pb)
{
int	x, y, zz;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s: sz %d, rgb  %d %d %d\n", __func__, taille, pr, pg, pb);
#endif

for (x=0; x<img->width; x++) {
	for (zz=0; zz<taille; zz++) {
		y = zz;
		(img->Rpix[y])[x] |= pr;
		(img->Gpix[y])[x] ^= pg;
		(img->Bpix[y])[x] &= pb;

		y = img->height-zz-1;
		(img->Rpix[y])[x] |= pr;
		(img->Gpix[y])[x] ^= pg;
		(img->Bpix[y])[x] &= pb;
		}
	}

for (y=taille; y<img->height-taille; y++) {
	for (zz=0; zz<taille; zz++) {
		x = zz;
		(img->Rpix[y])[x] |= pr;
		(img->Gpix[y])[x] ^= pg;
		(img->Bpix[y])[x] &= pb;

		x = img->width-zz-1;
		(img->Rpix[y])[x] |= pr;
		(img->Gpix[y])[x] ^= pg;
		(img->Bpix[y])[x] &= pb;
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
int Image_cadre_burp_3(Image_Desc *img, int taille, int pr, int pg, int pb)
{
int 	foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p   %d   %d %d %d)\n", __func__, img, taille,
								pr, pg, pb);
#endif

foo = Image_cadre_burp_2(img, taille, pr, pg, pb);
foo = Image_cadre_burp_2(img, taille/2, pr/2, pg/2, pb/2);

img->modified = 2;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int Image_cadre_rasta_0(Image_Desc *img, int taille)
{
Image_Rect	rect;
int		foo;
int		r, g, b;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d )\n", __func__, img, taille);
#endif

#define RASTA_RAND	4

rect.x = rect.y = 0;
rect.w = img->width;
rect.h = img->height;

for (foo=0; foo<taille; foo++) {
	r = 220 + (rand() % RASTA_RAND);
	b = 10 + (rand() % RASTA_RAND);
	Image_draw_rect(img, &rect, r, 12, b);
	rect.x++;	rect.y++;
	rect.h-=2;	rect.w-=2;
	}
for (foo=0; foo<taille; foo++) {
	r = 220 + (rand() % RASTA_RAND);
	g = 200 + (rand() % RASTA_RAND);
	Image_draw_rect(img, &rect, r, g, 12);
	rect.x++;	rect.y++;
	rect.h-=2;	rect.w-=2;
	}
for (foo=0; foo<taille; foo++) {
	g = 220 + (rand() % RASTA_RAND);
	b = 10 + (rand() % RASTA_RAND);
	Image_draw_rect(img, &rect, 12, g, b);
	rect.x++;	rect.y++;
	rect.h-=2;	rect.w-=2;
	}

img->modified = 1;

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
