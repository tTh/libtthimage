#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include   <stdint.h>
#include  <math.h>

#include  "../tthimage.h"

/*		==============================				*/

/* new Wed Aug 14 19:17:20 UTC 2024
 *
 *	parameters :
 *		ip: Image_Desc * of input picture
 *		ix, iy:	coordinate of the pixel in this pic.
 *		op, ox, oy : same things for output picture
 */
#define PIXEL_COPY(ip, ix, iy, op, ox, oy)			\
			do {					\
			(op->Rpix[oy])[ox] = (ip->Rpix[iy])[ix];	\
			(op->Gpix[oy])[ox] = (ip->Gpix[iy])[ix];	\
			(op->Bpix[oy])[ox] = (ip->Bpix[iy])[ix];	\
			} while (0)

#define SZPIC   4096

void essai_pixcopy(int usemacro)
{
Image_Desc	*alice, *bob;
int		x, y, foo;
double		T0, T1, perpix;
long		count;
char		*fname;

alice = Image_alloc(SZPIC, SZPIC, IMAGE_RGB);
bob   = Image_alloc(SZPIC, SZPIC, IMAGE_RGB);

Image_pattern_000(alice, 0);
Image_TGA_save("alice.tga", alice, 0);

T0 = tthi_dtime();
count = 0L;

if (usemacro) {
	for (y=0; y<SZPIC; y++) {
		for (x=0; x<SZPIC; x++) {
			PIXEL_COPY(alice, x, y, bob, x, y);
			count++;
			}
		}
	fname = "bob1.tga";
	}
else {
	for (y=0; y<SZPIC; y++) {
		for (x=0; x<SZPIC; x++) {
			Image_pixel_copy(alice, x, y, bob, x, y);
			count++;
			}
		}
	fname = "bob0.tga";
	}

T1 = tthi_dtime();

foo = Image_TGA_save(fname, bob, 0);
if (foo) {
	fprintf(stderr, "%s: save -> %d\n", __func__, foo);
	}

perpix = ((T1-T0) / (double)count) * 1e6;
fprintf(stderr, "%s : %10ld pixels, elapsed %9.6f seconds ==> %9.6f µs/pix\n",
			usemacro ? "macro" : "func ",
			count, T1-T0, perpix);

/* end */
}

/*		==============================				*/

int main(int argc, char *argv[])
{
int	i;

fprintf(stderr, "*********** %s ************\n", argv[0]);

if (argc > 1) {
	fprintf(stderr, "argument: %s\n", argv[1]);
	Image_print_version(2);
	Image_print_sizeof_structs("foo");
	}
else	{
	for (i=0; i<5; i++) {
		essai_pixcopy(0);
		essai_pixcopy(1);
		}
	}

/*
foo = essai_show_t16x24(NULL);
fprintf(stderr, "essai show t16x24 --> %d\n", foo);
*/

return 0;
}
