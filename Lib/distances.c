/*
 *                CALCULS DE LA DISTANCE ENTRE DEUX IMAGES
 *
 * new 12 janvier 2009 - avenue St Exupery - dans un etat de grosse fatique
 */

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 */
int
Image_distance_0(Image_Desc *ia, Image_Desc *ib, 
				double *dr, double *dg, double *db)
{
int		foo, x, y;
int		ra, ga, ba, rb, gb, bb;

if ( (foo=Image_compare_desc(ia, ib)) ) {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

*dr = *dg = *db = 0.0;

for (y=0; y<ia->height; y++)
	{
	for (x=0; x<ia->width; x++)
		{
		ra = (ia->Rpix[y])[x];
		ga = (ia->Gpix[y])[x];
		ba = (ia->Bpix[y])[x];
		rb = (ib->Rpix[y])[x];
		gb = (ib->Gpix[y])[x];
		bb = (ib->Bpix[y])[x];
		*dr += ((double)abs(ra - rb)) / 255.9999999;
		*dg += ((double)abs(ga - gb)) / 255.9999999;
		*db += ((double)abs(ba - bb)) / 255.9999999;
		}
	}

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s:   %12.3f   %12.3f   %12.3f\n", __func__, *dr, *dg, *db);
#endif

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 */
int
Image_distance_1(Image_Desc *ia, Image_Desc *ib, 
				double *dr, double *dg, double *db)
{
int		foo, x, y;

if ( (foo=Image_compare_desc(ia, ib)) ) {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

*dr = *dg = *db = 42e42;

for (y=0; y<ia->height; y++)
	{
	for (x=0; x<ia->width; x++)
		{

		/* FIXME need more code */

		}
	}

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
