/*
                    op2x2.c
		--------------
	you can also look at 'dither.c' and search for '2x2'
*/

#include   <stdio.h>
#include   <stdlib.h>		/* pour abs() */
#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	6 Fev 2001: on aimerait bien avoir la doc de cette fonction
 *	27 Juillet 2008: on attend toujours...
 */
int Image_2x2_contours_0(Image_Desc *im)
{
int	x, y, q;
int	tbl[] = {
	0,	1,	1,	1,
	1,	1,	0,	1,
	1,	0,	1,	1,
	1,	1,	1,	0
	};

#if DEBUG_LEVEL
fprintf(stderr, "%s : w=%d h=%d\n", __func__, im->width, im->height);
#endif

for (x=0; x<(im->width-1); x++)	{
	for (y=0; y<(im->height-1); y++) {
		q = ((im->Rpix[y]  [x]   ? 0 : 1) << 3 ) |
		    ((im->Rpix[y]  [x+1] ? 0 : 1) << 2 ) |
		    ((im->Rpix[y+1][x]   ? 0 : 1) << 1 ) |
		    ((im->Rpix[y+1][x+1] ? 0 : 1)      ) ;
		im->Rpix[y][x] = tbl[q] ? 255 : 0;

		q = ((im->Gpix[y]  [x]   ? 0 : 1) << 3 ) |
		    ((im->Gpix[y]  [x+1] ? 0 : 1) << 2 ) |
		    ((im->Gpix[y+1][x]   ? 0 : 1) << 1 ) |
		    ((im->Gpix[y+1][x+1] ? 0 : 1)      ) ;
		im->Gpix[y][x] = tbl[q] ? 255 : 0;

		q = ((im->Bpix[y]  [x]   ? 0 : 1) << 3 ) |
		    ((im->Bpix[y]  [x+1] ? 0 : 1) << 2 ) |
		    ((im->Bpix[y+1][x]   ? 0 : 1) << 1 ) |
		    ((im->Bpix[y+1][x+1] ? 0 : 1)      ) ;
		im->Bpix[y][x] = tbl[q] ? 255 : 0;
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/* updated 31 august 2008 */
/* le seul bit utilise dans l'image source est le bit 7  */
int Image_2x2_contours_1(Image_Desc *src, Image_Desc *dst)
{
int	foo;
int	x, y, q;

if ( (foo=Image_compare_desc(src, dst)) ) {
	fprintf(stderr, "%s: err compare %d\n", __func__, foo);
	return foo;
	}
#if DEBUG_LEVEL
fprintf(stderr, "*** %s *** catastrophic function :)\n", __func__);
#endif
Image_clear(dst, 0, 0, 0);

for (x=0; x<(src->width-1); x++) {
	for (y=0; y<(src->height-1); y++) {
		q = ((0x80 & (src->Rpix[y]  [x]  ) ? 0 : 1) << 3 ) |
		    ((0x80 & (src->Rpix[y]  [x+1]) ? 0 : 1) << 2 ) |
		    ((0x80 & (src->Rpix[y+1][x]  ) ? 0 : 1) << 1 ) |
		    ((0x80 & (src->Rpix[y+1][x+1]) ? 0 : 1)      ) ;
		dst->Rpix[y][x] = q;
		q = ((0x80 & (src->Gpix[y]  [x]  ) ? 0 : 1) << 3 ) |
		    ((0x80 & (src->Gpix[y]  [x+1]) ? 0 : 1) << 2 ) |
		    ((0x80 & (src->Gpix[y+1][x]  ) ? 0 : 1) << 1 ) |
		    ((0x80 & (src->Gpix[y+1][x+1]) ? 0 : 1)      ) ;
		dst->Gpix[y][x] = q;
		q = ((0x80 & (src->Bpix[y]  [x]  ) ? 0 : 1) << 3 ) |
		    ((0x80 & (src->Bpix[y]  [x+1]) ? 0 : 1) << 2 ) |
		    ((0x80 & (src->Bpix[y+1][x]  ) ? 0 : 1) << 1 ) |
		    ((0x80 & (src->Bpix[y+1][x+1]) ? 0 : 1)      ) ;
		dst->Bpix[y][x] = q;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	6 Fev 2001: on aimerait bien avoir la doc de cette fonction
 */
int Image_2x2_contrast(Image_Desc *src, Image_Desc *dst)
{
int	foo, x, y, r, g, b;
int	mir, mig, mib, mar, mag, mab;

int	dx[4] = { 0, 1, 0, 1 };
int	dy[4] = { 0, 0, 1, 1 };

if ( (foo=Image_compare_desc(src, dst)) ) {
	fprintf(stderr, "%s: err compare %d\n", __func__, foo);
	return foo;
	}

for (y=0; y<src->height-1; y++)	{
	for (x=0; x<src->width-1; x++) {
		mir = mig = mib = 342;
		mar = mag = mab = -42;
		for (foo=0; foo<4; foo++) {
			r = (src->Rpix[y+dy[foo]])[x+dx[foo]];
			g = (src->Gpix[y+dy[foo]])[x+dx[foo]];
			b = (src->Bpix[y+dy[foo]])[x+dx[foo]];

			if (mir>r)	mir = r;
			if (mig>g)	mig = g;
			if (mib>b)	mib = b;
			if (mar<r)	mar = r;
			if (mag<g)	mag = g;
			if (mab<b)	mab = b;

			r = abs(mar-mir);
			g = abs(mag-mig);
			b = abs(mab-mib);
			}
		Image_plotRGB(dst, x, y, r, g, b);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	6 Fev 2001: on aimerait bien avoir la doc de cette fonction
 */
int Image_2x2_lissage(Image_Desc *src, Image_Desc *dst)
{
int	foo, x, y;
int	tir, tig, tib;

int	dx[4] = { 0, 1, 0, 1 };
int	dy[4] = { 0, 0, 1, 1 };

if ( (foo=Image_compare_desc(src, dst)) ) {
	fprintf(stderr, "%s: err compare %d\n", __func__, foo);
	return foo;
	}

/*
 * attention, il faut bien reflechir avant de tenter d'inverser l'ordre
 * de parcours en ligne & colonne !
 */
for (y=0; y<src->height-1; y++)	{
	for (x=0; x<src->width-1; x++) {
		tir = tig = tib = 0;
		for (foo=0; foo<4; foo++) {
			tir += (src->Rpix[y+dy[foo]])[x+dx[foo]];
			tig += (src->Gpix[y+dy[foo]])[x+dx[foo]];
			tib += (src->Bpix[y+dy[foo]])[x+dx[foo]];
			}
		(dst->Rpix[y])[x] = tir/4;
		(dst->Gpix[y])[x] = tig/4;
		(dst->Bpix[y])[x] = tib/4;
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	8 Feb 2001: another kludge (without doc)
 */
int Image_2x2_rot4pix(Image_Desc *src, Image_Desc *dst, int rot)
{
int	x, y, foo, bar, r, g, b;

int	dx[4] = { 0, 1, 0, 1 };
int	dy[4] = { 0, 0, 1, 1 };

for (y=0; y<src->height-1; y+=2) {
	for (x=0; x<src->width-1; x+=2)	{
		for (foo=0; foo<4; foo++) {
			bar = (foo+rot)%4;	/* XXX ? */
			r = (src->Rpix[y+dy[foo]])[x+dx[foo]];
			g = (src->Gpix[y+dy[foo]])[x+dx[foo]];
			b = (src->Bpix[y+dy[foo]])[x+dx[foo]];
			}
		(dst->Rpix[y])[x] = r;
		(dst->Gpix[y])[x] = g;
		(dst->Bpix[y])[x] = b;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
