/*
                pov_hf15b.c
                ===========

    for information about this file:
			ask Thierry Boudet 

*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"        

/*::------------------------------------------------------------------::*/
/*
 *  new 24 oct 2001 - not really crah-proff
 */
int
Image_hf15_normalize(Image_Desc *src, Image_Desc *dst, int low, int high)
{
int	x, y, minH, maxH, foo;
int	delta, ecart;
double	mult, dh;

foo = Image_hf15_calc_minmax(src, NULL, &minH, &maxH);
#if DEBUG_LEVEL
fprintf(stderr, "%s: src %p is %d < H < %d\n", __func__, src, minH, maxH);
#endif

delta = maxH - minH;
ecart = high - low;
if ((0==delta) || (0==ecart))
	{
	fprintf(stderr, "Pov hf15 normalize: ZERO DIVIDE!\n");
	return DIVISOR_IS_ZERO;
	}

mult = (double)ecart / (double)delta;
#if DEBUG_LEVEL
fprintf(stderr, "Pov hf15 normalize: delta %d, mult %f\n", delta, mult);
#endif

for (y=0; y<src->height; y++)
	{
	for (x=0; x<src->width; x++)
		{
		dh = (double)Image_hf15_height(src, x, y);
		dh = (dh * mult) + (double)low;
		Image_hf15_plot(dst, x, y, (int)dh);
		}
	}

#if DEBUG_LEVEL
foo = Image_hf15_calc_minmax(dst, NULL, &minH, &maxH);
fprintf(stderr, "Pov hf15 normalize: we got %d < H < %d\n", minH, maxH);
#endif

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/* New: 10 Nov 2001
 *
 */
int
Image_hf15_make_colmap_0(Image_Desc *src, Image_Desc *dst, char *mapname)
{
int	foo, x, y, h;
RGB_map	map;

if ( (foo=Image_compare_desc(src, dst)) )
	{
	fprintf(stderr, "Hf15 make colmap 0: differents dimensions %d\n",
									foo);
	return foo;
	}

foo=Image_load_color_Map(mapname, NULL, &map);
if ( foo && foo!=MAP_TOO_BIG )
	{
	fprintf(stderr, "Hf15 make colmap 0: err load colormap '%s': %d / %s\n",
					mapname, foo, Image_err2str(foo));
	return foo;
	}

for (y=0; y<src->height; y++)
	{
	for (x=0; x<src->width; x++)
		{
		h = Image_hf15_height(src, x, y) / 128;
#if FORCE_ABORT
		if (h<0 || h>255)	abort();
#endif
		Image_plotRGB(dst, x, y, map.red[h], map.green[h], map.blue[h]);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* New: 18 Dec 2001
 *
 */
int
Image_hf15_make_colmap_1(Image_Desc *src, Image_Desc *dst, RGB_map *map)
{
int	foo, x, y, h;

if ( (foo=Image_compare_desc(src, dst)) )
	{
	fprintf(stderr, "Hf15 make colmap 1: differents dims %d\n", foo);
	return foo;
	}

#if DEBUG_LEVEL
fprintf(stderr, "%s : data@ is %p\n", __func__, map);
#endif

for (y=0; y<src->height; y++)
	{
	for (x=0; x<src->width; x++)
		{
		h = Image_hf15_height(src, x, y) / 128;
#if FORCE_ABORT
		if (h<0 || h>255)	abort();
#endif
		Image_plotRGB(dst, x, y,
			map->red[h], map->green[h], map->blue[h]);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
