/*
 *	Colors in the strange XYZ space. new 9 avr 2007, ave St Exupery
 */

#include  <stdio.h>
#include  <math.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int
Image_pix_rgb2xyz_d(int r, int g, int b, double *px, double *py, double *pz)
{
double		dr, dg, db, dx, dy, dz;

#if TRACE
fprintf(stderr, "%s  (%3d %3d %3d)  ->  ", __func__, r, g, b);
#endif

dr = (double)r / 256.0;
dg = (double)g / 256.0;
db = (double)b / 256.0;
dx = 2.7688 * dr  +  1.7517 * dg  +  1.1301 * db;
dy = 1.0000 * dr  +  4.5907 * dg  +  0.0601 * db;
dz = 0.0001 * dr  +  0.0565 * dg  +  5.5942 * db;

#if TRACE
fprintf(stderr, "(%f, %f, %f)\n", dx, dy, dz);
#endif

*px = dx, *py = dy, *pz = dz;

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/

