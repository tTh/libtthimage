/*

		--------- libtthimage -----------
		trying to code a JPEG file reader

	new Mon 26 Dec 2022 05:43:47 AM CET
*/

#include  <stdio.h>

#include  <jpeglib.h>			/* XXX to be checked ? */

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int try_to_read_a_jpeg_file(char *fname, Image_Desc *img)
{

fprintf(stderr, ">>> %s ( '%s' %p )\n", __func__, fname, img);
fprintf(stderr, "    %s compiled %s at %s\n", __FILE__, __DATE__, __TIME__);

fprintf(stderr, "    JPEG_LIB_VERSION = %d\n", JPEG_LIB_VERSION);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
