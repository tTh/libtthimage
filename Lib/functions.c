/*
 *		miscellaneous functions
 *		-----------------------
 *
 * new:  Thu Aug 15 09:55:36 UTC 2024
 *
 */

#include  <stdio.h>
#include  <sys/time.h>

#include  "../tthimage.h"

/* ------------------------------------------------------------------- */
/*
 *	for what FSCKING reason, this func is not in the stdlib ???
 */
double tthi_dtime(void)
{
struct timeval  t;
double          d;
(void)gettimeofday(&t, NULL);
d = (double)t.tv_sec + (double)t.tv_usec / 1e6;
return d;
}
/* ------------------------------------------------------------------- */
/* ------------------------------------------------------------------- */
