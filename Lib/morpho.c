/*
                    morpho.c
		---------------
*/

#include   <stdio.h>
#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
static struct 
	{
	int	x, y;
	} deltas[] = 
{	{ -1,	-1, },
	{  0,	-1, },
	{  1,	-1, },
	{ -1,	 0, },
	{  0,	 0, },
	{  1,	 0, },
	{ -1,	 1, },
	{  0,	 1, },
	{  1,	 1 }
};	
/*::------------------------------------------------------------------::*/
/*
 *	n'est-ce pas une dilatation ?
 *	=============================
 * 23 avril 2007: switched to direct pixel access.
 */
int Image_expand_max(Image_Desc *src, Image_Desc *dst, int factor)
{
int	foo, x, y, maxr, maxg, maxb;
int	r, g, b;

if ( (foo=Image_compare_desc(src, dst)) ) {
	fprintf(stderr, "%s: images non compatibles\n", __func__);
	return foo;
	}

if ( 0 != factor ) {
	fprintf(stderr, "%s: factor (%d) must be 0\n", __func__, factor);
	}

for (y=1; y<src->height-1; y++) {
	for (x=1; x<src->width-1; x++) {
		maxr = maxg = maxb = 0;
		for (foo=0; foo<9; foo++) {
			/* XXX Faster pussy cat !
			Image_getRGB(src, x+deltas[foo].x, y+deltas[foo].y,
					&r, &g, &b);	
			*/
			r = src->Rpix[y+deltas[foo].y][x+deltas[foo].x];
			g = src->Gpix[y+deltas[foo].y][x+deltas[foo].x];
			b = src->Bpix[y+deltas[foo].y][x+deltas[foo].x];

			if (maxr < r)	maxr = r;
			if (maxg < g)	maxg = g;
			if (maxb < b)	maxb = b;
			}
		/* XXX Kill Kill !y cat !
		Image_plotRGB(dst, x, y, maxr, maxg, maxb);
		*/
		dst->Rpix[y][x] = maxr;
		dst->Gpix[y][x] = maxg;
		dst->Bpix[y][x] = maxb;
		}
	}
dst->modified = 1;
return 0;
}
/*::------------------------------------------------------------------::*/
int Image_expand_min(Image_Desc *src, Image_Desc *dst, int factor)
{
int	foo, x, y, minr, ming, minb;
int	r, g, b;

if ( (foo=Image_compare_desc(src, dst)) ) {
	return foo;
	}

if ( 0 != factor ) {
	fprintf(stderr, "%s: factor (%d) must be 0\n", __func__, factor);
	}

#if DEBUG_LEVEL
fprintf(stderr, "expanding MIN %p to %p\n", src, dst);
#endif

for (y=1; y<src->height-1; y++)	{
	for (x=1; x<src->width-1; x++) {
		minr = ming = minb = 1664;
		for (foo=0; foo<9; foo++) {
			Image_getRGB(src, x+deltas[foo].x, y+deltas[foo].y,
					&r, &g, &b);	

			if (minr > r)	minr = r;
			if (ming > g)	ming = g;
			if (minb > b)	minb = b;

			}
		Image_plotRGB(dst, x, y, minr, ming, minb);
		}
	}
dst->modified = 1;
return 0;
}
/*::------------------------------------------------------------------::*/
struct sort_pixel
	{
	int	r, g, b;
	int	gris;
	int	rang;
	};

static void
Image_sortmat_display(FILE *ou, struct sort_pixel *table)
{
fprintf(ou, "%s ( %p )\n", __func__, table);
}

int Image_Gray_Sorted_Points(Image_Desc *src, Image_Desc *dst, int *filtre)
{
int	x, y, r, g, b;
int	foo, gris;

struct sort_pixel pixels[9];

#if DEBUG_LEVEL
fprintf(stderr, "Image Gray Sorted Points: %p -> %p\n", src, dst);
#endif

for (y=1; y<src->height-1; y++)	{
	for (x=1; x<src->width-1; x++) {
		/* collecte des infos sur les voisins. */
		for (foo=0; foo<9; foo++) {
			Image_getRGB(src, x+deltas[foo].x, y+deltas[foo].y,
					&r, &g, &b);	

			pixels[foo].r = r;
			pixels[foo].g = g;
			pixels[foo].b = b;
			pixels[foo].gris = r + g + b;
			pixels[foo].rang = foo;
			}

		/* tri des informations collectées. */
		gris = 0;

		/* application des coeficients  */

		}
	}

fprintf(stderr, "**** Ha %p Ha this function is _not_ written ****\n", dst);

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
