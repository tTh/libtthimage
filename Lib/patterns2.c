/*
                    patterns2.c
		------------------
	various random patterns.

*/

#include   <stdio.h>
#include   <stdlib.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
int
Image_texture_0(Image_Desc *dst, int bas, int haut)
{
int	foo;

fprintf(stderr, "Texture 0: obsolete! use 'Image_gray_noise_0' now!\n");

foo = Image_gray_noise_0(dst, bas, haut);

return foo;
}
/*::------------------------------------------------------------------::*/
int
Image_texture_1(Image_Desc *dst, int bas, int haut)
{
int	x, y, r, g, b;

#if DEBUG_LEVEL
fprintf(stderr, "Texture 1:  %d %d\n", bas, haut);
#endif

for (y=0; y<dst->height; y++)
	{
	for (x=0; x<dst->width; x++)
		{
		r = rand()%(haut-bas) + bas;
		g = rand()%(haut-bas) + bas;
		b = rand()%(haut-bas) + bas;

		(dst->Rpix[y])[y] = r;
		(dst->Gpix[y])[y] = g;
		(dst->Bpix[y])[y] = b;
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	en cours de mise au point ? � quoi servent les param�tres ?
 *	ou est la doc ? mais que fait tTh ? il moule dans une tribune ?
 */
int
Image_texture_2(Image_Desc *dst, int bas, int haut, int mod)
{
int	x, y, r, g, b, foo;


#if DEBUG_LEVEL
fprintf(stderr, "Image texture 2: this func is experimental ;)\n");
#endif

if (mod == 0)
	{
	fprintf(stderr, "Image texture 2: 'mod' must be positive.\n");
	return DIVISOR_IS_ZERO;
	}

r = g = b = 127;
for (y=0; y<dst->height; y++)
	{
	for (x=0; x<dst->width; x++)
		{
		foo = rand() % mod;
		switch(foo)
			{
			case  0: r = rand()%(haut-bas) + bas;	break;
			case  1: g = rand()%(haut-bas) + bas;	break;
			case  2: b = rand()%(haut-bas) + bas;	break;
			case  3: r = b = g = haut;		break;
			case  4: r = b = g = bas;		break;
			case  5: r = 0;				break;
			case  6: g = 0;				break;
			case  7: b = 0;				break;
		/*
		 * � partir de l�, il y a beaucoup de 'case'
		 * qui se ressemblent trop...
		 */
			case  8: r = (x * rand()) & 0xff;	break;
			case  9: g = (x * rand()) & 0xff;	break;
			case 10: b = (x * rand()) & 0xff;	break;
			case 11: r = (y * rand()) & 0xff;	break;
			case 12: g = (y * rand()) & 0xff;	break;
			case 13: b = (y * rand()) & 0xff;	break;
			}
		(dst->Rpix[y])[x] = r;
		(dst->Gpix[y])[x] = g;
		(dst->Bpix[y])[x] = b;
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	bon sang, j'ai le prototype sous les yeux, mais je n'ai pas
 *	la moindre id�e de ce que doit faire la fonction !
 */
int
Image_texture_3(Image_Desc *dst, int bas, int haut, char *ctl, int quux)
{
int	x, y;

fprintf(stderr, "Texture 3 is not here\n");

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
