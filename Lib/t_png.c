/*
 *   test des fonctionsPNG
 */

#include  <stdio.h>
#include  <stdlib.h>

#include  <png.h>

#include  "../tthimage.h"

void write_png_version_info(void);

/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
int main(int argc, char *argv[])
{
Image_Desc	*img;
int		foo;
char		*fname = "proto.png";

Image_print_version(0);

if (2==argc) {
	fname = argv[1];
	}

img = Image_alloc(640, 480, IMAGE_RGB);

foo = Image_save_as_PNG(img, fname, 0, 0);
if (foo) {
	fprintf(stderr, "save as '%s' -> %d\n", fname, foo);
	exit(1);
	}

return 0;
}
/*::------------------------------------------------------------------::*/

