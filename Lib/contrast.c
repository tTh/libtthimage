/*
 *	contrast.c - 31 dec 2013
 */

#include  <stdio.h>
#include  <math.h>
#include  "../tthimage.h"


/*::------------------------------------------------------------------::*/
/*
 *	new: Fri Sep 15 20:18:35 UTC 2023
 *	inspired by the same func in FloatImg
 */
int Image_egalise_cos01(Image_Desc *source, Image_Desc *but, int k)
{
(void)k;				/* WARNING KILLER */
int		lut[256], uc, idx, foo;
float		fidx;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p  %p  %d )\n", __func__,
		source, but, k);
#endif

if ( (foo=Image_compare_desc(source, but)) ) {
	fprintf(stderr, "%s: images not compatible, %d\n", __func__, foo);
	return foo;
	}
for (idx=0; idx<256; idx++) {
	fidx = (float)idx / 255.0;
	uc = (unsigned char)(255.0*(0.5 - 0.5 * cos(3.141592654*fidx)));
	lut[idx] = uc;
	/* printf("%7d    %7d\n", idx, uc); */
	}
Image_LUT_mono(source, but, lut);

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	new: Fri Sep 15 20:32:12 UTC 2023
 *	inspired by the same func in FloatImg
 */
int Image_egalise_cos010(Image_Desc *source, Image_Desc *but, int k)
{
(void)k;				/* WARNING KILLER */
int		lut[256], uc, idx, foo;
float		fidx;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p  %p  %d )\n", __func__,
		source, but, k);
#endif

if ( (foo=Image_compare_desc(source, but)) ) {
	fprintf(stderr, "%s: images not compatible, %d\n", __func__, foo);
	return foo;
	}
for (idx=0; idx<256; idx++) {
	fidx = (float)idx / 255.0;
	uc = (unsigned char)(255.0*(0.5 - 0.5 * cos(3.141592654*fidx*2.0)));
	lut[idx] = uc;
	/* printf("%7d    %7d\n", idx, uc); */
	}
Image_LUT_mono(source, but, lut);

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	cette fonction peut etre utilisee avec la meme image
 *	en source et en destination.
 */
int Image_pix_square(Image_Desc *source, Image_Desc *but, int k)
{
int     x, y, r, g, b, foo;
float	fr, fg, fb;

#if DEBUG_LEVEL
fprintf(stderr, "%s : %p -> %p\n", __func__, source, but);
#endif

if (k)	{
	fprintf(stderr, "In %s, k must be 0, was %d\n", __func__, k);
	}

if ( (foo=Image_compare_desc(source, but)) ) {
    fprintf(stderr, "%s : images are differents %d\n", __func__, foo);
    return foo;
    }

for (y=0; y<source->height; y++) {
	for (x=0; x<source->width; x++) {
        	fr = ((float)(source->Rpix[y])[x]) / 255.0;
        	fg = ((float)(source->Gpix[y])[x]) / 255.0;
        	fb = ((float)(source->Bpix[y])[x]) / 255.0;
		r = (int)(fr * fr * 255.0);
		g = (int)(fg * fg * 255.0);
		b = (int)(fb * fb * 255.0);
        	(but->Rpix[y])[x] = r;
        	(but->Gpix[y])[x] = g;
        	(but->Bpix[y])[x] = b;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	cette fonction peut etre utilisee avec la meme image
 *	en source et en destination.
 */
int Image_pix_sqroot(Image_Desc *source, Image_Desc *but, int k)
{
int     x, y, foo;
float	fr, fg, fb;

#if DEBUG_LEVEL
fprintf(stderr, "%s : %p -> %p\n", __func__, source, but);
#endif

if (k)	{
	fprintf(stderr, "In %s, k must be 0, was %d\n", __func__, k);
	}

if ( (foo=Image_compare_desc(source, but)) ) {
    fprintf(stderr, "%s : images are differents %d\n", __func__, foo);
    return foo;
    }

for (y=0; y<source->height; y++) {
	for (x=0; x<source->width; x++) {
        	fr = ((float)(source->Rpix[y])[x]) / 255.0;
        	fg = ((float)(source->Gpix[y])[x]) / 255.0;
        	fb = ((float)(source->Bpix[y])[x]) / 255.0;
		(but->Rpix[y])[x] = (int)(sqrt(fr) * 255.0);
		(but->Gpix[y])[x] = (int)(sqrt(fg) * 255.0);
		(but->Bpix[y])[x] = (int)(sqrt(fb) * 255.0);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
