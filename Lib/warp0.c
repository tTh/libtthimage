/*
                    warp0.c
		---------------
		diverses deformations d'image.
*/

#include   <stdio.h>
#include   <math.h>

#include   "../tthimage.h"

#ifndef DEBUG_LEVEL
#define DEBUG_LEVEL 1
#endif

/*::------------------------------------------------------------------::*/
/*
 *	bon, caisse que peut bien faire ce truc ?
 */
int
Image_warp_essai_0(Image_Desc *src, Image_Desc *dst, double angle,
				int xc, int yc)
{
int	x, y, r, g, b;
long	lfoo;
double	arad, dheight, dwidth, tmax, dist;
int	ofx, ofy;

dheight = (double)dst->height;
dwidth  = (double)dst->width;

lfoo = ((dst->height*dst->height)+(dst->width*dst->width));
tmax = sqrt((double)lfoo) / 2.0;

#if DEBUG_LEVEL
fprintf(stderr, "%s : %d,%d dh = %f  dw = %f  tmax = %f\n", __func__, 
			xc, yc, dheight, dwidth, tmax);
#endif

for (y=0; y<dst->height; y++) {
	/* printf("------- Ligne %d -----------------\n", y); */
	arad = ((double)y / dheight) * M_PI;	

	for (x=0; x<dst->width; x++) {
		dist = (double)x / dwidth;

		ofx = xc + (int)(sin(arad)*dist*tmax);
		ofy = yc + (int)(cos(arad)*dist*tmax);

		/* printf("%15f  %15f   %6d  %6d\n", arad, dist, ofx, ofy); */

		if ( ofx>=0 && ofx<src->width && ofy>=0 && ofy<src->height )
			{
			Image_getRGB(src, ofx, ofy, &r, &g, &b);
			}
		else
			{
			r = g = b = 0;
			}

		Image_plotRGB(dst, x, y, r, g, b);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int Image_warp_essai_1(Image_Desc *src, Image_Desc *dst, double angle, double shift)
{

fprintf(stderr, ">>> %s ( %p %p %f %f )\n", __func__, src, dst, angle, shift);




return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
/*
 *		voir aussi les module scale.c et warp1.c
 */
/*::------------------------------------------------------------------::*/
