/*
 *		mircol.c
 *		========
 */

#include   <stdio.h>
#include   <stdlib.h>
#include   <time.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_mircol_0(Image_Desc *dst, char *txt, int flag)
{
int	x, y;
RGBA	fond, encre;

if (dst->width < 512 || dst->height < 512) {
	fprintf(stderr, "Image mircol 0: image %p < 512x512\n", dst);
	return IMAGE_TOO_SMALL;
	}

if (0 != flag) {
	fprintf(stderr, "%s: flag is %d and must be 0\n", __func__, flag);
	}
if (NULL != txt) {
	fprintf(stderr, "%s: txt is '%s'\n", __func__, txt);
	}

for (x=0; x<256; x++) {
	for (y=0; y<256; y++) {
		Image_plotRGB(dst, x, y,	x, y, 0);
		Image_plotRGB(dst, x+256, y,	x, 0, y);
		Image_plotRGB(dst, x, y+256,	0, x, y);
		}
	}

for (x=0; x<256; x++)
	for (y=300; y<342; y++)
		Image_plotRGB(dst, x+256, y,	x, x, x);

fond.r = 0;	fond.g = 0;	fond.b = 0;	fond.a = 255;
encre.r = 220;	encre.g = 255;	encre.b = 255;	encre.a = 255;

if (txt != NULL)
	Image_trace_chaine_1(dst, txt, 270, 270, "libimage.fonte",
						&fond, &encre);

Image_trace_chaine_1(dst, "Libtthimage de tTh", 270, 370, "libimage.fonte",
						&fond, &encre);
Image_trace_chaine_1(dst, IMAGE_VERSION_STRING, 270, 385, NULL, &fond, &encre);
Image_trace_chaine_1(dst, __DATE__, 270, 400, NULL, &fond, &encre);

for (x=1; x<256; x++)
	for (y=420; y<462; y++)
		Image_plotRGB(dst, x+256, y,
			rand()%x, rand()%x, rand()%x);

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* a quoi sert ce flag ? */
int Image_mircol_1(Image_Desc *dst, char *txt, int flag)
{
RGBA	fond, encre;
int	x, y, r, g, b;

if (dst->width < 512 || dst->height < 512) {
	fprintf(stderr, "Image mircol 1: image %p < 512x512\n", dst);
	return IMAGE_TOO_SMALL;
	}

for (x=0; x<512; x++) {
	for (y=0; y<512; y++) {
		r = x;
		g = (x + y) / 2;
		b = y;
		Image_plotRGB(dst, x, y, r, g, b);
		}
	}

fond.r = 0;	fond.g = 0;	fond.b = 0;	fond.a = 255;
encre.r = 255;	encre.g = 255;	encre.b = 255;	encre.a = 255;

/* ya pas moyen de mettre ces textes dans des jolies boites ? */
Image_trace_chaine_1(dst, txt, 270, 270, "libimage.fonte", &fond, &encre);
Image_trace_chaine_1(dst, "Libtthimage de tTh", 270, 370, NULL, &fond, &encre);
Image_trace_chaine_1(dst, IMAGE_VERSION_STRING, 270, 390, NULL, &fond, &encre);

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
/* new 31 aout 2009 - ave StEx */
/* 5 fev 2010 : hacked for "BETA tech" */
int Image_mircol_2(Image_Desc *dst, char *txt, int mode)
{
int	x, y, r, g, b, kx, ky;
float	fx, fy, fd;

#if DEBUG_LEVEL
fprintf(stderr, ">>>> %s ( %p '%s' %d )\n", __func__, dst, txt, mode);
#endif

for (y=0; y<dst->height; y++) {
	fy = (float)y / (float)dst->height;
	ky = (int)(fy * 256.0);
	for (x=0; x<dst->width; x++)
		{
		fx = (float)x / (float)dst->width;
		kx = (int)(fx * 256.0);
		r = kx;
		g = ky;
		fd = sqrt((fx*fx)+(fy*fy));
		b = (int)(fd * 256.0);
#if DEBUG_LEVEL > 1
		if (x==y)
			printf("%4d %4d   %7.3f %7.3f    %f\n",
					x, y, fx, fy, fd);
#endif
		Image_plotRGB(dst, x, y, r, g, b);
		}
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/* nouveau 6 fevrier 2010 */
int Image_mircol_3(Image_Desc *dst, char *txt, int mode)
{
int			x, y, r, g, b;
int			*p1, *p2, *p3;
float			fx, fx2, fy, dist;
float			mystk;

#if DEBUG_LEVEL
fprintf(stderr, ">>>> %s ( %p '%s' %d )\n", __func__, dst, txt, mode);
#endif

switch (mode) {
	case 0:	p1 = &r, p2 = &g, p3 = &b;		break;
	case 1: p1 = &g, p2 = &b, p3 = &r;		break;
	case 2: p1 = &b, p2 = &r, p3 = &g;		break;

	default:
		fprintf(stderr, "case error in %s:%d\n", __func__, __LINE__);
		return BAD_PARAMETER;
	}

mystk = 0.90;		/* FIXME */

for (y=0; y<dst->height; y++) {
	fy = (float)y / (float)dst->height;
	for (x=0; x<dst->width; x++) {
		fx = (float)x / (float)dst->width;

		*p1 = (int)((fx*fy) * 255);
		*p2 = (int)(((1.0-fx)*fy) * 255);

		fx2 = fx - 0.5;
		dist = sqrt((fx2*fx2)+(fy*fy)) * 255.0 * mystk;
		
		*p3 = (int)(256.0-dist);

#if DEBUG_LEVEL
		if ((dst->width < 42)&&(dst->height < 42))
			printf("%3d %3d   %3d %3d %3d\n", x, y, r, g, b);
#endif

		/* XXX Image_plotRGB(dst, x, y, r, g, b); */
		(dst->Rpix[y])[x] = r;
		(dst->Gpix[y])[x] = g;
		(dst->Bpix[y])[x] = b;
		}
	}

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s: verifier le resultat, svp.\n", __func__);
#endif

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* nouveau 10 fevrier 2010 */
int Image_mircol_4(Image_Desc *dst, char *txt, int mode)
{
int	x, y, r, g, b;
int	*p1, *p2, *p3;
float	fx, fx2, fy, dist;

#if DEBUG_LEVEL
fprintf(stderr, ">>>> %s ( %p '%s' %d )\n", __func__, dst, txt, mode);
#endif

switch (mode) {
	case 0:	p1 = &r, p2 = &g, p3 = &b;		break;
	case 1: p1 = &g, p2 = &b, p3 = &r;		break;
	case 2: p1 = &b, p2 = &r, p3 = &g;		break;

	default:
		fprintf(stderr, "switch/case error in %s:%d\n", 
							__func__, __LINE__);
		abort();
		break;
	}

for (y=0; y<dst->height; y++) {
	fy = (float)y / (float)dst->height;
	for (x=0; x<dst->width; x++) {
		fx = (float)x / (float)dst->width;
		/* FIXME TODO OMG WTF XXX */
		}
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Image_mirRGB_0(Image_Desc *dst, int flag)
{
int	x, y;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d )\n", __func__, dst, flag);
#endif

if (flag) {
	fprintf(stderr, "flag is %d in %s\n", flag, __func__);
	}

if (dst->width != 256 || dst->height != 256) {
	fprintf(stderr, "Image mire RGB 0: image is not 256x256\n");
	return IMAGE_BAD_DIMS;
	}

for (x=0; x<256; x++) {
	for (y=0; y<256; y++) {
		(dst->Rpix[y])[x] = x;
		(dst->Gpix[y])[x] = y;
		(dst->Bpix[y])[x] = (y+x)/2;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* le bit 0 du flag controle l'ecriture des noms de composantes */
int Image_decompose(Image_Desc *src, Image_Desc *dst, int flag)
{
int		foo, x, y, hx, hy;
Image_Desc	*moitie;
int		r, g, b;

if ( (foo=Image_compare_desc(src, dst)) ) {
	fprintf(stderr, "%s: bad dimensions %d\n", __func__, foo);
	return foo;
	}

moitie = Image_MakeHalfSize(src, 1);
if (moitie == NULL) {
	fprintf(stderr, "Image Decompose: NO MEM\n");
	exit(5);
	}

if (0 != flag)
	fprintf(stderr, "%s : flag is 0x%X\n", __func__, flag);

hx = moitie->width;
hy = moitie->height;

for (x=0; x<moitie->width; x++)	{
	for (y=0; y<moitie->height; y++) {
		Image_getRGB(moitie, x, y, &r, &g, &b);
		Image_plotRGB(dst, x, y, r, r, r);
		Image_plotRGB(dst, x+hx, y, g, g, g);
		Image_plotRGB(dst, x, y+hy, b, b, b);
		Image_plotRGB(dst, x+hx, y+hy, r, g, b);
		}
	}

/* faire le menage */
Image_DeAllocate(moitie);	free(moitie);

if (flag & 1) {
	RGBA	ink, pap;	/* hu ho ? local vars here ? */
	Image_load_fnt8x8("libimage.fonte", NULL, 0);
	pap.a = 0;	ink.a = 255;
	ink.r = 255;	ink.b = 0;	ink.g = 0;
	Image_trace_chaine_1(dst, "RED", 2, 2, NULL, &pap, &ink);
	ink.r = 0;	ink.g = 255;	ink.b = 0;
	Image_trace_chaine_1(dst, "GREEN", 2+hx, 2, NULL, &pap, &ink);
	ink.r = 10;	ink.g = 10;	ink.b = 255;
	Image_trace_chaine_1(dst, "BLUE", 2, 2+hy, NULL, &pap, &ink);
	}

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
