/*
		BASIC_IO		Sept 2001
		--------		---------

		Ce module a ete ecrit pour tenter de regler les 
		problemes de "boutisme" pour que c,a puisse aussi
		tourner sur les vrais processeurs, parce que, bon,
		les 386, parfois, on s'en gave, hein...

		Ceci dit, je ne sais pas vraiment comment traiter
		le probleme. Pour le moment (Septembre 2001) c'est
		un peu beaucoup du "try and test".

		D'autre part, comment les choses vont se passer sur
		des CPUs a 64 (ou 128) bits ?
		-------------------------------------------------

	9 oct 2001: pourquoi pas de fonction pour ecrire des BYTEs ?

*/

#include  <stdio.h>
#include  <stdlib.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int	Image_basicIO_teste_boutisme(char *txt)
{
char	*unix = "Unix";
long	lfoo;

fprintf(stderr, "Image:\ttests de 'boutisme'\n\tmerci linux-31@culte.org\n");
fprintf(stderr, "\t- %s -\n", txt);

lfoo = * (long *) unix;
fprintf(stderr, "\t%08lx\n", lfoo);

/* ah ah ah, mais il y a autre chose dans le source 'imprime.c' 
 * a propos du boutisme... */

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
int	Image_basicIO_read_I_short(FILE *fp, uint16_t *pvalue)
{
unsigned char	byteH, byteL;
int		foo = 0;

foo += fread(&byteL, 1, 1, fp);
foo += fread(&byteH, 1, 1, fp);

*pvalue = ( (byteH<<8) + byteL );

return foo==2 ? OLL_KORRECT : BASIC_IO_RD_ERR;
}
/*::------------------------------------------------------------------::*/
int	Image_basicIO_read_M_short(FILE *fp, uint16_t *pvalue)
{
unsigned char	byteH, byteL;
int		foo = 0;

foo += fread(&byteH, 1, 1, fp);
foo += fread(&byteL, 1, 1, fp);

*pvalue = ( (byteH<<8) + byteL );

return foo==2 ? OLL_KORRECT : BASIC_IO_RD_ERR;
}
/*::------------------------------------------------------------------::*/
int	Image_basicIO_write_I_short(FILE *fp, short value)
{
unsigned char byte;
int  foo = 0;

#if DEBUG_LEVEL > 2
fprintf(stderr, "Basic IO: write I short: %d\n", value);
#endif

byte = (unsigned short)value & 0xff;
#if DEBUG_LEVEL  > 2
fprintf(stderr, "Basic IO:   low byte = %02x\n", byte);
#endif
foo += fwrite(&byte, 1, 1, fp);

byte = ((unsigned short)value >> 8) & 0xff;
#if DEBUG_LEVEL  > 2
fprintf(stderr, "Basic IO:   hight byte = %02x\n", byte);
#endif
foo += fwrite(&byte, 1, 1, fp);

return foo==2 ? OLL_KORRECT : BASIC_IO_WR_ERR;
}
/*::------------------------------------------------------------------::*/
int	Image_basicIO_write_M_short(FILE *fp, short value)
{
unsigned char byte;
int  foo = 0;

#if DEBUG_LEVEL > 2
fprintf(stderr, "Basic IO: write M short: %d\n", value);
#endif

byte = ((unsigned short)value >> 8) & 0xff;
foo += fwrite(&byte, 1, 1, fp);

byte = (unsigned short)value & 0xff;
foo += fwrite(&byte, 1, 1, fp);

return foo==2 ? OLL_KORRECT : BASIC_IO_WR_ERR;
}
/*::------------------------------------------------------------------::*/
int	Image_basicIO_read_I_long(FILE *fp, uint32_t *pvalue)
{
unsigned char	byte;
int		foo;
uint32_t		value;

#if DEBUG_LEVEL > 2
fprintf(stderr, "Basic IO: read I long (a tester) !\n");
#endif

value = 0L;
for (foo=0; foo<4; foo++)
	{
	if (1 != fread(&byte, 1, 1, fp))
		{
		fprintf(stderr, "bad fread in %s\n", __func__);
		abort();
		}
	value <<= 8;
	value += byte;
#if DEBUG_LEVEL > 2
	fprintf(stderr, "   %d  %02x %08lx %ld\n", foo, byte, value, value);
#endif
	}

*pvalue = value;

return FUNC_IS_BETA;		/* XXX ? */
}
/*::------------------------------------------------------------------::*/
int	Image_basicIO_write_I_long(FILE *fp, long value)
{
unsigned char byte;
int	foo, bar;

#if DEBUG_LEVEL > 2
fprintf(stderr, "Basic IO: write I long: %ld\n", value);
#endif

for (foo=0; foo<4; foo++)
	{
	bar = foo * 8;
	byte = (value >> bar) & 0xff;
#if DEBUG_LEVEL > 2
	fprintf(stderr, "    %3d  %3d  %02x\n", foo, bar, byte);
#endif
	fwrite(&byte, 1, 1, fp);
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
