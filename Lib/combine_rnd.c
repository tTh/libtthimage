/*	
	combine_rnd.c			random effects
	=============			--------------

	made by Thierry Boudet, aka "Oulala", aka "Tonton Th".

*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	on va prendre au hasard un point sur une des deux images
 *		le parametre 'yo' doit etre entre 0 et 10000
 */
int
Image_combine_random_point(Image_Desc *s1, Image_Desc *s2, Image_Desc *d,
							int yo)
{
int	foo, x, y, r, g, b;

if ( (foo=Image_compare_desc(s1, s2)) )
	{
	fprintf(stderr, "%s: sources are differents (%d)\n", __func__, foo);
	return foo;
	}

if ( (foo=Image_compare_desc(s1, d)) )
	{
	fprintf(stderr, "%s: destination different (%d)\n", __func__, foo);
	return foo;
	}

if		(yo > 10000)		yo = 10000;
else if		(yo < 0)		yo = 0;

for (y=0; y<s1->height; y++)
	{
	for (x=0; x<s1->width; x++)
		{
		if ( (rand()%10000) > yo )
			{
			r = (s1->Rpix[y])[x];
			g = (s1->Gpix[y])[x];
			b = (s1->Bpix[y])[x];
			}
		else
			{
			r = (s2->Rpix[y])[x];
			g = (s2->Gpix[y])[x];
			b = (s2->Bpix[y])[x];
			}

		(d->Rpix[y])[x] = r;
		(d->Gpix[y])[x] = g;
		(d->Bpix[y])[x] = b;
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/* new 7 mars 2010 */
int
Image_combine_random_rgb(Image_Desc *s1, Image_Desc *s2, Image_Desc *d,
							int yo)
{
int	foo, x, y, r, g, b, r7, g7, b7;

if ( (foo=Image_compare_desc(s1, s2)) )
	{
	fprintf(stderr, "%s: sources are differents (%d)\n", __func__, foo);
	return foo;
	}

if ( (foo=Image_compare_desc(s1, d)) )
	{
	fprintf(stderr, "%s: destination different (%d)\n", __func__, foo);
	return foo;
	}

for (y=0; y<s1->height; y++)
	{
	for (x=0; x<s1->width; x++)
		{
		if ( (rand()%100) > yo )	r = (s1->Rpix[y])[x];
		else				r = (s2->Rpix[y])[x];
		if ( (rand()%100) > yo )	g = (s1->Gpix[y])[x];
		else				g = (s2->Gpix[y])[x];
		if ( (rand()%100) > yo )	b = (s1->Bpix[y])[x];
		else				b = (s2->Bpix[y])[x];

		(d->Rpix[y])[x] = r;
		(d->Gpix[y])[x] = g;
		(d->Bpix[y])[x] = b;
		}
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
