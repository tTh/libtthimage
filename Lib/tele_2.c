/*
			tele_2.c
			--------
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <math.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *		comme au bon vieux temps...
 */
int Image_TV_old(Image_Desc *src, Image_Desc *dst, int p1, int p2, int p3)
{
int	foo;
int	x, y, v1, v2;
int	resteY;
int	sigma;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p  %d %d %d )\n", __func__, 
			src, dst,  p1, p2, p3);
#endif

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

sigma = p1 + p2 + p3;
if (!sigma) sigma++;

resteY = src->height % 3;
resteY /= 2;		/* half for the top, half for the bottom */

#if DEBUG_LEVEL
fprintf(stderr, "%s : height %d reste Y %d\n", __func__, src->height, resteY);
fprintf(stderr, "%s : %d %d %d -> %d\n",       __func__, p1, p2, p3, sigma);  
#endif

for (y=resteY; y<src->height-resteY-1; y+=3) {
	for (x=0; x<src->width; x++) {
		v1 =  ( Image_R_pixel(src, x, y) +
			Image_R_pixel(src, x, y+1) +
			Image_R_pixel(src, x, y+2) +
		        Image_G_pixel(src, x, y) +
			Image_G_pixel(src, x, y+1) +
			Image_G_pixel(src, x, y+2) +
		        Image_B_pixel(src, x, y) +
			Image_B_pixel(src, x, y+1) +
			Image_B_pixel(src, x, y+2) ) / 9 ;

		v2 = (v1 * p1) / sigma;
		Image_plotRGB(dst, x,   y, v2, v2, v2);
		v2 = (v1 * p2) / sigma;
		Image_plotRGB(dst, x, y+1, v2, v2, v2);
		v2 = (v1 * p3) / sigma;
		Image_plotRGB(dst, x, y+2, v2, v2, v2);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*	
 *	this is juste a wrapper function - need more work...
 */
int Image_TV_veryold(Image_Desc *src, Image_Desc *dst, int p1, int p2, int p3)
{
int	foo;

/* 15 janvier 2007: chasse au coredump... */
if ( (src->magic!=MAGIC_OF_IMAGE) || (dst->magic!=MAGIC_OF_IMAGE) ) {
    fprintf(stderr, "Image TV veryold: bad image descriptor\n");
    abort();
    }

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "Image TV veryold: images are differents %d\n", foo);
    return foo;
    }

foo = Image_TV_old(src, dst, p1, p2, p3);
fprintf(stderr, "%s : ret %d\n", __func__, foo);

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
/*
 *	new 10 novembre 2013, in mixart-myrys
 */
int Image_TV_pix_0(Image_Desc *src, Image_Desc *dst, int w, int h, int grey)
{
int		foo, ox, oy;
int		x, y, r, g, b;
int		xmax, ymax;
Image_Rect      zone;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p  %d %d %d )\n", __func__, 
			src, dst,  w, h, grey);
#endif

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

#if DEBUG_LEVEL
	fprintf(stderr, "%s : w %d h %d grey %d\n", __func__, w, h, grey);
#endif

if (0==h || 0==w) {
	return BAD_PARAMETER;
	}

zone.w = w-1;			zone.h = h-1;
xmax = src->width - w;		ymax = src->height - h;
ox = (src->width % w) / 2;
oy = (src->height % h) / 2;
#if DEBUG_LEVEL
fprintf(stderr, "%s : offsets = %d & %d\n", __func__, ox, oy);
#endif
Image_clear(dst, grey, grey, grey);

for (x=ox; x<(xmax-ox); x+=w) {
	zone.x = x + (w/2);
	for (y=oy; y<(ymax-oy); y+=h) {
		zone.y = y + (h/2);
		r = src->Rpix[y][x] & 0xfe;
		g = src->Gpix[y][x] & 0xfe;
		b = src->Bpix[y][x] & 0xfe;

		Image_paint_rect(dst, &zone, r, g, b);
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	new 10 novembre 2013, in mixart-myrys
 */
int Image_TV_pix_1(Image_Desc *src, Image_Desc *dst, int w, int h, int p3)
{
int		foo;
int		x, y, v1, v2, r, g, b;
int		xmax, ymax;
Image_Rect      zone;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p  %d %d %d )\n", __func__, 
			src, dst,  w, h, p3);
#endif

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

#if DEBUG_LEVEL
fprintf(stderr, "%s : w %d h %d g%d\n", __func__, w, h, p3);
#endif

if ((0==h) || (0==w)) {
	return BAD_PARAMETER;
	}

zone.w = w-1;
zone.h = h-1;
xmax = src->width - w;
ymax = src->height - h;
v1 = v2 = 0;

for (x=0; x<xmax; x+=w) {
	zone.x = x;
	for (y=0; y<ymax; y+=h) {
		zone.y = y;
		r = src->Rpix[y][x];
		g = src->Gpix[y][x];
		b = src->Bpix[y][x];

		Image_paint_rect(dst, &zone, r, g, b);
		}
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/*
 * new 3 jan 2013 - at Mixart-Myrys
 */
int Image_TV_cplus_v1(Image_Desc *src, Image_Desc *dst, int p)
{
int		foo;
int		y, taille;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__, src, dst, p);
#endif

if (p <= 1) {
	fprintf(stderr, "%s: p must be > 1, but was %d\n", __func__, p);
	return BAD_PARAMETER;
	}

if ( (foo=Image_compare_desc(src, dst)) ) {
	fprintf(stderr, "%s : images are differents %d\n", __func__, foo);
	return foo;
	}

taille = src->width - (abs(p*2));
#if DEBUG_LEVEL
fprintf(stderr, "%s: largeur %d, taille %d\n", __func__, src->width, taille);
#endif

/* make a background for left and right border's pixels */
Image_gray_noise_0(dst, 20, 90);

for (y=0; y<src->height; y++) {
	switch (rand()%3) {
		case 0 :
			memcpy((dst->Rpix[y]), src->Rpix[y], taille);
			memcpy((dst->Gpix[y]), src->Gpix[y], taille);
			memcpy((dst->Bpix[y]), src->Bpix[y], taille);
		 	break;
		case 1 :
			memcpy((dst->Rpix[y])+p, src->Rpix[y], taille);
			memcpy((dst->Gpix[y])+p, src->Gpix[y], taille);
			memcpy((dst->Bpix[y])+p, src->Bpix[y], taille);
		 	break;
		case 2 :
			memcpy((dst->Rpix[y])+p+p, src->Rpix[y], taille);
			memcpy((dst->Gpix[y])+p+p, src->Gpix[y], taille);
			memcpy((dst->Bpix[y])+p+p, src->Bpix[y], taille);
		 	break;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/* new 28 janvier 2015 - MixartMyrys */
/* le parametre 'mode' est un flag */

int Image_TV_triligne(Image_Desc *src, Image_Desc *dst, int mode)
{
int	x, y, tri, v, idx;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p %p %d )  -> ", __func__, src, dst, mode);
if (mode)	fprintf(stderr, "   rand\n");
else		fprintf(stderr, " modulo\n");
#endif

Image_clear(dst, 60, 60, 60);		/* XXX necessaire ? */

idx = rand() % 42;

for (y=0; y<src->height; y++) {
	if (0==mode) {
		tri = (idx + y) % 3;
		}
	else	{
		tri = rand() % 3;
		}
#if DEBUG_LEVEL > 1
	fprintf(stderr, "     %5d -> %d\n", y, tri);
#endif
	switch (tri) {
		case 0:
			for (x=0; x<(src->width)-1; x++) {
				v =  (src->Rpix[y][x] + src->Rpix[y][x+1]);
				dst->Rpix[y][x] = v / 2;
				v =  (src->Gpix[y][x] + src->Gpix[y][x+1]);
				dst->Gpix[y][x] = v / 2;
				v =  (src->Bpix[y][x] + src->Bpix[y][x+1]);
				dst->Bpix[y][x] = v / 2;
				}
			break;
		case 1:
			for (x=1; x<(src->width)-1; x++) {
				v =  src->Rpix[y][x-1] +  src->Rpix[y][x+1];
				dst->Rpix[y][x] = v / 2;
				v =  src->Gpix[y][x-1] +  src->Gpix[y][x+1];
				dst->Gpix[y][x] = v / 2;
				v =  src->Bpix[y][x-1] +  src->Bpix[y][x+1];
				dst->Bpix[y][x] = v / 2;
				}
			break;
		case 2:
			for (x=1; x<src->width; x++) {
				v =  src->Rpix[y][x] +  src->Rpix[y][x-1];
				dst->Rpix[y][x] = v / 2;
				v =  src->Gpix[y][x] +  src->Gpix[y][x-1];
				dst->Gpix[y][x] = v / 2;
				v =  src->Bpix[y][x] +  src->Bpix[y][x-1];
				dst->Bpix[y][x] = v / 2;
				}
			break;
		default:
			/* blam< */
			abort();
		}	
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
