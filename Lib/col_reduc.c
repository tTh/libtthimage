/*
 *		COLOR REDUCTION
 *	new 27 Jan 2002.
 */

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"

#define DEBUG_LEVEL 1

/*::------------------------------------------------------------------::*/
/*
 *	ATTENTION: cette fonction sert � faire des essais, et ne doit
 *		pas �tre utilis� en production. Le prototype et le
 *		comportement vont bient�t changer.
 *
 *	n is the number of wanted colors 
 */
int Image_essai_col_reduce(Image_Desc *src, Image_Desc *dst, int n, int t)
{
RGB_map		map;
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, "--------------- essai col reduce ---------------\n");
fprintf(stderr, "nombre de couleurs demande: %d\n", n);
fprintf(stderr, "type de la reduction: %d\n", t);
#endif

foo = Image_calc_Map_4bits(src, &map, n);
#if DEBUG_LEVEL
Image_print_error("calc map 4 bits", foo);
Image_plot_Map("/tmp/aaaa-map.tga", &map, "map 4 bits");
#endif
Image_save_color_Map("reduce.map", "reduce ?", &map);

foo = Image_colors_2_Map(src, dst, &map, t);
#if DEBUG_LEVEL
fprintf(stderr, "Image_colors_2_Map -> %d\n", foo);
#endif

return foo;
}
/*::------------------------------------------------------------------::*/
