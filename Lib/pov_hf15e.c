/*
                pov_hf15e.c
                ===========

	operations de morphologie mathematique appliquees
	aux height-fields.

*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"        

/*::------------------------------------------------------------------::*/
static struct 
	{
	int	x, y;
	} off[] = 
	{
	{ -1, -1 },	{  0, -1 },	{  1, -1 },
	{ -1,  0 },	{  0,  0 },	{  1,  0 },
	{ -1,  1 },	{  0,  1 },	{  1,  1 }
	};
/*::------------------------------------------------------------------::*/
/*
 *	le parametre 'coef' n'est pas utilise. D'ailleurs, je ne vois
 *	pas trop quoi y mettre ?
 */
int Image_hf15_dilate(Image_Desc *src, Image_Desc *dst, int coef)
{
(void)coef;		/* KILL WARNING */
int	foo;
int	x, y, h, hmax;

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "Image hf15 dilate: images differents %d\n", foo);
    return foo;
    }

for (y=1; y<dst->height-1; y++) {
	for (x=1; x<dst->width-1; x++) {
		hmax = -1664;
		for (foo=0; foo<9; foo++) {
			h = Image_hf15_height(src, x+off[foo].x, y+off[foo].y);
			if (h > hmax) hmax = h;
			}
		Image_hf15_plot(dst, x, y, hmax);	
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	le parametre 'coef' n'est pas utilise.
 */
int Image_hf15_erode(Image_Desc *src, Image_Desc *dst, int coef)
{
(void)coef;		/* KILL WARNING */
int	foo;
int	x, y, h, hmin;

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "Image hf15 erode: images differents %d\n", foo);
    return foo;
    }

for (y=1; y<dst->height-1; y++) {
	for (x=1; x<dst->width-1; x++) {
		hmin = 42042;
		for (foo=0; foo<9; foo++) {
			h = Image_hf15_height(src, x+off[foo].x, y+off[foo].y);
			if (h < hmin) hmin = h;
			}
		Image_hf15_plot(dst, x, y, hmin);	
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *		Et maintenant, il reste a coder le chapeau haut-de-forme
 */
/*::------------------------------------------------------------------::*/

