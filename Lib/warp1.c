/*
                    warp1.c
		_______________
*/

#include   <stdio.h>
#include   <math.h>
#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	Rotation d'image. le parametre 'angle' est en degres.
 */
int Image_center_rotate(Image_Desc *src, Image_Desc *dst, double angle)
{
int	xs, ys, xd, yd;
double	arad, fxs, fys, fxd, fyd;
double	sincalc, coscalc;
int	mx, my;
int	r, g, b;

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s : work in progress...\n", __func__);
#endif

arad = angle/180*M_PI;
mx = dst->width / 2;
my = dst->height / 2;
sincalc = sin(arad);
coscalc = cos(arad);

for (xd=0; xd<dst->width; xd++)	{
	fxd = (double)(xd-mx);
	for (yd=0; yd<dst->height; yd++) {
		fyd = (double)(yd-my);
		fxs = fxd * coscalc - fyd * sincalc;
		fys = fxd * sincalc + fyd * coscalc;

		xs = (int)fxs + mx;
		ys = (int)fys + my;

		if (xs>=0 && xs<src->width && ys>=0 && ys<src->height) {
			/* Image_getRGB(src, xs, ys, &r, &g, &b); */
			r = (src->Rpix[ys])[xs];
			g = (src->Gpix[ys])[xs];
			b = (src->Bpix[ys])[xs];
			}
		else
			{ r = g = b = 127; }

		/* Image_plotRGB(dst, xd, yd, r, g, b); */
		(dst->Rpix[yd])[xd] = r;
		(dst->Gpix[yd])[xd] = g;
		(dst->Bpix[yd])[xd] = b;
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	La meme chose, en gros...
 *	Sauf qu'on pourrait theoriquement choisir la position du
 *	point de rotation.
 *
 *					reste plus qu'� coder...
 */
int Image_center_rotate_xy(Image_Desc *src, Image_Desc *dst, double angle,
						double x, double y)
{
int	foo, xd, yd;
double	arad, fxs, fys, fxd, fyd;
double	sincalc, coscalc;
int	mx, my;

#if DEBUG_LEVEL
fprintf(stderr, "Center Rotate XY: work in progress...\n");
#endif

fprintf(stderr, "%s on %p at %g,%g\n", __func__, src, x, y);

arad = angle/180*M_PI;
mx = dst->width / 2;
my = dst->height / 2;
sincalc = sin(arad);
coscalc = cos(arad);

fxs = fys = 0.0;	/* ? hein ? */
foo = 0;

for (xd=0; xd<dst->width; xd++) {
	fxd = (double)(xd-mx);
	for (yd=0; yd<dst->height; yd++) {
		fyd = (double)(yd-my);


		}
	}

			/* MAIS C'EST PAS ENCORE FINI CE TRUC ? */
			/* 4 mai 2007: ben non, ya rien :) */
			/* 5 juillet 2022 : toujours pas fait ??? */

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/*
 *		voir aussi les modules scale.c et warp0.c
 */
/*::------------------------------------------------------------------::*/
