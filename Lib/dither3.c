/*
		dither3.c
		---------
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_dither_atkinson(Image_Desc *s, Image_Desc *d, int uh)
{
int	x, y, r, g, b;
int	foo, sr, sg, sb;
int	old, new;

if ( (foo=Image_compare_desc(s, d)) )
    {
    fprintf(stderr, "%s : images are differents %d\n", __func__, foo);
    return foo;
    }

Image_clear(d, 0, 0, 0);
r = g = b = sr = sg = sb = 0; 

for (y=0; y<s->height; y++) {
	for (x=0; x<s->width; x++) {

		}
	}

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
int Image_dither_4x4_0(Image_Desc *s, Image_Desc *d, int uh)
{
int	x, y, r, g, b;
int	foo, sr, sg, sb;

fprintf(stderr, "* Image_dither_4x4_0 (%s) is experimental\n", __FILE__);
fprintf(stderr, "* Image_dither_4x4_0 le parametre est %d\n", uh);

if ( (foo=Image_compare_desc(s, d)) ) {
    fprintf(stderr, "dither 4x4 0: images are differents %d\n", foo);
    return foo;
    }

Image_clear(d, 0, 0, 0);

for (y=0; y<s->height; y++) {
	for (x=0; x<s->width; x++) {

		/*     XXX */

		}
	}

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
int Image_dither_bayer8x8rnd(Image_Desc *s, Image_Desc *d, int shuff, int mono)
{
uint16_t		matrice[16][16];
int		foo, itmp, m, n;
int		x, y, r, g, b, pix;

fprintf(stderr, "%s ( %p %p %d %d )\n", __func__, s, d, shuff, mono);

/* remplissage de la matrice */
fprintf(stderr, "   init matrice\n");
for(foo=0; foo<256; foo++) {
	((uint16_t *)matrice)[foo] = foo;
	}
/* brasssage de la matrice */
fprintf(stderr, "   shuffle matrice %d\n", shuff);
for (foo=0; foo<shuff; foo++) {		/* magic number ? */
	m = rand() & 0xff;
	n = rand() & 0xff;
	itmp = ((uint16_t *)matrice)[n];
	((uint16_t *)matrice)[n] = ((uint16_t *)matrice)[m];
	((uint16_t *)matrice)[m] = itmp;
	}

for (y=0; y<s->height; y++) {
	for (x=0; x<s->width; x++) {
		r = s->Rpix[y][x];
		g = s->Gpix[y][x];
		b = s->Bpix[y][x];
		if (mono) {
			pix = (r + g +b) / 3;
			d->Rpix[y][x] = d->Gpix[y][x] = d->Bpix[y][x] = \
					(pix > matrice[x%16][y%16]) * 255;
			}
		else	{
			d->Rpix[y][x] =  (r > matrice[x%16][y%16]) * 255;
			d->Gpix[y][x] =  (g > matrice[x%16][y%16]) * 255;
			d->Bpix[y][x] =  (b > matrice[x%16][y%16]) * 255;
			}
		}
	}

fprintf(stderr, "   done\n");

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/

