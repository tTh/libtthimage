/*
 *	cadresbox.c - new 30 janvier 2014
 */

#include  <stdio.h>
#include  <math.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_cadre_box_0(Image_Desc *img, int t1, RGBA *color)
{
int		foo;
Image_Rect	rect;
RGBA		tmpcol;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p %d )\n", __func__, img, t1);
#endif

/* in first action, we make left & right borders of the picz */
rect.w = t1; rect.h = img->height;
rect.x = rect.y = 0;
foo = Image_paint_rect(img, &rect, color->r, color->g, color->b);
rect.x = img->width - t1;
foo = Image_paint_rect(img, &rect, color->r, color->g, color->b);

/* next part : top and bottom borders */
rect.h = t1; rect.w = img->width;
rect.x = rect.y = 0;
foo = Image_paint_rect(img, &rect, color->r, color->g, color->b);
rect.y = img->height - t1;
foo = Image_paint_rect(img, &rect, color->r, color->g, color->b);

tmpcol.r = color->r ^ 0x80;
tmpcol.g = color->b ^ 0x80;
tmpcol.b = color->b ^ 0x80;
rect.x = t1;	rect.y = t1;
rect.w = img->width - (t1 * 2);
rect.h = img->height - (t1 * 2);
foo = Image_draw_rect(img, &rect, tmpcol.r, tmpcol.g, tmpcol.b);

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
int Image_cadre_4coins_0(Image_Desc *img, int taille, RGBA *color)
{
int		foo;
Image_Rect	rect;
RGBA		tmpcol;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p %d )\n", __func__, img, taille);
#endif

tmpcol.r = color->r ^ 0x80;
tmpcol.g = color->b ^ 0x80;
tmpcol.b = color->b ^ 0x80;

rect.w = rect.h = taille;

rect.x = rect.y = 0;
foo = Image_paint_rect(img, &rect, color->r, color->g, color->b);
foo = Image_draw_rect(img, &rect, tmpcol.r, tmpcol.g, tmpcol.b);

rect.x = img->width - taille;
foo = Image_paint_rect(img, &rect, color->r, color->g, color->b);
#if DEBUG_LEVEL
fprintf(stderr, "********* %s ***********\n", __func__);
#endif
foo = Image_draw_rect(img, &rect, tmpcol.r, tmpcol.g, tmpcol.b);
#if DEBUG_LEVEL
fprintf(stderr, "      ********* ---> %d\n", foo);
#endif

rect.y = img->height - taille;
foo = Image_paint_rect(img, &rect, color->r, color->g, color->b);
foo = Image_draw_rect(img, &rect, tmpcol.r, tmpcol.g, tmpcol.b);

rect.x = 0;
foo = Image_paint_rect(img, &rect, color->r, color->g, color->b);
foo = Image_draw_rect(img, &rect, tmpcol.r, tmpcol.g, tmpcol.b);

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
int Image_cadre_box4c_0(Image_Desc *img, int t1, int t2, RGBA *color)
{
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p %d %d %p )\n", __func__, img, t1, t2, color);
#endif

foo = Image_cadre_box_0(img, t1, color);
foo = Image_cadre_4coins_0(img, t2, color);

return 666;
}
/*::------------------------------------------------------------------::*/
