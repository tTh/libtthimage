/*
 *	GADGRECT : des gadgets dans des rectangles
 */

#include <stdio.h>
#include  <string.h>
#include  <stdlib.h>
#include  <math.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*		private variables					*/

/*::------------------------------------------------------------------::*/
/*		private functions					*/

/*::------------------------------------------------------------------::*/
int Image_gadrct_premier_essai(Image_Desc *src, Image_Rect *rect,
				Image_Desc *dst, int ka, int kb)
{
int	x, y, r, g, b;

#if DEBUG_LEVEL
fprintf(stderr, "Oups, trapped in '%s'\n", __func__);
#endif

for (x=0; x<rect->w; x++)
	{
	for (y=0; y<rect->h; y++)
		{
		r = g = b = x ^ y;
		}
	}
#if DEBUG_LEVEL
fprintf(stderr, "Yooo, back from '%s'\n", __func__);
#endif

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Image_gadrct_Hsweep_0(Image_Desc *dst, Image_Rect *rect, int flag)
{
int	x, y, r, g, b, foo;
double	val, adder;

if (dst->magic != MAGIC_OF_IMAGE)
	{
	fprintf(stderr, "%s: need Dead Beef!\n", __func__);
	return NOT_AN_IMAGE_DESC;
	}

#if DEBUG_LEVEL > 1
fprintf(stderr, "Oups, trapped in '%s'\n", __func__);
#endif

adder = 256.0 / (double)rect->w;
val = 0.0;

#if DEBUG_LEVEL > 1
fprintf(stderr, "   val=%f  adder=%f\n", val, adder);
#endif

for (x=0; x<rect->w; x++)
	{
	foo = (int)val;
	r = g = b = foo;
	for (y=0; y<rect->h; y++)
		{
		Image_plotRGB(dst, x+rect->x, y+rect->y, r, g, b);
		}
	val += adder;
	}


#if DEBUG_LEVEL > 1
fprintf(stderr, "Yoohoo, back from '%s'\n", __func__);
#endif

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Image_gadrct_Vsweep_0(Image_Desc *dst, Image_Rect *rect, int flag)
{
int	x, y, r, g, b, foo;
double	val, mult, adder;

val = adder = 0;
for (x=0; x<rect->w; x++)
	{
	foo = (int)val;
	r = g = b = foo;
	for (y=0; y<rect->h; y++)
		{
		Image_plotRGB(dst, x+rect->x, y+rect->y, r, g, b);
		}
	val += adder;
	}


#if DEBUG_LEVEL
fprintf(stderr, "Yooo, back from '%s'\n", __func__);
#endif

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/*
 *	attention parametres bizarres
 */
int Image_gadrct_poke_from_tga(char *namesrc, Image_Rect *fromrect,
			Image_Desc *dst, int xdst, int ydst, int flags)
{
Image_Desc 	*source;
int		foo, x, y, r, g, b;
int		xs, ys;

#if DEBUG_LEVEL
fprintf(stderr, "%s : source is      %s\n", __func__, namesrc);
#endif

source = Image_TGA_alloc_load(namesrc);
if (NULL == source)
	{
	fprintf(stderr, "%s '%s' : FAIL !\n", __func__, namesrc);
	return FULL_NUCKED;
	}

xs = fromrect->x;
ys = fromrect->y;
	
#if DEBUG_LEVEL > 1
Image_dump_descriptor(source, "image source poke_from_tga");
Image_dump_rect(fromrect, "source fragment", 1);
#endif
#if DEBUG_LEVEL
fprintf(stderr, "%s : go to %d %d in %p\n", __func__, xdst, ydst, dst);
#endif

for (y=0; y<fromrect->w; y++)
	{
	for (x=0; x<fromrect->h; x++)
		{
		Image_getRGB(source, x+xs, y+ys, &r, &g, &b);
		Image_plotRGB(dst, x+xdst, y+ydst, r, g, b);
		}
	}

Image_DeAllocate(source);	free(source);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Image_gadrct_to_gray(Image_Desc *src, Image_Rect *rect,
				Image_Desc *dst, int ka, int kb)
{

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p %p %p %d %d\n", __func__, src, rect, dst, ka, kb);
#endif

if (dst->magic != MAGIC_OF_IMAGE)
	{
	fprintf(stderr, "%s: DST need Dead Beef!\n", __func__);
	return NOT_AN_IMAGE_DESC;
	}
return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/* assombrissement/eclaircissement d'un rectangle dans une image */
int Image_gadrct_dimmer(Image_Desc *img, Image_Rect *rect, float dimf)
{
#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p %p %f )\n", __func__, img, rect, dimf);
#endif

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/* new 2 decembre 2009 */
/* first use in "plotteur.c|Image_plot_square_Map" */

int Image_gadrct_cross(Image_Desc *img, Image_Rect *rect, int k)
{
RGBA	rgba;
int	x1, y1, x2, y2;

rgba.r = rgba.g = rgba.b = 200;

x1 = rect->x;	y1 = rect->y;
x2 = rect->x + rect->w;
y2 = rect->y + rect->h;
Image_draw_line(img, x1, y1, x2, y2, &rgba);

x1 = rect->x + rect->w;
y1 = rect->y;
x2 = rect->x;
y2 = rect->y + rect->h;
Image_draw_line(img, x1, y1, x2, y2, &rgba);

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
