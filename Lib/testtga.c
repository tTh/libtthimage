/*
 *		TESTTGA.C
 *
 *	see also:	essais.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <math.h>

#include  "../tthimage.h"
#include  "essais.h"

/*::------------------------------------------------------------------::*/

int test_du_jpeg_reader(char *filename)
{
Image_Desc	destimg;
int		foo;

fprintf(stderr, ">>> %s ( '%s' ) \n", __func__, filename);

#if 0
foo = try_to_read_a_jpeg_file(filename, &destimg);
#endif

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int main(int argc, char *argv[])
{
char	*fichier;
int	foo, idx;

printf("\n==========  TestTga  %s  %s  ==========\n", __DATE__, __TIME__);
Image_print_version(2);

#if DEBUG_LEVEL
Image_print_sizeof_structs("tth was here");
#endif

if (argc != 2) {
	printf("no args, using the testfile\n");
	fichier = "wou.tga";
	}
else
	{
	srand(getpid());
	fichier = argv[1];
	}

Image_start_chrono("Essai", 0);

for (idx=0; idx<10; idx++) {
	foo = Test_Egalisations(fichier, 0);
	fprintf(stderr, "    essai egalisation -> %d\n", foo);
	foo = Essai_Televisions(fichier, 10); 
	fprintf(stderr, "    essai televisions -> %d\n", foo);
	foo = Test_Dithering(fichier, 0);
	fprintf(stderr, "    essai dithering   -> %d\n", foo);
	foo = Test_des_filtres(fichier, 0);
	fprintf(stderr, "    essai filtres     -> %d\n", foo);
	foo = Test_RGBmask(fichier);
	fprintf(stderr, "    essai rgb mask    -> %d\n", foo);
	foo = Test_Classif(fichier, 0);
	fprintf(stderr, "    essai classif     -> %d\n", foo);
	foo = Test_des_warpings(fichier, 0);
	fprintf(stderr, "    essai warping     -> %d\n", foo);
	foo = Essai_color_2_map(fichier, idx);
	fprintf(stderr, "    essai col2map     -> %d\n", foo);
	

	fprintf(stderr, "\n*************** %s: fin passe %d ******\n\n",
						argv[0], idx);
	}

#if 0


foo = test_du_jpeg_reader("in.jpeg");
fprintf(stderr, "    essai lecture jpeg -> %d\n", foo);

Test_des_patterns("/tmp/pattt", 0, 0);
foo = Essai_des_jauges(fichier, 17);

foo = Essai_Extractbits(fichier, 17);

Essai_des_Glitches(fichier, 900); 
Essai_Television(fichier, 10); 

Essai_des_drawings(fichier, 17);
foo = Test_Effects_A(fichier, 17);
Test_des_warpings(fichier, 17);

foo = Essai_des_Combines(fichier, 17);
fprintf(stderr, "essai combine -> %d\n", foo);

foo = Test_Dithering(fichier, 17);
Essai_des_Contrastes(fichier, 0);

Essai_anamorphoses(fichier, 0.666, 42);

Test_copie_de_rectangles(fichier); 
Essai_des_7_segments(fichier, 0x0F); 
/* Essai_color_2_map(fichier, 0);  */
Essais_plot_Map(5); 

foo = Essai_des_bitplanes("Grgrgr !", 1600, 1200);
printf("retour essai bitplane -> %d\n", foo);

Test_hf15_synth_fromPtl(fichier, "aaaa.tga");

Test_classif(fichier, 0);
Test_new_scale(fichier, 0);

Essai_des_cadres(fichier, 0);
Essai_luminance(fichier, 0); 
Essayer_les_alphas(fichier, 0); 

foo = Essai_de_la_Recursion(fichier, 0);

#endif

Image_stop_chrono("Essai", 0);

/*
 *	Warning: this function was a GNU/Linuxism !
 */
#if DEBUG_LEVEL > 1
malloc_stats(); 
#endif

return 0;
}
/*::------------------------------------------------------------------::*/
