
/*
                    rgbmask.c
		----------------
		WORK IN PROGRESS
	- manque de documentation, toussa -
*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	horizontal lines
 */
int Image_rgbmask_H(Image_Desc *src, Image_Desc *dst, int gris)
{
int  foo, x, y;

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "%s : images are differents %d\n", __func__, foo);
    return foo;
    }

Image_copy(src, dst);		/* XXX pourquoi cette copie ? */

for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		switch (y%3) {
			case 0:
				Image_plot_channel(dst, 'r', x, y, gris);
				Image_plot_channel(dst, 'g', x, y, gris);
				break;
			case 1:
				Image_plot_channel(dst, 'r', x, y, gris);
				Image_plot_channel(dst, 'b', x, y, gris);
				break;
			case 2:
				Image_plot_channel(dst, 'b', x, y, gris);
				Image_plot_channel(dst, 'g', x, y, gris);
				break;
			}
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	vertical lines
 */
int Image_rgbmask_V(Image_Desc *src, Image_Desc *dst, int gris)
{
int  foo, x, y;

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "Image rgbmask V: images are differents %d\n", foo);
    return foo;
    }
Image_copy(src, dst);

for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		switch (x%3) {
			case 0:
				Image_plot_channel(dst, 'r', x, y, gris);
				Image_plot_channel(dst, 'g', x, y, gris);
				break;
			case 1:
				Image_plot_channel(dst, 'r', x, y, gris);
				Image_plot_channel(dst, 'b', x, y, gris);
				break;
			case 2:
				Image_plot_channel(dst, 'b', x, y, gris);
				Image_plot_channel(dst, 'g', x, y, gris);
				break;
			}
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	sequential dots - _very_ dependant on image dimensions.
 */
int Image_rgbmask_2(Image_Desc *src, Image_Desc *dst, int gris)
{
int  foo, x, y;

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "Image rgbmask 2: images are differents %d\n", foo);
    return foo;
    }
Image_copy(src, dst);

foo = 0;
for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		foo++;
		switch (foo%3) {
			case 0:
				Image_plot_channel(dst, 'r', x, y, gris);
				Image_plot_channel(dst, 'g', x, y, gris);
				break;
			case 1:
				Image_plot_channel(dst, 'r', x, y, gris);
				Image_plot_channel(dst, 'b', x, y, gris);
				break;
			case 2:
				Image_plot_channel(dst, 'b', x, y, gris);
				Image_plot_channel(dst, 'g', x, y, gris);
				break;
			}
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	random dots - VERY BAD RESULTS :(
 */
int Image_rgbmask_R(Image_Desc *src, Image_Desc *dst, int gris)
{
int  foo, x, y;

#if DEBUG_LEVEL
fprintf(stderr, "this function, %s(%d), is very buggy...\n", __func__, gris);
#endif

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "Image rgbmask R: images are differents %d\n", foo);
    return foo;
    }
Image_copy(src, dst);

for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		foo = rand() % 3;	/* mmmm pas tres bon :( */
		switch (foo) {
			case 0:
				Image_plot_channel(dst, 'r', x, y, gris);
				Image_plot_channel(dst, 'g', x, y, gris);
				break;
			case 1:
				Image_plot_channel(dst, 'r', x, y, gris);
				Image_plot_channel(dst, 'b', x, y, gris);
				break;
			case 2:
				Image_plot_channel(dst, 'b', x, y, gris);
				Image_plot_channel(dst, 'g', x, y, gris);
				break;
			}
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
