/*
 *		glitch.c - new sept 2014 - MYRYS
 */

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_GlitchMe(Image_Desc *src, Image_Desc *dst, char code, 
					int k1, char *txt1)
{
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p %p %d '%s' )\n", __func__, src, dst, k1, txt1);
#endif

switch (code)
	{
	case 'x' :
		foo = Image_Glitch_xor(src, dst, k1);
		break;	
	case 's' :
		foo = Image_Glitch_simple(src, dst, k1);
		break;	
	default :
		abort();
		break;
	}
if (foo) {
	fprintf(stderr, "%s got a %d\n", __func__, foo);
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
int Image_Glitch_xor(Image_Desc *src, Image_Desc *dst, int k)
{
int		foo;
int		nbre;
int		x, y, d, v;
int		r, g, b;

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

for (nbre=0; nbre<k; nbre++)
	{
	x = rand() % src->width;
	y = rand() % src->height;
	Image_getRGB(src, x, y, &r, &g, &b);

	d = rand() & 3;
	switch (d)
		{
		case 0:
			for (v=0; v<x; v++)
				Image_XOR_pixel(dst, v, y, r, g, b);
			break;
		case 1:
			for (v=0; v<y; v++)
				Image_XOR_pixel(dst, x, v, r, g, b);
			break;
		case 2:
			for (v=x; v<src->width; v++)
				Image_XOR_pixel(dst, v, y, r, g, b);
			break;
		case 3:
			for (v=y; v<src->height; v++)
				Image_XOR_pixel(dst, x, v, r, g, b);
			break;
		}
	}
	
return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
/*
 *	ok, 12 oct 2014
 */
int Image_Glitch_simple(Image_Desc *src, Image_Desc *dst, int k)
{
int		foo;
int		nbre;
int		x, y, d, v;
int		r, g, b;

if ( (foo=Image_compare_desc(src, dst)) )
    {
    fprintf(stderr, "%s: images are differents %d\n", __func__, foo);
    return foo;
    }

for (nbre=0; nbre<k; nbre++)
	{
	x = rand() % src->width;
	y = rand() % src->height;
	d = rand() & 3;
#if 0
	printf("%6d        %6d  %6d   %d\n", nbre, x, y, d);
#endif
	Image_getRGB(src, x, y, &r, &g, &b);

	switch (d)
		{
		case 0:
			for (v=0; v<x; v++)
				Image_plotRGB(dst, v, y, r, g, b);
			break;
		case 1:
			for (v=0; v<y; v++)
				Image_plotRGB(dst, x, v, r, g, b);
			break;
		case 2:
			for (v=x; v<src->width; v++)
				Image_plotRGB(dst, v, y, r, g, b);
			break;
		case 3:
			for (v=y; v<src->height; v++)
				Image_plotRGB(dst, x, v, r, g, b);
			break;
		default:
			fprintf(stderr, "blurg in %s\n", __func__);
			return PASTIS;
		}
	}
	
return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
