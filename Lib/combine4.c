/*
                    combine4.c
		    ----------
*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *
 */
int
Image_combine_4img_0(	Image_Desc *s1, Image_Desc *s2,
			Image_Desc *s3, Image_Desc *s4,
			Image_Desc *dst)
{
int	x, y;
int	r, g, b;

fprintf(stderr, "Image combine 4img 0: NO SANITY CONTROL !\n");

r = g = b = 0;		/* warning killer */

for (y=0; y<dst->height; y++)
	{
	for (x=0; x<dst->width; x++)
		{
		if (  (x&1) &&  (y&1) )
			{
			r = (s1->Rpix[y])[x];
			g = (s1->Gpix[y])[x];
			b = (s1->Bpix[y])[x];
			}
		if ( !(x&1) &&  (y&1) )
			{
			r = (s2->Rpix[y])[x];
			g = (s2->Gpix[y])[x];
			b = (s2->Bpix[y])[x];
			}
		if ( (x&1) && !(y&1) )
			{
			r = (s3->Rpix[y])[x];
			g = (s3->Gpix[y])[x];
			b = (s3->Bpix[y])[x];
			}
		if ( !(x&1) && !(y&1) )
			{
			r = (s4->Rpix[y])[x];
			g = (s4->Gpix[y])[x];
			b = (s4->Bpix[y])[x];
			}
		(dst->Rpix[y])[x] = r;
		(dst->Gpix[y])[x] = g;
		(dst->Bpix[y])[x] = b;
		}
	}

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
/*
 * first step : 10 avril 2009
 */
int
Image_combine_4img_1(	Image_Desc *s1, Image_Desc *s2,
			Image_Desc *s3, Image_Desc *s4,
			Image_Desc *dst)
{
Image_Desc	*source;
int		x, y, alea;
int		r, g, b;

fprintf(stderr, "%s: NO SANITY CONTROL !\n", __func__);

r = g = b = 0;		/* warning killer */
source = NULL;		/* another warning killer */

for (y=0; y<dst->height; y++)
	{
	for (x=0; x<dst->width; x++)
		{
		alea = rand() % 4;
		switch (alea)
			{
			case 0:		source=s1;	break;		
			case 1:		source=s2;	break;		
			case 2:		source=s3;	break;		
			case 3:		source=s4;	break;		
			}
		r = (source->Rpix[y])[x];
		g = (source->Gpix[y])[x];
		b = (source->Bpix[y])[x];
		(dst->Rpix[y])[x] = r;
		(dst->Gpix[y])[x] = g;
		(dst->Bpix[y])[x] = b;
		}
	}

return 9999;
}
/*::------------------------------------------------------------------::*/
/*
 * hacked the 11 april 2009 - avenue ST Exupery
 */
int
Image_combine_4img_2(	Image_Desc *s1, Image_Desc *s2,
			Image_Desc *s3, Image_Desc *s4,
			Image_Desc *dst, int meth_reduc)
{
Image_Desc	*r1, *r2, *r3, *r4;
int		x, y;
int		h2, w2;

fprintf(stderr, "*** %s: NO SANITY CONTROL !\n", __func__);

r1 = Image_MakeHalfSize(s1, meth_reduc);
r2 = Image_MakeHalfSize(s2, meth_reduc);
r3 = Image_MakeHalfSize(s3, meth_reduc);
r4 = Image_MakeHalfSize(s4, meth_reduc);

#if DEBUG_LEVEL
fprintf(stderr, "reduced pictures: %p %p %p %p\n", r1, r2, r3, r4);
#endif

h2 = r1->height;
w2 = r1->width;

for (y=0; y<h2; y++)
	{
	for (x=0; x<w2; x++)
		{
		(dst->Rpix[y])[x]	= (r1->Rpix[y])[x];
		(dst->Rpix[y])[x+w2]	= (r2->Rpix[y])[x];
		(dst->Rpix[y+h2])[x]	= (r3->Rpix[y])[x];
		(dst->Rpix[y+h2])[x+w2]	= (r4->Rpix[y])[x];

		(dst->Gpix[y])[x]	= (r1->Gpix[y])[x];
		(dst->Gpix[y])[x+w2]	= (r2->Gpix[y])[x];
		(dst->Gpix[y+h2])[x]	= (r3->Gpix[y])[x];
		(dst->Gpix[y+h2])[x+w2]	= (r4->Gpix[y])[x];

		(dst->Bpix[y])[x]	= (r1->Bpix[y])[x];
		(dst->Bpix[y])[x+w2]	= (r2->Bpix[y])[x];
		(dst->Bpix[y+h2])[x]	= (r3->Bpix[y])[x];
		(dst->Bpix[y+h2])[x+w2]	= (r4->Bpix[y])[x];
		}
	}

Image_DeAllocate(r1);		free(r1);
Image_DeAllocate(r2);		free(r2);
Image_DeAllocate(r3);		free(r3);
Image_DeAllocate(r4);		free(r4);

return 9999;
}
/*::------------------------------------------------------------------::*/
int
Image_combine_4img_3(	Image_Desc *s1, Image_Desc *s2,
			Image_Desc *s3, Image_Desc *s4,
			Image_Desc *dst, int k)
{
#if DEBUG_LEVEL
fprintf(stderr, "%s : %p %p %p %p --> %p\n", __func__,
			s1, s2, s3, s4, dst);
#endif

fprintf(stderr, "%s: NO SANITY CONTROL !\n", __func__);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
