/*
	D'apres un article paru dans 'Pour la Science', et il faudrait
	que je retrouve les references exactes... Et je ,'ai meme pas
	la date de ce numero de la revue :(
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <malloc.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int
Image_photomaton_0(Image_Desc *src, Image_Desc *dst)
{
int		x, y, w, h;
int		r, g, b;

if (src->width & 1)
	{
	w = src->width - 1;
	fprintf(stderr, "photomaton: width is odd, ajust %d\n", w);
	}
else
	w = src->width;

if (src->height & 1)
	{
	h = src->height - 1;
	fprintf(stderr, "photomaton: height is odd, ajust %d\n", h);
	}
else
	h = src->height;


for (y=0; y<src->height/2; y++)
	{
	for (x=0; x<src->width/2; x++)
		{
		Image_getRGB(src, x*2, y*2, &r, &g, &b);
		Image_plotRGB(dst, x, y, r, g, b);

		Image_getRGB(src, (x*2)+1, y*2, &r, &g, &b);
		Image_plotRGB(dst, x+(src->width/2), y, r, g, b);

		Image_getRGB(src, x*2, (y*2)+1, &r, &g, &b);
		Image_plotRGB(dst, x, y+(src->height/2), r, g, b);

		Image_getRGB(src, (x*2)+1, (y*2)+1, &r, &g, &b);
		Image_plotRGB(dst, x+(src->width/2), y+(src->height/2), r,g,b);
		}
	}

dst->modified = 1;

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
int
Image_photomaton_n(Image_Desc *src, Image_Desc *dst, int nb)
{
Image_Desc	*tmp;
int		foo;

if (nb < 1)
	{
	fprintf(stderr, "photomaton_n: %d invalid\n", nb);
	return INVALID_PARAM;
	}

tmp = Image_clone(src, 1);	/* copier aussi les pixels */
if (tmp==NULL)
	{
	fprintf(stderr, "no mem in %s %d %s\n", __FILE__, __LINE__, __func__);
	abort();
	}

for (foo=0; foo<nb; foo++)
	{
	Image_photomaton_0(tmp, dst);
	Image_copy(dst, tmp);
	fprintf(stderr, "photomaton N: passe %d\n", foo);
	}

Image_copy(tmp, dst);
Image_DeAllocate(tmp);	free(tmp);

dst->modified = 1;

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
