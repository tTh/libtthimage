/*
			calcluts.c
			----------

   voir aussi: luts15bits.c & palettes.c

*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#ifdef NEED_ALLOCA_H
#include  <alloca.h> 
#endif

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_calclut_lin(int *lut, int v0, int v255)
{
int foo, quux, delta;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d %d )\n", __func__, lut, v0, v255);
#endif

delta = v255 - v0;
if (delta == 0)	{
	return DIVISOR_IS_0;
	}

#if DEBUG_LEVEL
fprintf(stderr, "    calclut lin: delta = %d\n", delta);
#endif

for (foo=0; foo<256; foo++) {
	quux = (foo * delta / 255) + v0;
#if DEBUG_LEVEL > 1
	printf("lut(%d) = %d\n", foo, quux);
#endif
	lut[foo] = quux;
	}

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
int Image_calclut_foo(int *lut, int v0, int v255)
{
fprintf(stderr, ">>> %s ( %p %d %d )\n", __func__, lut, v0, v255);

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
int Image_print_lut(int *lut, char *fname, int flag)
{
FILE	*fp;
int	foo;

if (strcmp(fname, "-"))		fp = fopen(fname, "w");
else				fp = stdout;

for (foo=0; foo<256; foo++) {
	if (flag) printf("%4d ", foo);
	printf("  %4d\n", lut[foo]);
	}

if (fp!=stdout)			fclose(fp);

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
int Image_load_lut(int *lut, char *fname, int flag)
{
FILE	*fp;
int	value, foo, idx;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p '%s' )\n", __func__, fname, lut);
#endif

if (flag) {
	fprintf(stderr, "in %s, flag must be O, was %d\n", __func__, flag);
	return WRONG_FLAG;
	}

if (NULL==(fp=fopen(fname, "r"))) {
	fprintf(stderr, "%s can't fopen %s\n", __func__, fname);
	return 666;
	}

/* WARNING : very crude code here (4 nov 1999) */
for (idx=0; idx<256; idx++) {
	foo = fscanf(fp, "%d", &value);
	if (1==foo)	lut[idx] = value;
	else		lut[idx] = -1;
	}

fclose(fp);

return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
