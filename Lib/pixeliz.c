/*
 *		pixeliz.c
 *		---------
 *
 *	nouveau 23 Juin 2003, je ne sais pas quoi mettre dedans...
 *	14 mars 2007: je ne sais toujours pas...
 *
 */

#include  <stdio.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_pixeliz_0(Image_Desc *src, Image_Desc *dst, int w, int h)
{
int		x, y, foo, N;
int		r, g, b;
int		xmax, ymax;
Image_Rect	zone;

#if DEBUG_LEVEL
fprintf(stderr, "Pixeliz 0: en chantier    w %d  h %d\n", w, h);
#endif

if ( (foo=Image_compare_desc(src, dst)) ) {
    fprintf(stderr, "%s : images are differents %d\n", __func__, foo);
    return foo;
    }

zone.w = w;
zone.h = h;

xmax = src->width - w;
ymax = src->height - h;

for (x=0; x<=xmax; x+=w) {
	zone.x = x;
	for (y=0; y<=ymax; y+=h) {
		zone.y = y;
/*		Image_dump_rect(&zone, "z", 0); */

		foo = Image_stats_zone_0(src, &zone, &r, &g, &b, &N, &N, &N);
		foo = Image_paint_rect(dst, &zone, r, g, b);
		}
	}

dst->modified = 1;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	fonction d'appel de test
 */
int Image_pixeliz_X(Image_Desc *src, Image_Desc *dst)
{
int foo;

#if DEBUG_LEVEL
fprintf(stderr, "Pixeliz X: appel de test, pas finalise.\n");
#endif

foo = Image_pixeliz_0(src, dst, 5, 16);

#if DEBUG_LEVEL
fprintf(stderr, "Pixeliz X: valeur obtenue: %d\n", foo);
#endif

return foo;
}
/*::------------------------------------------------------------------::*/
/*
 *	fonction d'appel de test
 */
int Image_pixeliz_Y(Image_Desc *src, Image_Desc *dst)
{
int foo;

#if DEBUG_LEVEL > 1
fprintf(stderr, "Pixeliz Y: appel de test, pas finalise.\n");
#endif

foo = Image_pixeliz_0(src, dst, 16, 5);

#if DEBUG_LEVEL
fprintf(stderr, "Pixeliz Y: valeur obtenue: %d\n", foo);
#endif

return foo;
}
/*::------------------------------------------------------------------::*/
