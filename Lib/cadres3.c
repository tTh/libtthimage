/*
			cadres3.c
			=========

	floating points 'cadres' around images.
	work in progress, you have to wait (a long time ?)

*/

#include  <stdio.h>
#include  <math.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	8 Dec 2001: this cadre was first designed for my ExPOVsition
 */
int
Image_cadre_waves_0(Image_Desc *img, int perx, int pery, RGBA *rgb, int amp)
{
int	x, y, val, foo;
double	dx, dy, dw, dh, damp;
double  kx, ky;

if ( amp>=img->width/2 || amp>=img->height/2 )
	{
	fprintf(stderr, "Image cadre waves 0: amp (%d) is too big\n", amp);
	return INVALID_PARAM;
	}

damp = (double)amp;
dw = (double)img->width;
dh = (double)img->height;

kx = (M_PI * 2.0 * (double)perx) / dw;
ky = (M_PI * 2.0 * (double)pery) / dh;

#if DEBUG_LEVEL
printf("Cadre3 waves 0: px %d py %d amp %d\n", perx, pery, amp);
printf("                dw %f kx %f\n", dw, kx);
printf("                dh %f ky %f\n", dh, ky);
#endif

for (x=0; x<img->width; x++)
	{
	dx = ((double)x * kx) - (M_PI/2);	
	val = (int)((sin(dx)*damp)+damp)+1;
	
	for (foo=0; foo<val; foo++)
		{
		Image_plotRGB(img, x, foo, rgb->r, rgb->g, rgb->a);	
		Image_plotRGB(img, img->width-x-1, img->height-foo-1,
						rgb->r, rgb->g, rgb->a);	
		}
	}

for (y=0; y<img->height; y++)
	{
	dy = (double)y * ky - (M_PI/2);
	val = (int)((sin(dy)*damp)+damp);
	
	for (foo=0; foo<val; foo++)
		{
		Image_plotRGB(img, foo, y, rgb->r, rgb->g, rgb->a);	
		Image_plotRGB(img, img->width-foo-1, y,
						rgb->r, rgb->g, rgb->a);	
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 *	celui-ci, il faudra l'inventer un de ces jours....
 */
int
Image_cadre_waves_1(Image_Desc *img, int perx, int pery, RGBA *rgb, int amp)
{
int	x, y, val, foo;

if ( amp>=img->width/2 || amp>=img->height/2 )
	{
	fprintf(stderr, "Image cadre waves 1: amp (%d) is too big\n", amp);
	return INVALID_PARAM;
	}


return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/*
 *	celui-ci, il faudra l'inventer un de ces jours....
 */
int
Image_cadre_waves_2(Image_Desc *img, int perx, int pery, int amp)
{
int	x, y, val, foo;
double	dx, dy, dw, dh, damp;
double  kx, ky;
int	r, g, b;

if ( amp>=img->width/2 || amp>=img->height/2 )
	{
	fprintf(stderr, "Image cadre waves 2: amp (%d) is too big\n", amp);
	return INVALID_PARAM;
	}

damp = (double)amp;
dw = (double)img->width;
dh = (double)img->height;

kx = (M_PI * 2.0 * (double)perx) / dw;
ky = (M_PI * 2.0 * (double)pery) / dh;

for (x=0; x<img->width; x++)
	{
	dx = ((double)x * kx) - (M_PI/2);	
	val = (int)((sin(dx)*damp)+damp)+1;
	for (foo=0; foo<val; foo++)
		{
		
		}
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
