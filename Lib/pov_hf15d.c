/*
                pov_hf15d.c
                ===========
	bruitages de height-fields.
	---------------------------
*/

#include  <stdio.h>
#include  <stdlib.h>

#include  "../tthimage.h"        

/*::------------------------------------------------------------------::*/
/*
 *	le hasard choisi quel pixel on va changer.
 *	le hasard choisi quelle valeur on va lui affecter, et cette
 *	 valeur sera comprise entre 'hi' et 'lo'.
 */
int
Image_hf15_noise_0(Image_Desc *dst, int hi, int lo, int prob100)
{
int	delta;
int	x, y, h;

#if DEBUG_LEVEL
fprintf(stderr, ">>>> %s ( %p %d %d %d )\n", __func__,
				dst, hi, lo, prob100);
#endif

delta = hi - lo;
if (0 == delta)
	{
	fprintf(stderr, "%s : delta is zero, fail.\n", __func__);
	return DIVISOR_IS_ZERO;
	}

for (y=0; y<dst->height; y++)
	{
	for (x=0; x<dst->width; x++)
		{
		if ((rand()%100) < prob100)
			{ 
			/*
			 * en fait je ne suis pas content de cette fonction
			 * de bruit, et je cherche une meilleur solution.
			 */
			h = (rand()%delta) + lo;
			Image_hf15_plot(dst, x, y, h);
			}
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 * new 21 avr 2009: the probability use is (mis)used.
 */
int
Image_hf15_noise_1(Image_Desc *dst, int mo, int of, int prob100)
{
int	x, y, h, vr;
#if DEBUG_LEVEL
long	hit, mess;
float	ratio;
#endif

#if DEBUG_LEVEL 
fprintf(stderr, "Hf15 noise 1 is very experimental.\n");
hit = mess = 0L;
#endif

for (y=0; y<dst->height; y++)
	{
	for (x=0; x<dst->width; x++)
		{
		if (prob100 > rand()%100)
			{
#if DEBUG_LEVEL
			hit++;
#endif
			h = Image_hf15_height(dst, x, y);
			vr = (rand()%mo) + of;
			h += vr;
			Image_hf15_plot(dst, x, y, h);
			}
#if DEBUG_LEVEL
		else
			{
			mess++;
			}
#endif
		}
	}

#if DEBUG_LEVEL
if (mess > 0)	ratio = (float)hit / (float)mess;
else		ratio = -42.42;
fprintf(stderr, "%s: hit %ld mess %ld\n", __func__, hit, mess);
#endif

return FUNC_IS_BETA;
	/*
	 * penser � faire des tests de cette fonction :)
	 */
}
/*::------------------------------------------------------------------::*/
/*
 * remplacement al�atoire d'un point par un de ses proches voisins
 */
int
Image_hf15_noise_2(Image_Desc *dst, int pr100, int dist, int k1, int k2)
{
Image_Desc	*clone;
int		x, y, h;
int		xs, ys;
int		hit, mess;

#if DEBUG_LEVEL
fprintf(stderr, "Hf15 noise 2 is _very_ experimental\n");
#endif

/*
 * faire une copie de l'image, qui servira de source.
 */
clone = Image_clone(dst, 1);

hit = mess = 0;

for (y=0; y<dst->height; y++)
	{
	for (x=0; x<dst->width; x++)
		{
		if ( pr100 > (rand()%100) )
			{
			xs = x + (rand() % dist);
			ys = y + (rand() % dist);
			if (Image_xy_inside(clone, xs, ys))
				{
				h = Image_hf15_height(clone, xs, ys);
				Image_hf15_plot(dst, x, y, h);
				hit++;
				}
			}
		else
			{
			mess++;
			}
		}
	}
/*
 * d�truire la copie de l'image.
 */
Image_DeAllocate(clone);	free(clone);

/* afficher les stats a la con */
fprintf(stderr, "%s : hit=%d mess=%d\n", __func__, hit, mess);

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
/*
 * deplacement aleatoire d'un point proportionnellement a sa hauteur.
 * new 2 juillet 2009 - pour la colline de Sonia dans le Parking Maudit. 
 */
int
Image_hf15_noise_3(Image_Desc *dst, double coef, int flag)
{
int		x, y, h;
double		dh, dh2, dr, maxdh;

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %p %f 0x%X )\n", __func__, dst, coef, flag);
#endif

if (0 != flag)
	{
	fprintf(stderr, "%s : flag is 0x%d and _must_ be 0\n",
							__func__, flag);
	}

maxdh = 0;
for (y=0; y<dst->height; y++)
	{
	for (x=0; x<dst->width; x++)
		{
		dh = (double)Image_hf15_height(dst, x, y);
		dr = ((double)rand() / (double)RAND_MAX) - 0.50000000;

		dh2 = dh + (dr * coef * (dh / 32768.0));
		if (dh2 > maxdh)
			{
			maxdh = dh2;
#if DEBUG_LEVEL > 1
			printf("dh is %f, dr is %f, max is %f\n",
						dh, dr, maxdh);
#endif
			}

		/* is the clipping mandatory ? */
		h = (int)dh2;
		if (h < 0)		h = 0;
		if (h > 32767)		h = 32767;

		Image_hf15_plot(dst, x, y, h);
		}
	}

#if DEBUG_LEVEL
printf("%s : maxdh = %f\n", __func__, maxdh);
#endif

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
