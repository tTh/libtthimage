/*
                    combine5.c
		    ----------
	d�grad�s lin�aires divers.
	avec des erreurs d'arrondi dedans.
*/

#include   <stdio.h>
#include   <stdlib.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
int
Image_combine_Vdegrade(Image_Desc *s1, Image_Desc *s2,
						Image_Desc *dst, int sens)
{
int	x, y, foo;
int	r1, g1, b1;
int	r2, g2, b2;
int	rd, gd, bd;
double	k, ka, kb;

if ( (foo=Image_compare_desc(s1, s2)) ) {
	fprintf(stderr, "%s: sources err: %s\n", __func__, Image_err2str(foo));
	return foo;
	}

for (y=0; y<s1->height; y++) {
	k = (double)y/(double)s1->height;
	if (sens)	{ ka = k;	kb = 1.0 - ka; }
	else		{ kb = k;	ka = 1.0 - kb; }

	for (x=0; x<s1->width; x++) {
		Image_getRGB(s1, x, y, &r1, &g1, &b1);
		Image_getRGB(s2, x, y, &r2, &g2, &b2);
		rd = (int)((double)r1*ka+(double)r2*(kb));
		gd = (int)((double)g1*ka+(double)g2*(kb));
		bd = (int)((double)b1*ka+(double)b2*(kb));
		Image_plotRGB(dst, x, y, rd, gd, bd);
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
int
Image_combine_Hdegrade(	Image_Desc *s1, Image_Desc *s2,
						Image_Desc *dst, int sens)
{
int	x, y, foo;
int	r1, g1, b1;
int	r2, g2, b2;
int	rd, gd, bd;
double	k, ka, kb;

if ( (foo=Image_compare_desc(s1, s2)) ) {
	fprintf(stderr, "%s: sources err: %s\n", __func__, Image_err2str(foo));
	return foo;
	}

for (x=0; x<s1->width; x++) {
	k = (double)x/(double)s1->width;
	if (sens)	{ ka = k;	kb = 1.0-ka; }
	else		{ kb = k;	ka = 1.0-kb; }
	for (y=0; y<s1->height; y++) {
		Image_getRGB(s1, x, y, &r1, &g1, &b1);
		Image_getRGB(s2, x, y, &r2, &g2, &b2);
		rd = (int)((double)r1*ka+(double)r2*(kb));
		gd = (int)((double)g1*ka+(double)g2*(kb));
		bd = (int)((double)b1*ka+(double)b2*(kb));
		Image_plotRGB(dst, x, y, rd, gd, bd);
		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/*
 *	bon, l�, on va essayer de faire �a en diagonale, mais
 *	je pense que �a ne va pas �tre simple, car il faut traiter
 *	le cas des images qui ne sont pas carr�es...
 */
int
Image_combine_Ddegrade(	Image_Desc *s1, Image_Desc *s2,
						Image_Desc *dst, int sens)
{
int		foo;

fprintf(stderr, "%s:%d:%s: no such function or procedure\n",
		__FILE__, __LINE__, __func__);

if ( (foo=Image_compare_desc(s1, s2)) ) {
	fprintf(stderr, "%s: sources err: %s\n", __func__, Image_err2str(foo));
	return foo;
	}
if ( (foo=Image_compare_desc(s1, dst)) ) {
	fprintf(stderr, "%s: dst err: %s\n", __func__, Image_err2str(foo));
	return foo;
	}

fprintf(stderr, "%s : sens=%d\n", __func__, sens);	/* XXX */

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
/* nouveau 29 novembre 2013 
*/
int Image_combine_Vsplit(Image_Desc *s1, Image_Desc *s2, Image_Desc *d,
					int xv, int uh)
{
int		foo, x, y;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p %d %d )\n", __func__, s1, s2, d, xv, uh);
#endif

if (0 != uh)
	fprintf(stderr, "%s : uh must be 0 and not %d\n", __func__, uh);

if ( (foo=Image_compare_desc(s1, s2)) ) {
	fprintf(stderr, "%s : src err: %s\n", __func__, Image_err2str(foo));
	return foo;
	}

for (y=0; y<s1->height; y++) {
	for (x=0; x<s1->width; x++) {
		if (x<xv) {
			(d->Rpix[y])[x] = (s1->Rpix[y])[x];
			(d->Gpix[y])[x] = (s1->Gpix[y])[x];
			(d->Bpix[y])[x] = (s1->Bpix[y])[x];
			}
		else {
			(d->Rpix[y])[x] = (s2->Rpix[y])[x];
			(d->Gpix[y])[x] = (s2->Gpix[y])[x];
			(d->Bpix[y])[x] = (s2->Bpix[y])[x];
			}

		}
	}

return OLL_KORRECT;
}
/*::------------------------------------------------------------------::*/
/* nouveau 29 novembre 2013 
*/
int Image_combine_Hsplit(Image_Desc *s1, Image_Desc *s2, Image_Desc *d,
					int yv, int uh)
{
int		x, y;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p %d %d )\n", __func__, s1, s2, d, yv, uh);
#endif

if (0 != uh)
	fprintf(stderr, "*** %s : uh must be 0 and not %d\n", __func__, uh);

for (y=0; y<s1->height; y++) {
	for (x=0; x<s1->width; x++) {
		if (y<yv)
			{
			(d->Rpix[y])[x] = (s1->Rpix[y])[x];
			(d->Gpix[y])[x] = (s1->Gpix[y])[x];
			(d->Bpix[y])[x] = (s1->Bpix[y])[x];
			}
		else
			{
			(d->Rpix[y])[x] = (s2->Rpix[y])[x];
			(d->Gpix[y])[x] = (s2->Gpix[y])[x];
			(d->Bpix[y])[x] = (s2->Bpix[y])[x];
			}
		}
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
