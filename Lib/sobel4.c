/*
 * 	Sobel4			new 26 avril 2007 - Fonsegrives
 *	======			===============================
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdlib.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_filtre_Sobel_4(Image_Desc *src, Image_Desc *dst, int rotation)
{
static	int	Sobel[] =
	{
	1,	2,	1,
	0,	0,	0,
	-1,	-2,	-1,
	1,	128
	};
int	filtre[11], foo;
Image_Desc	*tmp[4];
int		x, y, r, g, b;

fprintf(stderr, ">>> %s ( %p %p  %d )\n", __func__, src, dst,rotation);

memcpy(filtre, Sobel, 11*sizeof(int));

for (foo=0; foo<4; foo++) {
	fprintf(stderr, "sobel4: pass %d\n", foo);
	if (NULL == (tmp[foo] = Image_clone(src, 0))) {
		fprintf(stderr,"Sobel 4: no memory, aborting\n");
		abort();
		}
	Image_convolueur_2(src, tmp[foo], filtre, "rgb");
	Image_rotate_filtre(filtre); 
	Image_rotate_filtre(filtre);
	}

for (x=0; x<src->width; x++) {
	for (y=0; y<src->height; y++) {
		r = ( tmp[0]->Rpix[y][x] + tmp[1]->Rpix[y][x] +
		      tmp[2]->Rpix[y][x] + tmp[3]->Rpix[y][x]) / 4;
		g = ( tmp[0]->Gpix[y][x] + tmp[1]->Gpix[y][x] +
		      tmp[2]->Gpix[y][x] + tmp[3]->Gpix[y][x]) / 4;
		b = ( tmp[0]->Bpix[y][x] + tmp[1]->Bpix[y][x] +
		      tmp[2]->Bpix[y][x] + tmp[3]->Bpix[y][x]) / 4;

		dst->Rpix[y][x] = r;
		dst->Gpix[y][x] = g;
		dst->Bpix[y][x] = b;
		}
	}
dst->modified   = 1;

for (foo=0; foo<4; foo++) {
#if DEBUG_LEVEL > 1
	fprintf(stderr, "%s: freeing %d\n", __func__, foo);
#endif
	Image_DeAllocate(tmp[foo]);	free(tmp[foo]);
	}

return FUNC_NOT_FINISH;
}
/*::------------------------------------------------------------------::*/
