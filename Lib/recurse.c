/*
		RECURSION 'QUADTREE' SUR LES IMAGES
		-----------------------------------
*/

#include   <stdio.h>
#include   <stdlib.h>
#include   <math.h>

#include   "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*
 *	bon, c'est qu'un essai, mais il me semble prometteur.
 *	ceci dit, � l'epoque du Copam, il me semble que j'avais
 *	fait le meme genre de truc, mais en bien mieux...
 *
 */

/*	Private variables */
static	Image_Desc	*S, *D;
static	int		seuil;
static  int		level, maxlevel;

static int recursion(Image_Rect *pRect)
{
Image_Rect 	rect;
int		h1, h2, w1, w2, xx, yy, foo;
int		mr, mg, mb, dr, dg, db, s;

level++;		/* pourquoi ? */

if (level > maxlevel)
	maxlevel = level;

#if DEBUG_LEVEL > 1
fprintf(stderr, "%5d  ->  %3d %3d    %3d %3d\n",
			level, pRect->x, pRect->y, pRect->w, pRect->h);
#endif 

foo = Image_stats_zone_0(S, pRect, &mr, &mg, &mb, &dr, &dg, &db);
if (foo) {
	fprintf(stderr, "%s: err stat zone %d\n", __func__, foo);
	exit(1);
	}

s = (dr + dg + db) / 3;

#if 1
printf(" %7d   V %3d %3d %3d    D %3d %3d %3d    S  %3d\n",
			 pRect->w*pRect->h, mr, mg, mb, dr, dg, db, s);
#endif

if ( (s < seuil) || (pRect->w < 6) || (pRect->h < 6) ) {
	Image_paint_rect(D, pRect, mr, mg, mb);
	/* printf("    paint %d %d %d\n", mr, mg, mb); */
	}
else	{
	Image_paint_rect(D, pRect, 255, 0, 0); 
	/* Image_dump_rect(pRect, "R", 0); */

	if ( pRect->h & 1 )	/* impair */
		{
		h1 = (pRect->h / 2) + 1;
		h2 = (pRect->h / 2);
		yy = (pRect->h / 2) + 1;
		}
	else			/* pair */
		{
		h1 = (pRect->h / 2);
		h2 = (pRect->h / 2);
		yy = (pRect->h / 2);
		}

	if ( pRect->w & 1 )	/* impair */
		{
		w1 = (pRect->w / 2) + 1;
		w2 = (pRect->w / 2);
		xx = (pRect->w / 2) + 1;
		}
	else			/* pair */
		{
		w1 = (pRect->w / 2);
		w2 = (pRect->w / 2);
		xx = (pRect->w / 2);
		}

	/*
	printf("        w1 %3d w2 %3d    \n", w1, w2);
	printf("        h1 %3d h2 %3d    \n", h1, h2);
	printf("        xx %3d yy %3d    \n", xx, yy);
	printf("\n");
	*/

	rect.x = pRect->x;		rect.y = pRect->y;
	rect.w = w1;			rect.h = h1;

	recursion(&rect);

	rect.x = pRect->x + xx;		rect.y = pRect->y;
	rect.w = w2;			rect.h = h1;

	recursion(&rect);

	rect.x = pRect->x;		rect.y = pRect->y + yy;
	rect.w = w1;			rect.h = h2;

	recursion(&rect);

	rect.x = pRect->x + xx;		rect.y = pRect->y + yy;
	rect.w = w2;			rect.h = h2;

	recursion(&rect);
	}

level--;

return 0;
}
/*::------------------------------------------------------------------::*/
/*
 *	a quoi peut bien servir le parametre ?
 */
int Image_call_recursion_0(Image_Desc *image, Image_Desc *dest, int param)
{
Image_Rect 	rect;
int		foo;

#if 1
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__, image, dest, param);
#endif

rect.x = rect.y = 0;
rect.h = image->height;
rect.w = image->width;

#if DEBUG_LEVEL
fprintf(stderr, "->  demarrage de la recursion\n");
#endif

S = image;	D = dest;
seuil = param;
level = maxlevel = 0;

foo = recursion(&rect);
#if DEBUG_LEVEL
fprintf(stderr, "->  fin recursion: %d, maxlevel=%d\n", foo, maxlevel);
#endif

dest->modified = 1;

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
