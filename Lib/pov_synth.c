/*
                pov_hf_synth.c
                ==============
		Thierry Boudet  
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <math.h>

#include  "../tthimage.h"        

/*::------------------------------------------------------------------::*/
/*   new 6 Jan 2002, pas fini le 6 Jan 2003.  */
int
Image_hf15_synth_0(Image_Desc *dst, Image_PtList *ptl)
{
int	foo, nb, idx;
int	x, y, dist, distmin;
int	ptx, pty, xx, yy;

fprintf(stderr, "%s:%d (%s) img %p ptl %p\n", __FILE__, __LINE__, __func__,
					dst, ptl);

if (ptl->control != 0xfde9601a)
	{
	fprintf(stderr, "%p is not a ptlist\n", ptl);
	return -1;
	}

nb = ptl->nbre;
printf("%s : il y a %d points dans la liste %p\n", __func__, nb, ptl);

for (y=0; y<dst->height; y++)
	{
	for (x=0; x<dst->width; x++)
		{
		idx = -9999;	
		distmin = 99999999;
		for (foo=0; foo<nb; foo++)
			{
			ptx = ptl->points[foo].x;
			pty = ptl->points[foo].y;
			xx = x - ptx;
			yy = y - pty;
			dist = (xx*xx) + (yy*yy);
			if (dist < distmin)
				{
				distmin = dist;
				idx = foo;
				}
			}
		/* got a value */
		Image_hf15_plot(dst, x, y, ptl->points[idx].h);
		}
	}

return FULL_NUCKED;
}
/*
 *	pourtant, il me semble qu'il existe un code Fortran qui 
 *	commence � bien fonctionner ?
 */
/*::------------------------------------------------------------------::*/
/*
 * remise en chantier de ce truc le 31 janvier 2008 -
 */
static int plot_synth_1(Image_Desc *img, int x, int y, double dh)
{
int	ih;

ih = (int)(dh);
#if DEBUG_LEVEL > 1
printf("   (%d,%d) -> %g  ->  %d\n", x, y, dh, ih);
#endif
Image_hf15_plot(img, x, y, ih);

#define COEF 0.777
ih = (int)(dh*COEF);
Image_hf15_plot(img, x-1, y  , ih);
Image_hf15_plot(img, x+1, y  , ih);
Image_hf15_plot(img, x  , y-1, ih);
Image_hf15_plot(img, x  , y+1, ih);

ih = (int)(dh*COEF);
Image_hf15_plot(img, x-1, y-1, ih);
Image_hf15_plot(img, x-1, y+1, ih);
Image_hf15_plot(img, x+1, y-1, ih);
Image_hf15_plot(img, x+1, y+1, ih);

ih = (int)(dh*COEF);
Image_hf15_plot(img, x-2, y  , ih);
Image_hf15_plot(img, x+2, y  , ih);
Image_hf15_plot(img, x  , y-2, ih);
Image_hf15_plot(img, x  , y+2, ih);

return 1;
}
/*	++++++++++++++++++++++++++++++++++++	*/
int
Image_hf15_synth_1(Image_Desc *dst, Image_PtList *ptl)
{
int		idx, nb;
double		dbl_h;

printf("**** %s:%s: %p %p\n", __FILE__, __func__, dst, ptl);

if (ptl->control != 0xfde9601a)
	{
	fprintf(stderr, "%s: %p is not a ptlist\n", __func__, ptl);
	return -1;
	}

nb = ptl->nbre;
printf("%s : il y a %d points dans la liste %p\n", __func__, nb, ptl);

for (idx=0; idx<nb; idx++)
	{
#if DEBUG_LEVEL > 1
	printf("point #%d\n", idx);
#endif
	dbl_h = (double)ptl->points[idx].h;
	plot_synth_1(dst, ptl->points[idx].x, ptl->points[idx].y, dbl_h);
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/* new 11 decembre 2007 - ave St Exupery */
int
Image_hf15_synth_fromfunc0(Image_Desc *dst, int param,
				double(*func)(int x, int y, int k))
{
int		x, y;
double		dx, dy, val;
double		minh, maxh, delta;
long		surface;
double		*array, *ptr;
double		a, b;

printf("%s: %p  %d\n", __func__, dst, param);
surface = (long)dst->height * (long)dst->width;
printf("   surface image: %ld pixels\n", surface);
array = malloc(sizeof(double)*surface);
if  (NULL == array)
	{
	fprintf(stderr, "no mem at %s:%s:%d\n", __FILE__, __func__, __LINE__);
	abort();
	}
minh =  9e99;
maxh = -9e99;
ptr  = array;
for (x=0; x<dst->width; x++)
	{
	dx = (double)x;
	for (y=0; y<dst->height; y++)
		{
		dy = (double)y;
		val = func(x, y, 51);
		*ptr++ = val;
		if (val < minh)		minh = val;
		if (val > maxh)		maxh = val;
		}
	}
delta = maxh - minh;
printf("extremums:     %.12g     %.12g\n", minh, maxh);
printf("delta:         %.12g\n", delta);

a = 32766.9999 / delta;
b = - ( a * minh );
printf("  a  &  b:     %.12g     %.12g\n", a, b);

minh =  9e99;
maxh = -9e99;
ptr  = array;
for (x=0; x<dst->width; x++)
	{
	for (y=0; y<dst->height; y++)
		{
		val = *ptr++;
		val = (a * val) + b;
		if (val < minh)		minh = val;
		if (val > maxh)		maxh = val;
		Image_hf15_plot(dst, x, y, ceil(val));
		}
	}
printf("altitudes:     %.12g     %.12g\n", minh, maxh);

free(array);

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
