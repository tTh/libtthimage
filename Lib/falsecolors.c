/*
 *		FALSECOLORS.C
 *
 *	new: Wed Oct  4 07:32:36 UTC 2023
 *
 *	see also	calcluts.c
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <stdint.h>
#include  <string.h>
#include  <math.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
int Image_gen_fc_lut(float maxval, int nbslots, RGB_map *plut)
{
int		idx, ilut;
float		fval;

fprintf(stderr, ">>> %s ( %f %d %p )\n", __func__, maxval, nbslots, plut);

/* RAZ de les luts */
memset(plut, 0, sizeof(RGB_map));

for (idx=0; idx<nbslots; idx++) {
	fval = (float)idx / (float)nbslots;
	ilut = (int)(fval*256.0);
	fprintf(stderr, "    %3d   %8.6f   %3d\n", idx, fval, ilut);
	}

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
