/*
 * afficheurs a 7 segments - 8 nov 2008 - avenue St Exupery
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <math.h>
#include  <ctype.h>
#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_7seg_m0v(Image_Desc *img, int xpos, int ypos, int bits, int k)
{
Image_Rect	r;
int		foo, numbit, mask;
int		failed;

#if DEBUG_LEVEL 
fprintf(stderr, ">>> %s ( %p, <%d,%d>, 0x%x, %d )\n", __func__,
				img, xpos, ypos, bits, k);
#endif

/* ==> check the parameters */

/* ==> tag the plotting zone */
r.x = xpos;		r.y = ypos;
r.h = 19;		r.w = 43;
foo = Image_paint_rect(img, &r, 25, 21, 42);
#if DEBUG_LEVEL > 1
fprintf(stderr, "   %s : paint rect -> %d\n", __func__, foo);
#endif

foo = Image_draw_rect(img, &r, 125, 101, 232);
#if DEBUG_LEVEL > 1
fprintf(stderr, "   %s : draw rect -> %d\n", __func__, foo);
#endif

failed = 0;

/* draw the lighten segments */
for (numbit=0; numbit<8; numbit++)
	{
	mask = 1 << numbit;
#if DEBUG_LEVEL > 1
	fprintf(stderr, "%s  numbit %2d  mask %02x\n", __func__, numbit, mask);
#endif
	if ( ! (mask & bits) )
		{
#if DEBUG_LEVEL > 1
		fprintf(stderr, "%d skip %d\n", numbit, mask);
#endif
		continue;
		}

	r.x = -1;		/* mark as 'no plot that'*/
	switch (mask)
		{
		case 0x01:
			r.x = xpos+6;		r.y = ypos+2;
			r.h = 15;		r.w = 4;
			break;
		case 0x02:
			r.x = xpos+9;		r.y = ypos + 14;
			r.h = 4;		r.w = 13;
			break;
		case 0x04:
			r.x = xpos+9;		r.y = ypos +1;
			r.h = 4;		r.w = 13;
			break;
		case 0x08:
			r.x = xpos+22;		r.y = ypos+2;
			r.h = 15;		r.w = 4;
			break;
		case 0x10:
			r.x = xpos+25;		r.y = ypos+14;
			r.h = 4;		r.w = 13;
			break;
		case 0x20:
			r.x = xpos+25;		r.y = ypos+1;
			r.h = 4;		r.w = 13;
			break;
		case 0x40:
			r.x = xpos+38;		r.y = ypos+2;
			r.h = 15;		r.w = 4;
			break;
		case 0x80:
			/* le point */
			r.x = xpos+1;		r.y = ypos+1;
			r.h = 6;		r.w = 4;
			break;

		default:
			fprintf(stderr, "segfaulting in %s\n", __func__);
			abort();
			break;			/* ah ah ah ah */
		}
	if (r.x >= 0)
		{
#if DEBUG_LEVEL > 1
		fprintf(stderr, "  painting...\n");
#endif
		Image_paint_rect(img, &r, 200, mask, 50);
		}
	else
		{
		failed++;
		}
	}

if (failed > 0)
	{
	fprintf(stderr, "%s : failed = %d\n", __func__, failed);
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int Image_7seg_digv(Image_Desc *img, int xpos, int ypos, char chiffre, int k)
{
static	int patterns[] = 
	{ 0x77, 0x24, 0x5d, 0x6d, 0x2e, 0x6b, 0xfb, 0x25, 0x7f, 0x6f };
	/*   0     1     2     3     4     5     6     7     8     9 */
int	foo, idx, bits;

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s: digit 0x%2x  sizeof(patterns) -> %d\n", __func__,
			chiffre,	sizeof(patterns));
#endif

if (0 != k)
	fprintf(stderr, "in %s, k was %d. It's bad.\n", __func__, k);

if ( ! isdigit(chiffre) )
	{
	fprintf(stderr, "*** in %s, value 0x%x is not a digit\n", __func__,
					chiffre);
	abort();
	}
idx = chiffre - '0';
bits = patterns[idx];

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s  patterns[%d] -> 0x%x\n", __func__, idx, bits);
#endif

foo = Image_7seg_m0v(img, xpos, ypos, bits, 0);
#if DEBUG_LEVEL > 1
fprintf(stderr, "foooooo ----> %d\n", foo);
#endif

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
int Image_7seg_tag0(Image_Desc *img, char *numstr, int k)
{
int		len, idx;
int		foo;
char		digit;

#if DEBUG_LEVEL > 1
fprintf(stderr, ">>> %s ( %p, '%s', %d )\n", __func__, img, numstr, k);
#endif

len = strlen(numstr);
#if DEBUG_LEVEL > 1
fprintf(stderr, "    len = %d\n", len);
#endif

for (idx=0; idx<len; idx++)
	{
	foo = len-idx-1;
	digit = numstr[foo];
#if DEBUG_LEVEL > 1
	fprintf(stderr, "  %s  %d->%d  '%c'\n", __func__, idx, foo, digit);
#endif
	foo = Image_7seg_digv(img, 10, 10+(idx*21), digit, k);
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/
/*
 * this is purely a test/demo routine - don't use in production !
 */
int Image_7_segments(Image_Desc *img, int nbre, int k)
{
int	foo, bar;
int	iter;

#if DEBUG_LEVEL 
fprintf(stderr, ">>> %s ( %p, %d, 0x%x )\n", __func__, img, nbre, k);
#endif

iter = 0;		/* safe value */

for (iter=0; iter<nbre; iter++) 
	{
	printf("----%s   passe %d -----\n", __func__, iter);
	bar = 1 << iter;
	foo = Image_7seg_m0v(img, 20, 5+(iter*22), bar, 0);
#if DEBUG_LEVEL > 1
	fprintf(stderr, "7seg_m0v %d 0x%02x -> %d\n", iter, bar, foo);
#endif

	bar = '0' + iter;
	foo = Image_7seg_digv(img, 80, 5+(iter*22), bar, 0);
#if DEBUG_LEVEL > 1
	fprintf(stderr, "7seg_digv %d 0x%02x -> %d\n", iter, bar, foo);
#endif
	}

return FUNC_IS_BETA;
}
/*::------------------------------------------------------------------::*/

