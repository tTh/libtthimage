/*
 * 	luts15bits.c - new 1 mars 2008 - ave St Exupery
 *
 * 	voir aussi "calcluts.c"
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#ifdef NEED_ALLOCA_H
#include  <alloca.h>
#endif

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int Image_lut15_fill_0(int r[32768], int *g, int *b)
{
int	foo;

for (foo=0; foo<32768; foo++)
	{
	r[foo] = foo;
	g[foo] = 0;
	b[foo] = 32768 - foo;
	}
return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
int Image_lut15_fill_1(int *r, int *g, int *b)
{
int	foo;

for (foo=0; foo<32768; foo++)
	{
	r[foo] = foo;
	g[foo] = 12345;
	b[foo] = 32768 - foo;
	}
return FUNC_IS_ALPHA;
}
/*::------------------------------------------------------------------::*/
/*::------------------------------------------------------------------::*/
int Image_lut15_essai(char *hfname, char *tganame, int ka)
{
Image_Desc	*src, *dst;
int		x, y, h;
int		r, g, b;
int		lr[32768];
int		lg[32768];
int		lb[32768];

#if DEBUG_LEVEL
fprintf(stderr, "'%s' : premier essai pour les LUTs en 15 bits\n", __func__);
fprintf(stderr, "   image source   : %s\n", hfname);
fprintf(stderr, "   image resultat : %s\n", tganame);
fprintf(stderr, "   dummy argument : %d\n", ka);
#endif

if (ka) {
	fprintf(stderr, "%s : ka (%d) not zero !\n", __func__, ka);
	return WRONG_FLAG;
	}

if ( NULL==(src=Image_TGA_alloc_load(hfname)) )
	{
	fprintf(stderr, "%s: err load %s, aborting\n", __func__, hfname);
	abort();
	}

/* convert the source image to an valid height-field */
Image_hf15_rgb2hf(src, src, 0);

/* save a 16 bits pgm for visual check */
Image_hf15_save_PGM("aaaa-hf15-lut15.pgm", src, __FILE__);

if ( NULL==(dst=Image_clone(src, 0)) )
	{
	abort();
	}

for (y=0; y<src->height; y++)
	{
	for (x=0; x<src->width; x++)
		{
		h = Image_hf15_height(src, x, y);
		r = lr[h] / 256;
		g = lg[h] / 255;
		b = lb[h] / 255;
		Image_plotRGB(dst, x, y, r, g, b);		
		}
	}

Image_TGA_save(tganame, dst, 0);

return FULL_NUCKED;
}
/*::------------------------------------------------------------------::*/


