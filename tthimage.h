/*
          	  tthimage.h
	  	  ----------
	http://la.buvette.org/devel/libimage/
*/
#ifndef IMAGE_VERSION_STRING
	#define IMAGE_VERSION_STRING "0.4.51 pl 91"

/*::------------------------------------------------------------------::*/
/*
 *      ces trois 'typedef' demandent des verifications
 *      message poste a Linux-31 le 16 Octobre 1998
 *
 *	les reponses recues semblent indiquer que c'est
 *	correct. verifications quand j'aurais une HP Pa-Risc
 *	ou un Alpha, ou mieux, une Playstation 2.
 *
 *	Octobre 2001: on dirait que ca marche sur Sparc/Solaris.
 * 	Octobre 2009: on dirait que ca marche sur Sparc64/OpenBSD.
 *
 *	Fevrier 2014: pourquoi ne pas utiliser <stdint.h> ?
 */
#include  <stdint.h>

/*
 *      les differents types d'images que on gere, nous ici
 */
#define IMAGE_NONE      0
#define IMAGE_GRAY      1
#define IMAGE_GREY      1		/* common typo */
#define IMAGE_RGB       3
#define IMAGE_RGBA      4
#define IMAGE_ALPHA	6		/* only the alpha channel */
#define IMAGE_PAL       10
#define IMAGE_PORTNAWAK 27212		/* you bastard */

/*
 *	sometime, specially in the Fortran interface, we need a 
 *	buffer for filename storage. Here is the max size for this
 *	buffer: a really magic number :)
 */
#define IMG_FILENAME_LEN	4242

/*
 *	some objects (structs) in the library have a name field
 *	and/or a comment field.
 *	here is the size of this field.
 */
#define IMG_OBJNAME_LEN		80
#define IMG_OBJCOMMENT_LEN	256

/*
 *	we use environment variables
 */

#define ENV_DEFAULT_RGBA  "TGA_DEFAULT_RGBA"
#define ENV_LIBIMAGE_PATH "LIBIMAGE_PATH"

/*
 *      descripteur d'une image en memoire
 *	----------------------------------
 */
#define MAGIC_OF_IMAGE		0xdeadbeef

typedef struct {
        unsigned long   magic;
        char            name[IMG_OBJNAME_LEN+2];
        int             width, height;
        int             type;               /* voir #define plus haut	*/
        int             nb_planes;
        uint8_t         **Rpix, **Gpix, **Bpix, **Apix;
	int		idx_palette;
        uint8_t         palette[3][256];
	int		nbcols;
        char            comment[IMG_OBJCOMMENT_LEN+2];
	int		errmsg;			/* not used */
	int		modified;
	double		fx, fy, fw, fh;		/* nouveau 18 Sept 2001	*/
	int		rt, gt, bt, at;		/* color of turtle */
	double		xt, yt;			/* position of turtle */
	double		ht;			/* angle of turtle (heading) */
	
	int		reserve[8];
        } Image_Desc;

/*=== macros for accessing the image descriptor ===*/
#define IMG_WIDTH(i)	((i)->width)
#define IMG_HEIGHT(i)	((i)->height)
#define IMG_TYPE(i)     ((i)->type)

/*
 *      this structure serve as a sub-image descriptor
 */
typedef struct  {
        int             x, y;
        int             w, h;
        long            reserved;
        } Image_Rect;

/*
 * new 11 Jan 2003: a struct like an Image_Rect, but with 
 *	double precision floating point coords.
 */
typedef struct  {
        double          x, y;
        double          w, h;
        int             kdr;			/* what is it ? */
        long            reserved;
        } Image_dblRect;

/*							new 11 Nov 1999
 *	a RGBA quadruplet
 */
typedef struct	{
	short		r, g, b, a;
	int		reserved;
	int		reserved2;
	} RGBA;

/*
 *	memory descriptor for bits-planes 
 *	27 Dec 2001: c,a devient quoi, cette histoire de bitmaps ?
 *	17 mars 2010: du code commence a venir...
 *	12 février 2024, on a toujours rien vu.
 */
typedef struct	{
	unsigned int	magic;
	int		width, height;
	unsigned int	id;
	uint8_t		r0, g0, b0, r1, g1, b1;
        char            name[IMG_OBJNAME_LEN+2];
	unsigned char	tagname;
	unsigned char	*plane;
	int		reserved;
	} A_BitPlane;

#define MAGIC_BIT_PLANE 0xb142ae17

/*
 *      this structure describe a color MAP 'a la fractint'
 *          see 'load' and 'save' code in palettes.c
 */
typedef struct {
    short           nbre;
    char            name[IMG_OBJNAME_LEN+2];
    short           red[256];
    short           green[256];
    short           blue[256];
    int		    reserved;
    } RGB_map;

/*
 *	new: 2001 october. ymmv.
 *	Context for 3D plotting
 */
typedef struct {
	unsigned long	magic;
	Image_Desc	*img;
	int		valid;
        char            name[IMG_OBJNAME_LEN+2];
	int		reserved;
	} Context_3D;

/*
 *	27 Dec 2001: a point in 2D space with some attributes.
 *	will be used in pov hf functions...
 */
typedef struct
	{
	short		x, y;			/* pixel position */
	short		h;			/* pixel value */
	short		c;			/* color index ? */
	} Image_Point;

typedef struct
	{
	unsigned long	control;		/* another DeadBeef ? */
	char		name[IMG_OBJNAME_LEN+2];
	int		width, height;		/* dims of reference image ? */
	int		nbre;			/* real number of points */
	int		alloc;			/* sizeof array */
	int		reserved[4];
	Image_Point	*points;
	} Image_PtList;

/*
 *  Nouveau 18 fevrier 2008 
 */
#define MAGIC_OF_DF3	0x0df30df3
typedef struct
	{
	unsigned long	control;
	char		name[IMG_OBJNAME_LEN+2];
	int		xdim, ydim, zdim;
	int		nbrbytes;		/* 1 2 or 4 */
	int		flags;
	double		scale;
	double		*datas;
	char		blabla[11];
	} DensityF3Head;

/*
 *	---------------------------------------
 */
#ifndef M_PI
#define	M_PI		3.14159265358979323846	/* pi */
#endif

/*
 *	---------------------------------------
 *      now, we #define some useful error codes
 */

/* BEGIN error_code */
#define     OLL_KORRECT		0
#define     DIFFERENT_WIDTH     2000
#define     DIFFERENT_HEIGHT    2001
#define     NOT_AN_IMAGE_DESC   2002
#define     IMAGE_NO_MEM        2003
#define     IMAGE_TOO_SMALL     2004
#define     UNKNOW_ERROR        2005
#define     STRING_ERROR        2006
#define     IMAGE_BAD_TYPE      2007
#define     STRING_TOO_LONG     2008
#define     NULL_DESCRIPTOR     2009
#define     VERY_STRANGE        2010
#define     DIVISOR_IS_ZERO     2011
#define     DIVISOR_IS_0        2011	/* yes, two name for the same code */
#define     FULL_NUCKED		2012
#define     BUFFER_NO_MEM	2013
#define     FILE_NOT_FOUND	2014
#define     OUT_OF_IMAGE	2015
#define     OUT_OF_PIC		2015	/* alias, don't use it, please */
#define     WRONG_CHANNEL	2016
#define     BAD_COLOR_NUMBER	2017
#define     SCALE_ERROR		2018
#define     MAP_TOO_BIG		2019
#define     SRC_TOO_SMALL	2020
#define     DST_TOO_SMALL	2021
#define     BAD_MAGIC		2024
#define     IMG_OVERWRITE	2027
#define     INVALID_PARAM	2028
#define     BAD_PARAMETER	INVALID_PARAM
#define     PROTOCOL_ERROR	2029
#define     RECT_TOO_SMALL	2030
#define     NO_ALPHA_CHANNEL    2031
#define     NO_BETA_CHANNEL     2032
#define	    FILE_CREATE_ERR	2033
#define     BAD_ANGLE		2034
#define     WRONG_FLAG		2035
#define     NOT_3D_IMG		2036
#define     INVALID_RGB		2037
#define     TOO_MUCH_VALUES	2038
#define     INVALID_PT_LIST	2039
#define     INVALID_POINT	2040
#define     NULL_POINTER	2041
#define     FMBL_ROULAIZE	2042
#define     PIXEL_OVERFLOW	2043
#define     PIXEL_UNDERFLOW	2044
#define     INVALID_GAMMA	2045
#define     INVALID_HF15	2046
#define     BAD_FILE_NAME	2047
#define     EMPTY_COL_MAP	2048
#define     BAD_CHRONO		2049
#define     IMAGE_BAD_DIMS	2050
#define     PASTIS		2051
#define     BAD_OPERATOR	2052
#define     BAD_FILTER          2053
#define     WTF_OSEF		2054		/* 18 janv 2010 */
#define     BASIC_IO_RD_ERR	2061
#define     BASIC_IO_WR_ERR	2062
#define     FP_GARBAGE          2080		/* c'est quoi ? FIXME */
#define     IMG_ARE_DIFFERENT	2500		/* added by Marcel */
#define     BAD_ZOOM		2550
#define     BAD_GRAY_OP		2660
#define     ERR_GARDRECT	2600
#define     FUNC_IS_ALPHA	2990
#define     FUNC_IS_BETA	2991
#define     FUNC_IS_XPER	2992
#define     FUNC_COREDUMP	2993
#define     FUNC_NOT_FINISH	2999
#define     IMAGE_FAIL		3000
#define     BAD_CADRE		3153

#define	    ERR_FORTRAN		9999
/* END error_code */

/*::------------------------------------------------------------------::*/
/*
 *	module image.c			les primitives
 */
void        Image_print_version(int verbose);

Image_Desc *Image_alloc(int width, int height, int type);
Image_Desc *Image_clone(Image_Desc *src, int copy);
int         Image_clear( Image_Desc *image, int r, int v, int b );
int	    Image_set_rgb(Image_Desc *img, RGBA *rgba);
int         Image_copy(Image_Desc *src, Image_Desc *dst);
int         Image_set_comment(Image_Desc *image, char *text);
int	    Image_copy_comment(Image_Desc *s, Image_Desc *d);
int         Image_DeAllocate( Image_Desc *im );	/* nice coredumper */

int Image_plot_gray(Image_Desc *img, int x, int y, int v);

int Image_plotRGB(Image_Desc *img, int x, int y, int r, int g, int b);
int Image_plotRGBA(Image_Desc *img, int x, int y, int r, int g, int b, int a);

int Image_getRGB(Image_Desc *img, int x, int y, int *pr, int *pg, int *pb);
int Image_getRGBA(Image_Desc *img, int x, int y, int *pr, int *pg, int *pb, int *pa);

int Image_plot_channel(Image_Desc *img, char channel, int x, int y, int value);

int Image_R_pixel(Image_Desc *img, int x, int y);
int Image_G_pixel(Image_Desc *img, int x, int y);
int Image_B_pixel(Image_Desc *img, int x, int y);
int Image_A_pixel(Image_Desc *img, int x, int y);

/* Mon Aug 12 2024 : this function may be rewrited with a macro ? */
int Image_pixel_copy(Image_Desc *s, int x, int y, Image_Desc *d, int i, int j);

int Image_compare_desc(Image_Desc *a, Image_Desc *b);

/*::------------------------------------------------------------------::*/
/*	module functions.c						*/

double tthi_dtime(void);

/*::------------------------------------------------------------------::*/
/*	module pixels.c							*/

int Image_XOR_pixel(Image_Desc *img, int x, int y, int r, int g, int b);
int Image_rot_pixel(Image_Desc *img, int x, int y, int r, int g, int b);

/*::------------------------------------------------------------------::*/
/*
 *              module basic_io.c
 *	exterminons les problemes de Boutisme !-)
 */

int	Image_basicIO_teste_boutisme(char *txt);
int	Image_basicIO_write_I_short(FILE *fp, short value);
int	Image_basicIO_write_M_short(FILE *fp, short value);
int	Image_basicIO_write_I_long(FILE *fp, long value);
int	Image_basicIO_read_I_short(FILE *fp, uint16_t *p);
int	Image_basicIO_read_M_short(FILE *fp, uint16_t *p);
int	Image_basicIO_read_I_long(FILE *fp, uint32_t *p);

/*::------------------------------------------------------------------::*/
/*		module mustopen.c					*/

int	Image_must_open(char *filename, int mode, int type);
FILE  * Image_must_fopen(char *filename, char *mode, int type);

/*::------------------------------------------------------------------::*/
/*              module operat.c						*/

int Image_adjust_minmax_0(Image_Desc *src, Image_Desc *dst, int yo,
				int minstone, int maxstone);
int Image_adjust_minmax_1(Image_Desc *src, Image_Desc *dst, int yo,
				int minstone, int maxstone);
int          Image_egalise_RGB(Image_Desc *, Image_Desc *, int);
int          Image_egalise_mono_0(Image_Desc *, Image_Desc *, int);
int          Image_luminance(Image_Desc *, Image_Desc *, int);
int          Image_negate(Image_Desc *, Image_Desc *);
int Image_clear_component(Image_Desc *img, char component, int value);
int          Image_and_pix( Image_Desc *image, uint8_t ar, uint8_t ag, uint8_t ab);
int          Image_or_pix( Image_Desc *image, int ar, int ag, int ab );
int Image_copie_seuil(Image_Desc *s, Image_Desc *d, int r, int g, int b);
int          Image_overlay(Image_Desc *s, Image_Desc *d, int xpos, int ypos);
int Image_overlay_mix(Image_Desc *s, Image_Desc *d, int x, int y, int k);
int	     Image_seuil_RGB(Image_Desc *s, Image_Desc *d, int r, int g, int b);
int	     Image_AutoSeuilRGB(Image_Desc *s, Image_Desc *d);

int          Image_operator(Image_Desc *in, char op, int val, Image_Desc *out);

/*::------------------------------------------------------------------::*/
/*		module octree.c						*/
int	Image_octree_0(Image_Desc *src, Image_Desc *dst);
int	Image_octree_1(Image_Desc *src, Image_Desc *dst);

/*::------------------------------------------------------------------::*/
/*		module pixeliz.c					*/

/* XXX proto peut-etre pas definitf XXX */
int	Image_pixeliz_0(Image_Desc *src, Image_Desc *dst, int w, int h);

int	Image_pixeliz_X(Image_Desc *src, Image_Desc *dst);
int	Image_pixeliz_Y(Image_Desc *src, Image_Desc *dst);

/*::------------------------------------------------------------------::*/
/*		module gray_ops.c
 *
 */
int Image_gray_ops_0(Image_Desc *src, Image_Desc *dst, int k, int mask);
int Image_gray_ops_1(Image_Desc *src, Image_Desc *dst, int k, int mask);

/*::------------------------------------------------------------------::*/
/*		module classif.c					*/

typedef struct
	{
	int	rc, gc, bc;
	int	rad;
	int	r, g, b;
	} Une_Classe_Sph;

int	Image_classif_0(Image_Desc *src, Image_Desc *dst);
int	Image_classif_1(Image_Desc *src, Image_Desc *dst,
		Une_Classe_Sph *classes, int nbrclasses, int flags);
int	Image_display_classes(Une_Classe_Sph *cla, int nbr, char *txt,
								int flag);

/*::------------------------------------------------------------------::*/
/*              module effects.c					*/

int     Image_water(Image_Desc *src, Image_Desc *dest, int intensite);
int     Image_noise(Image_Desc *src, Image_Desc *dest, int intensite);
int	Image_mirror(Image_Desc *src, Image_Desc *dest, int reserved);
int	Image_upside_down(Image_Desc *src, Image_Desc *dst, int reserved);


int Image_scratch ( Image_Desc *source, Image_Desc * but, long nombre );
int Image_swap_lines( Image_Desc *src, Image_Desc * dst );
int Image_swap_cols( Image_Desc *src, Image_Desc * dst );
int Image_swap_nibbles( Image_Desc *src, Image_Desc * dst );

/*              module effects2.c					*/

int Image_sinwave_0( Image_Desc *source, Image_Desc * but, double *table );
int Image_sinwave_1( Image_Desc *source, Image_Desc * but, double *table );
int Image_sinwave_2( Image_Desc *source, Image_Desc * but, double *table );
int Image_degouline_0(Image_Desc *source, Image_Desc *but, int k1, int k2);

/*              module effects3.c					*/
int Image_effect_x_0(Image_Desc *s, Image_Desc *d, int kr, int kg, int kb);
int Image_effect_x_1(Image_Desc *s, Image_Desc *d);
int Image_effect_x_2(Image_Desc *s, Image_Desc *d, int kx, int ky, int kv);
int Image_effect_x_3(Image_Desc *s, Image_Desc *d, int kx, int ky, char *comp);
int Image_effect_x_4(Image_Desc *s, Image_Desc *d, int flags);
int Image_effect_x_5(Image_Desc *s, Image_Desc *d, int kx, int ky, int kv);

/*		module recurse.c					*/

int Image_call_recursion(Image_Desc *image, Image_Desc *dest, int param);
int Image_call_recursion_0(Image_Desc *image, Image_Desc *dest, int param);

/*::------------------------------------------------------------------::*/
/*		module television.c
 */
int Image_TV_grink(Image_Desc *src, Image_Desc *dst, int yo);
int Image_TV_grok(Image_Desc *src, Image_Desc *dst, int yo);
int Image_TV_gruud(Image_Desc *src, Image_Desc *dst, int yo);
int Image_TV_griiiz(Image_Desc *src, Image_Desc *dst, int yo);
int Image_TV_old(Image_Desc *src, Image_Desc *dst, int a, int b, int c);
int Image_TV_veryold(Image_Desc *src, Image_Desc *dst, int a, int b, int c);
int Image_TV_pix_0(Image_Desc *src, Image_Desc *dst, int w, int h, int grey);
int Image_TV_pix_1(Image_Desc *src, Image_Desc *dst, int w, int h, int grey);

int Image_TV_cplus_v1(Image_Desc *src, Image_Desc *dst, int yo);
int Image_TV_triligne(Image_Desc *src, Image_Desc *dst, int p);

/*::------------------------------------------------------------------::*/
/*              module rgbmask.c					*/
int Image_rgbmask_H(Image_Desc *src, Image_Desc *dst, int gris);
int Image_rgbmask_V(Image_Desc *src, Image_Desc *dst, int gris);
int Image_rgbmask_2(Image_Desc *src, Image_Desc *dst, int gris);
int Image_rgbmask_R(Image_Desc *src, Image_Desc *dst, int gris);

/*::------------------------------------------------------------------::*/
/*		module mosaic.c
 *
 *	on va essayer de construire un iterateur de mosaique,
 *	et il faut definir une structure de controle.
 */
typedef struct
	{
	Image_Desc	*src;
	Image_Desc	*dst;
	int		stepx, stepy;
	int		offx, offy;
	int		p1;
	int		p2;
/* XXX	int		*func(void *); */
	} IMAGE_MOSAIC_CTRL;
/*
 *	vous remarquerez que cette structure est recursive, et
 *	que Gcc grinke feroce dessus...
 */

int  Image_mosaic_0(Image_Desc *src, Image_Desc *dst, int sx, int sy, int flg);
int  Image_mosaic_simple(Image_Desc *src, Image_Desc *dst);
void Image_mosaic_prctrl(IMAGE_MOSAIC_CTRL *ctrl, char *txt);
int  Image_mosaic_iterateur(IMAGE_MOSAIC_CTRL *ctrl, int yo);

/*::------------------------------------------------------------------::*/
/*		module des vignettes (nex 15 mai 2007)			*/

int Image_vignetize_x_0(Image_Desc *src, Image_Desc *dst, int prout);
int Image_vignetize_x_1(Image_Desc *src, Image_Desc *dst, int prout);

/*::------------------------------------------------------------------::*/
/*		module combine.c					*/

int Image_combine_lines(Image_Desc *s1, Image_Desc *s2, Image_Desc *d,
				int sy, int oy, int zak);
int Image_combine_columns(Image_Desc *s1, Image_Desc *s2, Image_Desc *d,
				int sx, int ox, int zak);
int Image_combine_checker(Image_Desc *s1, Image_Desc *s2, Image_Desc *d,
				int sx, int sy, int ox, int oy, int zak);
int Image_combine_cercle_flou(Image_Desc *s1, Image_Desc *s2,
					Image_Desc *d, int yo);
int Image_combine_diagonale(Image_Desc *s1, Image_Desc *s2,
					Image_Desc *d, int yo, int p1, int p2);
int Image_combine_minmax(Image_Desc *s1, Image_Desc *s2,
						Image_Desc *d, int flg);
int Image_combine_if_not_black(Image_Desc *s1, Image_Desc *s2, Image_Desc *d);
int Image_poke_2zones(Image_Desc *src, Image_Desc *ia, Image_Desc *ib,
				   Image_Rect *za, Image_Rect *zb,
			  Image_Desc *dst);

/*::------------------------------------------------------------------::*/
/*		module combine2.c					*/

int Image_mix(Image_Desc *a, Image_Desc *b, Image_Desc *c, int k);
int Image_mix_rgb(Image_Desc *a, Image_Desc *b, Image_Desc *c,
						int kr, int kg, int kb);
int Image_trimix(Image_Desc *a, Image_Desc *b, Image_Desc *c, Image_Desc *d,
						char w);

/*		module combine3.c					*/
int Image_combine_waou(Image_Desc *s1, Image_Desc *s2, Image_Desc *dst,
					int a, int b, int c, int d);
int Image_combine_wauo(Image_Desc *s1, Image_Desc *s2, Image_Desc *d, int f);
int Image_combine_seuils(Image_Desc *s1, Image_Desc *s2, Image_Desc *dst,
						int r, int b, int g);

/*		module combine4.c					*/
int Image_combine_4img_0(	Image_Desc *s1, Image_Desc *s2,
				Image_Desc *s3, Image_Desc *s4,
				Image_Desc *dst);
int Image_combine_4img_1(	Image_Desc *s1, Image_Desc *s2,
				Image_Desc *s3, Image_Desc *s4,
				Image_Desc *dst);
int Image_combine_4img_2(	Image_Desc *s1, Image_Desc *s2,
				Image_Desc *s3, Image_Desc *s4,
				Image_Desc *dst, int meth_reduc);
int Image_combine_4img_2(	Image_Desc *s1, Image_Desc *s2,
				Image_Desc *s3, Image_Desc *s4,
				Image_Desc *dst, int k);

/*		module combine5.c					*/
int Image_combine_Vdegrade(Image_Desc *, Image_Desc *, Image_Desc *, int);
int Image_combine_Hdegrade(Image_Desc *, Image_Desc *, Image_Desc *, int);
int Image_combine_Ddegrade(	Image_Desc *s1, Image_Desc *s2,
						Image_Desc *dst, int sens);

int Image_combine_Vsplit(Image_Desc *, Image_Desc *, Image_Desc *, int, int);
int Image_combine_Hsplit(Image_Desc *, Image_Desc *, Image_Desc *, int, int);

int Image_combine_stereo_0(Image_Desc *s1, Image_Desc *s2, Image_Desc *d);
int Image_combine_stereo_1(Image_Desc *s1, Image_Desc *s2, Image_Desc *d,
						int kr, int kg, int kb);
int Image_combine_stereo_2(Image_Desc *s1, Image_Desc *s2, Image_Desc *d,
					char cr, char cl);


/*		module combine_rnd.c					*/
int Image_combine_random_point(Image_Desc *s1, Image_Desc *s2,
					Image_Desc *d, int shift);
int Image_combine_random_rgb(Image_Desc *s1, Image_Desc *s2,
					Image_Desc *d, int yop);

/*		module combine6.c					*/
int Image_combine_power(Image_Desc *s1, Image_Desc *s2, Image_Desc *d);
int Image_combine_power_m(Image_Desc *s1, Image_Desc *s2, Image_Desc *d);
int Image_XOR(Image_Desc *s1, Image_Desc *s2, Image_Desc *d, int notused);

/*::------------------------------------------------------------------::*/
/*   module glitch.c - octobre 2014 */

int Image_GlitchMe(Image_Desc *src, Image_Desc *dst, char code, 
			int k1, char *txt1);
int Image_Glitch_simple(Image_Desc *src, Image_Desc *dst, int k);
int Image_Glitch_xor(Image_Desc *src, Image_Desc *dst, int k);

/*::------------------------------------------------------------------::*/
/*
 *		module ascii.c			December 2000
 */
int Image_ascii_0(Image_Desc *src, char *fname, int largeur);
int Image_ascii_1(Image_Desc *src, char *fname, int width, char *cars);
int Image_ascii_2(Image_Desc *src, char *fname, int w, int h, char *c, int f);
int Image_ascii_3(Image_Desc *src, char *fname, int width, char *cars);
/*
 *		module asciiart.c			December 2000
 */
int Image_asciiart_0(Image_Desc *src);
int Image_asciiart_1(Image_Desc *src, char *pattern, int reverse);
int Image_asciiart_2(Image_Desc *src, char *pattern, int param);

/*::------------------------------------------------------------------::*/
/*              module calculs.c					*/

int     Image_clamp_pixel(int value);	/* XXX must be a macro ? */

/* param 'res' is a 8 elements array */
int	Image_minmax_RGB(Image_Desc *img, int *res);

int Image_stats_zone_0(Image_Desc *img, Image_Rect *zone,
				int *pmr, int *pmg, int *pmb,
				int *pdr, int *pdg, int *pdb);

int	Image_quelques_calculs(Image_Desc *image);

int Image_histo_RGB(Image_Desc *im, long *hr, long *hg, long *hb);


int Image_LUT_RGB(Image_Desc *, Image_Desc *, int *, int *, int *);
int Image_LUT_mono(Image_Desc *src, Image_Desc *dst, int *lut);

/* 	falsecolors.c 		*/

int Image_gen_fc_lut(float maxval, int nbslots, RGB_map *plut);


/*::------------------------------------------------------------------::*/
/*		module distances.c					*/

int Image_distance_0(Image_Desc *ia, Image_Desc *ib, 
					double *dr, double *dg, double *db);
int Image_distance_1(Image_Desc *ia, Image_Desc *ib, 
					double *dr, double *dg, double *db);

/*::------------------------------------------------------------------::*/
/*		module dissolve.c					*/

int Image_dissolve_0(Image_Desc *src, Image_Desc *dst, Image_PtList *pts);

/*		module photomaton.c					*/
int Image_photomaton_0(Image_Desc *src, Image_Desc *dst);
int Image_photomaton_n(Image_Desc *src, Image_Desc *dst, int nb);

/*::------------------------------------------------------------------::*/
/* new 14 Jan 2001
 *		module gamma.c
 */
int Image_gamma_calcul(double coef, int table[]);

/*::------------------------------------------------------------------::*/
/*              module colors.c						*/

int	Image_swap_colors(Image_Desc *im, char *cols);
int	Image_couleur_moyenne(Image_Desc *im, int *pr, int *pg, int *pb);
int Image_saturate(Image_Desc *src, Image_Desc *dst, int sr, int sg, int sb);
int Image_desaturate(Image_Desc *src, Image_Desc *dst, int sr, int sg, int sb);

int	Image_color_x(double v1, double v2, int *pr, int *pg, int *pb);
int     Image_RGB_2_HLS(int r, int g, int b,  int *ph, int *pl, int *ps);

int	Image_colors_recenter_0(Image_Desc *s, Image_Desc *d);

int Image_colors_2_Map(Image_Desc *s, Image_Desc *d, RGB_map * map, int mode);

int Image_to_gray_k(Image_Desc *s, Image_Desc *d,
					int kr, int kg, int kb, int flag);
int Image_to_gray(Image_Desc *s, Image_Desc *d, int flag);

int Image_color_shift_rgb(Image_Desc *s, Image_Desc *d, int r, int g, int b);

int	Image_apply_Map(Image_Desc *src, Image_Desc *dst, RGB_map *map);
int	Image_gray_Map(Image_Desc *src, Image_Desc *dst, RGB_map *map);

int	Image_default_RGBA(RGBA *ptr, char *texte, int flags);

/*::------------------------------------------------------------------::*/
/*              module colors2.c					*/

int Image_AutoSeuilGray(Image_Desc *s, Image_Desc *d, int *ps);
int Image_BiColor_0(Image_Desc *src, Image_Desc *dst, int k);
int Image_BiColor_1(Image_Desc *src, Image_Desc *dst, int ka, int kb);

/*::------------------------------------------------------------------::*/
/*              module col_reduc.c
 */
int Image_essai_col_reduce(Image_Desc *src, Image_Desc *dst, int n, int p);

/*::------------------------------------------------------------------::*/
/*		module col_xyz.c (new 9 avril 2007)			*/
int Image_pix_rgb2xyz_d(int r, int g, int b, double *px, double *py, double *pz);

/*::------------------------------------------------------------------::*/
/*              module col4bits.c
 */
int	Image_col4bits_and(Image_Desc *src, Image_Desc *dst);
int	Image_calc_Map_4bits(Image_Desc *img, RGB_map *map, int n);

/*::------------------------------------------------------------------::*/
/*              module extractbits.c					*/

int Image_extrbits_0(Image_Desc *src, Image_Desc *dst, int r, int g, int b);

/*::------------------------------------------------------------------::*/
/*		module ptlist.c	
 */
int	Image_print_point(Image_Point *ppt);
int	Image_ptl_dump(Image_PtList *pl, char *txt);
Image_PtList *Image_ptl_alloc(int nbre, char *name);
int	Image_ptl_add(Image_PtList *ptl, int x, int y, int h, int c);
int	Image_ptl_write(char *filename, Image_PtList *ptl);
int	Image_ptl_read(char *filename, Image_PtList *ptl);
int	Image_ptl_kill(Image_PtList *ptl, char *msg);		/* XXX */
int	Image_ptl_get_size(Image_PtList *ptl, int *nbre, int *avail);
int	Image_ptl_get_point(Image_PtList *ptl, Image_Point *pt, int idx);
int	Image_ptl_boundingbox(Image_PtList *ptl, Image_Rect *box);

/*::------------------------------------------------------------------::*/
/*		module palettes.c					*/

int     Image_load_color_Map(char *file, char *name, RGB_map *where);
int     Image_save_color_Map(char *file, char *name, RGB_map *where);

int	Image_attach_Map(Image_Desc *im, char *nom_map, int flags);

int	Image_make_random_Map(char *nom, RGB_map *map, int nombre);
int	Image_make_222_Map(char *nom, RGB_map *map, int noise);
int	Image_palette_3sinus(char *nom, RGB_map *ou, double pars[8]);

int	Image_mix_palettes(RGB_map *p1, RGB_map *p2, RGB_map *d,
							char *txt, int k);

int	Image_scale_palette(RGB_map *p, double val, int clip_it);
int	Image_scale3_palette(RGB_map *p, double rk, double gk, double bk,
						int clip_it);
int	Image_map2ppm(RGB_map *p, char *fname, char *comment);

/*::------------------------------------------------------------------::*/
/*		module indexcol.c					*/

int	Image_idxcol_add(Image_Desc *im, int r, int g, int b);
int	Image_idxcol_clear(Image_Desc *im, int flag);
int	Image_idxcol_plot(Image_Desc *im, int x, int y, int c);

/*::------------------------------------------------------------------::*/
/*              module filtres.c					*/

void    Image_filtre_display(FILE *ou, int *mat);
void    Image_rotate_filtre(int *filtre);
int     Image_convolueur3(Image_Desc *in, Image_Desc *out, int *mat);
int     Image_convolueur_2(Image_Desc *in, Image_Desc *out, int *mat, char*pl);
int     Image_raz_sides(Image_Desc *im);
int     Image_lissage_3x3(Image_Desc *in, Image_Desc *out);
int	Image_filtre_Prewitt(Image_Desc *src, Image_Desc *dst, int rot);
int	Image_filtre_Sobel(Image_Desc *src, Image_Desc *dst, int rot);
int	Image_filtre_passe_bas(Image_Desc *src, Image_Desc *dst);
int	Image_filtre_passe_haut(Image_Desc *src, Image_Desc *dst);
int	Image_filtre_random(Image_Desc *src, Image_Desc *dst, int p1, int p2);
int	Image_filtre_random_2(Image_Desc *src, Image_Desc *dst,
				int p1, int p2, int p3, int p4);

/*              module filtadapt.c					*/
int Image_filtadapt_0(Image_Desc *src, Image_Desc *dst, int param);
int Image_filtadapt_1(Image_Desc *src, Image_Desc *dst, int param);
int Image_filtadapt_2(Image_Desc *src, Image_Desc *dst, int param);
int Image_filtadapt_3(Image_Desc *src, Image_Desc *dst, int param);

/*              module sobel4.c					*/
int Image_filtre_Sobel_4(Image_Desc *src, Image_Desc *dst, int rotation);

/*::------------------------------------------------------------------::*/
/*              module morpho.c						*/

int	Image_expand_max(Image_Desc *src, Image_Desc *dst, int factor);
int	Image_expand_min(Image_Desc *src, Image_Desc *dst, int factor);
int Image_Gray_Sorted_Points(Image_Desc *src, Image_Desc *dst, int *filtre);

/*::------------------------------------------------------------------::*/
/*		module msgsys.c						*/

char	* Image_type2str(int type);
char	* Image_err2str(int codeerr);
void	Image_print_error(char * texte, int err);

/*::------------------------------------------------------------------::*/
/*              module tools.c						*/

void	Image_pacifier(char *texte, int div);
void	Image_pacifier2(char *texte, int cur, int top);
int	Image_start_chrono(char *, int);
long	Image_stop_chrono(char *, int);
int	Image_dump_colormap(RGB_map *map, int verbose_flag);
int     Image_dump_descriptor(Image_Desc *im, char *text);
int	Image_dump_rect(Image_Rect *rect, char *texte, int flag);
void	Image_warning(char caractere, char *lig1, char *lig2, int k);
int	Image_print_rgba(char *txt, RGBA *rgba, int hexa);
int	Image_print_DF3head(DensityF3Head *h, char *txt, int flag);
int     Image_Overlay_Palette(Image_Desc *im, int xo, int yo);
int     Image_fabrique_une_mire(Image_Desc *im, RGB_map *map);
int	Image_Grille(Image_Desc *im, int stepx, int stepy, int rvb);
int	Image_print_minmax(Image_Desc *img);
int	Image_xy_inside(Image_Desc *img, int x, int y);

/*::------------------------------------------------------------------::*/
/*		module marques.c					*/
int	Image_marque_0(Image_Desc *img, int val);
int	Image_marque_1(Image_Desc *img, char *txt, int flags);
int	Image_marque_2(Image_Desc *img, char *txt, RGBA *rgba, int k1, int k2);

int Image_marque_timestamp(Image_Desc *img, char *txt, RGBA *rgba, int flags);
int Image_grille(Image_Desc *im, int sx, int ox, int sy, int oy, RGBA *rgba);
int Image_grille_xor(Image_Desc *i, int sx, int ox, int sy, int oy, RGBA *c);

/*::------------------------------------------------------------------::*/
/*		module mircol.c						*/
int	Image_mircol_0(Image_Desc *dst, char *txt, int flag);
int	Image_mircol_1(Image_Desc *dst, char *txt, int flag);
int	Image_mircol_2(Image_Desc *dst, char *txt, int flag);
int	Image_mircol_3(Image_Desc *dst, char *txt, int flag);
int	Image_mircol_4(Image_Desc *dst, char *txt, int mode);

int	Image_mirRGB_0(Image_Desc *dst, int flag);
int	Image_decompose(Image_Desc *src, Image_Desc *dst, int flag);

/*::------------------------------------------------------------------::*/
/*		module dumppix.c					*/

int	Image_dump_pixels_hexa(char *f, Image_Desc *im, int x, int y, char *t);
int	Image_dump_pixels_ascii(char *fname, Image_Desc *im,
		int x, int y, int w, int h, int seuil, char canal, char *texte);

/*::------------------------------------------------------------------::*/
/* 		module plotteur.c 					*/

int Image_plot_luts(char *nomtga, int *lr, int *lg, int *lb, char *texte);
int Image_plot_histo(char *nomtga, long *hr, long *hg, long *hb, char *txt);
int Image_calc_plot_histo(Image_Desc *img, char *tganame);
int Image_plot_h_Map(Image_Desc *img, RGB_map *map, int x, int y, int h);
int Image_plot_v_Map(Image_Desc *img, RGB_map *map, int x, int y, int w);
int Image_plot_little_h_Map(char *nom, RGB_map *map, char *texte);
int Image_plot_little_v_Map(char *nom, RGB_map *map, char *texte);

int Image_plot_square_Map(char *fname, RGB_map *map, char *texte, int k);
int Image_plot_Map(char *nomtga, RGB_map *map , char *txt);
int Image_plot_histo_hf15(Image_Desc *img, char *nom, char *txt,
							int p1, int p2);

/*::------------------------------------------------------------------::*/
/*
 *              module gif-op.c
 *	pour celui-la, il faut linker avec gdlib
 */

int Image_Gif_Overlay(Image_Desc *im, char *nomgif, int x, int y, int t);

/*::------------------------------------------------------------------::*/
/*		module png.c 						*/
/* new 22 fevrier 2015 - mixart-myrys					*/

Image_Desc * Image_PNG_alloc_load(char *fname, int k);
int Image_save_as_PNG(Image_Desc *img, char *fname, int p1, int p2);

/*::------------------------------------------------------------------::*/
/*		module pcx.c 						*/
int	Image_PCX_fileinfo(char *nom);
int	Image_wrPCX_8colors(char *nom, Image_Desc *im);
int	Image_bp2pcx(A_BitPlane *bp, char *fname, int flags);

/*::------------------------------------------------------------------::*/
/* 		module pht.c						*/
int Image_PHT_save_component(char *name, Image_Desc *img, char color);
Image_Desc *Image_load_PHT(char *fname);

/*::------------------------------------------------------------------::*/
/* 		module pnm.c 						*/
int	Image_wr_pbm_0(char *nom, Image_Desc *im, char channel);
/* warning: this function was misnamed. */
int	Image_wr_pnm_0(char *nom, Image_Desc *im, int mode);
/* ... use that instead ... */
int	Image_wr_ppm_0(char *nom, Image_Desc *im, int mode);
int	Image_load_ppm_0(char *name, Image_Desc *ou, int yo);
int	Image_wr_pgm_0(char *nom, Image_Desc *im, char channel);

/*::------------------------------------------------------------------::*/
/* 		module bmp.c 						*/
int Image_BMP_infos(char *nom, int *pw, int *ph, int *pt, int verb);
Image_Desc * Image_BMP_alloc_load(char *nom, int flag);
int Image_BMP_save_24(char *filename, Image_Desc *img, int flag);

/*::------------------------------------------------------------------::*/
/* 		module tga.c						*/
int     Image_TGA_save(char *nom, Image_Desc *img, int compress);
int     Image_TGA_save_component(char *nom, Image_Desc *img, char chanl, int comp);
/* int	Image_TGA_print_header(void *ph); XXX */
int	Image_TGA_read_header(FILE *fpin, void *phead);
int	Image_TGA_show_header(char *filename, int flag);
int	Image_TGA_get_dims(char *path, int *pwidth, int *pheight);
Image_Desc   * Image_TGA_alloc_load(char *nom);

/*::------------------------------------------------------------------::*/
/*
 *		module eps.c			(new 28 Jan 2002)
 */
int	Image_EPS_save_0(char *nom, Image_Desc *img);

/*::------------------------------------------------------------------::*/
/*	module des afficheurs sept segments 				*/

int Image_7seg_m0v(Image_Desc *img, int xpos, int ypos, int bits, int k);
int Image_7seg_digv(Image_Desc *img, int xpos, int ypos, char chiffre, int k);
int Image_7seg_tag0(Image_Desc *img, char *numstr, int k);
int	Image_7_segments(Image_Desc *img, int nbre, int k);

/*::------------------------------------------------------------------::*/
/*
		module dither.c
 */
int	Image_dither_Bayer_0(Image_Desc *s, Image_Desc *d, int uh);
int	Image_dither_2x2(Image_Desc *s, Image_Desc *d, int uh);
int	Image_dither_simple_error(Image_Desc *s, Image_Desc *d, int uh);
int	Image_dither_seuil_random(Image_Desc *s, Image_Desc *d, int uh);
int	Image_dither_crude(Image_Desc *s, Image_Desc *d, int uh);
int Image_dither_double_seuil(Image_Desc *, Image_Desc *, int, int, int);

/*
		module dither2.c
 */
int	Image_dither_3x3_0(Image_Desc *s, Image_Desc *d, int uh);
int	Image_dither_3x3_1(Image_Desc *s, Image_Desc *d, int uh);
int	Image_dither_3x3_2(Image_Desc *s, Image_Desc *d, int uh);
int	Image_dither_3x3_3(Image_Desc *s, Image_Desc *d, int uh);

/*		module dither3.c */
int	Image_dither_atkinson(Image_Desc *s, Image_Desc *d, int uh);
int	Image_dither_4x4_0(Image_Desc *s, Image_Desc *d, int uh);
int	Image_dither_bayer8x8rnd(Image_Desc *s, Image_Desc *d, int uh, int m);

/*::------------------------------------------------------------------::*/
/*
 *		module textures.c
 */
int	Image_texture_0(Image_Desc *dst, int bas, int haut);
int	Image_texture_1(Image_Desc *dst, int bas, int haut);
int	Image_texture_2(Image_Desc *dst, int bas, int haut, int modulo);
int	Image_texture_3(Image_Desc *dst, int b, int h, char *ctrl, int qux);

/*::------------------------------------------------------------------::*/
/*
 *		module gradient.c
 */
int Image_plot_H_gradient(char *fname, int w, int h);
int Image_plot_V_gradient(char *fname, int w, int h);

/*::------------------------------------------------------------------::*/
/*
		module patterns.c
 */
int	Image_pattern_000(Image_Desc *img, int foo);
int	Image_pattern_001(Image_Desc *img, int foo);
int	Image_pattern_002(Image_Desc *img, int foo);
int	Image_pattern_003(Image_Desc *img, int foo);
int	Image_pattern_004(Image_Desc *img, int a, int b, int c, int d);
int	Image_pattern_005(Image_Desc *img, RGB_map *map);

int	Image_pattern_042(Image_Desc *img, int foo);

/*		patterns3.c						*/

int	Image_pattern_100(Image_Desc *dst, int kr, int kg, int kb);
int	Image_pattern_101(Image_Desc *dst, int kr, int kg, int kb);
int	Image_pattern_102(Image_Desc *dst, int par, int kr, int kg, int kb);
int	Image_pattern_103(Image_Desc *dst, int par, int kr, int kg, int kb);
int Image_pattern_104(Image_Desc *dst, int sx, int sy, RGBA *a, RGBA *b);

/*		patterns4.c  random noises 				*/
int	Image_gray_noise_0(Image_Desc *dst, int low, int high);
int	Image_rgb_noise_0(Image_Desc *dst, int low, int high);
int	Image_rgba_noise_0(Image_Desc *dst, int low, int high);
int	Image_rgb_noise_1(Image_Desc *dst, RGBA *low, RGBA *high);
int	Image_gray_noise_2(Image_Desc *dst, int low, int high);
int	Image_rgb_noise_2(Image_Desc *dst, RGBA *low, RGBA *high);

int Image_fill_pat_0(Image_Desc *img, Image_Desc *pat, int centered);
int Image_fill_pat_1(Image_Desc *img, Image_Desc *pat, int centered);
int Image_fill_pat_2(Image_Desc *img, Image_Desc *pat, int centered);

/*::------------------------------------------------------------------::*/
/*
		module bitblt.c			Septembre 1999
 */
int Image_get_rect(Image_Desc *s, Image_Rect *z, Image_Desc *d, int x, int y);
int Image_put_rect(Image_Desc *s, Image_Rect *z, Image_Desc *d, int x, int y);
int Image_copy_rect(Image_Desc *s, Image_Rect *z, Image_Desc *d, int x, int y);

Image_Desc *Image_create_subimg(Image_Desc *src, Image_Rect *r, int gray);

int rc_intersect (const Image_Rect * r1, Image_Rect * r2); /* 2014-01-24 */

/*::------------------------------------------------------------------::*/
/*		module bitplanes.c		Mars 2010		*/

int Image_dump_BP_head(A_BitPlane *bp, char *txt, int flag);
A_BitPlane * Image_give_me_a_BP(int largeur, int hauteur);
int Image_free_that_BP(A_BitPlane *bp, int k);        /* new 5 oct 2015 */
int Image_BP_setname(A_BitPlane *bp, const char *name);
int Image_BP_clear(A_BitPlane *bp, int bit);
int Image_BP_plot(A_BitPlane *, int x, int y, int bit);
int Image_BP_pget(A_BitPlane *bp, int x, int y, int *pbit);
int	Image_BP_to_pbm(A_BitPlane *bp, char *fname, int k);

/*::------------------------------------------------------------------::*/
/*
  		module op2x2.c			 5 Novembre 1999
 */
int	Image_2x2_contours_0(Image_Desc *);
int	Image_2x2_contours_1(Image_Desc *src, Image_Desc *dst);
int	Image_2x2_contrast(Image_Desc *src, Image_Desc *dst);
int	Image_2x2_lissage(Image_Desc *src, Image_Desc *dst);
int	Image_2x2_rot4pix(Image_Desc *src, Image_Desc *dst, int rot);
int	Image_2x2_shiftUL(Image_Desc *src, Image_Desc *dst);

/*::------------------------------------------------------------------::*/
/*
	module de trace de primitives.		(drawing.c)
 */
int Image_draw_circle(Image_Desc *i, int xc, int yc, int ray, RGBA *q, int m);
int Image_H_line(Image_Desc *i, int xa, int xb, int ypos, RGBA *col);
int Image_draw_line  (Image_Desc *i, int x1, int y1, int x2, int y2, RGBA *q);
int Image_paint_rect (Image_Desc *, Image_Rect *, int, int, int);
int Image_noise_rect (Image_Desc *, Image_Rect *, int, int, int);
int Image_draw_rect  (Image_Desc *, Image_Rect *, int, int, int);

/*
 *	la meme chose, avec le canal alpha	(draw_alpha.c)
 */
int Image_paint_A_rect(Image_Desc *img, Image_Rect *rect, RGBA *rgba);
int Image_fade_A_rect(Image_Desc *img, Image_Rect *rect, int alpha);
int Image_tampon_alpha_0(Image_Desc *s, Image_Desc *tampon, Image_Desc *d);

/*
 *	same things with patterns.
 */
int Image_draw_rect_from_patt(Image_Desc *img, Image_Rect *rect,
							Image_Desc *patt);

/*::------------------------------------------------------------------::*/
/* 	module pov_hf15.c */
int	Image_hf15_plot(Image_Desc *im, int x, int y, int h);
int	Image_hf15_height(Image_Desc *im, int x, int y);
int	Image_hf15_hf2gray(Image_Desc *src, Image_Desc *dst, int mode);
int	Image_hf15_rgb2hf(Image_Desc *src, Image_Desc *dst, int mode);
int	Image_hf15_save_PGM(char *nom, Image_Desc *img, char *comment);
int	Image_hf15_lissage(Image_Desc *s, Image_Desc *d, int coef, int flag);
int Image_hf15_calc_minmax(Image_Desc *img, char *txt, int *pmin, int *pmax);
int Image_hf15_mul_add(Image_Desc *src, Image_Desc *dst, int mul, int add);
int	Image_hf15_invert(Image_Desc *src, Image_Desc *dst);
int Image_hf15_sweep(Image_Desc *im, int hfin, int taille, int reserved);
int Image_hf15_getmin(Image_Desc *s1, Image_Desc *s2, Image_Desc *d);
int Image_hf15_getmax(Image_Desc *s1, Image_Desc *s2, Image_Desc *d);

/*	module pov_hf15b.c						*/
int Image_hf15_normalize(Image_Desc *src, Image_Desc *dst, int low, int high);
int Image_hf15_make_colmap_0(Image_Desc *src, Image_Desc *dst, char *mapname);
int Image_hf15_make_colmap_1(Image_Desc *src, Image_Desc *dst, RGB_map *map);

/*	module pov_hf15c.c						*/

int Image_hf15_mix(Image_Desc *s1, Image_Desc *s2, Image_Desc *dst, int k);
int Image_hf15_mult_2(Image_Desc *s1, Image_Desc *s2, Image_Desc *dst);
int Image_hf15_div(Image_Desc *s1, Image_Desc *s2, Image_Desc *dst);
int Image_hf15_sqrt(Image_Desc *src, Image_Desc *dst);
int Image_hf15_pow(Image_Desc *src, Image_Desc *dst, double pow4a);
int Image_hf15_exp1(Image_Desc *src, Image_Desc *dst);
int Image_hf15_exp2(Image_Desc *src, Image_Desc *dst);

/*	module pov_hf15d.c						*/
int Image_hf15_noise_0(Image_Desc *dst, int low, int high, int prob100);
int Image_hf15_noise_1(Image_Desc *dst, int low, int high, int prob100);
int Image_hf15_noise_2(Image_Desc *dst, int lo, int hi, int k1, int k2);
int Image_hf15_noise_3(Image_Desc *dst, double coef, int flag);

/*	module pov_hf15d.c						*/
int Image_hf15_dilate(Image_Desc *src, Image_Desc *dst, int coef);
int Image_hf15_erode(Image_Desc *src, Image_Desc *dst, int coef);

/*	module pov_hf_synth.c						*/
int	Image_hf15_synth_0(Image_Desc *dst, Image_PtList *ptl);
int	Image_hf15_synth_1(Image_Desc *dst, Image_PtList *ptl);
int	Image_hf15_synth_fromfunc0(Image_Desc *dst, int param,
					double(*func)(int x, int y, int p));

/*::------------------------------------------------------------------::*/
/* nouveau. d'apres la doc de povray 3.1 ymmv				*/

DensityF3Head * Image_df3_alloc(char *nom, int x, int y, int z, int b);
int		Image_df3_free(DensityF3Head *ptr);
int Image_df3_set_bytes(DensityF3Head *ptr, int nbb);
int Image_df3_set_scale(DensityF3Head *ptr, double scale);

int Image_df3_put(DensityF3Head *ptr, int x, int y, int z, double dv);
int Image_df3_get(DensityF3Head *ptr, int x, int y, int z, double *pdv);

int	Image_df3f_get_dims(char *fname, int dims[], int display);
int	Image_df3f_explique(char *fname);
int Image_df3_export(DensityF3Head *ptr, char *fname, int nbbytes);

/* fonctions en vrac de df3b.c */
int Image_df3_xper_0(Image_Desc *src, char * fname_df3, int k);
int Image_df3f_plot_histo(char *fname_df3, char *nomtga, int flag);
int Image_df3f_2_txt(char *nomfd3, char *nomtxt);

int Image_df3f_2_povinc(char *nomdf3, char *nominc, char *nomobj, int k,
							double dv);

/*::------------------------------------------------------------------::*/
/*
	module detect.c
 */
int	Image_detect_tops(Image_Desc *src, Image_Desc *dst, int flags);
int	Image_detect_flat_gray(Image_Desc *, Image_Desc *, int, int);
int	Image_correlator_0(Image_Desc *src, Image_Desc *dst, int matrice[]);

/*::------------------------------------------------------------------::*/
/*
	module detect2.c
 */
int	Image_pix2ptl(Image_Desc *img, int seuil, Image_PtList *ptl);

/*::------------------------------------------------------------------::*/
/*
	module cadres.c			new 24 Jan 2000
 */
int	Image_cadre_A(Image_Desc *im);
int	Image_cadre_B(Image_Desc *im, int larg);
int	Image_cadre_cracra(Image_Desc *im, int larg, int k);
int	Image_cadre_C(Image_Desc *, int r, int g, int b, int sz);
int	Image_cadre_bruit(Image_Desc *, int, int, int, int);
int	Image_cadre_E(Image_Desc *, int, int, int, int);

int	Image_cadre_std2(Image_Desc *im, int r, int g, int b, int nh, int nv);

int	Image_cadre_pattern_0(Image_Desc *, Image_Desc *, int);
int	Image_cadre_pattern_1(Image_Desc *im, Image_Desc *pat, int n);
int	Image_cadre_pattern_2(Image_Desc *im, Image_Desc *pat, int n);

/*	module cadres2.c						*/

int
Image_cadre2_soft_1(Image_Desc *src, Image_Desc *dst, RGBA *fond, RGBA *cadre,
				int larg, int v1, int v2, int v3, int v4);

int Image_cadre2_pixx(Image_Desc *src, Image_Desc *dst, int p1, int p2);
int Image_cadre_f(Image_Desc *im, int a, int b);

/*	module cadres3.c						*/

int Image_cadre_waves_0(Image_Desc *img, int perx, int pery,
						RGBA *rgb, int amp);
int Image_cadre_waves_1(Image_Desc *img, int perx, int pery,
						RGBA *rgb, int amp);
int Image_cadre_waves_2(Image_Desc *img, int perx, int pery, int amp);

/*	module cadres4.c						*/

int Image_cadre_burp_0(Image_Desc *img, int p1, int p2, int flags);	
int Image_cadre_burp_1(Image_Desc *img, int p1, int p2, int flags);	
int Image_cadre_burp_2(Image_Desc *img, int t, int pr, int pg, int pb);
int Image_cadre_burp_3(Image_Desc *img, int t, int pr, int pg, int pb);
int Image_cadre_rasta_0(Image_Desc *img, int foo);

/*	module cadre84.c						*/

int Image_cadre_b84_a(Image_Desc *img, int px, int py, int k);
int Image_cadre_b84_b(Image_Desc *img, int foo, int k1);
int Image_cadre_b84_c(Image_Desc *img, int foo, int k1, int k2);

/*	module cadresbox.c	new janvier 2014			*/
int Image_cadre_box_0(Image_Desc *img, int t1, RGBA *color);
int Image_cadre_4coins_0(Image_Desc *img, int taille, RGBA *color);
int Image_cadre_box4c_0(Image_Desc *img, int t1, int t2, RGBA *color);

/*::------------------------------------------------------------------::*/
/*
	module alpha.c			new 31 Jan 2000
		operations sur les transparences
 */
int	Image_RGBA_over_RGB(Image_Desc *a, Image_Desc *src, Image_Desc *dst);
int	Image_add_alpha_channel(Image_Desc *img, int value);
int	Image_kill_alpha_channel(Image_Desc *img);
int	Image_map_alpha_on_img(Image_Desc *img);
int	Image_alpha_op_0(Image_Desc *src, Image_Desc *dst, int v1, int v2,
		    int rb, int rt, int gb, int gt, int bb, int bt, int par);
int	Image_copy_component_to_alpha(Image_Desc *img, char component);

int	Image_alpha_reduce(Image_Desc *src, Image_Desc *dst, int yo);
int	Image_poke_alpha_from_rgb(Image_Desc *src, Image_Desc *dst, 
					int r, int g, int b, int flag);

/* 	module alpha2.c							*/

int	Image_alpha_setvalue(Image_Desc *, int v_alpha);
int	Image_alpha_pattern_0(Image_Desc *, Image_Desc *, Image_Desc *);
int	Image_alpha_pattern_1(Image_Desc *, Image_Desc *, Image_Desc *);

/*::------------------------------------------------------------------::*/
/*	module insert.c 						*/
int	Image_insert_with_alpha(Image_Desc *img, Image_Desc *ins, int foo);

/*::------------------------------------------------------------------::*/
/*	module contrast.c 		new: dec 2013			*/

int Image_pix_square(Image_Desc *source, Image_Desc *but, int k);
int Image_pix_sqroot(Image_Desc *source, Image_Desc *but, int k);

int Image_egalise_cos01  (Image_Desc *source, Image_Desc *but, int k);
int Image_egalise_cos010 (Image_Desc *source, Image_Desc *but, int k);

/*::------------------------------------------------------------------::*/
/*
	module levels.c			new: Jan 2001
 */
int	Image_level_0(Image_Desc *, Image_Desc *, int);

/*::------------------------------------------------------------------::*/
/*
	module calcluts.c
*/
int	Image_calclut_lin(int *lut, int v0, int v255);
int	Image_calclut_foo(int *lut, int v0, int v255);
int	Image_print_lut(int *lut, char *fname, int flag);
int	Image_load_lut(int *lut, char *fname, int flag);

/*	module luts15bits.c  (new avril 2008)		*/

int Image_lut15_fill_0(int r[32768], int *g, int *b);
int Image_lut15_fill_1(int *r, int *g, int *b);
int Image_lut15_essai(char *hfname, char *tganame, int ka);
/*::------------------------------------------------------------------::*/
/* 	scale.c								*/
int	Image_scale_simple(Image_Desc *src, Image_Desc *dst, int k);
int	Image_scale_bilinear(Image_Desc *src, Image_Desc *dst, int k);
int	Image_getpix_bilin(Image_Desc *src, double fx, double fy, 
					int *pr, int *pg, int *pb);
Image_Desc * Image_new_scale( Image_Desc *, float, float, int );

/*	halfsize.c							*/
Image_Desc * Image_MakeHalfSize(Image_Desc *image, int methode);

/*	doublesz.c							*/
Image_Desc * Image_MakeDoubleSize(Image_Desc *image, int methode);
Image_Desc * Image_MakeDoubleSize_H(Image_Desc *image, int methode);

/*::------------------------------------------------------------------::*/
/*	addborder.c							*/

int	Image_ReAllocate(Image_Desc *img, int neww, int newh);

/*::------------------------------------------------------------------::*/
/*	showdiff.c							*/

int Image_showdiff_0(Image_Desc *im1, Image_Desc *im2, Image_Desc *dst);
int Image_showdiff_1(Image_Desc *i1, Image_Desc *i2, Image_Desc *d, int nu);
int Image_showdiff_2(Image_Desc *im1, Image_Desc *im2, Image_Desc *dst,
				int dmin);
int Image_showdiff_3(Image_Desc *im1, Image_Desc *im2, Image_Desc *dst,
				int kr, int kg, int kb);

/*::------------------------------------------------------------------::*/
/*	text0.c								*/

int Image_load_fnt8x8(char *nomfnt, uint8_t *ou, int flags);
int Image_trace_caractere(Image_Desc *im, int x, int y, int code);
int Image_trace_caractere_2(Image_Desc *im, uint8_t *fnt, int x, int y,
				int code, RGBA *paper, RGBA *ink);

int Image_trace_chaine_0(Image_Desc *im, char *txt, int x, int y,
				RGBA *paper, RGBA *ink, int flags);
int Image_trace_chaine_1(Image_Desc *im, char *txt, int x, int y,
				char *nomfonte, RGBA *paper, RGBA *ink);

int Image_trace_big_char_0(Image_Desc *im, int x, int y, int c, int kx, int ky);

/*	text1.c								*/
int Image_txt1_box_0(Image_Desc *img, char *txt, int x, int y, int w, 
				RGBA *paper, RGBA *ink, int flags);
int Image_txt1_big_0(Image_Desc *im, char *txt, int x, int y, int xf, int yf);

/*	text2.c								*/
int Image_texte_fonte(char *nomfonte, char *nomtga);

/*	text16x24.c							*/
int	Image_t16x24_binload(char *fontname, uint8_t *zone, int flag);
int	Image_t16x24_txtload(char *fontname, uint8_t *zone, int flag);
int	Image_t16x24_pltch_exp(Image_Desc *img, int lettre, int x, int y);
int	Image_t16x24_pltch_1(Image_Desc *img, int lettre, int x, int y,
				RGBA *pap, RGBA *ink, int flags);
int Image_t16x24_pltstr_1(Image_Desc *img, char *str, int x, int y,
                                         RGBA *paper, RGBA *ink, int flags);
int	Image_t16x24_essai(char *fontname, char *texte, char *tganame);
int	Image_t16x24_chars_map(char *fontname, char *tganame, int flag);

/*::------------------------------------------------------------------::*/
/*	vectfont.c							*/

int Image_vectfont_init(char *fontname);
int Image_vectfont_char_0(Image_Desc *im, char ch, int x, int y,
						int r, int g, int b);
int Image_vectfont_text_0(Image_Desc *im, char *txt, int x, int y);

/*::------------------------------------------------------------------::*/
/*	
	warpeurs: deformation d'image
*/
	
int Image_warp_essai_0(Image_Desc *src, Image_Desc *dst, double angle, 
						int xc, int yc);
int Image_warp_essai_1(Image_Desc *src, Image_Desc *dst, double ang, double off);

/*::------------------------------------------------------------------::*/
int Image_center_rotate(Image_Desc *src, Image_Desc *dst, double angle);

/*::------------------------------------------------------------------::*/
	
int Image_shift_xy(Image_Desc *src, Image_Desc *dst, int ox, int oy);
int Image_shift_x(Image_Desc *src, Image_Desc *dst, int ox);
int Image_shift_y(Image_Desc *src, Image_Desc *dst, int oy);
int Image_center_rotate_xy(Image_Desc *src, Image_Desc *dst, double angle,
						double x, double y);

/*::------------------------------------------------------------------::*/
/*	anamorphose.c							*/

int Image_anmo_X(Image_Desc *src, Image_Desc *dst);
int Image_anmo_grille(Image_Desc *dst);
int Image_anmo_essai_0(char *nomtga, int w, int h, int flgtxt);

/*::------------------------------------------------------------------::*/
/*	fits.c
		format de fichier utilise par les astronomes		*/

int Image_fits_print_fileinfo(char *filename, int flags);

/*::------------------------------------------------------------------::*/
/*	troisD.c							*/

int Image_3d_infos(Image_Desc *img, int flags);

/*::------------------------------------------------------------------::*/
/*	tortue.c							*/

int Image_turtle_infos(Image_Desc *img, int flags);
int Image_turtle_setcolors(Image_Desc *img, int r, int g, int b);
int Image_turtle_move(Image_Desc *img, double xt, double yt);
int Image_turtle_draw(Image_Desc *img, double xt, double yt);

/*::------------------------------------------------------------------::*/
/*	calling 'cjpeg' 						*/
int Image_system_cjpeg(Image_Desc *img, char *filename, int quality);

/*::------------------------------------------------------------------::*/
/*
 * gestion de pools d'images - new 21 mars 2007 
 */

void	Image_Pool_infos(int flag);
void  * Image_Pool_create(char *name, int nbre, int width, int height);
int	Image_Pool_describe(void *ptr, char *text, int flags);
int	Image_Pool_destroy(void *ptr, char *text, int flags);

/*::------------------------------------------------------------------::*/
/*  quadpics.c - nouveau juillet 2014 a mixart-myrys */

int Image_quadsym_simple(Image_Desc *src, Image_Desc *dst, int mode);

/*::------------------------------------------------------------------::*/
/* from zoom.c								*/

int Image_essai_zoom(Image_Desc *src, Image_Desc *dst,
					double kx, double ky, int flags);
int Image_zoom_000(Image_Desc *src, Image_Desc *dst, Image_Rect *rect,
					float kx, float ky);
int Image_zoom_001(Image_Desc *src, Image_Desc *dst, Image_Rect *rect,
					float kx, float ky);
int Image_zoom_extract(Image_Desc *src, Image_Desc *dst, Image_Rect *rect,
								int flags);

/*::------------------------------------------------------------------::*/
/*		imprime.c						*/

int Image_print_histo_0(long *histo, char *fname, char *txt);
void Image_print_sizeof_structs(char *texte);

/*::------------------------------------------------------------------::*/
int Image_gadrct_premier_essai(Image_Desc *src, Image_Rect *rect,
				Image_Desc *dst, int ka, int kb);
int Image_gadrct_Hsweep_0(Image_Desc *dst, Image_Rect *rect, int flag);
int Image_gadrct_Vsweep_0(Image_Desc *dst, Image_Rect *rect, int flag);
int Image_gadrct_poke_from_tga(char *namesrc, Image_Rect *fromrect,
			Image_Desc *dst, int xdst, int ydst, int flags);
int Image_gadrct_to_gray(Image_Desc *src, Image_Rect *rect,
				Image_Desc *dst, int ka, int kb);
int Image_gadrct_dimmer(Image_Desc *img, Image_Rect *rect, float dimf);
int Image_gadrct_cross(Image_Desc *img, Image_Rect *rect, int k);

/*::------------------------------------------------------------------::*/
/*
 * #endif du test anti-double-inclusion de la-bas,
 * tout en haut du fichier...
 */
#endif

