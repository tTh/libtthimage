#!/bin/bash

echo "XXXXXXX warning: $0 is an ugly installer"

DESTDIR="/usr/local"

install -d                             $DESTDIR/lib/
install -m 0644    libtthimage.a       $DESTDIR/lib/libtthimage.a
install -m 0644    tthimage.h          $DESTDIR/include/tthimage.h

cp -v Datas/8x8thin  Datas/16x24thin	$DESTDIR/share/libimage/
cp -v Datas/8x8std   Datas/16x24gruik	$DESTDIR/share/libimage/
cp -v Datas/*.map			$DESTDIR/share/libimage/

liste="genplot2 tga_cadre tga_effects tga_filtres tga_remap tga_tools	\
       tga_combine tga_television tga_dither tga_applymap tga_makehf15	\
       tga_mires tga_incrust tga_pattern tga_equalize tga_alpha		\
       tga_to_text tga_plotmap tga_resize tga_extract tga_2x2contrast	\
       tga_plothisto tga_export tga_fractales"

for binaire in $liste
do
	install  --strip       Tools/$binaire        $DESTDIR/bin/
done
