

# ---------------------------------------------------------------
test_des_xpers ()
{
command=$1
echo ; echo "======= Test du xper $command"

# generation d'une image sur laquelle travailler
$COMDIR/tga_mires mircol0 $SRC "hard xper '$command'"


case $command in
	zero)
		$COMDIR/tga_effects $SRC x0 $DST 64 128 192
		;;
	un)
		$COMDIR/tga_effects $SRC x1 $DST 64 128 192
		;;
	deux)
		$COMDIR/tga_effects $SRC x2 $DST 64 128 192
		;;
	trois)
		$COMDIR/tga_effects $SRC x3 $DST 42 R1 "rg"
		;;
	quatre)
		$COMDIR/tga_effects $SRC x4 $DST 27
		;;
	cinq)
		$COMDIR/tga_effects $SRC x5 $DST 64 128 192
		;;
	*)
		echo "==== ERROR ON $command ===="
		exit 1		;;
esac

gif89a=$(printf "flashy_%s.gif" $command)

convert -delay 80 $SRC $DST $gif89a
}
# ---------------------------------------------------------------
# ---------------------------------------------------------------

