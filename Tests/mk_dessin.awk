#!/usr/bin/awk -f

# ce joli programme fabrique un fichier de donnees qui sera
# utile au bon deroulement de 'essai_genplot2.sh'

BEGIN		{ nbretraits = 20000 }

END		{
		for (trait=0; trait<nbretraits; trait++)
			{
			if (0.498 > rand())
				{
				posx += rand()
				posy += rand()
				print posx, posy, 1
				}
			else
				{
				posx -= rand()
				posy -= rand()
				print posx, posy, 2
				}
			}	
		}

# job done

