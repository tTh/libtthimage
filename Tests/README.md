# Some useless tests ?

<u>Tester, c'est douter.</u>

## État des lieux

Pas grand chose pour le moment. Mais comme j'utilise régulièrement
et depuis très longtemps cet amas de fonctions, on peut dire que
les tests sont fait *in-vivo* sans formalisme formalisé. 

Allez quand même voir `config.sh` et `functions.sh` pour avoir une idée
du désastre actuel. Il reste beaucoup de choses à faire.

## Mais encore ?

Dans le répertoire `Lib/` il y a un `essais.c` assez
[prometteur](../Lib/essais.c) et en constante évolution.
Il manque à son [driver](../Lib/testtga.c) une gestion plus
fluide de la sélection
des essais à faire tourner.
