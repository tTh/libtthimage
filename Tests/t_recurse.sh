#!/bin/bash

set -e

source="dauphine2.tga"
if [ $# -eq 2 ] ; then
	source="$1"
fi

identify $source ; echo

rm -f frames/R*.tga

maxidx=250

for idx in $(seq 0 4 $maxidx)
do

	dst=$(printf "frames/R%03d.tga" $idx)
	coef=$(( $maxidx - $idx ))
	echo "======>  "$dst  "    coef " $coef

	../Tools/tga_effects $source recurse $dst $coef

done

convert -delay 10 frames/R*.tga recurse.gif
wc -c recurse.gif
