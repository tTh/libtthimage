/*
     Proggy de test pour les fontes 16x24 - new 4 mai 2007, a StEx.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include  "../tthimage.h"

/*::------------------------------------------------------------------::*/
int essai_f1(char *tgasrc, char *tgadst, char *texte)
{
Image_Desc	*img;
int		foo;
RGBA		encre, papier;

fprintf(stderr, ">>> %s ( %p  %p  '%s' )\n", __func__, tgasrc, tgadst, texte);

img = Image_TGA_alloc_load(tgasrc);
if (NULL==img) {
	fprintf(stderr, "%s: can't read '%s'\n", __func__, tgasrc);
	return FILE_NOT_FOUND;
	}

foo = Image_t16x24_txtload("16x24thin", NULL, 0);
if (foo) {
	fprintf(stderr, "%s err %d loading font '16x24thin'\n", __func__, foo);
	return foo;
	}

encre.r = 0;	encre.g = 255;	encre.b = 100;	encre.a = 255;
papier.r = 255;	papier.b = 0;	papier.g = 0;	papier.a = 200;

foo = Image_t16x24_pltstr_1(img, texte, 2, 42, &papier, &encre, 0);

foo = Image_TGA_save(tgadst, img, 0);

return -1;
}
/*::------------------------------------------------------------------::*/
int main(int argc, char *argv[])
{
int		foo;

fprintf(stderr, "--------------[ %s ]------------\n", argv[0]);

foo = Image_t16x24_chars_map("16x24thin",   "all16x24chars.tga", 0);
if (foo) { Image_print_error("load thin font", foo); }

foo = Image_t16x24_chars_map("16x24gruik", "all16X24chars.tga", 2);
if (foo) { Image_print_error("load gruik font", foo); }

fprintf(stderr, "--------------------------------------\n");

/*
foo = essai_f1("pov.tga", "/tmp/aaaa_plop.tga", "0123456789");
if (foo) {
	fprintf(stderr, "essai f1 --->  %d\n", foo);
	}
*/
return 0;
}
/*::------------------------------------------------------------------::*/
