#!/bin/bash

source ./config.sh

DST="foo.tga"

../Tools/tga_tools version

../Tools/tga_tools mk_rgb $SRC 800 600 30 40 50
../Tools/tga_effects $SRC noise $SRC 255
../Tools/tga_effects $SRC noise $SRC 255
# display $SRC

../Tools/tga_effects $SRC gray $BW
# display $BW

../Tools/tga_combine $SRC $BW circle0 $DST 0
display $DST

