# libtthimage, la doc

Les fichiers [HTML](html/) que vous avez ici sont des versions historiques,
qui seront peut-être mise à jour au prochain grand refactoring.
Leur contenu est donc à prendre avec des pincettes, ymmv.

Ceci dit, les principes généraux sont encore bons. Mais il y a quelques
points à préciser.

## Compilation

Certaines options de compilation sont dans le fichier de
[paramètres](../Paramakes.mk) pour les différents fichiers make.
Je vous conseille de vérifier leurs pertinences, et en particulier
les options `-p` et `-pg` que j'oublie parfois d'enlever avant
le _push_.

Le script [build.sh](../build.sh)  tente d'automatiser les
compilations de la bibilothèque, des tests et des outils
en ligne de commande. Il est encore loin d'être parfait.

## Installation

Le script [install.sh](../install.sh) va installer divers fichiers
aux bonnes places là où on lui dit avec la variable `DESTDIR`.
Attention, celle-ci doit être cohérente avec les valeurs déclarée
dans le fichier Paramakes.mk !

## Utilisation

??? *Insérez ici quelques exemples d'utilisation des tools.*
