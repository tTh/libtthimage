#!/bin/bash

set -e				# abort on error

cd Lib
echo "========== in $PWD"
make testtga
cd ..

cd Tools
echo "========== in $PWD"
make 
cd ..

cd Tests
echo "========== in $PWD"
make 
cd ..
